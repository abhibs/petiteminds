<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Google API Configuration
| -------------------------------------------------------------------
| 
| To get API details you have to create a Google Project
| at Google API Console (https://console.developers.google.com)
| 
|  client_id         string   Your Google API Client ID.
|  client_secret     string   Your Google API Client secret.
|  redirect_uri      string   URL to redirect back to after login.
|  application_name  string   Your Google application name.
|  api_key           string   Developer key.
|  scopes            string   Specify scopes
*/
$config['google']['client_id']        = '956027680086-k80guqg5q5m3nhka749pqhe0tstoilvl.apps.googleusercontent.com';
$config['google']['client_secret']    = 'TpeWq0QP2el5uSMcHiAkotal';
$config['google']['redirect_uri']     = 'https://www.aestheticbeauty.in/login';
$config['google']['application_name'] = 'Aesthetic Beauty';
$config['google']['api_key']          = '';
$config['google']['scopes'] = array('USERINFO_EMAIL','USERINFO_PROFILE');
?>