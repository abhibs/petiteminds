<?php
function meta_data($page)
{

    $current_url =  $this->uri->uri_string();
    $meta = '<meta charset="utf-8">';
    switch ($page) {
        case 'home':
            $meta .= '<meta name="title" content="Online SAP courses and Certification Exam available at SapPrep.com">';
            $meta .= '<meta name="description" content="SapPrep professionals offers various types of SAP Exams, courses & training which help you in Job Interviews. Also, we will improve your speed, skill and confidence. Register today!">';
            $meta .= '<meta name="keywords" content="SAP certification, SAP global certification, online SAP training and certification, SAP Sample questions, SAP Sample answer, SAP online exam, SAP online certification exam, practice for SAP exam, SAP preparation, preparation of SAP, SAP courses, SAP course details, list of SAP courses, SAP online course, learn SAP online, SAP practice exams">';
            break;

        case '120':
            $meta .= '<meta name="title" content="SAP S/4HANA Cloud implementation with Sap Activate Certification Exam - SapPrep.com">';
            $meta .= '<meta name="description" content="SapPrep provides 100% valid SAP S/4HANA Cloud Implementation with SAP Activate (C_TS4C_2018) exam questions and answer available at affordable price. We help you to pass your certification exam in First Attempt without any worries.">';
            $meta .= '<meta name="keywords" content="SAP S 4HANA cloud implementation, s 4HANA cloud implementation, SAP cloud implementation, cloud implementation with SAP activate, SAP S 4HANA cloud implementation certification, SAP S 4HANA cloud implementation course">';
            break;

        default:
            $meta .= '<meta name="description" content="default page description">';
            $meta .= '<meta name="keywords" content="default page keywords">';
            break;
    }
    return $meta;
}
