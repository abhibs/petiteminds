<?php
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Google
{

    /**
     * Googleplus constructor.
     */
    public function __construct()
    {

        require APPPATH."third_party/google-api-php/src/Google/autoload.php";
        $this->client = new Google_Client();
        $this->client->setApplicationName('Aesthetic Beauty');
        $this->ci =& get_instance();
        $this->ci->config->load('google');
        $this->client->setClientId('956027680086-k80guqg5q5m3nhka749pqhe0tstoilvl.apps.googleusercontent.com');
        $this->client->setClientSecret('TpeWq0QP2el5uSMcHiAkotal');
        $this->client->setRedirectUri('https://www.aestheticbeauty.in/login');
        $this->client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
        $this->client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
        //$this->client->addScope('profile');

    }

    public function loginUrl()
    {

        return $this->client->createAuthUrl();

    }

    public function getAuthenticate()
    {

        return $this->client->authenticate( $_GET['code'] );

    }

    public function getAccessToken()
    {

        return $this->client->getAccessToken();

    }

    public function setAccessToken()
    {

        return $this->client->setAccessToken();

    }

    public function revokeToken()
    {

        return $this->client->revokeToken();

    }

    public function client()
    {

        return $this->client;

    }

    public function getUser()
    {

        $google_ouath = new Google_Service_Oauth2($this->client);

        return (object)$google_ouath->userinfo->get();

    }

    public function isAccessTokenExpired()
    {

        return $this->client->isAccessTokenExpired();

    }

}