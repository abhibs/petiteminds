<?php
class Blog_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function getblogs() {

        $this->db->select('*');
        $this->db->from('blog');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function insertblog($data)
    {
        return $this->db->insert('blog', $data);
    }

    function editblogs($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where($condition);
        $this->db->limit(1);
        return $this->db->get()->result_array();

    }
	
    function updateblog($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('blog');
    }

      function deleteblog($id)
    {
        $this->db->delete('blog', array('id' => $id)); 
        $this->db->where('id', $id);

    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img_file');
        $this->db->from('blog');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img_file'];
    }

    function deleteimage($id)
    {
        $this->db->set('img_file', ''); 
        $this->db->where('id', $id);
         $this->db->update('blog');
    }


 

}

?>