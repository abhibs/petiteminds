<?php
date_default_timezone_set('Asia/Kolkata');
class GeneralModel extends CI_Model {

    var $loc_settings;

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		$this->loc_settings['usertable'] = "usermaster";		
		$this->loc_settings['userfield'] = "User_ID";		
		$this->loc_settings['passfield'] = "Password";	

		$this->load->database();		
	}
	
	function AddNewCouponRow($table, $data)
    {

        $this->db->insert($table, $data);
		//echo $this->db->last_query();exit;
		$last_id = $this->db->insert_id();
		 
		return $last_id;
	}
	
	    function GetInfoCouponRow($table, $key)
    {
		if( !empty( $key ) )
		$query = $this->db->where($key);
	
        $query = $this->db->get($table);

        return $query->result();
    }

	    function UpdateCouponRow($table, $data, $key)
    {
		$key2 ='';
		foreach($key as  $k => $v)
		{
			$key2 .= "$k = $v";
		}

        $this->db->update($table, $data, $key);
		

		return $this->db->affected_rows();
		
    }
    
    	function DeleteCouponRow($table, $key)
    {
		$key2 ='';
		foreach($key as  $k => $v)
		{
			$key2 .= "$k = $v";
		}

		$this->db->delete($table, $key); 

		if ($this->db->affected_rows() > 0)
		{
            return TRUE;
		}else
		{
			return FALSE;	
		}
    }


	function GetSelectedCustomRows($table, $limit, $start, $columns = '', $orderby ='', $key='',$search = '')
	{
		if(!empty($columns))
		{
			$this->db->select($columns,FALSE);		
		}
		/*
		var_dump( $search ); 
		var_dump( $key ); 
		var_dump( $orderby ); 
		var_dump( $columns ); 
		exit;*/
		
		if($search)
		{
		
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{	
			if( !empty($key) )
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}
		
		if( !empty( $limit ) )
		$this->db->limit($limit, $start);
	
		$query = $this->db->get($table);
		//print $this->db->last_query() . "<hr>\n";exit;
		return $query->result();			
	}
	
   function AddNewCustomRow($table, $data)
    {

        $this->db->insert($table, $data);
		//echo $this->db->last_query();exit;
		$last_id = $this->db->insert_id();
		 
		return $last_id;
	}
	
	
    function AddNewRow($table, $data)
    {
		#$datestring = "%Y-%m-%d %H:%i:%s";
		#echo mdate($datestring, time());
		
        $this->db->insert($table, $data);
		//echo $this->db->last_query();exit;
		$last_id = $this->db->insert_id();
		 
		//$this->insert_trails($this->session->userdata('logged_in'),"Added in $table");
		
		return $last_id;
	}
	
    function UpdateRow($table, $data, $key)
    {
		$key2 ='';
		foreach($key as  $k => $v)
		{
			$key2 .= "$k = $v";
		}
		#$value = array_shift( $key );
		
        $this->db->update($table, $data, $key);
		
		//echo $this->db->last_query();

		// $this->insert_trails($this->session->userdata('logged_in'),"Update $table for key $key2");
		
		return $this->db->affected_rows();
		
		/*
		if ($this->db->affected_rows() > 0)
            return TRUE;
        return FALSE;		
		*/
    }

	function DeleteRow($table, $key)
    {
		$key2 ='';
		foreach($key as  $k => $v)
		{
			$key2 .= "$k = $v";
		}

		$this->db->delete($table, $key); 
		//echo $this->db->last_query();
		
		if ($this->db->affected_rows() > 0)
		{
			//$this->insert_trails($this->session->userdata('logged_in'),"Delete $table for key $key2");
			
            return TRUE;
		}else
		{
			return FALSE;	
		}
    }

	
    function CountRecords($table, $key ='', $search = '')
    {
		if($search)
		{
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		$this->db->from($table);
		#$this->db->get();
		#echo $this->db->last_query();exit;
		//print $this->db->count_all_results() . "<hr>\n";exit;
		return $this->db->count_all_results();
    }
	
    function CountRecordsWithDate($table, $key ='', $search = '', $date_field,$from_date, $to_date)
    {
		$custom = '';
		foreach($key as $k => $v)
		{
			#print $k;
			if( $k != 'type' )
			$custom .= "OR $k like '%$v%' ";
		}
		$custom = trim(substr($custom,2));
		#print $custom;exit;
		#print_r($key);exit;
		if($search)
		{
			if(!empty($key))
			{
			
			if( $custom !== '' && $custom !== false )	
			$custom = " ($custom) ";
			
			
			//if( isset( $key['type'] ) )
			
			if( $custom !== '' && isset( $key['type'] ) )
			$custom .= " AND ";

			if( isset( $key['type'] ) )
			$custom .= " type = '". $key['type'] ."' ";
			
				$query = $this->db->where(" ($custom)",'',FALSE); 
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if($from_date!=""  && $to_date!="")
		{
			$this->db->where("Date($date_field) >= '$from_date'");
			$this->db->where("Date($date_field) <= '$to_date'");
			//$this->db->where('datec >=', $from_date);
			//$this->db->where('datec <=', $to_date);
		}
		else if($from_date!="" && $to_date=="")
		{
			$this->db->where("Date($date_field) = '$from_date'");
		}
		else if($from_date=="" && $to_date!="")
		{
			$this->db->where("Date($date_field) = '$to_date'");
		}
		
		$this->db->from($table);
		#$this->db->get();
		//echo $this->db->last_query();exit;
		return $this->db->count_all_results();
    }	
	
    function CountNotCentralRecords($table, $key ='', $search = '')
    {
		if($search)
		{
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		$query = $this->db->where("central != '1'"); 
		$this->db->from($table);
		#$this->db->get();
		#echo $this->db->last_query();
		return $this->db->count_all_results();
    }
	
	function CountRecordsJoins($table, $key = '', $search = '', $join_ar = '', $group_by = '')
    {
		if($search)
		{
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if( !empty($join_ar) )
		{
			foreach( $join_ar as $jval )
			{
				$this->db->join( $jval["table"], $jval["condition"], $jval["type"]);
			}			
		}
		
		if(!empty($group_by))
		{
			$this->db->group_by("$group_by");
		}
		
		$this->db->from($table);
		//$this->db->get();
		//echo "<hr>". $this->db->last_query();exit;
		return $this->db->count_all_results();
    }
	
	function CountRecordsJoins_2key( $table, $key='', $key1='', $join_ar = '', $group_by = '', $col_in = '', $key_in = '' )
	{
		if(!empty($columns))
		{
			$this->db->select($columns);
		}
	
		$cust_str = '';	

		if(!empty($key1))
		{
			$cust_str = "(";
			
			foreach( $key1 as $k => $v )
			{
				//$cust_str .= " $k = '$v' AND ";
				
				if( strpos($k, "!=") !== false )
				{
					$cust_str .= " $k '$v' AND ";
				}
				else
				{
					$cust_str .= " $k = '$v' AND ";
				}
				
			}
			
		$lpos = strrpos($cust_str, "' AND");
		$cust_str = trim( substr_replace($cust_str, "", $lpos + 1). " ) " );
		
		if( !empty( $key ) )
		{
			$cust_str .= "AND";
		}
			
		}
		
		$cust_str = str_replace("( ) AND", "", $cust_str);
		
		/*
		echo "<pre>";
		var_dump( $cust_str );
		var_dump( $lpos );
		exit;
		*/	
		
		if(!empty( $key ))
		{			
			$cust_str .= " ( ";
	
			foreach( $key as $k => $v )
			{
				$cust_str .= " $k LIKE '%$v%' OR ";
			}		
			
		$lpos = strrpos($cust_str, "%' OR");
		$cust_str = trim( substr_replace($cust_str, "", $lpos + 2). " ) " );
			
		}
			
		if( !empty( $key_in ) )
		{			
	
			$cust_str .= " AND ( $col_in IN ( ";
				
			foreach( $key_in as $k => $v )
			{
				$cust_str .= " $v ,";
			}		
			
			$cust_str = substr( $cust_str, 0, -1) . " ) ) ";
			
		}		
		
		if( $cust_str != "(  )" && $cust_str != '' )	
		$query = $this->db->where( $cust_str );
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}	
	
		/*
		echo "<pre>";
		var_dump( $join_ar );
		exit;
		*/
	
	
		if( !empty($join_ar) )
		{
			foreach( $join_ar as $jval )
			{
				$this->db->join( $jval["table"], $jval["condition"], $jval["type"]);
			}
		}
	
		if(!empty($group_by))
		{
			$this->db->group_by("$group_by");
		}
	
		$this->db->from($table);
		//$this->db->get();
		//echo "<hr>". $this->db->last_query();exit;
		return $this->db->count_all_results();
	}	
	
    function GetInfoRow($table, $key)
    {
		if( !empty( $key ) )
		$query = $this->db->where($key);
	
        $query = $this->db->get($table);
		#echo $this->db->last_query();exit;
		
		#$query = $this->db->get_where('news', array('news_id' => $id));
		#echo $query->num_rows();
		//echo $this->db->last_query(); exit;
        return $query->result();
    }
	
	function GetInfoRow_SelCol($table, $columns = '', $key)
    {
		if(!empty($columns))
		{
			$this->db->select($columns,FALSE);		
		}
		
		if( !empty( $key ) )
		$query = $this->db->where($key);
	
        $query = $this->db->get($table);
		#echo $this->db->last_query();exit;
		
		#$query = $this->db->get_where('news', array('news_id' => $id));
		#echo $query->num_rows();
		//echo $this->db->last_query(); exit;
        return $query->result();
    }
	
	function GetSelectedRows($table, $limit, $start, $columns = '', $orderby ='', $key='',$search = '')
	{
		if(!empty($columns))
		{
			$this->db->select($columns,FALSE);		
		}
		/*
		var_dump( $search ); 
		var_dump( $key ); 
		var_dump( $orderby ); 
		var_dump( $columns ); 
		exit;*/
		
		if($search)
		{
		
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{	
			if( !empty($key) )
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}
		
		if( !empty( $limit ) )
		$this->db->limit($limit, $start);
	
		$query = $this->db->get($table);
		//print $this->db->last_query() . "<hr>\n";exit;
		return $query->result();			
	}
	
	function GetSelectedRowsJoins( $table = '', $limit = '', $start = '', $columns = '', $orderby ='', $key='', $search = '', $join_ar = '', $group_by = '' )
	{ 	
		if(!empty($columns))
		{
			$this->db->select($columns); 
			
		}
		
		if($search)
		{
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}		
		
		
		if(!empty($group_by))
		{
			$this->db->group_by("$group_by");
		}
		
		
		
		/*
		echo "<pre>";
		var_dump( $join_ar );
		exit;
		*/
		
		
		if( !empty($join_ar) )
		{
			foreach( $join_ar as $jval )
			{
				$this->db->join( $jval["table"], $jval["condition"], $jval["type"]);
			}			
		}		
		
		if( $limit !== "" )
		$this->db->limit($limit, $start);
		
		$query = $this->db->get($table);
		//var_dump($query); exit;
		// print "<hr>".$this->db->last_query();//exit;
		return $query->result();		
		//return "executed";
	}
	
	function GetSelectedRowsJoins_Col_In( $table = '', $limit = '', $start = '', $columns = '', $orderby ='', $key='', $search = '', $join_ar = '', $group_by = '', $col_in = '', $in_oprtr = '', $key_in = '' )
	{
		if(!empty($columns))
		{
			$this->db->select($columns);		
		}
		
		if($search)
		{
			if(!empty($key))
			{
				$query = $this->db->or_like($key); 
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}		
		
		
		if(!empty($group_by))
		{
			$this->db->group_by("$group_by");
		}
		
		
		
		/*
		echo "<pre>";
		var_dump( $join_ar );
		exit;
		*/
		$cust_str = '';
		
		if( !empty( $key_in ) )
		{			
	
			$cust_str .= " ( $col_in $in_oprtr ";
			
			if( is_array( $key_in ) )
			{
			
				$cust_str .= " ( ";			
					
				foreach( $key_in as $k => $v )
				{
					$cust_str .= " '$v' ,";
				}	
			
				$cust_str = substr( $cust_str, 0, -1) . " ) ";			
				
			}
			else
			{
				$cust_str .= " ( $key_in ) ";
			}
			
			$cust_str .= " ) ";
			
		}
		
		if( ! empty( $cust_str ) )
		$query = $this->db->where($cust_str, null, FALSE); 
		
		if( !empty($join_ar) )
		{
			foreach( $join_ar as $jval )
			{
				$this->db->join( $jval["table"], $jval["condition"], $jval["type"]);
			}			
		}		
		
		if( $limit !== "" && $start !== "" )
		$this->db->limit($limit, $start);
		
		$query = $this->db->get($table);
		
		//print "<hr>".$this->db->last_query();//exit;
		return $query->result();			
	}
	
	function GetSelectedRowsWithDate($table, $limit, $start, $columns = '', $orderby ='', $key='',$search = '', $date_field, $from_date, $to_date)
	{
		if(!empty($columns))
		{
			$this->db->select($columns,FALSE);		
		}
		$custom = '';
		foreach($key as $k => $v)
		{
			#print $k;
			if( $k != 'type' )
			$custom .= "OR $k like '%$v%' ";
		}
		$custom = trim(substr($custom,2));
		#print $custom;exit;
		#print_r($key);exit;
		if($search)
		{
			if(!empty($key))
			{
			
			if( $custom !== '' && $custom !== false )
			$custom = " ($custom)";
			
			if( $custom !== '' && isset( $key['type'] ) )
			$custom .= " AND ";

			if( isset( $key['type'] ) )
			$custom .= " type = '". $key['type'] ."' ";
			
				$query = $this->db->where(" ($custom)",'',FALSE); 
				
				
				
				
			}
		}
		else
		{
			if(!empty($key))
			{
				$query = $this->db->where($key); 
			}		
		}
		
		if($from_date!=""  && $to_date!="")
		{
			$this->db->where("Date($date_field) >= '$from_date'");
			$this->db->where("Date($date_field) <= '$to_date'");
			//$this->db->where('datec >=', $from_date);
			//$this->db->where('datec <=', $to_date);
		}
		else if($from_date!="" && $to_date=="")
		{
			$this->db->where("Date($date_field) = '$from_date'");
		}
		else if($from_date=="" && $to_date!="")
		{
			$this->db->where("Date($date_field) = '$to_date'");
		}
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}
		
		$this->db->limit($limit, $start);
		$query = $this->db->get($table);
		
		#print_r($this->db->ar_like);exit;
		//print $this->db->last_query() . "<hr>\n";exit;
		return $query->result();			
	}
	
	function GetSelectedRowsJoins_2key( $table, $limit, $start, $columns = '', $orderby ='', $key='', $key1='', $join_ar = '', $group_by = '', $col_in = '', $key_in = '' )
	{
		if(!empty($columns))
		{
			$this->db->select($columns);
		}
	
		$cust_str = '';	

		if(!empty($key1))
		{
			$cust_str = "(";
			
			foreach( $key1 as $k => $v )
			{
				//$cust_str .= " $k = '$v' AND ";
				
				if( strpos($k, "!=") !== false )
				{
					$cust_str .= " $k '$v' AND ";
				}
				else
				{
					$cust_str .= " $k = '$v' AND ";
				}
				
			}
			
		$lpos = strrpos($cust_str, "' AND");
		$cust_str = trim( substr_replace($cust_str, "", $lpos + 1). " ) " );
		
		if( !empty( $key ) )
		{
			$cust_str .= "AND";
		}
			
		}
		
		$cust_str = str_replace("( ) AND", "", $cust_str);
		
		/*
		echo "<pre>";
		var_dump( $cust_str );
		var_dump( $lpos );
		exit;
		*/	
		
		if(!empty( $key ))
		{			
			$cust_str .= " ( ";
	
			foreach( $key as $k => $v )
			{
				$cust_str .= " $k LIKE '%$v%' OR ";
			}		
			
		$lpos = strrpos($cust_str, "%' OR");
		$cust_str = trim( substr_replace($cust_str, "", $lpos + 2). " ) " );
			
		}
			
		
		if( !empty( $key_in ) )
		{			
	
			$cust_str .= " AND ( $col_in IN ( ";
				
			foreach( $key_in as $k => $v )
			{
				$cust_str .= " $v ,";
			}		
			
			$cust_str = substr( $cust_str, 0, -1) . " ) ) ";
			
		}
		
		
		
		if( $cust_str != "(  )" && $cust_str != '' )	
		$query = $this->db->where( $cust_str );
		
		if(!empty($orderby))
		{
			$this->db->order_by("$orderby");
		}	
	
		/*
			echo "<pre>";
		var_dump( $join_ar );
		exit;
		*/
	
	
		if( !empty($join_ar) )
		{
			foreach( $join_ar as $jval )
			{
				$this->db->join( $jval["table"], $jval["condition"], $jval["type"]);
			}
		}
	
		if(!empty($group_by))
		{
			$this->db->group_by("$group_by");
		}
	
		if( $limit !== "" && $start !== "" )
			$this->db->limit($limit, $start);
	
		$query = $this->db->get($table);
	
		//print $this->db->last_query();exit;
		return $query->result();
	}
	
    function GetSearch($table, $cond = '')
	{
		/*
		if(isset($s_date) && $s_date!='')
		{
            if(isset($f_date) && $f_date!='')
			{
				$this->db->where('datec >=', $s_date); 
			}
			else
			{
				$this->db->where('datec =', $s_date); 
			}
		}	
		if(isset($f_date) && $f_date!='')
		{
             $this->db->where('datec <=', $f_date); 
		}	
		*/
		
		if(!empty($cond))
		{
			$this->db->where($cond);
		}
		
        $query = $this->db->get($table);
		//echo $this->db->last_query();
		return $query->result();
	}
	
	function UserLogin($username, $password, $cond = '')
	{		
		$this->db->from($this->loc_settings['usertable']);
		$this->db->where($this->loc_settings['userfield'], $username);
		$this->db->where($this->loc_settings['passfield'], $password);

		if(!empty($cond))
		{
			$this->db->where($cond);
		}

		#$this->db->where('is_active', 'Y');
	
		$query = $this->db->get();
		#return $this->db->count_all_results();	
		//echo $this->db->last_query();exit;
		return $query->result();
	}
	
	function V_UserLogin($username, $password, $cond = '')
	{		
		$this->db->from('vendor');
		$this->db->where('Encrept_PrimaryEmailID', $username);
		$this->db->where('Password', $password);

		if(!empty($cond))
		{
			$this->db->where($cond);
		}

		#$this->db->where('is_active', 'Y');
	
		$query = $this->db->get();
		#return $this->db->count_all_results();	
		//echo $this->db->last_query();exit;
		return $query->result();
	}	
	
	function V_UserChangePass($username, $password, $cond = '')
	{		

		$query = $this->db->from('passwordhistory'); 
		//$query = $this->db->where('Password', $password); 
		$query = $this->db->where('PrimaryEmailID', $username); 
		//$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
		$query = $this->db->limit(5, 0);
		$query = $this->db->order_by('PassID','Desc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		#echo $query->num_rows();
		#return $query->result();
		//$count = $query->num_rows();
		#print $count;exit;
		//if($count >0) // password used recently
		
		$passwordmatched = 0;
		$result = $query->result();
		foreach($result as $row)
		{
			$oldpass = $row->Password;
			
			if($oldpass == $password)
			{
				$passwordmatched = 1;
			}
		}
		$passwordmatched = 0;
		
		if($passwordmatched == 1)
		{
			print "Password can not be from last 5 used";exit;
		}else
		{
			$data2 = array(
			   'PrimaryEmailID' => $username ,
			   'Password' => $password ,
			   'ipaddress' => $this->input->ip_address() ,
			   'ChangeDate' => mdate("%Y-%m-%d %H:%i:%s", time())
			);
			$this->db->insert('passwordhistory', $data2); 

			$data = array();
			$data[$this->loc_settings['passfield']] = $password;
			
			if(!empty($cond))
			{
				$this->db->where($cond);
			}
			$this->db->where('PrimaryEmailID', $username);
			$this->db->update('vendor', $data);
			#echo $this->db->last_query();//exit;
		}
		//exit;
		
		if ($this->db->affected_rows() > 0)
            return TRUE;
        return FALSE;		
	}	
	
	function insert_trails($user_id,$code)
	{
        #$this->user_id   = $user_id; 
		// please read the below note
        #$this->trails   = $code; 
		// please read the below note
        #$this->ipaddress   = $this->input->ip_address(); 
		// please read the below note

		$datestring = "%Y-%m-%d %H:%i:%s";
		$time = time();
		#print $time . "<hr>";
		#echo mdate($datestring, $time);
		
		#$this->datec = mdate($datestring, $time);
        #$this->db->insert('audit_trails', $this);	
		$data = array(
		   'user_id' => $user_id ,
		   'trails' => $code ,
		   'ipaddress' => $this->input->ip_address() ,
		   'datec' => mdate($datestring, $time)
		);
		$this->db->insert('audit_trails', $data); 		
	}
	
	function login_trails($user_id,$code)
	{	
		if($code == 'F')
		{
			$data = array(
			   'user_id' => $user_id ,
			   'status' => $code ,
			   'flag' => 0 ,
			   'ipaddress' => $this->input->ip_address() ,
			   'datec' => mdate("%Y-%m-%d %H:%i:%s", time())
			);
			$this->db->insert('loginrecords', $data); 	

			$query = $this->db->from('loginrecords'); 
			$query = $this->db->where('flag', 0); 
			$query = $this->db->where('user_id', $user_id); 
			$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
			$query = $this->db->get();
			#echo $this->db->last_query();exit;
			#echo $query->num_rows();
			#return $query->result();
			$count = $query->num_rows();
			if($count >=5 && $count <=10)
			{
				//print "5 attempt for login has done, now no login with current IP address";exit;
			}
			
			$query = $this->db->from('loginrecords'); 
			$query = $this->db->where('flag', 0); 
			$query = $this->db->where('user_id', $user_id); 
			//$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
			$query = $this->db->get();
			#echo $this->db->last_query();exit;
			#echo $query->num_rows();
			#return $query->result();
			$count = $query->num_rows();
			if($count >=10)
			{
				//print "10 attempt for login failure, now User ID is locked";exit;
			}
			
		}
		if($code == 'S')
		{
			$data = array(
			   'user_id' => $user_id ,
			   'status' => $code ,
			   'flag' => 0 ,
			   'ipaddress' => $this->input->ip_address() ,
			   'datec' => mdate("%Y-%m-%d %H:%i:%s", time())
			);
			$this->db->insert('loginrecords', $data); 

			$query = $this->db->from('loginrecords'); 
			$query = $this->db->where('flag', 0); 
			$query = $this->db->where('user_id', $user_id); 
			$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
			$query = $this->db->get();
			#echo $this->db->last_query();exit;
			#echo $query->num_rows();
			#return $query->result();
			$count = $query->num_rows();
			if($count >=5 && $count <=10)
			{
				$data = array(
				   'user_id' => $user_id ,
				   'status' => $code ,
				   'flag' => 0 ,
				   'ipaddress' => $this->input->ip_address() ,
				   'datec' => mdate("%Y-%m-%d %H:%i:%s", time())
				);
				#$this->db->insert('loginrecords', $data); 	
				//print "5 attempt for login has done, now no login with current IP address";exit;
			}

			$query = $this->db->from('loginrecords'); 
			$query = $this->db->where('flag', 0); 
			$query = $this->db->where('user_id', $user_id); 
			//$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
			$query = $this->db->get();
			#echo $this->db->last_query();exit;
			#echo $query->num_rows();
			#return $query->result();
			$count = $query->num_rows();
			if($count >=10)
			{
				$data = array(
				   'user_id' => $user_id ,
				   'status' => $code ,
				   'flag' => 0 ,
				   'ipaddress' => $this->input->ip_address() ,
				   'datec' => mdate("%Y-%m-%d %H:%i:%s", time())
				);
				#$this->db->insert('loginrecords', $data); 	

				//print "10 attempt for login failure, now User ID is locked";exit;
			}		
		
		
			$key['user_id'] = $user_id;
			$key['ipaddress'] = $this->input->ip_address();
			$data2['flag'] = 1; 
			$this->db->update('loginrecords', $data2, $key);
			
			$data3 = array();
			$key3['User_ID'] = $user_id;
			$data3['Last_Login'] = mdate("%Y-%m-%d %H:%i:%s", time());
			
			$this->db->update('usermaster', $data3, $key3);			
			#echo $this->db->last_query();exit;
		}		
	
	}
	
	function LockedAc()
	{
		$query = $this->db->from('loginrecords'); 
		$query = $this->db->where('flag', 0); 
		//$query = $this->db->where('user_id', $user_id); 
		$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
		$query = $this->db->get();
		#echo $this->db->last_query();exit;
		#echo $query->num_rows();
		if($query->num_rows() >= 5)
		{
			//print "Login denied";exit;
		}
		#return $query->result();	
	}
	
	
	function UserChangePass($username, $password, $cond = '')
	{		

		$query = $this->db->from('passwordhistory'); 
		//$query = $this->db->where('Password', $password); 
		$query = $this->db->where('UserID', $username); 
		//$query = $this->db->where(array('ipaddress' => $this->input->ip_address())); 
		$query = $this->db->limit(5, 0);
		$query = $this->db->order_by('PassID','Desc');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		#echo $query->num_rows();
		#return $query->result();
		//$count = $query->num_rows();
		#print $count;exit;
		//if($count >0) // password used recently
		
		$passwordmatched = 0;
		$result = $query->result();
		foreach($result as $row)
		{
			$oldpass = $row->Password;
			
			if($oldpass == $password)
			{
				$passwordmatched = 1;
			}
		}
		$passwordmatched = 0;
		
		if($passwordmatched == 1)
		{
			print "Password can not be from last 5 used";exit;
		}else
		{
			$data2 = array(
			   'UserID' => $username ,
			   'Password' => $password ,
			   'ipaddress' => $this->input->ip_address() ,
			   'ChangeDate' => mdate("%Y-%m-%d %H:%i:%s", time())
			);
			$this->db->insert('passwordhistory', $data2); 

			$data = array();
			$data[$this->loc_settings['passfield']] = $password;
			
			if(!empty($cond))
			{
				$this->db->where($cond);
			}
			$this->db->where('UserID', $username);
			$this->db->update($this->loc_settings['usertable'], $data);
			#echo $this->db->last_query();//exit;
		}
		//exit;
		
		if ($this->db->affected_rows() > 0)
            return TRUE;
        return FALSE;		
	}
	
	function CustomQuery( $qry = '', $ret_res = true )
	{
		if( empty( $qry ) )
			return false;
		else
		{
			$res = $this->db->query( $qry );
			//echo "<hr>".$this->db->last_query();exit;
			if( $ret_res )
				return $res->result();
		}		
		
	}
	function currency_update($price){


$this->db->set('price_value', $price, FALSE);
$this->db->set('time', time(), FALSE);
$this->db->where('id', 1);
$this->db->update('currency');

return true;
 
	}

    function GetCurrencyData()
    {
	$query = $this->db->select('price_value')
                ->where('id', 1)
                ->get('currency');
        return $query->result_array()[0]['price_value'];
    }	
	
}	
?>