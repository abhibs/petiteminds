<?php
class sell_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertsell($data)
    {
        $this->db->insert('sell', $data);
        return $this->db->insert_id();
    }

    public function getsells() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop_email", $this->session->userdata('email'));
        }

        $this->db->from('sell');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getreturnsells() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop_email", $this->session->userdata('email'));
        }

        $this->db->from('sell_return');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getselldetails($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('sell');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getselllist($order_id) {
        $this->db->select('*');
        $this->db->where("order_id", $order_id);
        $this->db->from('sell');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getselltotal($order_id) {

        $this->db->select('sum(rate*quantity) as total');
        $this->db->where("order_id", $order_id);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }

    function editsells($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('sell');
        $this->db->where($condition);
        $this->db->limit(1);
        return $this->db->get()->result_array();
    }

    function getstocksells($shop,$design_no)
    {
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where("shop",$shop);
        $this->db->where("design_no",$design_no);
        $this->db->where("flag",1);
        return $this->db->get()->result_array();
    }

    function updatesell($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('sell');
    }

    function updatesellstock($data1, $shop_email, $design_no)
    { 
        $this->db->set($data1);
        $this->db->where('shop', $shop_email);
        $this->db->where('design_no', $design_no);
        $this->db->where('flag', 1);
        return $this->db->update('stock');
    }

    function deletesell($id)
    {
        $this->db->delete('sell', array('id' => $id)); 
    }



    public function getcustomer() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop_email", $this->session->userdata('email'));
        }

        $this->db->from('customer');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getdesign_no() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop", $this->session->userdata('email'));
        }

        $this->db->group_by("design_no");
        $this->db->where("confirm", "1");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    function check_stock_quantity($design_no,$shop)
    {
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $shop);
        $this->db->where("flag", 1);
        $query = $this->db->get();
        return $query->result();
    }

    /*function get_sell_quantity($design_no,$shop)
    {
        $this->db->select('sum(quantity) as total');
        $this->db->from('sell');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop_email", $shop);
        $query = $this->db->get();
        return $query->result();
    }*/

    function get_selling_price($design_no,$customer)
    {
        $this->db->select('*');
        $this->db->from('sell');
        $this->db->where("design_no",$design_no);
        $this->db->where("customer",$customer);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getproductqty($design_no,$shop) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $shop);
        $this->db->where("flag", 1);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getcustomerdetails($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('customer');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getdesigndetails($id) {
        $this->db->select('*');
        $this->db->where("design_no", $id);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getshopdetails($email) {
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

}
?>