<?php
class good_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertgood($data)
    {
        $this->db->insert('good', $data);
        return $this->db->insert_id();
    }

    public function getgoodsout() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->where("sent", "1");
        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop_email", $this->session->userdata('email'));
        }

        $this->db->from('good');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getgoodsmanage() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "1");
        $this->db->from('good');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getgoodsmanagenew() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("notify", "1");
        $this->db->from('good');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function updategoodsmanagenew()
    { 
        $this->db->set('notify',0);
        $this->db->where('notify', 1);
        return $this->db->update('good');
    }

    function editgoodsout($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('good');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function updategood($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('good');
    }

    function deletegood($id)
    {
        $this->db->delete('good', array('id' => $id)); 
    }

    public function getdesign_no() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop", $this->session->userdata('email'));
        }

        $this->db->where("confirm", "1");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getdesigndetails($design) {
        $this->db->select('*');
        $this->db->where("design_no", $design);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getshopdetails($email) {
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getgoodsdetails($email,$design_no) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->where("design_no", $design_no);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    function updategoodconfirm($id)
    { 
        $this->db->set('confirm','1');
        $this->db->where('id', $id);
        return $this->db->update('good');
    }

    function updategoodpending($id)
    { 
        $this->db->set('confirm','0');
        $this->db->where('id', $id);
        return $this->db->update('good');
    }


    public function getgoodindetails($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('good');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getdesignno($id) {
        $this->db->select('*');
        $this->db->where('design_no', $id);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstockdesignno($email,$design_no) {
        $this->db->select('*');
        $this->db->where('design_no', $design_no);
        $this->db->where('shop', $email);
        $this->db->where('flag', 1);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    function check_stock_amount($design_no)
    {
        $condition = "design_no =" . "'" . $design_no . "'";
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }


    function updateproductqty($data1, $design_no)
    { 
        $this->db->set($data1);
        $this->db->where('design_no', $design_no);
        return $this->db->update('product');
    }

    function updatestockqty($data2,$email,$design_no)
    { 
        $this->db->set($data2);
        $this->db->where('design_no', $design_no);
        $this->db->where('shop', $email);
        $this->db->where('flag', 1);
        return $this->db->update('stock');
    }


    function check_stock_quantity($design_no,$shop)
    {
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $shop);
        $this->db->where("flag", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_good_selling_price($design_no)
    {
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where("design_no",$design_no);
        $this->db->group_by("rate");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }




}
?>