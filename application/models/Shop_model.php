<?php
class shop_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertshop($data)
    {
        $this->db->insert('shop', $data);
        return $this->db->insert_id();
    }

    function insertadminshop($shopdata)
    {
        $this->db->insert('admin_login', $shopdata);
        return $this->db->insert_id();
    }

    public function getshops() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");

       /* if($this->session->userdata('role')=='factory') {
        $this->db->where("factory_email", $this->session->userdata('email'));
        }*/

        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getshopdetails($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editshops($id)
    {//and factory_email =" . "'" . $this->session->userdata('email') . "'
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('shop');
        $this->db->where($condition);
        $this->db->limit(1);
        return $this->db->get()->result_array();
    }

    function updateshop($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('shop');
    }

    function updateadminshop($shopdata, $email)
    { 
        $this->db->set($shopdata);
        $this->db->where('email', $email);
        return $this->db->update('admin_login');
    }

    function deleteshop($id)
    {
        $this->db->delete('shop', array('id' => $id)); 
    }

    function deleteadminshop($email)
    {
        $this->db->delete('admin_login', array('email' => $email)); 
    }



    public function getshopstockdetails($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->order_by("created_at", 'desc');
        $this->db->group_by("order_id");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getshopstockdetailslist($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->where("confirm", "1");
        $this->db->where("flag", "1");
        $this->db->order_by("created_at", 'desc');
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getimagedetails($design_no) {
        $this->db->select('img');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function gettotalqty($email,$design_no) {
        $this->db->select('sum(quantity) as total');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $email);
        $this->db->where("confirm", 1);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    /*public function getshopfabricdetails($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->order_by("created_at", 'desc');
        $this->db->from('fabric');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }*/

    public function getgoodssentlist($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->where("confirm", "1");
        $this->db->order_by("created_at", 'asc');
        $this->db->group_by("order_id");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getgoodsreturnlist($email) {
        $this->db->select('*');
        $this->db->where("shop_email", $email);
        $this->db->where("confirm", "1");
        $this->db->order_by("created_at", 'asc');
        $this->db->from('good');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getpayrecievedlist($email) {
        $this->db->select('*');
        $this->db->where("shop_email", $email);
        $this->db->where("confirm", "1");
        $this->db->order_by("created_at", 'asc');
        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getgoodssentamount($order_id) {
        $this->db->select('sum(total) as total');
        $this->db->where("order_id", $order_id);
        $this->db->where("confirm", "1");
        $this->db->from('stock');
        $query = $this->db->get();
        return $query->result();
   }

    public function getcustomerstockamount($email) {

        $this->db->select('sum(total) as total');
        $this->db->where("shop", $email);
        $this->db->where("confirm", '1');
        $this->db->from('stock');
        $query = $this->db->get();
        return $query->result();
   }


    public function getcustomersaleamount($email) {

        $this->db->select('sum(amount) as total');
        $this->db->where("shop_email", $email);
        $this->db->where("confirm", '1');
        $this->db->from('good');
        $query = $this->db->get();
        return $query->result();
   }

    public function getcustomerrecievedamount($email) {

        $this->db->select('sum(amount) as total');
        $this->db->where("shop_email", $email);
        $this->db->where("confirm", '1');
        $this->db->from('payment');
        $query = $this->db->get();
        return $query->result();
   }

    /*public function getfabricrecievedlist($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->order_by("created_at", 'asc');
        $this->db->from('fabric');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }*/

    /*public function getfabricpayrecievedlist($email) {
        $this->db->select('*');
        $this->db->where("shop_email", $email);
        $this->db->order_by("created_at", 'asc');
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }*/

    public function gettotalitem($email) {

        $this->db->select('sum(quantity) as total');
        $this->db->where("shop", $email);
        $this->db->where("confirm", '1');
        $this->db->from('stock');
        $query = $this->db->get();
        return $query->result();
   }

    public function getitemsold($email) {

        $this->db->select('sum(quantity) as total');
        $this->db->where("shop_email", $email);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }

    public function getitemreturn($email) {

        $this->db->select('sum(quantity) as total');
        $this->db->where("shop_email", $email);
        $this->db->from('good');
        $query = $this->db->get();
        return $query->result();
   }


    public function get_daily_sales_amount($email) {

$now = new DateTime();
$now->setTimezone(new DateTimezone('Asia/Kolkata'));
$temp = $now->format('Y-m-d');

        $this->db->select('sum(rate*quantity) as total');
        $where = "created_at LIKE '%".$temp."%' ";
        $this->db->where($where);
        $this->db->where("shop_email", $email);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }

    public function get_daily_sales_list($email) {

$now = new DateTime();
$now->setTimezone(new DateTimezone('Asia/Kolkata'));
$temp = $now->format('Y-m-d');

        $this->db->select('*');
        $where = "created_at LIKE '%".$temp."%' ";
        $this->db->where($where);
        $this->db->where("shop_email", $email);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }

    public function getcustomerdetails($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('customer');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getdesign_rate($design_no,$email) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $email);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function get_alltime_sales_amount($email) {

        $this->db->select('sum(rate*quantity) as total');
        $this->db->where("shop_email", $email);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }

    public function get_alltime_sales_list($email) {

        $this->db->select('*');
        $this->db->order_by('created_at','desc');
        $this->db->where("shop_email", $email);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }


    public function get_custom_sales_amount($email,$fromdate,$todate) {

        $this->db->select('sum(rate*quantity) as total');
        $this->db->where("shop_email", $email);
        $where = "created_at BETWEEN '".$fromdate."' AND '".$todate."' ";
        $this->db->where($where);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }

    public function get_custom_sales_list($email,$fromdate,$todate) {

        $this->db->select('*');
        $where = "created_at BETWEEN '".$fromdate."' AND '".$todate."' ";
        $this->db->where($where);
        $this->db->where("shop_email", $email);
        $this->db->from('sell');
        $query = $this->db->get();
        return $query->result();
   }



}
?>