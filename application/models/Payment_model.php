<?php
class payment_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertpayment($data)
    {
        $this->db->insert('payment', $data);
        return $this->db->insert_id();
    }

    public function getpaymentsout() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->where("sent", "1");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop_email", $this->session->userdata('email'));
        }

        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getpaymentreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "0");

        if($this->session->userdata('role')=='shop') {
        $this->db->where("shop_email", $this->session->userdata('email'));
        }

        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getpaymentsmanage() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "1");
        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getpaymentsmanagenew() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("notify", "1");
        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function updatepaymentsmanagenew()
    { 
        $this->db->set('notify',0);
        $this->db->where('notify', 1);
        return $this->db->update('payment');
    }

    function editpaymentsout($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('payment');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('payment');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('payment');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updatepayment($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('payment');
    }

    function deletepayment($id)
    {
        $this->db->delete('payment', array('id' => $id)); 
    }


    public function getshopdetails($email) {
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }





    function updatepaymentconfirm($id)
    { 
        $this->db->set('confirm','1');
        $this->db->where('id', $id);
        return $this->db->update('payment');
    }

    function updatepaymentpending($id)
    { 
        $this->db->set('confirm','0');
        $this->db->where('id', $id);
        return $this->db->update('payment');
    }

    public function getpaymentindetails($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function updatemanagepayment($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('payment');
    }


    public function getpaymentfreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "0");
        $this->db->from('payment');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }



}
?>