<?php
class Dashboard_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function total_employee() {
        $this->db->select('*');
        $this->db->from('employee');
        $query = $this->db->get();
        return $query->num_rows();
   }       public function total_product() {        $this->db->select('*');        $this->db->from('product');        $query = $this->db->get();        return $query->num_rows();   }
    public function total_form( $fk_empid ) {        $this->db->select('*');        $this->db->from('visits');		        $this->db->where('fk_empid', $fk_empid);        $query = $this->db->get();        return $query->num_rows();   }
}
?>