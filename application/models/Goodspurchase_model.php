<?php
class goodspurchase_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertgoodspurchase($data)
    {
        $this->db->insert('goodspurchase', $data);
        return $this->db->insert_id();
    }

    public function getgoodspurchase() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->from('goodspurchase');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function get_design_no() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->from('goodsfirst');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editgoodspurchase($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('goodspurchase');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('goodspurchase');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('goodspurchase');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updategoodspurchase($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('goodspurchase');
    }

    function deletegoodspurchase($id)
    {
        $this->db->delete('goodspurchase', array('id' => $id)); 
    }



    public function getcheckproduct($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getlastproduct($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getupdatefirstproduct($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("flag", 1);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function gettotalqty($design_no) {
        $this->db->select('sum(quantity) as total');
        $this->db->where("design_no", $design_no);
        $this->db->from('goodspurchase');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }



    function get_design_no_name($design_no)
    {
        $this->db->select('*');
        $this->db->from('goodsfirst');
        $this->db->where("design_no",$design_no);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }





}
?>