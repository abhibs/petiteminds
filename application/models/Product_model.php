<?php
class product_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertproduct($data)
    {
        $this->db->insert('product', $data);
        return $this->db->insert_id();
    }

    public function getproduct() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->where("flag","1");
        $this->db->from('product');
        $query = $this->db->get(); //echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getgood() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $condition = "name IS NOT NULL";
        $this->db->where($condition);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editproduct($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('product');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('product');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updateproduct($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('product');
    }

    function deleteproduct($id)
    {
        $this->db->delete('product', array('id' => $id)); 
    }



    public function getcheckproduct($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getlastproduct($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getupdatefirstproduct($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("flag", 1);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function gettotalqty($design_no) {
        $this->db->select('sum(quantity) as total');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getimg($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->from('goodsfirst');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }







}
?>