<?php
class fabricin_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function getfabricin() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabricinreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        //$this->db->group_by("design_no");
        $this->db->from('fabric_return');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabricinsell() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        //$this->db->group_by("design_no");
        $this->db->from('fabric_sell');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabricinsellreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        //$this->db->group_by("design_no");
        $this->db->from('fabric_sellreturn');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editfabricin($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('fabric_buy');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('fabric_buy');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('fabric_buy');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updatefabricin($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('fabric_buy');
    }

    function deletefabricin($id)
    {
        $this->db->delete('fabric_buy', array('id' => $id)); 
    }





    public function getsupplier() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabric() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getsupplierdetails($email) {

        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }





    public function getfabricqty($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("flag", 1);
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function gettotalitem() {

        $this->db->select('sum(quantity) as total');
        $this->db->from('fabric_buy');
        $query = $this->db->get();
        return $query->result();
   }

    public function getitemsold() {

        $this->db->select('sum(quantity) as total');
        $this->db->from('fabric_sell');
        $query = $this->db->get();
        return $query->result();
   }


    public function getstockamount() {

        $this->db->select('sum(total) as total');
        $this->db->from('fabric_buy');
        $query = $this->db->get();
        return $query->result();
   }


    public function getsaleamount() {

        $this->db->select('sum(total) as total');
        $this->db->from('fabric_sell');
        $query = $this->db->get();
        return $query->result();
   }

    public function getrecievedamount() {

        $this->db->select('sum(amount) as total');
        $this->db->where("sent", '1');
        $this->db->from('fabricpay');
        $query = $this->db->get();
        return $query->result();
   }



    public function getcheckfabric($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getlastfabric($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getupdatefirstfabric($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("flag", 1);
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function gettotalqty($design_no) {
        $this->db->select('sum(quantity) as total');
        $this->db->where("design_no", $design_no);
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    function get_selling_price($design_no,$shop)
    {
        $this->db->select('*');
        $this->db->from('fabric_buy');
        $this->db->where("design_no",$design_no);
        $this->db->where("shop",$shop);
        $this->db->group_by("rate");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function get_selling_return_price($design_no,$shop)
    {
        $this->db->select('*');
        $this->db->from('fabric_sell');
        $this->db->where("design_no",$design_no);
        $this->db->where("shop",$shop);
        $this->db->group_by("rate");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }










}
?>