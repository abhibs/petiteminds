<?php
class stockout_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertstock($data)
    {
        $this->db->insert('stock', $data);
        return $this->db->insert_id();
    }

    public function getstocksout() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->where("sent","1");
        $this->db->group_by("order_id");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstocksoutdetail($order_id) {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent","1");
        $this->db->where("order_id",$order_id);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstocksreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "0");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstocksreturnnew() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("notify", "1");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function updatestocksreturnnew()
    { 
        $this->db->set('notify',0);
        $this->db->where('notify', 1);
        return $this->db->update('stock');
    }

    function editstocksout($id)
    {
        $condition = "order_id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function editstocksreturnout($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('stock');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('stock');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updatestock($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('stock');
    }

    function deletestockout($order_id)
    {
        $this->db->delete('stock', array('order_id' => $order_id)); 
    }

    public function getshop() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getproductqty($design_no) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getcheckstock($design_no,$shop) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $shop);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getupdatefirststock($design_no,$shop) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $shop);
        $this->db->where("flag", 1);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getorderidstock($order_id,$design_no,$shop) {
        $this->db->select('*');
        $this->db->where("design_no", $design_no);
        $this->db->where("shop", $shop);
        $this->db->where("order_id", $order_id);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstock($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstocklist($order_id) {
        $this->db->select('*');
        $this->db->where("order_id", $order_id);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getstocktotal($order_id) {

        $this->db->select('sum(total) as total');
        $this->db->where("order_id", $order_id);
        $this->db->from('stock');
        $query = $this->db->get();
        return $query->result();
   }

    public function getshopdetails($email) {
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getimagedetails($design_no) {
        $this->db->select('img');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getproduct() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    function check_stock_quantity($design_no)
    {
        $condition = "design_no =" . "'" . $design_no . "'";
        $condition .= "and flag =" . "'1'";
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where($condition);
        $this->db->limit(1);
        return $this->db->get()->result_array();
    }


    public function getstockstatus($order_id) {
        $this->db->select('*');
        $this->db->where("order_id", $order_id);
        $this->db->where("confirm", '0');
        $this->db->from('stock');
        return $this->db->get()->result_array();  
   }



    function deletestockreturnout($id)
    {
        $this->db->delete('stock', array('id' => $id)); 
    }


    function updatestockreturn($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('stock');
    }

    function updateproductqty($data1, $design_no)
    { 
        $this->db->set($data1);
        $this->db->where('design_no', $design_no);
        return $this->db->update('product');
    }




}
?>