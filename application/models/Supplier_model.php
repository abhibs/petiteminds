<?php
class supplier_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertsupplier($data)
    {
        $this->db->insert('supplier', $data);
        return $this->db->insert_id();
    }

    public function getsuppliers() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");

       /* if($this->session->userdata('role')=='factory') {
        $this->db->where("factory_email", $this->session->userdata('email'));
        }*/

        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getsupplierdetails($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editsuppliers($id)
    {//and factory_email =" . "'" . $this->session->userdata('email') . "'
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('supplier');
        $this->db->where($condition);
        $this->db->limit(1);
        return $this->db->get()->result_array();
    }

    function updatesupplier($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('supplier');
    }

    function deletesupplier($id)
    {
        $this->db->delete('supplier', array('id' => $id)); 
    }

    function deleteadminsupplier($email)
    {
        $this->db->delete('admin_login', array('email' => $email)); 
    }



    public function getsupplierstockdetails($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->order_by("created_at", 'desc');
        $this->db->from('fabric_buy');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabricrecievedlist($email) {
        $this->db->select('*');
        $this->db->where("shop", $email);
        $this->db->order_by("created_at", 'asc');
        $this->db->from('fabric_return');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getgoodssentamount($order_id) {
        $this->db->select('sum(total) as total');
        $this->db->where("order_id", $order_id);
        $this->db->where("confirm", "1");
        $this->db->from('stock');
        $query = $this->db->get();
        return $query->result();
   }

    public function getcustomerstockamount($email) {

        $this->db->select('sum(total) as total');
        $this->db->where("shop", $email);
        $this->db->where("confirm", '1');
        $this->db->from('stock');
        $query = $this->db->get();
        return $query->result();
   }


    public function getcustomersaleamount($email) {

        $this->db->select('sum(amount) as total');
        $this->db->where("shop_email", $email);
        $this->db->where("confirm", '1');
        $this->db->from('good');
        $query = $this->db->get();
        return $query->result();
   }

    public function getcustomerrecievedamount($email) {

        $this->db->select('sum(amount) as total');
        $this->db->where("shop_email", $email);
        $this->db->where("confirm", '1');
        $this->db->from('payment');
        $query = $this->db->get();
        return $query->result();
   }

    public function getfabricpayrecievedlist($email) {
        $this->db->select('*');
        $this->db->where("shop_email", $email);
        $this->db->order_by("created_at", 'asc');
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


}
?>