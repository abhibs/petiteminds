<?php
class employee_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertemployee($data)
    {
        $this->db->insert('employee', $data);
        return $this->db->insert_id();
    }

    public function getemployees() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->from('employee');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getemployeedetails($id) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $this->db->from('employee');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editemployees($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where($condition);
        $this->db->limit(1);
        return $this->db->get()->result_array();
    }

    function updateemployee($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('employee');
    }

    function deleteemployee($id)
    {
        $this->db->delete('employee', array('id' => $id)); 
    }




}
?>