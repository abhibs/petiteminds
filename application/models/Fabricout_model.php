<?php
class fabricout_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertfabricout($data)
    {
        $this->db->insert('fabric', $data);
        return $this->db->insert_id();
    }

    public function getfabricout() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->where("shop", $this->session->userdata('email'));
        $this->db->from('fabric');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editfabricout($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('fabric');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('fabric');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('fabric');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updatefabricout($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('fabric');
    }

    function deletefabricoutout($id)
    {
        $this->db->delete('fabric', array('id' => $id)); 
    }





    public function getsupplier() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getshop() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getshopdetails($email) {
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
       
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
        }
   }






}
?>