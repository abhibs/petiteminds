<?php
class stockin_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function getstocksin($email) {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $where = "sent = '1' and shop = '".$email."' ";
        $this->db->where($where);
        $this->db->group_by("order_id");
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstocksindetail($email,$id) {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $where = "sent = '1' and shop = '".$email."' and order_id = '".$id."' ";
        $this->db->where($where);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getimagedetails($design_no) {
        $this->db->select('img');
        $this->db->where("design_no", $design_no);
        $this->db->from('product');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getstocksreturn($email) {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $where = "sent = '0' and shop = '".$email."' ";
        $this->db->where($where);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editstocksout($id,$email)
    {
        $this->db->select('*');
        $this->db->from('stock');
        $this->db->where('id', $id);
        $this->db->where('shop', $email);
        return $this->db->get()->result_array();
    }

    function updatestockin($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('stock');
    }

    function updatestockconfirm($id,$email)
    { 
        $this->db->set('confirm','1');
        $this->db->where('id', $id);
        $this->db->where('shop', $email);
        return $this->db->update('stock');
    }

    function updatestockpending($id,$email)
    { 
        $this->db->set('confirm','0');
        $this->db->where('id', $id);
        $this->db->where('shop', $email);
        return $this->db->update('stock');
    }


    public function getstocksindetails($id,$email) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->where('shop', $email);
        $this->db->from('stock');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


    public function getstockstatus($order_id) {
        $this->db->select('*');
        $this->db->where("order_id", $order_id);
        $this->db->where("confirm", '0');
        $this->db->from('stock');
        return $this->db->get()->result_array();  
   }








}
?>