<?php
class goodsfirst_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertgoodsfirst($data)
    {
        $this->db->insert('goodsfirst', $data);
        return $this->db->insert_id();
    }

    public function getgoodsfirst() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->group_by("design_no");
        $this->db->from('goodsfirst');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editgoodsfirst($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('goodsfirst');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('goodsfirst');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('goodsfirst');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updategoodsfirst($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('goodsfirst');
    }

    function deletegoodsfirst($id)
    {
        $this->db->delete('goodsfirst', array('id' => $id)); 
    }








}
?>