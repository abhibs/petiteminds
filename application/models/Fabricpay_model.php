<?php
class fabricpay_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insertfabricpay($data)
    {
        $this->db->insert('fabricpay', $data);
        return $this->db->insert_id();
    }

    public function getfabricpaysout() {
        $this->db->select('*');
		$this->db->order_by("created_at", "desc");
        $this->db->where("sent", "1");
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabricpayreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "0");
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getfabricpaysmanage() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "1");
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function editfabricpaysout($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('fabricpay');
        $this->db->where($condition);
        return $this->db->get()->result_array();
    }

    function deleteimage($id)
    {
        $this->db->set('img', ''); 
        $this->db->where('id', $id);
         $this->db->update('fabricpay');
    }

       public function getfeaturefile($id)
    {
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('img');
        $this->db->from('fabricpay');
        $this->db->where($condition);
        $this->db->limit(1);
        $arr = $this->db->get()->result_array();  
       return $arr[0]['img'];
    }

    function updatefabricpay($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('fabricpay');
    }

    function deletefabricpay($id)
    {
        $this->db->delete('fabricpay', array('id' => $id)); 
    }



    public function getsupplier() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function updatefabricpayconfirm($id)
    { 
        $this->db->set('confirm','1');
        $this->db->where('id', $id);
        return $this->db->update('fabricpay');
    }

    function updatefabricpaypending($id)
    { 
        $this->db->set('confirm','0');
        $this->db->where('id', $id);
        return $this->db->update('fabricpay');
    }

    public function getfabricpayindetails($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    function updatemanagefabricpay($data, $id)
    { 
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('fabricpay');
    }


    public function getfabricpayfreturn() {
        $this->db->select('*');
        $this->db->order_by("created_at", "desc");
        $this->db->where("sent", "0");
        $this->db->from('fabricpay');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function getshopdetails($email) {
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('shop');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
       
        $this->db->select('*');
        $this->db->where("email", $email);
        $this->db->from('supplier');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
        }
   }


}
?>