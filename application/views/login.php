<style>
.error{
	color: red !important;
}
</style>


<div class="pg-header">
    
    <h1>Login</h1>
    
</div>

<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <div class="form-container">

  <div class="form-bg">
    <div class="container-btn">
       
    </div>
  </div>
<!-- FORM INFO -->
  <div class="form-info-container">

    <form action="" class="form-info" method="post" id="loginform">
	
		<span class="text-danger"><?php echo @$msg; ?></span>
	
      <div class="container-email">
        <label for="email">Email</label>
        <input id="email" name="email" type="text" placeholder="Email">
		
		<span class="text-danger"><?php echo form_error('email'); ?></span>
		
      </div>   
      <div class="container-password">
        <label for="password">Password</label>
        <input id="password" name="password" type="password" placeholder="Password">
		
		<span class="text-danger"><?php echo form_error('password'); ?></span>
		
      </div>
       
      <div class="container-button">
        <button type="submit" id="submit" name="submit_btn" class="btn btn-transparent btn-rounded btn-large">SIGN IN</button>
      </div>
      
		<div>
			<a href="<?=base_url()?>forgot">Forgot Password?</a>
		   
		</div>
		
    </form>
    
    
  </div>

</div>
            </div>
             <div class="col-md-4">
                 <div id="login-contn" class="form-box">
                     <img src="<?=base_url()?>front/images/login.png" class="img-responsive">
                    <h2 class="title">Don't have an account yet?</h2>
                    <p>Create your account now </p>
						
                        <div class="pt-20">
							<a href="<?=base_url()?>register" class="btn btn-transparent btn-rounded btn-large">Register</a>
                        </div>
                        
                        
            
                </div>
             </div>
        </div>
    </div>
</section>



   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#loginform").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    password: {
                        required: "Enter password"
                    }
                }
            }); //validate
    </script> 