      </div>

        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/startmin.js"></script>
        <script src="<?php echo base_url();?>assets/js/dataTables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/dataTables/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>

        <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>


        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
                
              $('#dataTables-example1').DataTable({
                      responsive: true
              });

              $('#dataTables-example2').DataTable({
                      responsive: true,
                        dom: 'Bfrtip',
                        buttons: [
                            'pdf'
                        ]
              });

              $('#dataTables-examplecvpl').DataTable({
                      responsive: true,
                        dom: 'Bfrtip',
                        buttons: [
                            'pdf'
                        ]
              });

              $('#dataTables-examplecvll').DataTable({
                      responsive: true,
                        dom: 'Bfrtip',
                        buttons: [
                            'pdf'
                        ]
              });

              $('#dataTables-exampletsl').DataTable({
                      responsive: true,
                        dom: 'Bfrtip',
                        buttons: [
                            'pdf'
                        ]
              });

              $('#dataTables-exampleasl').DataTable({
                      responsive: true
              });

              $('#dataTables-examplestock').DataTable({
                      responsive: true,
                        dom: 'Bfrtip',
                        buttons: [
                            'pdf'
                        ]
              });


              $("[data-fancybox]").fancybox();

            });

        </script>
<script type="text/javascript"> 	$(document).ready(function() {				console.log('ready');				$("body").on("click", ".viewprod", function(){						var visitid = $(this).data('visitid');			console.log( 'visitid:' + visitid );						var custcontper = $(this).data('custcontper');			console.log( 'custcontper:' + custcontper );			$('.cname').html(custcontper);						var custname = $(this).data('custname');			console.log( 'custname:' + custname );			$('.comname').html(custname);						var visdate = $(this).data('visdate');			console.log( 'visdate:' + visdate );			$('.visdate').html(visdate);									$.ajax({						url:"<?php echo base_url(); ?>form/get_visprods/",						type: "POST",						data:{visitid: visitid},						 success: function (res) {								console.log( res );																					$(".products_card_box_wrap").html( res ); 														 }			});											});			});	</script>
    </body>
</html>
