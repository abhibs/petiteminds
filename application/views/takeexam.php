<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="icon" href="<?=base_url()?>assets/images/favicon.png">

    <title>Admin Panel</title>

    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/metisMenu.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/startmin.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet">

<style>
.web_btn{
	margin-right: 5px !important;
}
</style>
	
</head>

<body>
    <div id="" style="/* background-image: linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.9)), url('<?=base_url()?>assets/images/login_banner.jpg'); */">
        <div class="">
            <div class="container">
                <div class="row"><h1><?=$exams[0]->examtitle?></h1>
				
				<div class="row">
                    <div class="col-md-12">					
				<form role="form" method="post" action="<?=base_url()?>exams/submit" id="examfrm" name="examfrm">
										<?php					if( !empty( $questions ) )
{	
	$backnav = $exams[0]->exambacknav;
	
	$showback = false;
	$shownext = true;
	$hidepanel = '';
	$srno = 0;	
	
	foreach( $questions as $quekey => $queval )
	{
		$srno++;

		if( $quekey )
		{
			$showback = true;
			$shownext = ( ( count($questions) - 1 ) == $quekey ) ? false : true;
			$hidepanel = 'style="display: none;"';
		}
		
?>

<div class="panel panel-default quepanel" <?=$hidepanel?>>
                            <div class="panel-heading">							<div class="row">                <div class="col-md-10">
                                <p class=""> <?=$queval->question?>?</p>
                                <?php    
                if( $queval->quetype == 'MANS' )
                {
                    echo '<p class="subtitle" style="/* display: none; */font-size: 12px;">Please choose '. $queval->correct_cnt .' correct answers.</p>';
                }
                else
                {
                    echo '<p class="subtitle" style="/* display: none; */font-size: 12px;">Please choose the correct answer.</p>';
                }
                ?>
                                
		</div>				
		<div class="col-md-2">    			
			<p style="">Question <?=$srno?> of 10</p><p></p>						
			<p class="quiztimer">Time left: 00:00:00</p>   
					
		</div>		
		
		</div>                            </div>
                            <div class="panel-body" style="">
                                                                    
								
                                        <div class="form-group">
                                        
                <?php    
                                          
                if( $queval->quetype == 'MANS' )
                {					
                    if( !empty( $queval->opt1 ) )
                        echo '<br><input name="opt1_'. $queval->queid .'" type="checkbox" value="Y"> ' . $queval->opt1 ;
                    if( !empty( $queval->opt2 ) )
                        echo '<br><input name="opt2_'. $queval->queid .'" type="checkbox" value="Y"> ' . $queval->opt2 ;
                    if( !empty( $queval->opt3 ) )
                        echo '<br><input name="opt3_'. $queval->queid .'" type="checkbox" value="Y"> ' . $queval->opt3 ;
                    if( !empty( $queval->opt4 ) )
                        echo '<br><input name="opt4_'. $queval->queid .'" type="checkbox" value="Y"> ' . $queval->opt4 ; 
                    if( !empty( $queval->opt5 ) )
                        echo '<br><input name="opt5_'. $queval->queid .'" type="checkbox" value="Y"> ' . $queval->opt5 ;    
                }
                else
                {										
			
					if( !empty( $queval->opt1 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt1_iscorr"> ' . $queval->opt1 ;                    
					if( !empty( $queval->opt2 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt2_iscorr"> ' . $queval->opt2 ;                    
					if( !empty( $queval->opt3 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt3_iscorr"> ' . $queval->opt3 ;                    
					if( !empty( $queval->opt4 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt4_iscorr"> ' . $queval->opt4 ;                     
					if( !empty( $queval->opt5 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt5_iscorr"> ' . $queval->opt5 ;
                    
                }
                                    
                ?>
                                                                                    </div>
                                        
										<div class="text-center">
										
										<?php
										
										if( $showback && $backnav == 'Y' )
										{
											echo '<button type="button" class="web_btn backbtn">Back</button>';
										}
										
										if( $shownext )
										{
											echo '<button type="button" class="web_btn nxtbtn">Next</button>';
										}
										
										if( ( count($questions) - 1 ) == $quekey )
										{
											echo '<button type="button" class="web_btn finbtn">Finish</button>';
										}
										
										?>
										
                                         <input type="hidden" name="queids[]" value="<?=$queval->queid?>" />   
											
                                        </div>
										
										
										
                                    
                            </div>
                           
                        </div>

<?php
		
	}
}										?>			

					<span id="realtime" style="display:none;"></span>
					<input type="hidden" name="checksubmit" id="checksubmit" value="0" />
					<input type="hidden" name="examid" value="<?=$exams[0]->examid?>" />
					
				</form>
					
                        
                    </div>
                </div>
				
				</div>
            </div>
        </div>
    </div>

    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/metisMenu.min.js"></script>
    <script src="<?=base_url()?>assets/js/startmin.js"></script>

	<script>	
	
		$(document).ready( function() {
			
			/* next prev button */
			
			$("body").on("click", ".nxtbtn", function(){
				
				$('.quepanel').hide();
				$(this).closest('.quepanel').next('.quepanel').show();
				
			});
			
			$("body").on("click", ".backbtn", function(){
				
				$('.quepanel').hide();
				$(this).closest('.quepanel').prev('.quepanel').show();
				
			});
			
			$("body").on("click", ".finbtn", function(){
				
				$('#checksubmit').val(1);
				$('#examfrm').submit();
				
			});
			
			/* countdown functionality */
			
			function coutdown(){

				<?PHP
					if($exams[0]->examtimelimit>0){
					
					/* if($quiz->exam_duration_slug == 'hour')
					{
						$duration = 3600000;
					}
					else
					{ */
						$duration = 60000;
					/* } */
					?>

				var mulval = '<?=$duration?>';
				var currentdate = new Date();

				/* var rquiz = parseInt( $('#rquiz').val() );
				console.log( 'rquiz asdf: ' + rquiz );
				if( rquiz )
				{
					<?php
					
					$tarr = explode(":", $time_left);
					$hour = @$tarr[0] * 3600000;	
					$min = @$tarr[1] * 60000;	
					$sec = @$tarr[2] * 1000;

					$actualtl = $hour + $min + $sec;	
					echo "console.log('actualtl: ' + $actualtl);";	
					?>
					
					var countDownDate = new Date(currentdate.getTime() + <?=$actualtl?>);
				}
				else
				{ */
					var countDownDate = new Date(currentdate.getTime() + <?=$exams[0]->examtimelimit?>*mulval);
				/* } */


				console.log( 'countDownDate:' + countDownDate );

				var x = setInterval(function() {
				  var now = new Date().getTime();
				  var distance = countDownDate - now;
				  
					// console.log( 'distance:' + distance );

				  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				  
					//console.log( 'days:' + days );
				  
				  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				  
				  $(".quiztimer").html('Time left: ' + hours + " : "+ minutes + " : " + seconds + "");
				  
				  $("#time_left").val( hours+':'+minutes+':'+seconds);
				  
				  if( $("#qttime").html() == '' )
				  {
					seconds += 2;
					$("#qttime").html(hours + " : "+ minutes+ " : "+seconds);
				  }

				  $("#realtime").html(hours + " : "+ minutes+ " : "+seconds);
				  
				  if(hours==0 && minutes==0 && seconds==0&&$("#checksubmit").val()==0){
					  
						alert('Your Quiz Time Exceeded');
						$('#examfrm').submit();
						clearInterval(x);
						
				  }
				  if (distance < 0) {
					  
						clearInterval(x);
					
				  }
				  if($("#checksubmit").val()==1)
				  {
					 $(".quiztimer").html('');
				  }
				}, 1000);
				<?PHP
					}
					?>
					
			}
			
			coutdown();
			
		} );
	
	</script> 

</body>

</html>