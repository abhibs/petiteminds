<style>
    .btn-large { 
    display: table-caption;
    margin-top: 5px;
}

 .btn-large .fas{
     color:#fff;
     margin-left:15px;
 }

 .amountcolor {
	 color:#0fa244;
	 font-size: larger;
 }

</style>


<div class="pg-header">
    
    <h1>Checkout</h1>
    
</div>


<section>
    <div class="container">

    <?php
    
	define("PAYU_LOG_FILE", "application/logs/ondemandpayulogs.log"); 

	$MERCHANT_KEY = $this->config->item('merchantkey');
	$SALT = $this->config->item('salt');
	// Merchant Key and Salt as provided by Payu.
	
	$PAYU_BASE_URL = $this->config->item('formactionpayu');
	
	$action = base_url() .'ondemand/checkout'; 
	
	$hash_string = '';	
	
	$posted = array();
	if(!empty($_POST)) {
		//print_r($_POST);
	  foreach($_POST as $key => $value) {    
		$posted[$key] = $value; 
		
	  }
	}
	
	$formError = 0;
	
	if(empty($posted['txnid'])) {
	  // Generate random transaction id
	  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	} else {
	  $txnid = $posted['txnid'];
	}
	$hash = '';
	// Hash Sequence
	$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
	
	if(empty($posted['hash']) && sizeof($posted) > 0) { 
	  if(
			  empty($posted['key'])
			  || empty($posted['txnid'])
			  || empty($posted['amount'])
			  || empty($posted['firstname'])
			  || empty($posted['email'])
			  || empty($posted['phone'])
			  || empty($posted['productinfo'])
			  || empty($posted['surl'])
			  || empty($posted['furl'])
			  || empty($posted['service_provider'])
	  ) {
		$formError = 1;
	  } else { 
		//$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
		$hashVarsSeq = explode('|', $hashSequence);
		
		foreach($hashVarsSeq as $hash_var) {
		  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
		  $hash_string .= '|';
		}
	
		$hash_string .= $SALT;
	
	
		$hash = strtolower(hash('sha512', $hash_string));
		$action = $PAYU_BASE_URL . '/_payment';
	  }
	} elseif(!empty($posted['hash'])) {
	  $hash = $posted['hash'];
	  $action = $PAYU_BASE_URL . '/_payment';
	}
	
	error_log(date('[Y-m-d H:i e] '). "On Demand Payu Checkout POST: ".print_r($_POST, true) . PHP_EOL, 3, PAYU_LOG_FILE);
	error_log(date('[Y-m-d H:i e] '). "On Demand Payu Checkout hashsequence: $hash_string" . PHP_EOL, 3, PAYU_LOG_FILE);
	
?>
	
	<form action="<?=$this->config->item('formaction')?>" method="post" target="_top" name="checkout" id="checkout">
        
        <div class="row">
            
            <div class="col-md-6 col-md-offset-3">
                
    <div class="table-resposive">
         <table class="table table-bordered checkout-table" >
             
             <tr>
                 <th>Exam Code </th>
                 <!-- <td><?=@$_POST['examcode']?> </td> -->
                 <td><?=@$examcode?> </td>
             </tr> 
             
             <tr>
                 <th>Exam Name </th>
                 <!-- <td><?=@$_POST['examname']?> </td> -->
                 <td><?=@$examname?> </td>
             </tr> 
             
             <tr>
                 <th>Customer Name </th>
                 <td><?=@$stuname?> </td>
             </tr>  
             <tr>
                 <th>Mobile </th>
                 <td><?=@$stumob?></td>
             </tr> 
             <tr>
                 <th>Email </th>
                 <td><?=@$stuemail?></td>
             </tr>
             
             <?php

        $ordtotal = $demprice;
        $item_name = 'On demand course';
        $item_number = '';
		
		/*$gst = ( $ordtotal * 18 ) / 100;*/
		$gst = 0;
		$ordgstrate = 0;
        $gtot = $ordtotal + $gst;
		
		$payu_amount = (int) ($gtot * $currencydata);
		$payu_amount = sprintf("%.2f", ($gtot * $currencydata));
        // $payu_amount = sprintf("%.2f", $gtot);
		
		/*
		
		save order of student then pass order_id to payment gateway
		
		*/
		
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		if( empty( $loggedid ) )
		{
			$fk_stuid = 0;
		}
		else
		{
		    $fk_stuid = $loggedid;
		}
		
		$time365 = date('Y-m-d H:i:s', strtotime(' + 60 days'));
	
		/* $ordtotal = 0; */
		$fk_ordd_id = $this->GeneralModel->AddNewRow( "orders_demand", array( 'fk_stuid' => $fk_stuid, 'ordd_stuname' => $stuname, 'ordd_stumob' => $stumob, 'ordd_stuemail' => $stuemail, 'ordtotal' => $gtot, 'ordgst' => $gst, 'ordgstrate' => $ordgstrate, 'ordstatus' => '', 'item_name' => $item_name, 'orddatec' => date('Y-m-d H:i:s') ) );
		
		
		/* store user subjects */
		$this->GeneralModel->AddNewRow( "order_demand_detail", array( 'fk_ordd_id' => $fk_ordd_id, 'odet_scatd_id' => $scatd_id, 'odetqty' => 1, 'odetprice' => $demprice, 'odet_validity' => $time365 ) );
		
		/* $ordtotal = $ordtotal + $item['price']; */
		
		/* $this->GeneralModel->UpdateRow( "orders", array( 'ordtotal' => $ordtotal, 'ordstatus' => 'Completed' ), array( 'ordid' => $fk_ordid ) ); */
		
		$this->session->set_userdata('user_ordd_id', $fk_ordd_id);
		
?>
            <!--    
              <tr>
                 <th>Order Total</th>
                 <td><?=@$ordtotal?> </td>
             </tr>  
             <tr>
                 <th>GST (18%) </th>
                 <td><?=@$gst?></td>
             </tr> 
             -->
             <tr>
                 <th>Grand Total </th>
                 <td>$ <?=@$gtot?></td>
             </tr>
         </table>
     </div>
          
                
             </div>   
            
        </div>
        
<div class="row">
            
            <div class="col-md-3">
             
            </div>
            
            <div class="col-md-6">
                <center>
				
					<!--
					<a href="<?=base_url()?>packages/saveord" class="btn btn-transparent btn-rounded btn-large">Pay</a>
					-->
					
					<input type='hidden' name='business'

						value='<?=$this->config->item('bizemail')?>'> <input type='hidden'

						name='item_name' value='<?=$item_name?>'> <input type='hidden'

						name='item_number' value='<?=$fk_ordd_id?>'> <input type='hidden'

						name='amount' value='<?=@$gtot?>'> <input type='hidden'

						name='no_shipping' value='0'> <input type='hidden'

						name='currency_code' value='USD'> <input type='hidden'

						name='notify_url'

						value='<?=base_url()?>ondemand/notify'>

					<input type='hidden' name='cancel_return'

						value='<?=base_url()?>ondemand/cancel'>

					<input type='hidden' name='return'

						value='<?=base_url()?>ondemand/return'>

					<input type="hidden" name="cmd" value="_xclick"> 

				<p class="text-center"><strong>Choose Payment Method</strong></p><br/>
				<div class="row clearfix">
				    
				    <div class="col-xs-4 col-sm-4 text-center">
							<div class="payment-box">
								<img src="/front/images/icon-sp.png" class="img-responsive" />
							<input type='radio' name='paymentgateway' id='paymentgatewaystripe' value="stripe"> 
							<label>
								 <br/><span class="amountcolor">$<?php echo $gtot?></span>
							</label>
							</div>
						</div>
						
					
							<?php if($hash != '') { ?>
								<div class="col-xs-4 col-sm-4 text-center">
							<div class="payment-box">
								<img src="/front/images/icon-pu.png" class="img-responsive" />
								<input type='radio' name='paymentgateway' id='paymentgatewaypayu' value="payu" checked> 
								<label>
									<br/><span class="amountcolor">₹<?php echo $payu_amount?></span>
								</label>
							</div>
						</div>
							<?php } ?>

							<!-- <div class="row">
								<input type='radio' name='paymentgateway' id='paymentgatewaypaypal' value="paypal"> <label>Pay with PayPal</label>
							</div> -->
						
						<div class="col-xs-4 col-sm-4 text-center">
							<div class="payment-box">
								<img src="/front/images/icon-rp.png" class="img-responsive" />
								<input type='radio' name='paymentgateway' id='paymentgatewayrazorpay' value="razorpay"> 
								<label>
									<br/><span class="amountcolor">₹<?php echo $payu_amount?></span>
								</label>
							</div>
						</div>
						
						
						
							<input

								type="button" name="pay_now" id="pay_now"

								Value="Pay Now" class="btn-transparent btn-rounded btn-large">
							
							<!-- <input

								type="submit" name="pay_now" id="pay_now"

								Value="Pay Now" class="btn-transparent btn-rounded btn-large"> -->
					
						</center>
					
					</div>
					
					<div class="col-md-4">
					
					</div>
					
				</div>
            
</div>

                
		</form>	

        <div class="table-resposive">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="table-resposive">
					<form action="<?=$action?>" method="post" target="_top" name="payucheckout" id="payucheckout">
					<input type='hidden' name='paymentthrough' value=''>
						<input type="hidden" name="key" value="<?=$this->config->item('merchantkey')?>" />
						<input type="hidden" name="hash" value="<?= $hash ?>"/>
						<input type="hidden" name="txnid" value="<?= $txnid ?>"/>
						<input type="hidden" name="service_provider" value="<?=$this->config->item('service_provider')?>" size="64" />
						<!-- <input type='hidden' name='amount' value="1.00"> -->
						<input type='hidden' name='amount' value="<?=@$payu_amount?>">
						<input type="hidden" name="firstname" value="<?=@$stuname?>"/>
						<input type="hidden" name="email" value="<?=@$stuemail?>"/>
						<input type="hidden" name="phone" value="<?=@$stumob?>"/>
						<input type="hidden" name="productinfo" value="<?=@$item_name?>"/>
						<input type="hidden" name="surl" value="<?=$this->config->item('odsurl')?>"/>
						<input type="hidden" name="furl" value="<?=$this->config->item('odfurl')?>"/>
						<input type="hidden" name="curl" value="<?=$this->config->item('odcurl')?>"/>
						<input type="hidden" name="udf1" value="<?=$this->config->item('currencycode')?>"/>
						<input type="hidden" name="udf2" value="<?=@$stuemail?>"/>
						<input type="hidden" name="udf3" value="<?=$this->config->item('udf3')?>"/>
						<input type="hidden" name="udf4" value="<?=$this->config->item('udf4')?>"/>
						<input type="hidden" name="udf5" value="<?=$this->config->item('udf5')?>"/>
						<input type="hidden" name="pg" value="<?=$this->config->item('pg')?>"/>


							<?php //if($hash != '') { ?>

								<!-- <div class="text-center"> 
									<input type="button" name="pay_now_payu" id="pay_now_payu" value="Pay through bank account" class="btn-transparent btn-rounded btn-large">
								</div> -->
								
							<?php //} ?>
												

					</form>
				</div>
			</div>
		</div>
	</div>
        
    </div>
</section>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>

var hash = '<?php echo $hash ?>';
function submitPayuForm(){
    //console.log('hash:'+hash);
	if(hash != '') {
		return;
	}
	var checkoutForm = document.forms.payucheckout;
	checkoutForm.submit();
}


$(document).ready(function() { //console.log('submit called');
	submitPayuForm();
});

$("#pay_now").on("click", function(){ 
	
	if($("input[name='paymentgateway']").is(':checked')){
		var payment_method = $("input[name='paymentgateway']:checked").val();
		
		if(payment_method == 'paypal'){

			document.forms["checkout"].submit();

		}else if(payment_method == 'payu'){

			document.forms["payucheckout"].submit();

		}else if(payment_method == 'razorpay'){

			var amount = "<?php echo $payu_amount*100 ?>";
					var ordid = "<?php echo $fk_ordd_id ?>";
					var orddesc = "<?php echo $examname?>";
					var SITEURL = "<?php echo base_url() ?>";
					
					var options = {
					"key": "rzp_live_Zog8cjNShBKrPf",
					"amount": amount, // 2000 paise = INR 20
					"name": "examroadmap.com",
					"description": orddesc,
					"image": "",
					"handler": function (response){
					$.ajax({
					type: 'post',
					//dataType: 'json',
					data: '',
					url: SITEURL + '', //Order/razorPaySuccess
					success:function(result){
					    if(result != "") {
					          window.location.href = SITEURL + 'Ondemand/RazorThankYou';
					    } else {
					        window.location.href = SITEURL + 'Ondemand/RazorFailed';
					    }
                       }
                       
					});
					},
					 
					"theme": {
					"color": "#1A67C1"
					},
					"notes": {
					    "order_id": ordid,
					},
					
					"modal": {
						"ondismiss": function(){
						window.location.replace(SITEURL + 'Ondemand/RazorFailed');
                        }
                    }
					};
					
					
					var rzp1 = new Razorpay(options);
					rzp1.open();

	}else if(payment_method == 'stripe'){

		 <?php $amount = $gtot*100 ?> 
		 <?php $orderid = $fk_ordd_id ?>
		 <?php $studname = $stuname ?>
		 <?php $loggedid = $this->session->userdata('loggedid'); ?>
		 <?php $exam = $examcode ?>
				
				var stripe = Stripe("pk_live_51I1mozDomQpbaMd8KZTEebzjj1FcPYSsn6BzAOvB9KLxDy0TAB6U9qC6SePM0cpVxqKTIb7bLmLktyGtqWMPm98v00e5pgL5np");
				fetch("/Checkoutsession/createsessionondemand/<?php echo $amount ?>/<?php echo $orderid; ?>/<?php echo $studname; ?>/<?php echo $loggedid; ?>/<?php echo $exam; ?>", {
				method: "POST",
				})
				.then(function (response) {
				return response.json();
				})
				.then(function (session) {
				console.log(session);
				return stripe.redirectToCheckout({ sessionId: session.id });
				})
				.then(function (result) {
				// If redirectToCheckout fails due to a browser or network
				// error, you should display the localized error message to your
				// customer using error.message.
				if (result.error) {
					alert(result.error.message);
				}
				})
				.catch(function (error) {
				console.error("Error:", error);
				});
		 
	}
			}else{
		alert('Please select a Payment Method to proceed!!!');
	}
});

</script>

<script type="text/javascript">
	$('.payment-box input:radio').change(function(){
	    if($(this).is(":checked")) {
	    	$('div.payment-box').removeClass("payment-box-active");
	        $(this).parent().addClass("payment-box-active");
	    } else {
	        $('div.payment-box').removeClass("payment-box-active");
	    }
	});
</script>
