
<script type="text/javascript">
// To conform clear all data in cart.
function clear_cart() {
var result = confirm('Are you sure want to clear the cart?');

if (result) {
window.location = "<?php echo base_url(); ?>packages/remove/all";
} else {
return false; // cancel button
}
}
</script>

<div class="pg-header">
    
    <h1>Your Cart</h1>
    
</div>


<section>

    <div class="container">
 
					<div class="row pt-20"> 
<div class="cart-content">
	
<?php

if($cart = $this->cart->contents())
{

?>

<div class="shopping-cart-box">
           
        <div class="cart-head hidden-xs">

            <div class="row">

                <div class="col-sm-4 col-xs-12">

                    <p>Packages</p>

                </div>

                <div class="col-sm-2 col-xs-6">

                    <p>Quantity</p>

                </div>

			   <div class="col-sm-2 col-xs-6">

                    <p>Amount</p>

                </div>

                <div class="col-sm-2 col-xs-6">

                    <p>Cancel product</p>

                </div>

            </div>

        </div>
		
		<div class="cart-content">

<?php

	echo form_open('packages/cart');

	$grand_total = 0;
	$i = 1;
	$loggedid = $this->session->userdata('loggedid');
	
	foreach ($cart as $item){

		
		echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
		echo form_hidden('cart[' . $item['id'] . '][rowid]', $item['rowid']);
		echo form_hidden('cart[' . $item['id'] . '][name]', $item['name']);
		echo form_hidden('cart[' . $item['id'] . '][price]', $item['price']);
		echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
		echo form_hidden('cart[' . $item['id'] . '][discount]', $item['discount']);
		

		// $cid[''] = $item['id'];
		// $new = implode(",", $cid);


		// $data = $this->db->get_where('student_discount',array('stuid'=>$loggedid, 'status'=>0))->result_array();
		// $cid = $data[0]['cid'];
		// echo $cid; exit;
		
		echo '<div class="cart-box">

				<div class="row">

					<div class="col-sm-4 col-xs-12">

						<!--<img src="'. base_url() .'uploads/'. $item['icon'] .'" class="cart-product-img">-->

						<p class="cart-product-name">'. $item['name'] .'</p>
						

					</div>
					

					<!--
					<div class="col-sm-2 col-xs-6">

					   <p class="visible-xs cart-price-label">Price</p>

						<p class="cart-product-price">$ '. number_format($item['price'], 2)  .'</p>

					</div>
					-->
					
					<div class="col-sm-2 col-xs-6">
					
						<p class="visible-xs cart-price-label">Quantity</p>

						'.$item['qty'].'

					</div>

					<div class="col-sm-2 col-xs-6">

					   <p class="visible-xs cart-price-label">Amount</p>

						<p class="cart-product-price">$ '. number_format($item['subtotal'], 2) .' <b>(₹ '. number_format($item['subtotal'] * $this->GeneralModel->GetCurrencyData(), 2) .')</b> </p>

					</div>

					

					<div class="col-sm-2 col-xs-6">

						<a class="cart-cancel-btn btn btn-danger" href="'. base_url('packages/remove/' . $item['rowid'] ) .'">X</a>

					</div>

				</div>

			</div>';
		
		$i++;
		
		
		// $discount = $discount + $item['discount'];
		// $total_amount = $total_amount + $item['subtotal'];
		// $grand_total = $total_amount - $discount;

		// $discondition = $discount;


		$comma_string[] = $item['id'];
		
		$price[]= $item['price'];

	}
	
	$price = array_sum($price);

	$cart_cid = implode(",", $comma_string);
	// print_r($cart_cid);

	$data = $this->db->get_where('student_discount',array('stuid'=>$loggedid, 'status'=>0))->result_array();
	// print_r($data);
	$cid = $data[0]['cid'];
	// echo $cid;

	//Sorting $cart_cid data in asc order
	$data1 = explode(",", $cart_cid);
	asort($data1);
	foreach($data1 as $x => $x_value) {
		$new1[] = $x_value.",";
	  }  
	$cart_id_sort = rtrim(implode("", $new1), ',');

	//Sorting $cid data in asc order
	// echo $cid;
	$data2 = explode(",", $cid);  //print_r($data2);
	asort($data2);
	foreach($data2 as $x1 => $x_value1) {
		$new2[] = $x_value1.",";
	  }  
	$discount_id_sort = rtrim(implode("", $new2), ','); //echo $discount_id_sort;

//$discount_id_sort
	//Comparing both values
	if($cart_id_sort == $discount_id_sort) {
		// echo "matched";
		$disc_type = $data[0]['amounttype'];

		if($disc_type == 'percent'){

			$percent = $data[0]['discpercent'];
			$disrate = $percent/100; 

			$discount = $price * $disrate;
			//echo $discount;
		}else if($disc_type == 'amount'){

			$discount = $data[0]['disamount'];
		}
	}

	$discount = $discount + $item['discount'];
	$total_amount = $price;
	$grand_total = $total_amount - $discount;
	$discondition = $discount;

	$order_session = array(
		"discount" => $discount, 
		"total_amount" => $total_amount, 
		"percent" => $percent
	);
	$this->session->set_userdata($order_session);
	
?>	

		</div>

		<?php

		?>
			
		<div class="cart-foot">
		<?php if($discondition != 0){?>
			<p class="cart-total-price">Total  Price : $<?php echo number_format($price, 2); ?> <b>(₹ <?php echo number_format($price * $this->GeneralModel->GetCurrencyData(), 2); ?>)</b></p>

		<p class="cart-total-price">Discount : $ <?php echo number_format($discount, 2); ?><b>(₹ <?php echo number_format($discount * $this->GeneralModel->GetCurrencyData(), 2);?>)</b></p>
			<?php } ?>

               <p class="cart-total-price">Order total : $ <?php

		//Grand Total.
		echo number_format($grand_total, 2); ?> <b>(₹ <?php echo number_format($grand_total * $this->GeneralModel->GetCurrencyData(), 2); ?>)</b> </p>

            <div class="cart-foot-content text-center">

             

				<input  class ="btn btn-transparent btn-rounded btn-large" type="button" value="Clear Cart" onclick="clear_cart()">
				
				<!--<input class ='common-btn'  type="submit" value="Update Cart">-->

                <a href="<?=base_url()?>packages/checkout" class="btn btn-transparent btn-rounded btn-large">Proceed to checkout</a>
                <a href="<?=base_url()?>packages" class="btn btn-transparent btn-rounded btn-large">Continue Shopping</a>

            </div>

        </div>
       </div>
		
<?php

	echo form_close();
	
	
}
else
{
	echo '<div class="cart-empty">
	    <img src="'.base_url().'uploads/empty-cart.png" />
	    <p>Your cart is empty</p>
	</div>';
}	


?> 
        </div>	 		
        
        </div>
				    

    </div>

</section>

 