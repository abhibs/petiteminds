<style>
.error{
	color: red !important;
}

.web_btn{
	margin-right: 5px !important;
}

.passcls{
	background-color: green !important;
	color: white !important;
}

.failcls{
	background-color: red !important;
	color: white !important;
}

</style>


<div class="pg-header">
    
    <h1><?=$exams[0]->examtitle?></h1>
    
</div>

<section id="login">
    <div class="container">
        
		<div class="row">
		
			<div class="col-md-12">
			
				<div class="panel panel-default quepanel">
			
				<div class="panel-heading <?=$resclass?>" style="">							
				
					<div class="row">                
					
						<div class="col-md-10">
						
							<p class=""><?=$msg?></p>
																		
						</div>				
						
			
					</div>                            
				
				</div>
									
				</div>
				
				<?=@$resques?>
				
				
			
		</div>
		
	</div>
		
    </div>
</section>



   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#loginform").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    password: {
                        required: "Enter password"
                    }
                }
            }); //validate
    </script>
	
<script>	
	
		$(document).ready( function() {
			
			/* next prev button */
			
			$("body").on("click", ".nxtbtn", function(){
				
				$('.quepanel').hide();
				$(this).closest('.quepanel').next('.quepanel').show();
				
			});
			
			$("body").on("click", ".backbtn", function(){
				
				$('.quepanel').hide();
				$(this).closest('.quepanel').prev('.quepanel').show();
				
			});
			
			$("body").on("click", ".finbtn", function(){
				
				$('#checksubmit').val(1);
				$('#examfrm').submit();
				
			});
			
			/* countdown functionality */
			
			function coutdown(){

				<?PHP
					if($exams[0]->examtimelimit>0){
					
					/* if($quiz->exam_duration_slug == 'hour')
					{
						$duration = 3600000;
					}
					else
					{ */
						$duration = 60000;
					/* } */
					?>

				var mulval = '<?=$duration?>';
				var currentdate = new Date();

				/* var rquiz = parseInt( $('#rquiz').val() );
				console.log( 'rquiz asdf: ' + rquiz );
				if( rquiz )
				{
					<?php
					
					$tarr = explode(":", $time_left);
					$hour = @$tarr[0] * 3600000;	
					$min = @$tarr[1] * 60000;	
					$sec = @$tarr[2] * 1000;

					$actualtl = $hour + $min + $sec;	
					echo "console.log('actualtl: ' + $actualtl);";	
					?>
					
					var countDownDate = new Date(currentdate.getTime() + <?=$actualtl?>);
				}
				else
				{ */
					var countDownDate = new Date(currentdate.getTime() + <?=$exams[0]->examtimelimit?>*mulval);
				/* } */


				console.log( 'countDownDate:' + countDownDate );

				var x = setInterval(function() {
				  var now = new Date().getTime();
				  var distance = countDownDate - now;
				  
					// console.log( 'distance:' + distance );

				  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				  
					//console.log( 'days:' + days );
				  
				  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				  
				  $(".quiztimer").html('Time left: ' + hours + " : "+ minutes + " : " + seconds + "");
				  
				  $("#time_left").val( hours+':'+minutes+':'+seconds);
				  
				  if( $("#qttime").html() == '' )
				  {
					seconds += 2;
					$("#qttime").html(hours + " : "+ minutes+ " : "+seconds);
				  }

				  $("#realtime").html(hours + " : "+ minutes+ " : "+seconds);
				  
				  if(hours==0 && minutes==0 && seconds==0&&$("#checksubmit").val()==0){
					  
						alert('Your Quiz Time Exceeded');
						$('#examfrm').submit();
						clearInterval(x);
						
				  }
				  if (distance < 0) {
					  
						clearInterval(x);
					
				  }
				  if($("#checksubmit").val()==1)
				  {
					 $(".quiztimer").html('');
				  }
				}, 1000);
				<?PHP
					}
					?>
					
			}
			
			coutdown();
			
		} );
	
	</script> 