<style>
.count_box .name a, .table td a{
	color: #000 !important;
}
</style>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Dashboard</h1>
                        </div>
                    </div>

                    <div class="count_card_wrap">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="count_card" style="border-color: #e74c3c;">
                                    <p class="title">Lead by Source</p>
                                    <div class="count_box">
									
                                        <p class="name"><a href="<?=base_url()?>customer?leadsrc=cc">Cold Calling</a></p>
                                        <p class="count"><span><?=@$src_cold_cnt[0]->numrows?></span></p>
										
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsrc=de">Direct Enquiry</a></p>
                                        <p class="count"><span><?=@$src_direnq_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsrc=fc">From Company</a></p>
                                        <p class="count"><span><?=@$src_frmcmp_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsrc=im">Indiamart</a></p>
                                        <p class="count"><span><?=@$src_indmrt_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsrc=web">Website</a></p>
                                        <p class="count"><span><?=@$src_web_cnt[0]->numrows?></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="count_card" style="border-color: #1abc9c;">
                                    <p class="title">Lead by Type</p>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadtype=N">New</a></p>
                                        <p class="count"><span><?=@$type_new_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadtype=R">Repeat</a></p>
                                        <p class="count"><span><?=@$type_rep_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadtype=C">Competitor</a></p>
                                        <p class="count"><span><?=@$type_comp_cnt[0]->numrows?></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="count_card" style="border-color: #3498db;">
                                    <p class="title">Lead by Status</p>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsts=HOT">Hot</a></p>
                                        <p class="count"><span><?=@$type_hot_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsts=WARM">Warm</a></p>
                                        <p class="count"><span><?=@$type_warm_cnt[0]->numrows?></span></p>
                                    </div>
                                    <div class="count_box">
                                        <p class="name"><a href="<?=base_url()?>customer?leadsts=COLD">Cold</a></p>
                                        <p class="count"><span><?=@$type_cold_cnt[0]->numrows?></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!--<div class="row">
                     
                    <?php if($this->session->userdata('role')=='admin') { ?>
   
                        <div class="col-lg-3 col-md-6">
                           <a href="<?php echo base_url().'employee';?>">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <i class="fa fa-product-hunt fa-4x"></i>
                                        </div>
                                        <div class="col-xs-10 text-right">
                                            <div class="huge"><?php echo $total_employee;?></div>
                                            <div>Total Employees!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </a>
                        </div>
						
						<div class="col-lg-3 col-md-6">
                           <a href="<?php echo base_url().'product';?>">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <i class="fa fa-product-hunt fa-4x"></i>
                                        </div>
                                        <div class="col-xs-10 text-right">
                                            <div class="huge"><?php echo $total_product;?></div>
                                            <div>Total Product!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </a>
                        </div>

                    <?php } ?>


                    <?php if($this->session->userdata('role')=='employee') { ?>

                        <div class="col-lg-3 col-md-6">
                           <a href="<?php echo base_url().'form';?>">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <i class="fa fa-product-hunt fa-4x"></i>
                                        </div>
                                        <div class="col-xs-10 text-right">
                                            <div class="huge"><?php echo $tot_form;?></div>
                                            <div>Total Visits!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </a>
                        </div>

                    <?php } ?>
					
					


                    </div>-->
					
					
					<div class="manage_visits">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Newly Added Leads
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                    
                                    <div class="web_table">
                                        <table class="table">
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Area</th>
                                                <th>Visit Date</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                            </tr>
											
<?php 
									foreach($visits_new as $visit){ 
									
									/**/
									
										echo ' <tr>
										<td><a href="'.base_url().'customer/view/'.$visit->custid.'">'. $visit->company .'</a></td>
										<td>'. $visit->areaname .'</td>
										<td>'. date('d-m-Y', strtotime( $visit->visdate ) ) .'</td>
										<td>'. $visit->empname .'</td>
										<td>'. $visit->reason .'</td>
									</tr>';

									} ?>
                                            
                                        </table>
                                    </div>

                                    
                            </div>
                        </div>
                    </div>

					<?php 
																									$role = $this->session->userdata('role');
											
					?>

                    <div class="manage_visits">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Upcoming Activities
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                    
                                    <div class="web_table">
                                        <table class="table">
                                            <tr>
                                                <th>Company Name</th>
												
									<?php 
											if( $role == 'admin' ) 
												echo '<th>Employee</th>'; 
											?>
                                                
                                                <th>Reminder Date</th>
                                                <th>Comment</th>
                                            </tr>
                                            
											<?php 
											
											$role = $this->session->userdata('role');
											
											foreach($visits_upcom as $visit){  
												echo '<tr>
                                                <td>
                                                    <a href="'.base_url().'customer/view/'.$visit->custid.'">'. $visit->company .'</a>
                                                </td>';
												
											if( $role == 'admin' )
												echo '<td>
                                                    '. $visit->empname .'
                                                </td>';
												
                                                echo '<td>
                                                    <p>'. date('d-m-Y', strtotime( $visit->taskstdate ) ) .'</p>
                                                </td>
                                                <td>
                                                    <p>'. $visit->tasksub .'</p>
                                                </td>
                                            </tr>';
											} 
											?>
											
                                        </table>
                                    </div>

                                    <?php foreach($visits_upcom as $visit){ ?>

                                    <!--<div id="<?php echo $visit->visitid;?>" class="contact_person_box">

                                        <table class="table table-bordered styled_table">
                                            <tr>
                                                <th>Reminder Date</th>
                                                <td>
                                                    <p class="name"><?php echo '' . date('d-m-Y', strtotime( $visit->visremdate ) ); ?></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Comment</th>
                                                <td>
                                                    <p class="cname"><?php echo '' . $visit->visremcom; ?></p>
                                                </td>
                                            </tr>
                                        </table>


                                    </div>-->

                                    <?php } ?>
                            </div>
                        </div>
                    </div>


                </div>
            </div>