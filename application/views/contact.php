<div class="pg-header">
		
		<h1>Contact Us</h1>
		
	</div>
	
	<section>
		<div class="container"> 
       
	 <div class="row" >
	  <div class="col-md-6">
	       <h1>Contact Form </h1>
	  <p>You can leave a message using the contact form below.</p>
	 
	       <div role="form pt-20">

                    <form action="#" method="post" class="form-div">


<?php

$logstuname = $this->session->userdata('logstuname');
$logstuemail = $this->session->userdata('logstuemail');

?>
                           
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-row">
                                        <span><input type="text" name="fname" placeholder="Your Name *" value="<?=@$logstuname?>" required=""></span>
                                        <span class="text-danger"><?php echo form_error('fname'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-row">
                                        <span><input type="email" name="email" placeholder="Your Email *" value="<?=@$logstuemail?>" required=""></span>
                                        <span class="text-danger"><?php echo form_error('email'); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-row">
                                        <span><input type="tel" name="number" placeholder="Your Phone Number"></span>
                                        <span class="text-danger"><?php echo form_error('number'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-row">
                                        <span><input type="text" name="subject" placeholder="Subject *" required=""></span>
                                        <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-row">
                                        <span><textarea name="message" cols="40" rows="4" placeholder="Message *" required="" ></textarea></span>
                                        <span class="text-danger"><?php echo form_error('message'); ?></span>
                                    </div>
                                </div>
                                
                                 

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-row last  ">
                                        <input type="submit" class="btn btn-transparent btn-rounded btn-large" value="SEND MESSAGE">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
	  </div>
	  
	  <div class="col-md-6">
	      <div class="contact-box">
                <h1>Get In touch </h1> 
                <h4 class="pt-20"> Send us an email on 
                    <a herf="mailto:info@examroadmap.com">info@examroadmap.com</a> </h4>
                        <!--<h4 class="pt-20"> Whatsapp Number <strong>0091 9004542428</strong>  </h4>-->
                    <hr>
            </div>
	  </div>

      <div class="col-md-6">
	      <div class="contact-box">
                <h1>Contact Address </h1> 
                    <h4 class="pt-20">Zoreza Global Business Pvt Ltd </h4>
                    <span>A 702 Millie Enclave Marve Road Orlem</span> <br>
                    <span>Malad West Mumbai </span><br>
                    <span>India 400064.</span> <br>
                    <span>Mobile No : +91 9004542428</span>
                    <hr>
            </div>
	  </div>
	  
	 </div>
		    
		</div>
	</section> 