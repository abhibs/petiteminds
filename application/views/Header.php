<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="icon" href="<?php echo base_url()?>assets/images/favicon.png">
    <meta name=”robots” content=”nofollow” />

    <title>Admin Panel</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/startmin.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/chosen.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header navbar-brand">
                Welcome <?php echo $this->session->userdata('username');?></a>
            </div>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <ul class="nav navbar-right navbar-top-links">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $this->session->userdata('username');?> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo base_url(); ?>home/change_password"><i class="fa fa-question-circle-o fa-fw"></i> Change Password</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>home/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="<?php echo base_url().'dashboard';?>"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>

                        <?php if($this->session->userdata('role')=='admin') { ?>

                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Masters<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#"><i class="fa fa-user"></i>Employee<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li><a href="<?php echo base_url().'employee/add';?>"><i class="fa fa-plus"></i>Add</a></li>
                                        <li><a href="<?php echo base_url().'employee';?>"><i class="fa fa-cog"></i>Manage</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#"><i class="fa fa-thumb-tack"></i>Area<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li><a href="<?php echo base_url().'area/add';?>"><i class="fa fa-plus"></i>Add</a></li>
                                        <li><a href="<?php echo base_url().'area';?>"><i class="fa fa-cog"></i>Manage</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#"><i class="fa fa-map-marker"></i>City<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li><a href="<?php echo base_url().'city/add';?>"><i class="fa fa-plus"></i>Add</a></li>
                                        <li><a href="<?php echo base_url().'city';?>"><i class="fa fa-cog"></i>Manage</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#"><i class="fa fa-cube"></i>Product<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li><a href="<?php echo base_url().'product/add';?>"><i class="fa fa-plus"></i>Add</a></li>
                                        <li><a href="<?php echo base_url().'product';?>"><i class="fa fa-cog"></i>Manage</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#"><i class="fa fa-question-circle"></i>Reason<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li><a href="<?php echo base_url().'reason/add';?>"><i class="fa fa-plus"></i>Add</a></li>
                                        <li><a href="<?php echo base_url().'reason';?>"><i class="fa fa-cog"></i>Manage</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="<?php echo base_url().'customer';?>"><i class="fa fa-handshake-o"></i>Customers</a>
                        </li>



                        <?php } ?>






                        <?php if($this->session->userdata('role')=='employee') { ?>

                        <li>
                            <a href="#"><i class="fa fa-handshake-o"></i>Customers<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_url().'form/add';?>"><i class="fa fa-plus"></i>Add</a></li>
                                <li><a href="<?php echo base_url().'form';?>"><i class="fa fa-cog"></i>Manage</a></li>
                            </ul>
                        </li>

                        <?php } ?>




                    </ul>
                </div>
            </div>
        </nav>