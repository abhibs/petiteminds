<section>
    <div class="container">
        <div class="content">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" id="headingOne" role="tab">
                        <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">How do I get access
                                to the free certification practice exam demo? <i class="pull-right fa fa-plus"></i></a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" id="collapseOne" role="tabpanel"
                        aria-labelledby="headingOne">
                        <div class="panel-body">
                            <p>Follow the steps given below to access the free certification practice exam demo:</p>
                            <ol>
                                <li>Sign in to <a href="https://www.petiteminds.com">www.petiteminds.com</a>
                                    (Registration and Login is required to access the personalized result book).</li>
                                <li>Click Certification exam module page.</li>
                                <li>Click on "Try Online Exam" button. You will be redirected to the Demo certification
                                    practice exam page.</li>
                                <li>
                                    View the certification practice exam demo criteria. You will be asked to answer 10
                                    different questions in the given time limit.
                                </li>
                                <li>
                                    Click on "Start Exam" button to start the free certification exam demo.
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingTwo" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                aria-controls="collapseTwo">What is personal certification practice exam? <i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <p>Personal certification practice exam will get you an access to our personal platform with
                                many other benefits.</p>

                            <p>
                                For more information, refer to the relative Certification practice exam page.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                aria-controls="collapseThree">How do I get access to the personal certification practice
                                exam? <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseThree" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Follow the steps given below to access the personal certification practice exam.</p>

                            <ol>
                                <li>Sign in to <a href="https://www.petiteminds.com">www.petiteminds.com</a>
                                    (Registration and Login is required to access the personalized result book)</li>
                                <li>Go to the Certification exam module page which covers the certification exam you would like to attend.</li>
                                <li>You have to purchase a Personal certification practice exam by following FAQ steps "How do I purchase Personal certification practice exam?".</li>
                                <li>Once your order is placed successfully, we will review the order and provide personal certification practice exam access to your user account.</li>
                                <li>You will be able to access personal certification practice exam under "My Exams" section in user menu.</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                                aria-controls="collapseFour">How do I report an error or mistake on any of your
                                certification practice exams? <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>If you think that you have found an incorrect answer, explanation, text typo or if a
                                question seems inappropriate. Please contact us at <a
                                    href="mailto: info@petiteminds.com"> info@petiteminds.com</a> with the
                                following details:</p>
                            <ol>
                                <li>The exam you are preparing for and version number.</li>
                                <li>Question text or Page title where you found the error.</li>
                                <li>Your contact information, including your email address associated with your account.</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                                aria-controls="collapseFive">How do I Submit My Testimonial? <i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseFive" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>To create testimonial, follow the steps:</p>
                            <p>Step 1: Login to our website using your credential</p>
                            <p>Step 2: Click on Testimonial link&nbsp;under your user name.&nbsp;</p>
                            <p>Step 3: Write Title of your testimonial and select your exam Module from drop down followed by your testimonial into the body.</p>
                            <p>Step 4: Click on SAVE&nbsp;button at last to submit your testimonial&nbsp;</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseSix" aria-expanded="false"
                                aria-controls="collapseSix">How Can I View the Result History and Review the Specific
                                Exam Attempt Questions?<i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseSix" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ol>
                                <li>Log In with your user credentials and click on My Account link in User menu.</li>
                                <li>You have to purchase a Personal certification practice exam by following FAQ steps "How do I purchase Personal certification practice exam?".</li>
                                <li> Once your order is placed successfully, we will review the order and provide personal certification practice exam access to your user account.</li>
                                <li>You will be able to access personal certification practice exam under "My Exams" section in user menu.</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseSeven" aria-expanded="false"
                                aria-controls="collapseSix">How Frequently is Questions are Updated on Site?<i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseSeven" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>We do update our question bank regularly as and when required. Our experts keep our personal question bank updated based on latest Certification exam conducted by the respective boards asked in very recent actual certification exams. We always keep our self in touch with certified candidates in recent past to conduct an interview of their certification exam experience and questions appeared in real certification exam.</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseEight" aria-expanded="false"
                                aria-controls="collapseEight">Is your Questions sets are valid for any exam location?<i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseEight" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Yes. You can appear certification exams anywhere in the world. exam will have same exam pattern with same questions set irrespective of region or location if conducted by respective boards or universities.</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseNine" aria-expanded="false"
                                aria-controls="collapseNine">How do I make payment<i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseNine" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Through Paypal,Stripe,PayU,RazorPay or Credit Card or Debit Card or right an email to us <a
                                    href="mailto:info@petiteminds.com">info@petiteminds.com</a> for further help.</p>
                            <p>
                                You will get detailed email once you are done with the payment.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseTen" aria-expanded="false"
                                aria-controls="collapseTen">How to Make Payment and How I will Get Access to Personal
                                Practice Exam? (Step By Step Guide) <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseTen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>To go with Personal certification online practice exam access, follow step by step payment process.</p>
                            <p>Step 1 :  Go to relevant product page and read the practice exam features in detail and click on Buy Now button.</p>
                            <p>Step 2: Review the product detail on next page and click on ADD TO CART button.</p>
                            <p>Step 3: Review the shopping cart and click on CHECKOUT button.</p>
                            <p>Step 4: Provide your user name and password to login or create new user to go ahead.</p>
                            <p>Step 5: Provide the billing detail and click on REVIEW ORDER&nbsp;at last.</p>
                            <p>Step 6: Review the order detail and Click on SUBMIT ORDER,&nbsp;It will redirect you to
                                payment gateway.</p>
                            <p>Step 7: Make the payment on the payment page to complete this order (Our payment gateway accepts payment through Credit Card or Debit Card or Paypal,Stripe,PayU,RazorPay).</p>
                            <p>Step 8: Our support team will process your order after reviewing the payment status.</p>
                            <p>Step 9:&nbsp;To access the online personal practice exam for certification, click on My Personal Exams as shown in below screen.</p>
                            <p>Step 10: Click on any exam listed and Click on START EXAM</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseEleven" aria-expanded="false"
                                aria-controls="collapseEleven">Why am I getting "Authorization Failed: Do Not Honour"
                                error? <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseEleven" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Follow the steps below which are recommended by CheckOut support if "Authorization
                                Failed: Do Not Honour" error occurs during a payment:</p>
                            <p><strong>Reason for the message:</strong> The information passes validation on CheckOut
                                end and an actual authorization request is sent but the bank is declining the
                                authorization. The most common cause for this error is that the seller is located
                                outside of the country that the bank is located in so it is showing up as an
                                international transaction.</p>
                            <p><strong>How to resolve the issue:</strong> Contact the bank and let them know that you
                                want the transaction to go through (they will be able to see the authorization attempts)
                                and then replace the order. If they still receive problems then their purchase session
                                is probably locked because of previous declines. They must wait 30 minutes, use a
                                different browser or clear their cookies and restart their browser so that a new
                                purchase session is used when they replace the order.</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseTwelve" aria-expanded="false"
                                aria-controls="collapseTwelve">How do I purchase personal certification practice exam?
                                <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseTwelve" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>Follow the steps given below to purchase the personal certification practice exam.</p>
                            <ol>
                                <li>Sign in to&nbsp;<a
                                        href="http://www.petiteminds.com">www.petiteminds.com</a>(Registration &amp;
                                    Login is required to purchase any product)</li>
                                <li>Open the Certification exam module page which covers the certification exam you would like to attend.</li>
                                <li>Click on "Buy Now" button to open product page.</li>
                                <li>Click on "Add To Cart" button to add the product to your shopping cart.</li>
                                <li>Click on "Checkout" button to proceed for the purchase.</li>
                                <li>Fill all your order details carefully and click on "Review Order" button.</li>
                                <li>Review your order details and click on "Submit Order" button to make the payment.
                                </li>
                                <li>Make the payment on the Checkout payment page to complete this order (Checkout payment gateway accepts payment through PayPal,Stripe,PayU,RazorPay,Credit Card or Debit Card).</li>
                                <li>You will be redirected back to <a
                                        href="http://www.petiteminds.com ">www.petiteminds.com </a> on order details page.
                                </li>
                                <li>Now you are all set to start practicing with personal practice exam under "&lt;User
                                    Menu&gt; &rarr;&nbsp;My personal Exams"</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingThree" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseThirteen" aria-expanded="false"
                                aria-controls="collapseThirteen">Where do I find my personal certificatiowww.petiteminds.comn practice exam
                                after I have made my purchase? <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseThirteen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>The purchased personal certification practice exam are available in the member's account.
                                To access personal certification practice exam, follow the steps given below:</p>
                            <ol>
                                <li>Go to <a href="https://www.petiteminds.com">www.petiteminds.com</a></li>
                                <li>Sign in to your examroadmap account with your user name and password.</li>
                                <li>Click on "My Personal Exams" option in User menu (Menu name starts with your user
                                    name and is present on right side panel)</li>
                            </ol>www.petiteminds.com
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseFourteen" aria-expanded="false"
                                aria-controls="collapseFourteen">Will I receive a receipt? <i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseFourteen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                You will receive an immediate email confirmation re-stating the order details and
                                welcoming you to the site. The welcome and order confirmation email is the only receipt
                                you will receive.
                                You do however have access to previous invoices in My Account section which you can
                                print on demand.

                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseFifteen" aria-expanded="false"
                                aria-controls="collapseFifteen">My bank account or credit card is not in US Dollars. How
                                do I proceed with the payment? <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseFifteen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                You can continue to make your purchase using your credit card as normal and your
                                financial institution/bank will make the conversion from your default currency to USD as
                                per your bank's exchange rate on the day of the purchase. The examroadmap.com web site
                                will only display the USD price and will not display any other currencies or the current
                                exchange rate.</p>
                            <p>
                                Your credit card or bank statement may show a different purchase amount based on your
                                default currency and any fees that your bank charges you for a conversion.
                                examroadmap.com only charges you for the product you are purchasing. examroadmap.com
                                does not charge you for currency conversions or exchange rate fees. The buyer is solely
                                responsible for any associated bank fees relating to conversions and exchange rates.

                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseSixteen" aria-expanded="false"
                                aria-controls="collapseSixteen">How secure is my personal information with
                                examroadmap.com? <i class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseSixteen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                Completely Secure!
                                We do not store credit card or personal identification information on our servers beyond
                                your email address and <a href="http://www.petiteminds.com">www.petiteminds.com</a>
                                account information.


                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseSeventeen" aria-expanded="false"
                                aria-controls="collapseSeventeen">What forms of payment do you accept? <i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseSeventeen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                ou can pay online using a credit card or debit card. We accept the following credit cards: Visa, Master Card, American Express, Maestro or PayPal, Stripe, PayU, RazorPay.

                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseEighteen" aria-expanded="false"
                                aria-controls="collapseEighteen">Can I use a Mac to take my exam? <i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseEighteen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                Yes, you can use a Mac to take your exam. You will need to download and install
                                Questionmark Secure for Mac.
                                Link to Questionmark Secure <a
                                    href="https://support.questionmark.com/content/get-questionmark-secure">https://support.questionmark.com/content/get-questionmark-secure</a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseNineteen" aria-expanded="false"
                                aria-controls="collapseNineteen">Take Your Exam<i class="pull-right fa fa-plus"></i></a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseNineteen" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                Examinations are secure and remotely proctored. You will be required to use certain
                                technology to take the exam:
                            </p>
                            <ul>
                                <li><a href="https://www.questionmark.com/content/get-questionmark-secure">Questionmark
                                        Secure</a>&nbsp;browser that you will need to download and install</li>
                                <ul>
                                    <li><strong>Even if you have previously installed Questionmark Secure, you must
                                            upgrade to ensure you are now using an appropriate version.</strong></li>
                                    <li>v6.0.26+ is required for Windows</li>
                                    <li>v6.0.19+ is required for Mac</li>
                                </ul>
                                <li>Webcam</li>
                                <li>Microphone or headset</li>
                                <li>Broadband connection</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseTwenty" aria-expanded="false"
                                aria-controls="collapseTwenty">How do I receive my exam score?<i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseTwenty" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                You will be notified of your score, on-screen, immediately after completing your exam. The on-screen message will display your exam score and if you passed or failed the exam. You will not be able to print the on-screen display of your score. Additionally, your results - pass or fail - are tracked in Certification Hub and are displayed accordingly in your Exam Dashboard
                            </p>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="headingFourteen" role="tab">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"
                                data-parent="#accordion" href="#collapseTwentyOne" aria-expanded="false"
                                aria-controls="collapseTwentyOne">How can I obtain my actual certificate? <i
                                    class="pull-right fa fa-plus"></i></a></h4>
                    </div>
                    <div class="panel-collapse collapse" id="collapseTwentyOne" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>
                                You can download the digital certificate immediately.
                            </p>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!--<h3 class="pt-20">Reputation and Recognition</h3>-->
        <!--<p><strong>-->
        <!--        Better role and paycheck is automatically followed by reputation and recognition among peers,-->
        <!--        colleagues, and clients. This can even act as a motivator to help them do better in their jobs and earn-->
        <!--        higher benefits. SAP professionals will be recognized by their certification. The accreditation can be-->
        <!--        used along with the names and this indeed makes a strong impression among recruiters and employers.-->
        <!--    </strong></p>-->

    </div>
</section>