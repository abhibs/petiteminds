<style>
    
.btn-large { 
    padding: 10px;
    display: block;
    margin-top: 5px;
}

 .btn-large .fas{
     color:#fff;
     margin-left:15px;
 }

</style>


<div class="pg-header" style="background: linear-gradient(rgba(000,000,000,0.5),rgba(000,000,000,0.5)),url(<?php echo base_url(); ?>front/images/pg-banner.jpg);">
    
    <h1>
    
    <?php
    
                $numplus = '';
    
				if( strtolower( @$subcategory[0]->catname ) != 'more' )
					echo @$subcategory[0]->catname;
					// print_r(@$subcategory[0]->catname);
					// exit;
				
				?>
    </h1>
    
</div>


<section>
    <div class="container">


<?php

if( !empty( $students ) && $students[0]->email_veri == 'N' )
{
    echo '<div class="row not-verified-singlepackg-box"><div class="col-md-12" style="text-align: center;"><p>Please check your email to verify your account.</p><p> Resend Email?<a href="javascript:void(0)" id="resverem"> <span style="text-decoration:underline;">Click here<span></a>.</p></div></div><br>';
}
?>

        
        <div class="row">
               <div class="col-md-10">
                <div class="pack-img-outer">
                    
                        <!--    
                        <div class="pack-img">
                <img src="<?=base_url() . 'uploads/' . @$subcategory[0]->icon?>" class="img-responsive">  
                        </div>
                        -->
                        
                        <div class="pack-img1">
                            <h4 class="img-text"><?=@$subcategory[0]->name?></h4>
                            <p class="constant">www.examroadmap.com</p>
                        </div>
                        
                        
                         <p class="sku"><span>Exam Code:</span> <?=@$subcategory[0]->scat_sku?></p>
                </div>
               
         
                <h3 class="course-name">
				<?=@$subcategory[0]->name?>
				</h3>
                <div class="pack-desc">
				
                    <p>
                        <?=@$subcategory[0]->scatdesc?>
					</p>
 
                </div>
            </div> 
            <div class="col-md-2">
			
<?php
if( !empty( $subcategory[0]->examid ) )
{
            $numplus = $this->GeneralModel->CountRecords('questions', $key = array( 'questions.fk_examid' => $subcategory[0]->examid ), $search = '');

            $numplus = $numplus . '+';

			$loggedin = $this->session->userdata('loggedin');
			$loggedid = $this->session->userdata('loggedid');

			$join_ar = array(
								0 => array(
									"table" => "orders",
									"condition" => "order_detail.fk_ordid = orders.ordid",
									"type" => "inner"
								),
								1 => array(
									"table" => "exams",
									"condition" => "order_detail.odetsid = exams.sid",
									"type" => "inner"
								)
							);

			$order_detail = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'orders.ordstatus' => 'Completed', 'exams.examid' => @$subcategory[0]->examid, 'order_detail.odet_validity >=' => date('Y-m-d') ), $search = '', $join_ar, $group_by = '' );
			
			if( ! empty( $order_detail ) )
			{
?>				
				<a href="<?=base_url() . 'exams/index/' . @$subcategory[0]->examid . '/'. @$subcategory[0]->id?>" class="btn btn-transparent btn-rounded btn-large">Try Full Exam</a> 
				
				
				<a href="<?=base_url() . 'exams/mini/' . @$subcategory[0]->examid . '/'. @$subcategory[0]->id?>" class="btn btn-transparent btn-rounded btn-large">Try Mini Exam</a>
				
				
<?php
			}
			else
			{
?>				
				<a href="<?=base_url() . 'packages/buynow/' . @$subcategory[0]->examid?>" class="btn btn-transparent btn-rounded btn-large  btn-block">Buy Now<i class="fas fa-arrow-right"></i></a>
				
                <div class="price">
                    <h4>$ <?=@$subcategory[0]->yearsubsc?></h4>
                </div>
                
			    <div class="price">
                    <h4>₹ <?=@$inr_value?></h4>
                </div>
			
<?php		
			



		
			}

}
				if( ! empty( $subcat_free ) )
				{

	?>

				

					<a href="<?=base_url() . 'exams/free/' . @$subcat_free[0]->examid . '/'. @$subcat_free[0]->id?>" class="btn btn-transparent btn-rounded btn-large btn-block" style="margin-top: 10px">Free Samples</a>
					
	<?php				
				}
?>

                
            </div>    
            
        </div>
    </div>
</section>

<section id="feature">
    <div class="container">
      
		
		<div class="row">
		    <div class="col-md-8">
		        
		         <div class="section-title"> 
					 <h2 class="title">Packages Features</h2>
					
				 </div>
		        <div class="feature-box">
		            <ul class="feature-list">
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong><?=@$numplus?> Scenario Based Practice Exam Questions and Answers</strong> similar to actual <?=@$subcategory[0]->catname?> Exam questions.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>2 Months Unlimited Access</strong> to Online <?=@$subcategory[0]->examtitle?> exam.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Guaranteed to have REAL Multiple Choice Questions with Correct Answers for assessment.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>Performance Check Meter</strong> during practice exam similar to <strong>Actual SAP SE exam.</strong></li>
 
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>100% Accurate</strong> and Verified Answers</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>We will provide <strong>PDF documents</strong> if required</li>
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i><a href="<?=base_url()?>moneybackguarantee"><strong>100% Money Back Guarantee</strong></a> if you can't clear your actual exam.</li>-->
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>100% Money Back Guarantee</strong> if you can't clear your actual exam.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Online practice exam to be completed in <strong>Specified Time Duration.</strong></li>

<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Results <strong>Data Stored</strong> with attended questions and answers.</li>
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Price is just <strong>$49 USD</strong> lowest compared to others with Quality Q &amp; A.</li>-->

<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Price is just <strong>$<?=@$subcategory[0]->yearsubsc?> USD</strong>, lowest compared to other online or offline materials.</li>


<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>Safe &amp; Secure Payment</strong> with the <strong>RazorPay,</strong> <strong>PayU</strong> and <strong>Stripe</strong> payment gateway.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Payments accepted through <strong>Credit Card, Debit Card, RazorPay, PayU and Stripe</strong>.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Goto website/select your exam/buy now/add to cart/check out. Once payment is done, you will get 3 things automatically. 1.Link to Download Invoice 2. Link to download PDF of the exam QnA. 3.Two months system access, you can practice ‘n’ number of times. A tab called My results tab, all your data will be stored, you can analyse your performance.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>We invite you to visit our Live User <strong>Testimonials</strong> and <strong>Blogs</strong>.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Please follow our website in <strong>LinkedIn, Facebook, Twitter, Instagram </strong> and <strong>YouTube</strong>.</li></li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>We donate 10% of the profit to charity.</li>


<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>Safe & Secure Payment</strong> with the <strong>Paypal</strong> payment gateway.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Payments accepted through <strong>Credit Card, Debit Card, and PayPal</strong>.</li>
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong> 100% Money Back Guarantee </strong> if you can't clear your actual exam.</li>-->
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i>We invite you to visit our <strong> Live User Testimonials </strong> and <strong> Facebook Community </strong> to build trust.</li>-->
</ul>
		        </div>
		    </div>
		    
		    <div class="col-md-4 feature-right">
		        <p><?=@$subcategory[0]->examtitle?> - Full</p>
		        <ul class="qt-list">
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Question: <?=@$subcategory[0]->qcnt_display?> </li>
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Time limit: <?=@$subcategory[0]->examtimelimit?> minutes</li>
		        </ul>
		        
		        <p><?=@$subcategory[0]->examtitle?> - Mini</p>
		        <ul class="qt-list">
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Question: <?php 
		            if( !empty( @$subcategory[0]->qcnt_display ) )
		                echo floor( intval( @$subcategory[0]->qcnt_display ) / 2 );
		            else 
		                echo '0';
		            ?> </li>
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Time limit: <?php 
		            if( !empty( @$subcategory[0]->examtimelimit ) )
		                echo round( @$subcategory[0]->examtimelimit / 2 );
		            else 
		                echo '0';
		            ?>
		             minutes</li>
		        </ul>
		        
		        <!--
		        <p>SAP Certified Application Associate - Ariba Integration - Mini</p>
		        <ul class="qt-list">
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Question: 40 </li>
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Time limit: 90 minutes</li>
		        </ul>
		        -->
		        
		   </div>      
		    
		</div>
		
    </div>
</section>

<script>
    
$("body").on("click", "#resverem", function(){
    
    $.ajax({
    			url:"<?php echo base_url(); ?>register/veriemail/<?=$students[0]->stuid?>",
    			type: "POST",
    			data:{},
    			 success: function (res) {
    
    					console.log( res );
    										
    					alert('Email Sent. Please check your email.');
    					
    			 }
    });
    
});
    
</script>