<section>
	<div class="container">
	  <div class="desc">
	      <h2 class="text-blue text-center pb-20">Welcome to Petiteminds.com</h2>
	  <h3 class="text-blue text-center pb-20">An ideal online platform to practice for Certification Exam</h3>
	    <p> 
		Petiteminds provides the ideal platform to meet the demands of the constantly evolving SAP market. We are one of the world’s leading certification exam questions and answer online training providers. We partner with companies and individuals to address their unique needs, providing online training and coaching that helps working professionals achieve their career goals.Petiteminds.com is here to help software users to make their certification dream real by practicing with our certification practice exam platform. Petiteminds.co is here to provide a platform where technology experts can join hands to impart knowledge available to software users that can help them clear their Certification exams with a good passing percentage score. We are a group of consultants having experience in various domains of Technology, with our long journey and extensive engagement in providing solutions across multiple domain.
	    </p>
	  </div>
	</div>
</section>

<section>
  <div class="container">
     <div class="text-blue text-center wow fadeInUp"> 
                 <h2 class="title">Benefits of Petiteminds.com </h2> 
             </div>
	<div class="row pt-20">
                        <div class="col-md-3">
                            <div class="benefits-box">
                                <p class="number">1</p>
                                <h3 class="title">Opportunity</h3>
                                <p>
                                    	Wider spectrum of opportunity in the global market
                                </p>
                            </div>
                        </div>
                         <div class="col-md-3">
                            <div class="benefits-box">
                                <p class="number">2</p>
                                <h3 class="title">Competition </h3>
                                <p>
                               	Competitive edge to meet the strategic goals of your organization
                                </p>
                            </div>
                        </div>
                         <div class="col-md-3">
                            <div class="benefits-box">
                                <p class="number">3</p>
                                <h3 class="title">International recognition </h3>
                                <p>
                                   	Gain international recognition and instill client/students confidence
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="benefits-box">
                                <p class="number">4</p>
                                <h3 class="title">Experience</h3>
                                <p>
                                    Our promoters are practicing consultants having in-depth product knowledge, project implementation experience and proficient course delivery skills
                                </p>
                            </div>
                        </div>
                    </div>		 
			 
		<!--<h3 class="title pt-20 text-blue">	 SAP Certification </h3>-->
 	<!--<p>When you are certified by SAP, it is an important benchmark of consultant expertise. You join a distinguished community of experts enjoying a significant advantage over peers…</p>-->

			 
  </div>
</section>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
 
