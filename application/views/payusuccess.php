<style>
.error{
	color: red !important;
}
</style>


<div class="pg-header">
    
    <h1><?=@$msgh1?></h1>
    
</div>

<section id="msg-container">

    <div class="container">

        <div class="row">
        
            <div class="col-md-12">
              
    
                <?php

                if( !empty( $msgsuc ) )
                {

                ?>
                    
                    <div class="msg-box">
                        <img src="<?=base_url()?>front/images/icons/tick.png" class="img-responsive"> 
                    <h3><?=$msgsuc?></h3>
                    </div>
                    
                <?php

                }
                if( !empty( $msgerr ) )
                {

                ?>
                    
                    <div class="msg-box">
                        <img src="<?=base_url()?>front/images/icons/close.png" class="img-responsive"> 
                    <h3><?=$msgerr?></h3>
                    </div>
                    
                <?php

                }

                ?>
    
            </div>
        
            <?php if($ondemand == 1){ ?>
                <a href="<?= base_url() ?>ondemand" class="btn-transparent btn-rounded">Continue to Order On Demand</a> 
            <?php }else{ ?>
                <a href="<?= base_url() ?>packages" class="btn-transparent btn-rounded">Continue Shopping</a> 
            <?php } ?>

        </div>

    </div>

</section>


 