<div class="pg-header">
    
    <h1>Your Password has been reset</h1>
    
</div>


<section>
    <div class="container">
        
       <div class="row">
           <div class="col-md-4"></div>
           <div class="col-md-4">
                <div class="forgot-box">
         <img src="https://www.sibinfotech.co.uk/projects/sap/front/images/icons/forgot.png" class="img-responsive"> 
      <h2>Your Password has been reset </h2>
      
      <p>To sign in with your new password <a  href="<?=base_url()?>login">Login here</a></p>
    
    
    </div>
           </div>
       </div>
       
    </div>
</section>
