<style>
.error{
	color: red !important;
}
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js"></script>

<div class="pg-header">
    
    <h1>On Demand</h1>
    
</div>

<section id="login" style="padding-bottom: 500px;">
    <div class="container">
        <div class="row">
            <h4 class="text-center">SAP Certification Exam is available On Demand,you can Pre-Order any SAP 
exam and we will arrange for you immediately.
</h4>
<br>  
            <div class="col-md-12">
                <div class="form-container">

  <div class="form-bg mt-20">
    <div class="container-btn">
       
    </div>
  </div>
<!-- FORM INFO -->
  <div class="form-info-container">

    <form action="<?php echo base_url() . 'ondemand/checkout'; ?>" class="form-info" method="post" id="signinform">
        
        <h4>SAP Certification Exam is available On Demand,you can Pre-Order any SAP 
exam and we will arrange for you immediately.
</h4>
<br>
        
      <div class="container-email">
        <label for="email">Name</label>
        <input id="stuname" name="stuname" type="text" placeholder="Name" value="<?=@$students[0]->stuname?>">		
		<span class="text-danger"><?php echo form_error('stuname'); ?></span>
		
      </div> 
	  <div class="container-email">
        <label for="email">Mobile</label>
        <input id="stumob" name="stumob" type="text" placeholder="Mobile" value="<?=@$students[0]->stumob?>">
		<span class="text-danger"><?php echo form_error('stumob'); ?></span>
      </div> 
      <div class="container-email">
        <label for="email">Email</label>
        <input id="stuemail" name="stuemail" type="text" placeholder="Email" value="<?=@$students[0]->stuemail?>">
		<span class="text-danger"><?php echo form_error('stuemail'); ?></span>
      </div>   
      
      <!--
      <div class="container-email">
        <label for="email">Category</label>

<?php

            /*
    		$opt = array( '' => 'Select' );
    		if( !empty( $catdemand ) )
    		{
    			
    			foreach( $catdemand as $key => $value )
    			{
    				$opt[ $value->catd_id ] = $value->catname;							
    			}
    			
    		}
    		echo form_dropdown('catd_id', $opt, set_value( 'catd_id' ), 'id="catd_id" class="form-control" required');
    		*/

?>
        
		<span class="text-danger"><?php echo form_error('catd_id'); ?></span>
      </div>
      -->
      
      <div class="container-email">
        <label for="email">Exam Code</label>

        <div id="scatdcodediv">        
<?php

    		$opt = array( '' => 'Select' );
    		
    		if( !empty( $subcatdemand ) )
    		{
    			
    			foreach( $subcatdemand as $key => $value )
    			{
    			    if( !empty( $value->scatdcode ) )
    			    	$opt[ $value->scatd_id ] = $value->scatdcode;							
    			}
    			
    		}
    		
    		echo form_dropdown('scatdcode', $opt, set_value( 'scatdcode' ), 'id="scatdcode" class="form-control" required');

?>
        </div>

        
        
		<span class="text-danger"><?php echo form_error('scatdcode'); ?></span>
      </div>
      
      <div class="container-email">
        <label for="email">Exam Name</label>

        <div id="scatddiv">
<?php

    		$opt = array( '' => 'Select' );
    		
    		if( !empty( $subcatdemand ) )
    		{
    			
    			foreach( $subcatdemand as $key => $value )
    			{
    			    if( !empty( $value->scatdcode ) )
    				    $opt[ $value->scatd_id ] = $value->scatdname;							
    			}
    			
    		}
    		
    		echo form_dropdown('scatd_id', $opt, set_value( 'scatd_id' ), 'id="scatd_id" class="form-control" required');

?>

        

        </div>
        
        <input type="hidden" name="examcode" id="examcode" value="" />
        <input type="hidden" name="examname" id="examname" value="" />
        
		<span class="text-danger"><?php echo form_error('scatd_id'); ?></span>
      </div>
      
      <div class="container-email">
        <label for="email">Price</label>
        <input id="demprice" name="demprice" type="number" placeholder="Price" value="<?php echo set_value( 'demprice' ); ?>" readonly>		
		<span class="text-danger"><?php echo form_error('demprice'); ?></span>
		
      </div>
       
      <div class="container-button">
        <button id="submit" name="submit_btn" class="btn btn-transparent btn-rounded btn-large">Proceed to pay</button>
      </div>
 
    </form>
    
    
  </div>

</div>
            </div>
             
        </div>
    </div>
</section>




 
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#signinform").validate({
                rules: {
                    stuname: {
                        required: true
                    },
                    stuemail: {
                        required: true,
                        email: true
                    },
                    stumob: {
                        required: true
                    },
                    catd_id: {
                        required: true
                    },
                    scatd_id: {
                        required: true
                    },
                    demprice: {
                        required: true
                    },
                },
                messages: {
                    stuname: {
                        required: "Enter name"
                    },
                    stuemail: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    stumob: {
                        required: "Enter mobile number"
                    },
                    catd_id: {
                        required: "Enter Category"
                    },
                    scatd_id: {
                        required: "Enter Subcategory"
                    },
                    demprice: {
                        required: "Enter Price"
                    }
                }
            }); //validate

            $.validator.addMethod("mypassword", function(value, element) {
                return this.optional(element) || (value.match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%&*])[a-zA-Z0-9!@#$%&*]+$/));
            }, 'Password must contain at least one capital letter, numeric, alphabetic and special character.');
			
	// Restricts input for each element in the set of matched elements to the given inputFilter.
	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		  if (inputFilter(this.value)) {
			this.oldValue = this.value;
			this.oldSelectionStart = this.selectionStart;
			this.oldSelectionEnd = this.selectionEnd;
		  } else if (this.hasOwnProperty("oldValue")) {
			this.value = this.oldValue;
			this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		  }
		});
	  };
	}(jQuery));			
			
		/*	
		$("#city").inputFilter(function(value) {			  
			return /^-?[a-zA-Z\s]*$/.test(value); 			
		});
		*/
		
		$('#city').change( function() {
			
			var city = $(this).val();
			//console.log( 'city:' + city );
			
			if( city == 'Other' )
			{
				$('#othercitydiv').show();
			}
			else
			{
				$('#othercitydiv').hide();
			}
			
		} );
		
		/*
        $("body").on("change", "#catd_id", function(){
        	var catd_id = $(this).val();
        	//console.log( 'catd_id:' + catd_id );
        	
            $.ajax({
            			url:"<?php echo base_url(); ?>ondemand/get_examname/",
            			type: "POST",
            			data:{catd_id: catd_id},
            			 success: function (res) {
            
            					//console.log( res );
            										
            					$("#scatddiv").html( res );
            					
            			 }
            });
        	
        });
        */
        
        $("body").on("change", "#scatdcode", function(){
        	var scatdcode = $(this).val();
        	//console.log( 'scatdcode:' + scatdcode );
        	
        	var examcode = $("#scatdcode option:selected").text();
        	//console.log( 'examcode:' + examcode );
        	$("#examcode").val( examcode );
        	
            $.ajax({
            			url:"<?php echo base_url(); ?>ondemand/get_examname/",
            			type: "POST",
            			data:{scatdcode: scatdcode},
            			 success: function (res) {
            
            					//console.log( res );
            										
            					$("body #scatddiv").html( res );
            					
            					$('body #scatd_id').trigger('change');
            					
            					$("body #scatd_id").chosen();
            					
            			 }
            });
        	
        });
        
        $("body").on("change", "#scatd_id", function(){
            
        	var scatd_id = $(this).val();
        	//console.log( 'scatd_id:' + scatd_id );
        	
        	var examname = $("#scatd_id option:selected").text();
        	//console.log( 'examname:' + examname );
        	$("#examname").val( examname );
        	
            $.ajax({
            			url:"<?php echo base_url(); ?>ondemand/get_subcatdprice/",
            			type: "POST",
            			data:{scatd_id: scatd_id},
            			 success: function (res) {
            
            					//console.log( res );
            										
            					$("#demprice").val( res );
            					
            			 }
            });
            
            /* show code according to exam name */
            
            $.ajax({
            			url:"<?php echo base_url(); ?>ondemand/get_examcode/",
            			type: "POST",
            			data:{scatd_id: scatd_id},
            			 success: function (res) {
            
            					//console.log( res );
            										
            					$("body #scatdcodediv").html( res );
            					
            					$("body #scatdcode").chosen();
            					
            			 }
            });
        	
        });
		
		$("#mobile").inputFilter(function(value) {			  
			return /^-?\d*$/.test(value); 			
		});
		


$(document).ready(function() {
    
    $("#scatdcode").chosen();
    $("body #scatd_id").chosen();
    
});

    </script> 