<style>
.error{
	color: red !important;
}
</style>
<div class="pg-header">

    

    <h1>Reset Password</h1>

    

</div>





<section>

    <div class="container">

        

       <div class="row">

           <div class="col-md-4"></div>

           <div class="col-md-4">

                <div class="forgot-box">

         <img src="https://www.sibinfotech.co.uk/projects/sap/front/images/icons/forgot.png" class="img-responsive"> 

      <h2>Reset Password?</h2>

      

      <p>Please type new password in the below fields. </p>

      

     



    <form action="<?=base_url()?>forgot/resetpasssub" class="pt-20" method="post" id="signinform"  >

	

		<span class="text-danger"></span>

	

      <div class="container-email">

       

        <input id="email" name="email" type="text" placeholder="Email" class="error" value="<?=$email?>" aria-invalid="true"> 
		
		<?php echo form_error('email'); ?>

		

      </div>   

	  
      <div class="container-email">

       

        <input id="password" name="password" type="password" placeholder="Password" class="error" aria-invalid="true"> 
		
		<?php echo form_error('password'); ?>

		

      </div> 

      <div class="container-email">

       

        <input id="confpassword" name="confpassword" type="password" placeholder="Confirm Password" class="error" aria-invalid="true"> 

		<?php echo form_error('confpassword'); ?>
		

      </div> 
    

       

      <div class="container-button">

        <button type="submit" id=" " name="submit_btn" class="btn btn-transparent btn-rounded btn-large">Submit</button>

      </div>

      

		<div>

			 

		   

		</div>

		

    </form>

    

 

    </div>

           </div>

       </div>

       

    </div>

</section>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#signinform").validate({
                rules: {
                    
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 4,
                        mypassword: true
                    },
                    confpassword: {
                        required: true,
                        minlength: 4,
						equalTo: "#password",
                        mypassword: true
						
                    }
                },
                messages: {
                    email: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    password: {
                        required: "Enter password"
                    },
                    confpassword: {
                        required: "Confirm password",
						equalTo: "Your password and confirm password do not match."
                    }
                }
            }); //validate

            $.validator.addMethod("mypassword", function(value, element) {
                return this.optional(element) || (value.match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%&*])[a-zA-Z0-9!@#$%&*]+$/));
            }, 'Password must contain at least one capital letter, numeric, alphabetic and special character.');
			
	// Restricts input for each element in the set of matched elements to the given inputFilter.
	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		  if (inputFilter(this.value)) {
			this.oldValue = this.value;
			this.oldSelectionStart = this.selectionStart;
			this.oldSelectionEnd = this.selectionEnd;
		  } else if (this.hasOwnProperty("oldValue")) {
			this.value = this.oldValue;
			this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		  }
		});
	  };
	}(jQuery));			
			
		/*	
		$("#city").inputFilter(function(value) {			  
			return /^-?[a-zA-Z\s]*$/.test(value); 			
		});
		*/
		
		
    </script>
