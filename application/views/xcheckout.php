<style>
    .btn-large { 
    display: table-caption;
    margin-top: 5px;
}

 .btn-large .fas{
     color:#fff;
     margin-left:15px;
 }

</style>


<div class="pg-header">
    
    <h1>Checkout</h1>
    
</div>

<section>
    <div class="container">
<?php
	define("PAYU_LOG_FILE", "application/logs/payulogs.log"); 

	$MERCHANT_KEY = $this->config->item('merchantkey');
	$SALT = $this->config->item('salt');
	// Merchant Key and Salt as provided by Payu.
	
	$PAYU_BASE_URL = $this->config->item('formactionpayu');
	
	//$action = base_url() .'packages/checkout'; 
	$action = 'https://secure.payu.in/_payment';
	// echo $action; exit;
	
	$hash_string = '';	
	
	$posted = array();
	if(!empty($_POST)) {
		//print_r($_POST);
	  foreach($_POST as $key => $value) {    
		$posted[$key] = $value; 
		
	  }
	}
	
	$formError = 0;
	
	if(empty($posted['txnid'])) {
	  // Generate random transaction id
	  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	} else {
	  $txnid = $posted['txnid'];
	}
	$hash = '';
	
	error_log(date('[Y-m-d H:i e] '). "Payu Checkout POST: ".print_r($_POST, true) . PHP_EOL, 3, PAYU_LOG_FILE);
	error_log(date('[Y-m-d H:i e] '). "Payu Checkout hashsequence: $hash_string" . PHP_EOL, 3, PAYU_LOG_FILE);
	
?>
<!-- <body onload="submitPayuForm()"></body>  -->
<?php

if( !empty( $students ) && $students[0]->email_veri == 'N' )
{
    echo '<div class="row not-verified-singlepackg-box"><div class="col-md-12" style="text-align: center;"><p>Please check your email to verify your account.</p><p> Resend Email?<a href="javascript:void(0)" id="resverem"> <span style="text-decoration:underline;">Click here<span></a>.</p></div></div><br>';
}

?>
	<!-- below for sandbox -->
	<!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top"> -->
	    
	<!-- below for live -->
	
	<form action="<?=$this->config->item('formaction')?>" method="post" target="_top" name="checkout" id="checkout">
        
        <div class="row">
            
            <div class="col-md-6 col-md-offset-3">
                
    <div class="table-resposive">
        
<?php


        $ordtotal = 0;
        $item_name = '';
        $item_name_dis = '';
        $item_number = '';
        // $item_test = 'new product';
		
		
        if ($cart = $this->cart->contents())
		{
			foreach ($cart as $item)
			{				
				$ordtotal = $ordtotal + $item['price'];
				$item_name .= $item['name'] . ',';
				$item_name_dis .= $item['name'] . '<br>';
			}
			
			$item_name = substr( trim( $item_name ), 0, -1 );
		}
		
		
		
		/*$gst = ( $ordtotal * 18 ) / 100;*/
		
		$gst = 0;
		$ordgstrate = '0';
		
		$gtot = $ordtotal + $gst;

		$payu_amount = (int) ($gtot * $currencydata);
		$payu_amount = sprintf("%.2f", ($gtot * $currencydata));
		// $payu_amount = sprintf("%.2f", $gtot);
?>
        
         <table class="table table-bordered checkout-table" >
                     
             <tr>
                 <th>Packages </th>
                 <td><?=@$item_name_dis?> </td>
             </tr> 
             <tr>
                 <th>Customer Name </th>
                 <td><?=@$students[0]->stuname?> </td>
             </tr>  
             <tr>
                 <th>Mobile </th>
                 <td><?=@$students[0]->stumob?></td>
             </tr> 
             <tr>
                 <th>Email </th>
                 <td><?=@$students[0]->stuemail?></td>
             </tr>
             
             <?php

		
		/*
		
		save order of student then pass order_id to payment gateway
		
		*/
		
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		$time365 = date('Y-m-d H:i:s', strtotime(' + 60 days'));
		
		if ($cart = $this->cart->contents())
		{
		    $user_ordid = $this->session->userdata('user_ordid');
		    
		    if( empty( $user_ordid ) )
		    {
		    
    			/* $ordtotal = 0; */
    			$fk_ordid = $this->GeneralModel->AddNewRow( "orders", array( 'fk_stuid' => $loggedid, 'ordtotal' => $gtot, 'ordgst' => $gst, 'ordgstrate' => $ordgstrate, 'ordstatus' => '', 'item_name' => $item_name, 'orddatec' => date('Y-m-d H:i:s') ) );
    			
    			foreach ($cart as $item)
    			{				
    				/* store user subjects */
    				$this->GeneralModel->AddNewRow( "order_detail", array( 'fk_ordid' => $fk_ordid, 'odetsid' => $item['id'], 'odetqty' => $item['qty'], 'odetprice' => $item['price'], 'odet_validity' => $time365 ) );
    				
    				/* $ordtotal = $ordtotal + $item['price']; */
    				
    			}
    			
    			
    			$this->session->set_userdata('user_ordid', $fk_ordid);
			
		    }
		    else
		    {
				$fk_ordid = $user_ordid;
		        /* 
		        if user goes back & adds new item to the cart, then handle. 
		        delete existsting order details, & create new one to include changes in order_detail
		        */
		        
		        $this->GeneralModel->DeleteRow( $table = 'order_detail', $key = array( 'fk_ordid' => $user_ordid ) );

    			foreach ($cart as $item)
    			{				
    				/* store user subjects */
    				$this->GeneralModel->AddNewRow( "order_detail", array( 'fk_ordid' => $user_ordid, 'odetsid' => $item['id'], 'odetqty' => $item['qty'], 'odetprice' => $item['price'], 'odet_validity' => $time365 ) );
    				
    				/* $ordtotal = $ordtotal + $item['price']; */
    				
    			}
    			
    			$this->GeneralModel->UpdateRow( "orders", array( 'item_name' => $item_name ), array( 'ordid' => $user_ordid ) );
		        
		    }
			
		}
		// My code
		// if(empty($fk_ordid))
		// 	$fk_ordid = $user_ordid;
		// My code ends
		/* $this->cart->destroy();	*/

		
		
		
?>
            <!--
              <tr>
                 <th>Order Total</th>
                 <td><?=@$ordtotal?> </td>
             </tr>  
             
             <tr>
                 <th>GST (18%) </th>
                 <td><?=@$gst?></td>
             </tr> 
             -->
             <tr>
                 <th>Grand Total </th>
                 <td id="grand_total" data-amount="<?=@$gtot?>" data-price="<?=@$currencydata?>"><strong>$ <?=@$gtot?></strong></td>
             </tr>
         </table>
     </div>
                
          
                
 
                
             </div>   
            
        </div>
        
<div class="row">
            
            <div class="col-md-6 col-md-offset-3">
                <center>
				
					<!--
					<a href="<?=base_url()?>packages/saveord" class="btn btn-transparent btn-rounded btn-large">Pay</a>
					-->
					
					<!-- below for sandbox -->
					<!-- <input type='hidden' name='business' value='sb-uhlpm1369755@business.example.com'> -->
					
					<!-- below for live -->
					<input type='hidden' name='business' value='<?=$this->config->item('bizemail')?>'> 	
						
						<input type='hidden'

						name='item_name' value='<?=$item_name?>'> <input type='hidden'

						name='item_number' value='<?=$fk_ordid?>'> <input type='hidden'

						name='amount' value='<?=@$gtot?>'> <input type='hidden'

						name='no_shipping' value='0'> <input type='hidden'

						name='currency_code' value='USD'> <input type='hidden'

						name='notify_url'

						value='<?=base_url()?>order/notify'>

					<input type='hidden' name='cancel_return'

						value='<?=base_url()?>order/cancel'>

					<input type='hidden' name='return'

						value='<?=base_url()?>order/return'>

					<input type="hidden" name="cmd" value="_xclick"> 

					
					
<?php

if( !empty( $students ) && $students[0]->email_veri == 'Y' )
{
    
?>
					<p class="text-center"><strong>Choose Payment Method</strong></p><br/>
					<input type='hidden' name='paymentthrough' value=''>
					
					<div class="row clearfix">

						<div class="col-xs-4 col-sm-4 text-center">
							<div class="payment-box">
								<img src="/front/images/icon-rp.png" class="img-responsive" />
								<input type='radio' name='paymentgateway' id='paymentgatewayrazorpay' value="razorpay"> 
								<label>Razorpay <br/>(₹<?php echo $payu_amount ?>)</label>
							</div>
						</div>

						<?php /* if($hash != '') { */?>
						<div class="col-xs-4 col-sm-4 text-center">
							<div class="payment-box">
								<img src="/front/images/icon-pu.png" class="img-responsive" />
								<input type='radio' name='paymentgateway' id='paymentgatewaypayu' value="payu" checked> 
								<label>
									PayU <br/>(₹<?php echo $payu_amount ?> )
								</label>
							</div>
						</div>
						<?php /* } */?>

						<!--<div class="row">-->
						<!--	<input type='radio' name='paymentgateway' id='paymentgatewaypaypal' value="paypal"> <label>Pay with PayPal</label>-->
						<!--</div>-->

						<div class="col-xs-4 col-sm-4 text-center">
							<div class="payment-box">
								<img src="/front/images/icon-sp.png" class="img-responsive" />
							<input type='radio' name='paymentgateway' id='paymentgatewaystripe' value="stripe"> 
							<label>Stripe</label>
							</div>
						</div>

					</div>

	                       

					<input

						type="button" name="pay_now" id="pay_now"

						Value="Pay Now" class="btn-transparent btn-rounded btn-large">

					<!-- <input

						type="submit" name="pay_now" id="pay_now"

						Value="Pay Now" class="btn-transparent btn-rounded btn-large"> -->
						
<?php

}
    
?>
						
						
             
                </center>
             
            </div>
            
            <div class="col-md-4">
             
            </div>
            
</div>

                
		</form>	
	<?php //$payu_amount = (int) ($gtot * $currencydata); ?>
	<div class="table-resposive">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="table-resposive">
					<form action="<?=$action?>" method="post" target="_top" name="payucheckout" id="payucheckout">
					<input type='hidden' name='paymentthrough' value=''>
						<input type="hidden" name="key" value="<?=$this->config->item('merchantkey')?>" />
						
						<input type="hidden" name="txnid" value="<?= $txnid ?>"/>
						<input type="hidden" name="service_provider" value="<?=$this->config->item('service_provider')?>" size="64" />
						<!-- <input type='hidden' name='amount' value="1.00"> -->
						<input type='hidden' name='amount' value="<?=@$payu_amount?>">
						<input type="hidden" name="firstname" value="<?=@$students[0]->stuname?>"/>
						<input type="hidden" name="email" value="<?=@$students[0]->stuemail?>"/>
						<input type="hidden" name="phone" value="<?=@$students[0]->stumob?>"/>
						<input type="hidden" name="productinfo" id="productinfo" value="<?=@$item_name_dis?>"/>
						<input type="hidden" name="surl" value="<?=$this->config->item('surl')?>"/>
						<input type="hidden" name="furl" value="<?=$this->config->item('furl')?>"/>
						<input type="hidden" name="curl" value="<?=$this->config->item('curl')?>"/>
						<input type="hidden" name="udf1" value="<?=$this->config->item('currencycode')?>"/>
						<input type="hidden" name="udf2" value="<?=@$students[0]->stuemail?>"/>
						<input type="hidden" name="udf3" value="<?=$this->config->item('udf3')?>"/>
						<input type="hidden" name="udf4" value="<?=$fk_ordid?>"/>
						<input type="hidden" name="udf5" value="<?=$this->config->item('udf5')?>"/>
						<input type="hidden" name="pg" value="<?=$this->config->item('pg')?>"/>

						<?php if( !empty( $students ) && $students[0]->email_veri == 'Y' ){ ?>


							<?php //if($hash != '') { ?>

								<!-- <div class="text-center"> 
									<input type="button" name="pay_now_payu" id="pay_now_payu" value="Pay through bank account" class="btn-transparent btn-rounded btn-large">
								</div> -->
								
							<?php //} ?>
												
						<?php } ?>
						
						<?php 						
						/*
						hashSequence = key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt.
                        $hash = hash("sha512", $hashSequence);
						*/
						
                        $hashSequence = $MERCHANT_KEY . '|' . $txnid . '|' . $payu_amount . '|' . $item_name_dis . '|'. $students[0]->stuname . '|' . @$students[0]->stuemail .'|'.$this->config->item('currencycode').'|' .$students[0]->stuemail.'|' .$this->config->item('udf4').'|'.$fk_ordid.'|'.$this->config->item('udf5').'|'.$this->config->item('pg').'|||||'. $SALT;
                        
                        //echo $hashSequence; exit;
                        
                        $hash = strtolower(hash('sha512', $hashSequence));
                        ?>
                        <input type="hidden" name="hash" value="<?= $hash ?>"/>

					</form>
					
					
					
					<?php 
					       $payulog = "paymentthrough=" . ",
					        key=".$this->config->item('merchantkey').",
					        hash=".$hash.",
					        txnid=".$txnid.",
					        service_provider=".$this->config->item('service_provider').",
					        amount=".$payu_amount.",
					        firstname=".$students[0]->stuname.",
					        email=".$students[0]->stuemail.",
					        phone=".$students[0]->stumob.",
					        surl=".$this->config->item('surl').",
					        furl=".$this->config->item('furl').",
					        curl=".$this->config->item('curl').",
					        udf1=".$this->config->item('currencycode').",
					        udf2=".$students[0]->stuemail.",
					        udf3=".$fk_ordid.",
					        udf4=".$this->config->item('udf3').",
					        udf5=".$this->config->item('udf5').",
					        pg=".$this->config->item('pg');
					        
					       error_log(date('[Y-m-d H:i e] '). "Payu Success POST: ".$payulog. PHP_EOL, 3, "application/logs/payuformdata.log");
					        ?>
					        
					
				</div>
			</div>
		</div>
	</div>				

    </div>
</section>
<script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
<script src="https://js.stripe.com/v3/"></script>
<link rel="stylesheet" href="style.css">
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<script>
    
	$("body").on("click", "#resverem", function(){
		
		$.ajax({
					url:"<?php echo base_url(); ?>register/veriemail/<?=$students[0]->stuid?>",
					type: "POST",
					data:{},
					success: function (res) {
		
				// 			console.log( res );
												
							alert('Email Sent. Please check your email.');
							
					}
		});
		
	});

	var hash = '<?php echo $hash ?>';
	$("#pay_now_payu").on("click", function(){ 
		var country = document.forms["checkout"].country.value;
		// var country = document.forms["stripecheckout"].country.value;
		
		if(country == 'india'){  
			document.forms["payucheckout"].action='<?=$action?>';
			document.forms["payucheckout"].paymentthrough.value="payu";
			document.forms["payucheckout"].submit();
		}else{
			alert('Please select Indian Currency to pay!!!');
		}

	});

	// document.addEventListener('DOMContentLoaded', function() {
	// 	submitPayuForm();
	// }, false);

	function submitPayuForm(){
// 		console.log('hash:'+hash);
		if(hash != '') {
			return;
		}
		var checkoutForm = document.forms.payucheckout;
		checkoutForm.submit();
	}


	$(document).ready(function() { 
	   // console.log('submit called');
		submitPayuForm();
	});

	// $("#country").on("change", function(){ 
	// 	var country = document.forms["checkout"].country.value;

	// 	if(country == 'india'){ 
	// 		var inr_amt = document.forms["payucheckout"].amount.value * '';
	// 		var inr_amt = 100;
	// 		document.forms["payucheckout"].amount.value = inr_amt; console.log('amount: '+document.forms["payucheckout"].amount.value);
	// 	}
	// });

	$("#pay_now").on("click", function(e){ 
		// alert("hello");
		if($("input[name='paymentgateway']").is(':checked')){
			var payment_method = $("input[name='paymentgateway']:checked").val();
			
			if(payment_method == 'paypal'){

				document.forms["payucheckout"].paymentthrough.value="paypal";
				document.forms["checkout"].submit();

			}else if(payment_method == 'payu'){

				document.forms["payucheckout"].paymentthrough.value="payu";
				document.forms["payucheckout"].submit();

			}
			else if(payment_method == 'stripe'){
			 //   console.log('<?php echo $item_name_dis ?>');
			 //   console.log('<?php echo $gtot ?>');
			 //console.log('<?php echo $gtot ?>');

				// document.forms["payucheckout"].paymentthrough.value="stripe";
				// document.forms["stripecheckout"].submit();
				// console.log("hello");
				 // var amount = <?php echo $gtot ?>;
				 
				 <?php $amount = $gtot*100 ?>
				 
				  <?php $stuname = $students[0]->stuname; ?>
				  //<?php $email = $students[0]->stuemail;?>
				
				var stripe = Stripe("pk_live_51I1mozDomQpbaMd8KZTEebzjj1FcPYSsn6BzAOvB9KLxDy0TAB6U9qC6SePM0cpVxqKTIb7bLmLktyGtqWMPm98v00e5pgL5np");
				fetch("/Checkoutsession/createsession/<?php echo $amount ?>/<?php echo $fk_ordid; ?>/<?php echo $stuname; ?>/<?php echo $loggedid; ?>", {
				method: "POST",
				})
				.then(function (response) {
				return response.json();
				})
				.then(function (session) {
				console.log(session);
				return stripe.redirectToCheckout({ sessionId: session.id });
				})
				.then(function (result) {
				// If redirectToCheckout fails due to a browser or network
				// error, you should display the localized error message to your
				// customer using error.message.
				if (result.error) {
					alert(result.error.message);
				}
				})
				.catch(function (error) {
				console.error("Error:", error);
				});

			}

			else if(payment_method == 'razorpay'){
			
					var amount = "<?php echo $payu_amount*100 ?>";
					var ordid = "<?php echo $fk_ordid ?>";
					var orddesc = "<?php echo $item_name_dis ?>";
					var SITEURL = "<?php echo base_url() ?>";
					
					var options = {
					"key": "rzp_live_Zog8cjNShBKrPf",
					"amount": amount, // 2000 paise = INR 20
					"name": "Sapprep.com",
					"description": orddesc,
					"image": "/front/images/logo.png",
					"handler": function (response){
					$.ajax({
					type: 'post',
					//dataType: 'json',
					data: "orderid="+ordid,
					url: SITEURL + '', //Order/razorPaySuccess
					success:function(result){
					    if(result != "") {
					          window.location.href = SITEURL + 'Order/RazorThankYou';
					    } else {
					        window.location.href = SITEURL + 'Order/RazorFailed';
					    }
                       }
                       
					});
					},
					 
					"theme": {
					"color": "#1A67C1"
					},
					"notes": {
					    "order_id": ordid,
					},
					
					"modal": {
                    "ondismiss": function(){
                     window.location.replace(SITEURL + 'Order/RazorFailed');
                        }
                    }
					};
					
					
					var rzp1 = new Razorpay(options);
					rzp1.open();
				
		}
	}

		else{
			alert('Please select a Payment Method to proceed!!!');
		}


	});
</script>

<script type="text/javascript">
	$('.payment-box input:radio').change(function(){
	    if($(this).is(":checked")) {
	    	$('div.payment-box').removeClass("payment-box-active");
	        $(this).parent().addClass("payment-box-active");
	    } else {
	        $('div.payment-box').removeClass("payment-box-active");
	    }
	});
</script>

<!-- ------------------------------------------------------------------------------------------------------------- -->


