<style>
.error{
	color: red !important;
}

.nav-tabs li.active{
	height: auto;
}

.tab-content{
	margin-top: 25px;
}
</style>


	<div class="pg-header">
		
		<h1>Blogs</h1>
		
	</div>

	<section>
		<div class="container">
			
<?php

	if( !empty( $blog ) )
	{	
		echo '<div class="row">';

		foreach( $blog as $key => $value )
		{
			echo '<div class="col-md-4">
					  <div class="blog-box">
				 	<!--<img class="img-responsive" src="'. base_url() .'uploads/blogs/'. $value->img_file .'" alt="'. $value->title .'">  -->
				 	
				 	    <!--
						<div style="background:url('. base_url() .'uploads/blogs/'. $value->img_file .');" class="blog-img"></div>
						-->
						
						<div class="blog-img1">
                            <h4 class="img-text">'. $value->img_txt .'</h4>
                            <p class="constant">www.examroadmap.com</p>
                        </div>
						
						<div class="blog-body">
						  <h4 class="blog-title"><a href="'. base_url() .'blog/'. $value->slug .'" class="read-btn">'. $value->title .'</a></h4>
						  <small class="text-muted cat">
							<i class="fa fa-clock-o"></i> '. date('d m Y h:i A', strtotime( $value->created_at ) ) .'      </small>
						  <p class="blog-smdesc">'. $value->small_edesc .'</p>
						  <a href="'. base_url() .'blog/'. $value->slug .'" class="read-btn">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
						</div>
					  </div>
					</div>';
		}
		
		echo '</div>';
	}

?>
					
		</div>
	</section>
	
	