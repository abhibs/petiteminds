
<div class="pg-header">
    
    <h1>packages</h1>
    
</div>


<section >

    <div class="container">

       

                 <div class="section-title text-center "> 
					 <h2 class="title">Packages</h2>
					 <span><i class="far fa-bookmark" aria-hidden="true"></i></span>
					 
				 </div>
				 
					<div class="row pt-20">
					    
				 <div class="col-md-10 col-md-offset-2 pb-20">
				       <form method="POST" action="">
					        <div class="row">
					            <div class="col-md-9">
        					        <input type="text" class="form-control" name="psearch" id="psearch" value="<?=@$_POST['psearch']?>" />
        					    </div>
        					    <div class="col-md-1">
        					        <button class="btn btn-transparent btn-rounded" type="submit">Search  </button>
        					    </div>
        					    <div class="col-md-2">
                                    <a href="<?=base_url()?>packages" class="btn btn-transparent btn-rounded">Show All <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
    					    </div>
					    </form>
				 </div>
					     
					
<?php

$loggedid = $this->session->userdata('loggedid');


function gen_string($string,$max=20)
{
    $tok=strtok($string,' ');
    $string='';
    while($tok!==false && strlen($string)<$max)
    {
        if (strlen($string)+strlen($tok)<=$max)
            $string.=$tok.' ';
        else
            break;
        $tok=strtok(' ');
    }
    return trim($string).'...';
}

if( !empty( $subcategory ) )
{	

	foreach( $subcategory as $key => $value )
	{
	    /*
	    $shortdesc = ( strlen( $value->scatdesc ) > 300 ) ? substr( $value->scatdesc, 0, 300 ) . '...' : $value->scatdesc;
	    */
	    
	    $shortdesc = strip_tags( $value->scatdesc );
	    
	    $shortdesc = gen_string( $shortdesc,$max=200 );
	    
		echo '<div class="col-md-4 ">
							<div class="course-box ">
								<!--<div class="course-box-img">
								<img src="'. base_url() . 'uploads/' . $value->icon .'" class="img-responsive">
								</div>-->
								
								<div class="course-box-img1">
							 <h4 class="img-text">'. $value->name .'</h4>
							 <p class="constant">www.examroadmap.com</p>
								</div>
								
								<div class="course-box-content">
									<h3 class="course-box-title">'. $value->name .'</h3>
									<p>
										'. $shortdesc .'
									</p> 
								</div>
								  <div class="see-more-box">
								  
								  <a href="'. base_url() .'packages/view/'. $value->id .'">
                                More Details
                            </a>
								  
<form action="'. base_url() .'packages/add" method="post" accept-charset="utf-8">

		<input type="hidden" name="id" value="'. $value->id .'">

		<input type="hidden" name="name" value="'. $value->name .'">

		<input type="hidden" name="price" value="'. $value->yearsubsc .'">
		
		<input type="hidden" name="icon" value="' . $value->icon .'">';
	 
            $exams = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'exams.sid' => $value->id ));

            if( !empty( $exams ) )
		    {
		        
    			$join_ar = array(
    								0 => array(
    									"table" => "orders",
    									"condition" => "order_detail.fk_ordid = orders.ordid",
    									"type" => "inner"
    								),
    								1 => array(
    									"table" => "exams",
    									"condition" => "order_detail.odetsid = exams.sid",
    									"type" => "inner"
    								)
    							);
    
    			$order_detail = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'orders.ordstatus' => 'Completed', 'exams.examid' => @$exams[0]->examid, 'exams.examtype' => 'PAID', 'order_detail.odet_validity >=' => date('Y-m-d') ), $search = '', $join_ar, $group_by = '' );
    			
    			if( ! empty( $order_detail ) )
    			{
    			    echo '<a href="'.base_url() . 'exams/index/' . @$exams[0]->examid . '/'. @$value->id .'" class=""><button type="button" class="btn white-btn">Try Full Exam</button></a>';
    			}
    	        else
    	        {
    		        echo '<button class="btn white-btn">Add to cart</button>';
    	        }
	        
		    }
		    else
		    {
		        echo '<button class="btn white-btn">Add to cart</button>';
		    }
		
echo '</form>
								  
										
									</div>
							</div>
						</div> ';
	}
}

?>
					
						
						
						
					</div>
				   
				   

               

    </div>

</section>

<!--
<section id="sign">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="signBox flexBox divideTwo">
                    
                    <div class="signBoxForm flexItem">
                       <div class="text-center">                            <h2>Login</h2>                        </div>
                        <?php if(isset($error)){?>
                       <div class="form-item form-type-textfield form-item-error">
                           <label class="error" ><?php echo $error?> </label>
                       </div>
                     <?php }?>
                        <form action="<?php echo base_url().'login';?>" method="post" id="loginform">
                            <div class="form-group">
                                <label for="">Email *</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group">
                                <label for="">Password *</label>
                                <input type="password" class="form-control" name="password" id="password">
                            </div>
                            <div class="text-center">
                                <button type="submit" name="sub" class="commonBtn">Login</button>
                            </div>
                        </form>
                        <div class="signNote">
                            <p>No account? Create one here <a href="<?php echo base_url();?>register">Register</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

-->


   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#loginform").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    password: {
                        required: "Enter password"
                    }
                }
            }); //validate
    </script> 