<section>
    <div class="container">
        
        <h2 class="text-blue text-center pb-20">100% Money Back Guarantee & Refund Policy</h2>
        <div class="term-row">
        <div class="text-center"><img src="<?=base_url()?>/assets/images/moneybackguarantee.jpg"></div>
        
       
<p>We promise you 100% Money Back Guarantee if you fail to clear the actual Certification exam. We are confident that you will not get a chance to use this option. PETITEMINDS provides high-quality products designed to develop a better understanding of real exams that candidates may face. We highly recommend that you try "Try for Free Exam " of every product that we provide "free of cost" so that you can know deeper of what you are buying. In order to further increase buyer's confidence and we so thoroughly believe that our products work, we provide 100% Money Back Guarantee - in case you prepare with our products and do not pass the exam. We will refund your full payment; no extra questions will be asked. 
</p>
</div>


<div class="term-row"><p><strong>Why PETITEMINDS Is Reliable?</strong></p>
<p>PETITEMINDS can be your trustworthy source for various International certifications, because we have the following advantages:</p>
<p>1. All the reference materials in PETITEMINDS are compiled by experienced IT professional and experts who are familiar with latest exam and testing centre for years. So, our products could cover most of the knowledge points and ensure good results for every customer.</p>
<p>2. PETITEMINDS continues to update the question pool in accord with real exams, which is to ensure the question coverage even up to 97%. Also, PETITEMINDS will inform the customers about the newest products in time so as to help you pass your exam easily. </p>
<p>3. Full refund guarantee! Our complete coverage of knowledge points will help most of the candidates pass the exams easily, but in case you fail on the very first trial, we guarantee a full refund on your purchase. Also, you could choose to accept more products from PETITEMINDS. Our only aim is to assist you to pass the exam.</p>
<p>4. Service first, customer first! PETITEMINDS will always accompany you during your preparation of the exams, so if any professional problems puzzle you, just contact our service staff any time. Good luck! PETITEMINDS wishes good results for every candidate on first attempt, but if you fail to pass it, you can always rely upon us.</p>
</div><div class="term-row"><p><strong>In What Situation Can I Claim the Refund? </strong></p>
<p>All PETITEMINDS users are entitled with full refund in 48 Hours if he/she fails the corresponding exam in 60 days after the purchase of our product. In order to save ourselves from scammers and continue this Money Back Guarantee for loyal customers, please notice: the money back guarantee is not applicable in the following situations:</p>
<p>1. One fails the exam within 24 hours of the purchase.</p>
<p>2. Corresponding exams only. We only ensure refunds for those who buy our product yet fail the corresponding exams in 60 days.</p>
<p>3. In case you have downloaded and practiced the product, but you have not attended the exam PETITEMINDS will not entertain any claims.</p>
<p>4. You must have scored 100% Six times during the entire practice with our full practice exam on www.Petiteminds.com and your access should be active on the date of your exam. This statement is just to make sure that premium users can have enough practice with our premium practice exam before they appear on the real Certification exam.</p>
<p>5. Orders out of date.</p>
<p>6. Different person. (Candidate's name is different from payer's name or Name Registered with us.)</p>
<p>7. We are not responsible for the exam version change from the board ; we don’t have any control over it.</p>
</div>

<div class="term-row"><p><strong>How to Claim Refund or Exchange? 
</strong></p>
<p>1.Government authorized User ID with Photo.</p>
<p>2. Send us Scanned copy of your Result/Score Report in photo/PDF format. For refunds you need to send all required information within 5 days after the test to us.</p>

<p>Be patient, we will deal with it in 48 hours after you submit. For those who do not want to exercise the right of claiming refund and want to exchange it for another exam QnA. PETITEMINDS would like to Offers One Free Exam Practice access as compensation.</o>
<p>If other cases that are not on the list appear, PETITEMINDS Reserves Final Interpretation Rights.
</p>

</div>
    
</div>    
</section>