<section>
    <div class="container">

        <h2 class="text-blue text-center pb-20">Terms and Conditions</h2>
        <div class="term-row">
            <p><strong>Acceptance of this Agreement </strong></p>
            <p>By clicking on the 'SIGNUP' option, the participant ("You" or "Your") agrees to the Terms and Conditions,
                obligations, representations, warranties, and agreements contained herein (the "Agreement"). In the
                event, You are not willing to accept the Agreement, You shall not be authorized or allowed to proceed
                further to view or use in any manner any content, information, courseware, products and services
                ("Services") published, available or provided on <a
                    href="https://www.petiteminds.com">www.Petiteminds.com</a> (the "Website"), which is owned,
                maintained and monitored by Petiteminds.com</p>
        </div>


        <div class="term-row">
            <p><strong>User ID and Password</strong></p>
            <p>By entering into this Agreement, you acknowledge and agree that Your user ID and password ("Participant
                Account") is for Your exclusive use only. Use or sharing of Your Participant Account with another user
                is not permitted and is cause for immediate blocking of Your access to the Website, the Services and the
                Content, the Courseware, and termination of this Agreement.</p>
            <p>You agree that You are solely responsible for maintaining the confidentiality of Your Participant Account
                and for all activities that occur under it. You agree to immediately notify our <a
                    href="https://simplilearn.secure.force.com/support/">Help and Support Team</a> if You become aware
                of or have reason to believe that there is any unauthorized use of Your Participant Account. You also
                agree to take all reasonable steps to stop such unauthorized use and to cooperate with Us in any
                investigation of such unauthorized uses. We shall not under any circumstances be held liable for any
                claims related to the use or misuse of Your Participant Account due to the activities of any third party
                outside of our control or due to Your failure to maintain the confidentiality and security of Your
                Participant Account.</p>
        </div>
        <div class="term-row">
            <p><strong>Content and Courseware </strong></p>
            <p>As a part of our Services offered through our Website, We shall grant you access to our content,
                courseware, practice tests, and other information, documents, and data which may be in audio, video,
                written, graphic, recorded, photographic, or any machine-readable format in relation to the specific
                certification training course You have registered for ("Content and Courseware").</p>
            <p>We reserve the right to amend, revise or update the Content and Courseware offered to You. In the event
                such an amendment, revision or updation occurs, we may require you pay an additional fee to access such
                amended, revised, or updated Content and Courseware.</p>
        </div>
        <div class="term-row">
            <p><strong>Usage of the Website and Services</strong></p>
            <p>We grant you a personal, restricted, non-transferable, non-exclusive, and revocable license to use the
                Website, the Services, and the Content and Courseware offered through the Website till the time the
                completion of the certification training course that You have enrolled for or the termination of this
                Agreement according to the Terms and Conditions set forth herein, whichever is earlier. The Services and
                the Content and Courseware are provided solely for Your personal and non-commercial use to assist you in
                completing the certification training course You have registered for ("Restricted Purpose").</p>
            <p>You are permitted online access to the Website, the Services, and the Content and Courseware and may
                download, save, or print the Content and Courseware solely for the Restricted Purpose.</p>
            <p>You are not permitted to reproduce, transmit, distribute, sub-license, broadcast, disseminate, or prepare
                derivative works of the Content and Courseware, or any part thereof, in any manner or through any
                communication channels or means, for any purpose other than the Restricted Purpose, without Our prior
                written consent.</p>

        </div>
        <div class="term-row">
            <p><strong>Intellectual Property Rights</strong></p>
            <p>While You are granted a limited and non-exclusive right to use the Website, the Services, and the Content
                and Courseware for the Restricted Purpose as set forth in this Agreement, You acknowledge and agree that
                We are the sole and exclusive owner of the Website, the Services and the Content and Courseware and as
                such are vested with all Intellectual Property Rights and other proprietary rights in the Website, the
                Services, and the Content and Courseware.</p>
            <p>You acknowledge and agree that this Agreement other than permitting You to use the Website, the Services,
                and the Content and Courseware for the Restricted Purpose does not convey to You in any manner or form
                any right, title or interest of a proprietary, or any other nature in the Website, the Services, and the
                Content and Courseware.</p>
        </div>
        <div class="term-row">
            <p><strong>Usage of Personal Information of Participants</strong></p>
            <p>We reserve the right to feature Your picture in any photos, videos, or other promotional material used by
                Us. Further, we may use Your personal information to inform You about other certification training
                courses offered by Us. However, we shall not distribute or share Your personal information with any
                third-party marketing database or disclose Your personal information to any third party except on a
                case-to-case basis after proper verification of such third party or if required under any applicable
                law.</p>
        </div>
        <div class="term-row">
            <p><strong>Limitation of Liability</strong></p>
            <p>You expressly agree that use of the Website, the Services, and the Content and Courseware are at Your
                sole risk. We do not warrant that the Website or the Services or access to the Content and Courseware
                will be uninterrupted or error free; nor is there any warranty as to the results that may be obtained
                from the use of the Website, the Services or the Content and Courseware or as to the accuracy or
                reliability of any information provided through the Website, the Services, or the Content and
                Courseware. In no event will We or any person or entity involved in creating, producing, or distributing
                the Website, the Services, or the Content and Courseware be liable for any direct, indirect, incidental,
                special, or consequential damages arising out of the use of or inability to use the Website, the
                Services, or the Content and Courseware.</p>
            <p>The disclaimer of liability contained in this clause applies to any and all damages or injury caused by
                any failure of performance, error, omission, interruption, deletion, defect, delay in operation or
                transmission, computer virus, communication line failure, theft or destruction or unauthorized access
                to, alteration of, or use of records or any other material, whether for breach of contract, negligence,
                or under any other cause of action.</p>
            <p>You hereby specifically acknowledge that We are not liable for any defamatory, offensive, wrongful, or
                illegal conduct of third parties, or other users of the Website, the Services or the Content and
                Courseware and that the risk of damage or injury from the foregoing rests entirely with each user.</p>
            <p>You agree that Our liability or the liability of Our affiliates, directors, officers, employees, agents,
                and licensors, if any, arising out of any kind of legal claim (whether in contract, tort or otherwise)
                in any way connected with the Services or the Content and Courseware shall not exceed the fee you paid
                to Us for the particular certification training course.</p>
        </div>
        <div class="term-row">
            <p><strong>Registration Information</strong></p>
            <p>For you to complete the sign-up process on our site, you must provide your full legal name, current
                address, a valid email address, member name and any other information needed in order to complete the
                sign-up process. You must confirm that you are 18 years or older and must be responsible for keeping
                your password secure and for all activities and contents that are uploaded under your account. You must
                not transmit any worms, viruses or code of a destructive nature. Any information provided by you or
                gathered by the site or third parties during any visit to the site shall be subject to the terms of This
                site&rsquo;s Privacy Policy.</p>
        </div>
        <div class="term-row">
            <p><strong>Payments and Processes of Invoice</strong></p>
            <p>The Petiteminds.com has the sole discretion to provide the terms of payment. Unless otherwise agreed,
                payment must be received by Petiteminds.com prior to the acceptance of an order. Unless credit term has
                been agreed upon, payment for the products shall be made by credit card,&nbsp;PayPal,&nbsp;Stripe,&nbsp;PayU,&nbsp;RazorPay or wire
                transfers. Invoices are due and payable within the time period noted on your invoice, measured from the
                date of the invoice. Petiteminds.com has discretion to cancel or deny orders. Petiteminds.com&nbsp;is
                not responsible for pricing, typographical, or other errors in any offer by Petiteminds.com and reserves
                the right to cancel any orders arising from such errors. Satisfactory payment must be received prior to
                goods being shipped.</p>
        </div>
        <div class="term-row">
            <p><strong>Refund Policy</strong></p>
            <p>The Petiteminds.com does not have to provide a refund if you have changed your mind about a particular
                purchase, so please choose carefully.</p>
        </div>
        <div class="term-row">
            <p><strong>Product Pricing &amp; Description</strong></p>
            <p>The List Price displayed for products on our website represents the full retail price listed on the
                product itself, suggested by the manufacturer or supplier, or estimated in accordance with standard
                industry practice; or the estimated retail value for a comparably featured item offered elsewhere. The
                List Price is a comparative price estimate and may or may not represent the prevailing price in every
                area on any particular day. For certain items that are offered as a set, the List Price may represent
                &ldquo;open-stock&rdquo; prices, which means the aggregate of the manufacturer&rsquo;s estimated or
                suggested retail price for each of the items included in the set. Where an item is offered for sale by
                one of our merchants, the List Price may be provided by the merchant. In cases of&nbsp;mispriced&nbsp;in
                our catalogs in which the item&rsquo;s correct price is higher than our stated price, we will, at our
                discretion, either contact you for instructions before shipping or cancel your order and notify you of
                such cancelation. We do not warrant that product descriptions or other content of this site is accurate,
                complete, reliable, current, or error-free. If a product offered in our website is not as described,
                your sole remedy is to return it in unused condition.</p>
        </div>
        <div class="term-row">
            <p><strong>Editing, Deleting and Modification</strong></p>
            <p>We may edit, delete or modify any of the terms and conditions contained in this Agreement, at any time
                and in our sole discretion, by posting a notice or a new agreement on our site. YOUR CONTINUED
                PARTICIPATION IN OUR PROGRAM, VISITING AND SHOPPING ON OUR SITE FOLLOWING POSTING OF A CHANGE NOTICE OR
                NEW AGREEMENT ON OUR SITE WILL CONSTITUTE BINDING ACCEPTANCE OF THE CHANGE.</p>
        </div>
        <div class="term-row">
            <p><strong>Acknowledgements of Rights</strong></p>
            <p>You hereby acknowledge that all rights, titles and interests, including but not limited to rights covered
                by the Intellectual Property Rights, belong to the site, and that you will not acquire any right, title,
                or interest in or to the Program except as expressly set forth in this Agreement. You will not modify,
                adapt, translate, prepare derivative works from,&nbsp;decompile, reverse engineer, disassemble or
                otherwise attempt to derive source code from any of our services, software, or documentation, or create
                or attempt to create a substitute or similar service or product through use of, or access to, the
                Program or proprietary information related thereto.</p>
        </div>
        <div class="term-row">
            <p><strong>Fraud</strong></p>
            <p>FRAUDULENT ACTIVITIES are highly monitored on our site and if fraud is detected
                Petiteminds.com&nbsp;shall resort to all remedies available to us and you shall be responsible for all
                costs and legal fees arising from these fraudulent activities.</p>
        </div>
        <div class="term-row">
            <p><strong>Confidentiality</strong></p>
            <p>You agree not to disclose information you obtain from us and or from our clients, advertisers and
                suppliers. All information submitted to by an end-user customer pursuant to a Program is proprietary
                information of Petiteminds.com&nbsp;Such customer information is confidential and may not be disclosed.
                Publisher agrees not to reproduce, disseminate, sell, distribute or commercially exploit any such
                proprietary information in any manner.</p>
        </div>
        <div class="term-row">
            <p><strong>Term</strong></p>
            <p>This Agreement will remain in full force and effect while you use the website. You may terminate your
                membership at any time for any reason by contacting us via email or the Contact Us page. We may
                terminate your membership for any reason at any time. Even after your membership is terminated, certain
                sections of this Agreement will remain in effect. We may delete the practice exam result history data
                after expiration of premium membership for premium users / at any time for registered users. The
                Petiteminds.com may go down for maintenance purpose or due to any technical issue with/without prior
                notice. As part of our content moderation system, we may modify or delete any content/part of content
                submitted by Petiteminds.com users.</p>
        </div>
        <div class="term-row">
            <p><strong>Non-Commercial use by members</strong></p>
            <p>Members on this website are prohibited to use the services of the website in connection with any
                commercial endeavors or ventures. This includes providing links to other websites, whether deemed
                competitive to this website or not.</p>
        </div>
        <div class="term-row">
            <p><strong>Links and Framings</strong></p>
            <p>Illegal and/or unauthorized uses of the Services, including unauthorized framing of or linking to the
                Sites will be investigated, and appropriate legal action may be taken. Some links, however, are welcome
                to the site and you are allowed to establish hyperlink to the appropriate part within the site provided
                that:</p>
            <ul>
                <li>You post your link only within the forum, chat or message board section.</li>
                <li>You do not remove or obscure any advertisements, copyright notices or other notices placed on the
                    site.</li>
                <li>The link does not state or imply any sponsorship or endorsement of your site.</li>
                <li>You immediately stop providing any links to the site on written notice from us. However, you must
                    check the copyright notice on the homepage to which you wish to link to make sure that one of our
                    content providers does not have its own policies regarding direct links to their content on our
                    sites.</li>
            </ul>
        </div>
        <div class="term-row">
            <p><strong>Non-Waiver</strong></p>
            <p>Failure of Petiteminds.com to insist upon strict performance of any of the terms, conditions and
                covenants hereof shall not be deemed a relinquishment or waiver of any rights or remedy that the we may
                have, nor shall it be construed as a waiver of any subsequent breach of the terms,</p>
            <p>conditions or covenants hereof, which terms, conditions and covenants shall continue to be in full force
                and effect.No waiver by either party of any breach of any provision hereof shall be deemed a waiver of
                any subsequent or prior breach of the same or any other provision.</p>
        </div>
        <div class="term-row">
            <p><strong>Miscellaneous</strong></p>
            <p>This Agreement shall be governed by and construed in accordance with the substantive laws of India,
                without any reference to conflict-of-laws principles.</p>
            <p>Any dispute, controversy or difference which may arise between the parties out of, in relation to or in
                connection with this Agreement is hereby irrevocably submitted to the exclusive jurisdiction of the
                courts of India, to the exclusion of any other courts without giving effect to its conflict of laws
                provisions or your actual state, province or country of residence. The entire agreement between the
                parties with respect to the subject matter hereof is embodied on this agreement and no other agreement
                relative hereto shall bind either party herein. Your rights of whatever nature cannot be assigned nor
                transferred to anybody, and any such attempt may result in termination of this Agreement, without
                liability to us. However, we may assign this Agreement to any person at any time without notice. In the
                event that any provision of these Terms and Conditions is found invalid or unenforceable pursuant to any
                judicial decree or decision, such provision shall be deemed to apply only to the maximum extent
                permitted by law, and the remainder of these Terms and Conditions shall remain valid and enforceable
                according to its terms. The Petiteminds.com practice exams are for self-evaluation purposes only and
                does not appear on the actual exams. Clearing the practice exam correctly is no guarantee that you will
                pass the actual exam. Site may provide links and affiliate information throughout, however, this is not
                an endorsement of a product. All opinions expressed on site are individual and separate from any
                compensation. Every effort is made to ensure the content integrity. Information used on this site is at
                your own risk. The content on this site may not be reproduced or redistributed without the express
                written permission of www.Petiteminds.com.User does not have authority nor is any right granted to
                license any use of the trademarks, service marks or logo&rsquo;s owned by Petiteminds.com or by any
                third party. All trademarks, service marks, trade names, trade dress, product names and logos appearing
                on the site are the property of their respective owners. Any rights not expressly granted herein are
                reserved.&nbsp;The site www.Petiteminds.com is in no way affiliated with SAP See-through the use of the
                site, Petiteminds.com, and accessing the site, you acknowledge that you have read these terms and agree
                to be bound by them.</p>


        </div>
        <strong>
            <p class="pt-20">I HAVE READ THE TERMS OF THIS AGREEMENT AND AGREE TO ALL OF THE PROVISIONS CONTAINED ABOVE.
            </p>
        </strong>

    </div>
</section>