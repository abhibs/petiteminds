<style>
.error{
	color: red !important;
}

.nav-tabs li.active{
	height: auto;
}

.tab-content{
	margin-top: 25px;
}

#testimon .row{
	margin-bottom: 10px;
}

</style>


<div class="pg-header">
    
    <h1>My Profile</h1>
    
</div>

<section>
    <div class="container">
 
 <div class="row">
     <div class="col-md-9">
         <ul class="nav nav-tabs myprofile">
    <li class="active"><a data-toggle="tab" href="#view">View</a></li>
    <li><a data-toggle="tab" href="#edit">Edit</a></li>
    <li><a data-toggle="tab" href="#myres">My Results</a></li>
    <li><a data-toggle="tab" href="#orders">Orders</a></li>
    <li><a data-toggle="tab" href="#testimon">Create Testimonial</a></li>
  </ul>

  <div class="tab-content">
    <div id="view" class="tab-pane fade in active">
     
     <div class="table-resposive">
         <table class="table-bordered" >
             <tr>
                 <th>Name </th>
                 <td><?=@$students[0]->stuname?> </td>
             </tr>  
             <tr>
                 <th>Mobile </th>
                 <td><?=@$students[0]->stumob?></td>
             </tr> 
             <tr>
                 <th>Email </th>
                 <td><?=@$students[0]->stuemail?></td>
             </tr>
             <tr>
                 <th>Date Registered </th>
                 <td><?=date('d-m-Y', strtotime( @$students[0]->studatec ) )?></td>
             </tr>
			 <tr>
                 <th>Member for </th>
                 <td><?php
				 
$date1 = $students[0]->studatec;
$date2 = date('Y-m-d');

$diff = abs(strtotime($date2) - strtotime($date1));

$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)) + 1;

$memfor = '';

if( $years > 1 )
{
	$memfor .= $years . ' years, ';
}
else if( $years == 1 )
{
	$memfor .= $years . ' year, ';
}

if( $months > 1 )
{
	$memfor .= $months . ' months, ';
}
else if( $months == 1 )
{
	$memfor .= $months . ' month, ';
}

if( $days > 1 )
{
	$memfor .= $days . ' days';
}
else if( $days == 1 )
{
	$memfor .= $days . ' day';
}

echo $memfor;

/* printf("%d years, %d months, %d days\n", $years, $months, $days); */

				 ?></td>
             </tr>
         </table>
     </div>
     
     
	       
    </div>
    <div id="edit" class="tab-pane fade">

	<form action="<?=base_url().'profile/editsave'?>" class="form-info" method="post" id="signinform">
	
		<div class="row">
			<div class="col-md-2">
				<label>Name</label>
			</div>
			<div class="col-md-3">
				<input id="stuname" name="stuname" type="text" value="<?=@$students[0]->stuname?>" class="form-control" placeholder="Name">	
			</div>
		 </div>	
		 
		 <div class="row">
			<div class="col-md-2">
				<label>Mobile</label>
			</div>
			<div class="col-md-3">
				<input id="stumob" name="stumob" type="text" value="<?=@$students[0]->stumob?>" class="form-control" placeholder="Mobile">
			</div>
		 </div>	
		 
		 <div class="row">
			<div class="col-md-2">
				<label>Email</label>
			</div>
			<div class="col-md-3">
				<input id="stuemail" name="stuemail" type="text" value="<?=@$students[0]->stuemail?>" class="form-control" placeholder="Email">
			</div>
		 </div>	
		 
		 <div class="row">
			<div class="col-md-2">
				<label>Password</label>
			</div>
			<div class="col-md-3">
				<input id="stupass" name="stupass" type="password" class="form-control" placeholder="Password">
			</div>
		 </div>	
		 
		 <div class="row">
			<div class="col-md-2">
				<label>Confirm Password</label>
			</div>
			<div class="col-md-3">
				
				
				<input id="cnfmpass" name="cnfmpass" type="password" class="form-control" placeholder="Confirm Password">
			</div>
		 </div>
		  
		   
		  <div class="container-button">
			<button type="submit" id="submit" name="submit_btn" class="btn btn-transparent btn-rounded btn-small">Save</button>
		  </div>
	 
	</form>	

    </div>
	
    <div id="myres" class="tab-pane fade">

<?php

	if( !empty( $exam_results ) )
	{	
		echo '<table class="table-bordered table-result" ><tr><th>Exam Title</th><th>Started</th><th>Finished</th><th>Score</th><th>Result</th></tr>';

		foreach( $exam_results as $key => $value )
		{
			$resstyle = ( $value->eres_status == 'PASS' ) ? 'style="color: green;"' : 'style="color: red;"' ;
			
			$extitle = $value->examtitle;
			
			if( !empty( $value->exam_type ) )
            	$extitle = $extitle . ' - ' . $value->exam_type;
			
			echo '<tr><td><a href="'. base_url() .'profile/examres/'. $value->eresid .'">'. $extitle .'</a></td><td>'. date('d-m-Y h:i A', strtotime( $value->eres_exstarted ) ) .'</td><td>'. date('d-m-Y  h:i A', strtotime( $value->eres_exfinished ) ) .'</td><td>'. $value->eres_score .' %</td><td class="'. strtolower( $value->eres_status ) .'_cls" '.$resstyle.'>'. $value->eres_status .'</td></tr>';
		}
		
		echo '</table>';
	}

?>
	
    </div>
	
    <div id="orders" class="tab-pane fade">

<table class="table table-bordered order-table">

<tr>
	<th>Date</th>
	<th>Order ID#</th>
	<th>Status</th>
	<th>Total</th>
</tr>

<?php

if( !empty( $orders ) )
{	
	foreach( $orders as $key => $value )
	{
	    if( !empty( $value->admin_added ) && $value->admin_added == 'N' )
        {
		    echo '<tr><td>'. date('d-m-Y', strtotime( $value->orddatec ) ) .'</td><td>'. $value->ordid .'</td><td>'. $value->ordstatus .'</td><td>'. $value->ordtotal .'</td></tr>';	
        }
	}
}

?>

</table>	

    </div>
	
    <div id="testimon" class="tab-pane fade">

		<form action="<?=base_url().'profile/addtestimon'?>" class="form-info" method="post" id="signinform">
		
			<div class="row">
				<div class="col-md-2">
					<label>Title</label>
				</div>
				<div class="col-md-3">
					<input id="tmtitle" name="tmtitle" type="text" value="" class="form-control" placeholder="" required>	
				</div>
			 </div>	
			 
			 <div class="row">
				<div class="col-md-2">
					<label>SAP Module</label>
				</div>
				<div class="col-md-3">
					
					<!--
					<select name="tmmodule" class="form-control" id="tmmodule" required>
						<option value="SAP Cloud Certification">SAP Cloud Certification</option>
						<option value="SAP Ariba Certification">SAP Ariba Certification</option>
						<option value="SAP ERP Certification">SAP ERP Certification</option>
						<option value="SAP Cloud Certification">SAP HANA Certification</option>
					</select>
					-->
					
<?php

					$opt = array( '' => 'Select' );
					
					if( !empty( $category ) )
					{	
						foreach( $category as $key => $value )
						{
							$opt[ $value->catname ] = $value->catname;
						}
					}
					
					echo form_dropdown('tmmodule', $opt, set_value( 'tmmodule' ), 'id="tmmodule" class="form-control" required');

?>
					
				</div>
			 </div>	
			 
			 <div class="row">
				<div class="col-md-2">
					<label>Body</label>
				</div>
				<div class="col-md-3">
					
					
					<textarea name="tmbody" id="tmbody" class="form-control" required></textarea>
					
				</div>
			 </div>	
			 
			 <div class="row">
				<div class="col-md-2">
					<label>Keywords</label>
				</div>
				<div class="col-md-3">
					<input id="tmkeyward" name="tmkeyward" type="text" class="form-control" placeholder="" required>
				</div>
			 </div>	
			   
			  <div class="container-button">
				<button type="submit" id="submit" name="submit_btn" class="btn btn-transparent btn-rounded btn-small">Save</button>
			  </div>
		 
		</form>	

    </div>
	
  </div>

     </div> 
     <div class="col-md-3">
          <?php include 'include_front/sidebar.php' ?>
     </div>
 </div>       


				
    </div>
</section>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#signinform").validate({
                rules: {
                    stuname: {
                        required: true
                    },
                    stuemail: {
                        required: true,
                        email: true
                    },
                    stumob: {
                        required: true
                    },
                    stupass: {
                        required: false,
                        minlength: 4,
                        mypassword: true
                    },
                    cnfmpass: {
                        required: false,
                        minlength: 4,
						equalTo: "#stupass",
                        mypassword: true
						
                    }
                },
                messages: {
                    stuname: {
                        required: "Enter name"
                    },
                    stuemail: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    stumob: {
                        required: "Enter mobile number"
                    },
                    stupass: {
                        required: "Enter password"
                    },
                    cnfmpass: {
                        required: "Confirm password",
						equalTo: "Your password and confirm password do not match."
                    }
                }
            }); //validate

            $.validator.addMethod("mypassword", function(value, element) {
                return this.optional(element) || (value.match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%&*])[a-zA-Z0-9!@#$%&*]+$/));
            }, 'Password must contain at least one capital letter, numeric, alphabetic and special character.');
			
	// Restricts input for each element in the set of matched elements to the given inputFilter.
	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		  if (inputFilter(this.value)) {
			this.oldValue = this.value;
			this.oldSelectionStart = this.selectionStart;
			this.oldSelectionEnd = this.selectionEnd;
		  } else if (this.hasOwnProperty("oldValue")) {
			this.value = this.oldValue;
			this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		  }
		});
	  };
	}(jQuery));			
			
		/*	
		$("#city").inputFilter(function(value) {			  
			return /^-?[a-zA-Z\s]*$/.test(value); 			
		});
		*/
		
		$('#city').change( function() {
			
			var city = $(this).val();
			console.log( 'city:' + city );
			
			if( city == 'Other' )
			{
				$('#othercitydiv').show();
			}
			else
			{
				$('#othercitydiv').hide();
			}
			
		} );
		
		$("#mobile").inputFilter(function(value) {			  
			return /^-?\d*$/.test(value); 			
		});

    </script> 