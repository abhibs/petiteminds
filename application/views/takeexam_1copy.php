<style>
.error{
	color: red !important;
}

.web_btn{
	margin-right: 5px !important;
}

.queview{
	/*background-color: #eee;*/
	margin-top:30px;
	text-align:center;
}


.ansgiven{
	background-color: green;
}

.ansblank{
	background-color: red;
    color: white;
}

</style>


<div class="pg-header">
    
    <h1><?=$exams[0]->examtitle?></h1>
    
</div>

<section class="take-exam">
    <div class="container">
        
			<div class="row">
                    <div class="col-md-12">					
				<form role="form" method="post" action="<?=base_url()?>exams/submit" id="examfrm" name="examfrm">
										<?php					
										
if( !empty( $questions ) )
{	
	$backnav = $exams[0]->exambacknav;
	
	$showback = false;
	$shownext = true;
	$hidepanel = '';
	$srno = 0;	
	$totque = count( $questions );
	
	foreach( $questions as $quekey => $queval )
	{
		$srno++;

		if( $quekey )
		{
			$showback = true;
			$shownext = ( ( count($questions) - 1 ) == $quekey ) ? false : true;
			$hidepanel = 'style="display: none;"';
		}
		
?>

						<div class="panel panel-default quepanel" <?=$hidepanel?> id="quepanel_<?=$queval->queid?>">
                            <div class="panel-heading">							<div class="row">                <div class="col-md-9">
                                <p class=""> <?=htmlentities($queval->question)?></p>
                                <?php 
                /*
                if( $queval->quetype == 'MANS' )
                {
                    echo '<p class="subtitle" style="font-size: 12px;">Please choose '. $queval->correct_cnt .' correct answers.</p>';
                }
                else
                {
                    echo '<p class="subtitle" style="font-size: 12px;">Please choose the correct answer.</p>';
                }
                */
                ?>
                                
		</div>				
		<div class="col-md-3">    			
			<p style="">Question <?=$srno?> of <?=$totque?></p><p></p>						
			<p class="quiztimer">Time left: 00:00:00</p>   
					
		</div>		
		
		</div>                            </div>
                            <div class="panel-body" style="">
                                                                    
								
                                        <div class="form-group">
                                        
                <?php    
                      
                      /*                  
	            if( !empty( $queval->queimg ) )
	                echo '<img src="'. base_url() .'uploads/'. $queval->queimg .'" style="width: 300px;" /><br>';
	                */
                                          
                if( $queval->quetype == 'MANS' )
                {					
                    if( !empty( $queval->opt1 ) )
                        echo '<br><input name="opt1_'. $queval->queid .'" type="checkbox" value="Y" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt1 ) ;
                    if( !empty( $queval->opt2 ) )
                        echo '<br><input name="opt2_'. $queval->queid .'" type="checkbox" value="Y" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt2 ) ;
                    if( !empty( $queval->opt3 ) )
                        echo '<br><input name="opt3_'. $queval->queid .'" type="checkbox" value="Y" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt3 ) ;
                    if( !empty( $queval->opt4 ) )
                        echo '<br><input name="opt4_'. $queval->queid .'" type="checkbox" value="Y" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt4 ) ; 
                    if( !empty( $queval->opt5 ) )
                        echo '<br><input name="opt5_'. $queval->queid .'" type="checkbox" value="Y" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt5 ) ;    
                }
                else
                {										
			
					if( !empty( $queval->opt1 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt1_iscorr" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt1 ) ;                    
					if( !empty( $queval->opt2 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt2_iscorr" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt2 ) ;                    
					if( !empty( $queval->opt3 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt3_iscorr" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt3 ) ;                    
					if( !empty( $queval->opt4 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt4_iscorr" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt4 ) ;                     
					if( !empty( $queval->opt5 ) )                        
						echo '<br><input name="opt_'. $queval->queid .'" type="radio" value="opt5_iscorr" class="queopt" data-queid="'. $queval->queid .'"> ' . htmlentities( $queval->opt5 ) ;
                    
                }
                                    
                ?>
                                                                                    </div>
                                        
										<div class="text-center">
										
										<?php
										
										if( $showback && $backnav == 'Y' )
										{
											echo '<button type="button" class="btn btn-transparent btn-rounded btn-large backbtn">Back</button>';
										}
										
										if( $shownext )
										{
											echo '<button type="button" class="btn btn-transparent btn-rounded btn-large nxtbtn" id="'.$queval->queid.'">Next</button>';
										}
										
										if( ( count($questions) - 1 ) == $quekey )
										{
											echo '<button type="button" class="btn btn-transparent btn-rounded btn-large finbtn">Finish</button>';
										}
										
										
										?>
										
                                         <input type="hidden" name="queids[]" value="<?=$queval->queid?>" />   
											
                                        </div>
										
										
										
                                    
                            </div>
                           
                        </div>

<?php
		
	}
}										?>		

				<div class="row">	
						
						
						
<?php

						if( !empty( $questions ) )
						{	
							echo '<div class="queview" style="display: none;"><h2>Question Overview</h2>';
					
							$srno = 0;
							
							foreach( $questions as $quekey => $queval )
							{
								$srno++;
								echo '<div class="qbox ansblank" id="qbox_'. $queval->queid .'" data-queid="'. $queval->queid .'">'. $srno .'</div>';
							}
							
							echo '</div>';
							
						}

?>					
					</div>
							
						

					<span id="realtime" style="display:none;"></span>
					<input type="hidden" name="checksubmit" id="checksubmit" value="0" />
					<input type="hidden" name="eres_exstarted" value="<?=date('Y-m-d H:i:s')?>" />
					<input type="hidden" name="exam_type" value="<?=$exam_type?>" />
					<input type="hidden" name="examid" value="<?=$exams[0]->examid?>" />
					
				</form>
					
                        
                    </div>
            </div>
		
    </div>
</section>



   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#loginform").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    password: {
                        required: "Enter password"
                    }
                }
            }); //validate
    </script>
	
<script>	
	
		$(document).ready( function() {
			
			/* next prev button */
			
			$("body").on("click", ".nxtbtn", function(){
				
				$('.quepanel').hide();
				$(this).closest('.quepanel').next('.quepanel').show();
				
			});
			
			$("body").on("click", ".backbtn", function(){
				
				$('.quepanel').hide();
				$(this).closest('.quepanel').prev('.quepanel').show();
				
			});
			
			$("body").on("click", ".finbtn", function(){
				
				var choice = confirm('Do you want to validate questions before submitting the test?');
				
				console.log( 'choice:' + choice );
				
				if( choice )
				{
					$('.queview').show();
				}
				else{
					
					$('#checksubmit').val(1);
					$('#examfrm').submit();
				
				}
				
			});
			
			$("body").on("click", ".valbtn", function(){
				
				$('#checksubmit').val(1);
				$('#examfrm').submit();
				
			});			
			
			$("body").on("click", ".qbox", function(){
				
				var qboxid = $(this).data('queid');
				console.log( 'qboxid:' + qboxid );
				
				$('.quepanel').hide();
				$('#quepanel_' + qboxid).show();
				
			});
			
			$("body").on("click", ".queopt", function(){
				
				/* update overview box */
				
				nxtbtnid = $(this).data('queid');
				console.log( 'nxtbtnid:' + nxtbtnid );
				
				queoptchk = $(this).closest('.quepanel').find('.queopt:checked').length;
				console.log( 'queoptchk:' + queoptchk );
				
				if( queoptchk )
				{
					$('#qbox_' + nxtbtnid).removeClass('ansblank');
					$('#qbox_' + nxtbtnid).addClass('ansgiven');
				}
				else
				{
					$('#qbox_' + nxtbtnid).removeClass('ansgiven');
					$('#qbox_' + nxtbtnid).addClass('ansblank');
				}
				
			});
			
			/* countdown functionality */
			
			function coutdown(){

				<?PHP
					if($exams[0]->examtimelimit>0){
					
					/* if($quiz->exam_duration_slug == 'hour')
					{
						$duration = 3600000;
					}
					else
					{ */
						$duration = 60000;
					/* } */
					?>

				var mulval = '<?=$duration?>';
				var currentdate = new Date();

				/* var rquiz = parseInt( $('#rquiz').val() );
				console.log( 'rquiz asdf: ' + rquiz );
				if( rquiz )
				{
					<?php
					
					$tarr = explode(":", $time_left);
					$hour = @$tarr[0] * 3600000;	
					$min = @$tarr[1] * 60000;	
					$sec = @$tarr[2] * 1000;

					$actualtl = $hour + $min + $sec;	
					echo "console.log('actualtl: ' + $actualtl);";	
					?>
					
					var countDownDate = new Date(currentdate.getTime() + <?=$actualtl?>);
				}
				else
				{ */
				
				
				<?php
				
				$examtimelimit = @$exams[0]->examtimelimit;
				
				if( @$exam_type == 'MINI' ){

		            if( !empty( @$exams[0]->examtimelimit) )
		                $examtimelimit = round( @$exams[0]->examtimelimit / 2 );
		            else 
		                $examtimelimit = '0';

				}
				?>
				
					var countDownDate = new Date(currentdate.getTime() + <?=$examtimelimit?>*mulval);
				/* } */


				console.log( 'countDownDate:' + countDownDate );

				var x = setInterval(function() {
				  var now = new Date().getTime();
				  var distance = countDownDate - now;
				  
					// console.log( 'distance:' + distance );

				  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				  
					//console.log( 'days:' + days );
				  
				  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				  
				  $(".quiztimer").html('Time left: ' + hours + " : "+ minutes + " : " + seconds + "");
				  
				  $("#time_left").val( hours+':'+minutes+':'+seconds);
				  
				  if( $("#qttime").html() == '' )
				  {
					seconds += 2;
					$("#qttime").html(hours + " : "+ minutes+ " : "+seconds);
				  }

				  $("#realtime").html(hours + " : "+ minutes+ " : "+seconds);
				  
				  if(hours==0 && minutes==0 && seconds==0&&$("#checksubmit").val()==0){
					  
						alert('Your Exam Time Exceeded');
						$('#examfrm').submit();
						clearInterval(x);
						
				  }
				  if (distance < 0) {
					  
						clearInterval(x);
					
				  }
				  if($("#checksubmit").val()==1)
				  {
					 $(".quiztimer").html('');
				  }
				}, 1000);
				<?PHP
					}
					?>
					
			}
			
			coutdown();
			
		} );
	
	</script> 