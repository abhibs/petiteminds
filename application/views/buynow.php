<style>
    .btn-large { 
    display: table-caption;
    margin-top: 5px;
}

 .btn-large .fas{
     color:#fff;
     margin-left:15px;
 }

</style>


<div class="pg-header">
    
    <h1><?php
    
                $numplus = '';
    
				if( strtolower( @$subcategory[0]->catname ) != 'more' )
					echo @$subcategory[0]->catname;
				
				?></h1>
    
</div>


<section>
    <div class="container">
        
<?php

if( !empty( $students ) && $students[0]->email_veri == 'N' )
{
/*	
    echo '<div class="row" style="background-color: crimson;color: white;
"><div class="col-md-12" style="text-align: center;">Please check your email to verify your account. Resend <a href="javascript:void(0)" id="resverem" style="color: white;">email</a>.</div></div><br>';
*/

    echo '<div class="row not-verified-singlepackg-box"><div class="col-md-12" style="text-align: center;"><p>Please check your email to verify your account.</p><p> Resend Email?<a href="javascript:void(0)" id="resverem"> <span style="text-decoration:underline;">Click here<span></a>.</p></div></div><br>';

}

?>
        
        <div class="row">
            <div class="col-md-3">
                
                <!--
                <div class="pack-img">
                <img src="<?=base_url() . 'uploads/' . @$subcategory[0]->icon?>" class="img-responsive"> 
                </div>
                -->
                
                <div class="pack-img1">
                    <h4 class="img-text"><?=@$subcategory[0]->examtitle?></h4>
                    <p class="constant">www.examroadmap.com</p>
                </div>
                
                <p class="sku"><span>Exam Code:</span> <?=@$subcategory[0]->scat_sku?></p>
            </div>
            <div class="col-md-7">
                <h3 class="course-name">
                    <?=@$subcategory[0]->examtitle?>
                    
                    </h3>
                <div class="pack-desc">
				
                    <p>
                        <?=@$subcategory[0]->examdesc?>
					</p>
 
                </div>
            </div> 
            <div class="col-md-2">
			
<?php
if( !empty( $subcategory[0]->examid ) )
{
    
            $numplus = $this->GeneralModel->CountRecords('questions', $key = array( 'questions.fk_examid' => $subcategory[0]->examid ), $search = '');

            $numplus = $numplus . '+';
    
?>
			
	<form action="<?=base_url()?>packages/add" method="post" accept-charset="utf-8">

			<input type="hidden" name="id" value="<?=$subcategory[0]->id?>">

			<input type="hidden" name="name" value="<?=$subcategory[0]->name?>">

			<input type="hidden" name="price" value="<?=$subcategory[0]->yearsubsc?>">
			
			<input type="hidden" name="icon" value="<?=$subcategory[0]->icon?>">
		 
			<button class="btn btn-transparent btn-rounded btn-large  btn-block"><h5>Add to cart <i class="fas fa-arrow-right"></h5></i></button>
			
	</form>

	<div class="price">
		<h5>$ <?=@$subcategory[0]->yearsubsc?></h5>
	</div>
	
	<div class="price">
		<h5>₹ <?=@$inr_value?></h5>
	</div>
				
<?php
}
?>
                
            </div>    
            
        </div>
    </div>
</section>

<section id="feature">
    <div class="container">
      
		
		<div class="row pt-20">
		    <div class="col-md-8">
		        
		         <div class="section-title"> 
					 <h2 class="title">Packages Features</h2>
					 
				 </div>
		        <div class="feature-box">
		            <ul class="feature-list">
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong><?=@$numplus?> Scenario Based Practice Exam Questions and Answers</strong> similar to actual <?=@$subcategory[0]->catname?> Exam questions.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>2 Months Unlimited Access</strong> to Online <?=@$subcategory[0]->examtitle?> exam.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Guaranteed to have REAL Multiple Choice Questions with Correct Answers for assessment.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>Performance Check Meter</strong> during practice exam similar to <strong>Actual SAP SE exam.</strong></li>
 
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>100% Accurate</strong> and Verified Answers</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Online practice exam to be completed in <strong>Specified Time Duration.</strong></li>

<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Results <strong>Data Stored</strong> with attended questions and answers.</li>
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Price is just <strong>$49 USD</strong> lowest compared to others with Quality Q &amp; A.</li>-->

<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Price is just <strong>$<?=@$subcategory[0]->yearsubsc?> USD</strong>, lowest compared to other online or offline materials.</li>


<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>Safe &amp; Secure Payment</strong> with the <strong>PayPal</strong> payment gateway.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Payments accepted through <strong>Credit Card, Debit Card and PayPal</strong>.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>We invite you to visit our Live User <strong>Testimonials</strong> and <strong>Blogs</strong>.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>We donate</strong> 10% of the profit to <strong>charity</strong>.</li>


<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong>Safe & Secure Payment</strong> with the <strong>Paypal</strong> payment gateway.</li>
<li><i class="fas fa-arrow-right" aria-hidden="true"></i>Payments accepted through <strong>Credit Card, Debit Card, and PayPal</strong>.</li>
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i><strong> 100% Money Back Guarantee </strong> if you can't clear your actual exam.</li>-->
<!--<li><i class="fas fa-arrow-right" aria-hidden="true"></i>We invite you to visit our <strong> Live User Testimonials </strong> and <strong> Facebook Community </strong> to build trust.</li>-->
</ul>
		        </div>
		    </div>
		    
		    <div class="col-md-4 feature-right">
		        <p><?=@$subcategory[0]->examtitle?> - Full</p>
		        <ul class="qt-list">
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Question: <?=@$subcategory[0]->qcnt_display?> </li>
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Time limit: <?=@$subcategory[0]->examtimelimit?> minutes</li>
		        </ul>
		        
		        <p><?=@$subcategory[0]->examtitle?> - Mini</p>
		        <ul class="qt-list">
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Question: <?php 
		            if( !empty( @$subcategory[0]->qcnt_display ) )
		                echo floor( intval( @$subcategory[0]->qcnt_display ) / 2 );
		            else 
		                echo '0';
		            ?>  </li>
		            <li><i class="fas fa-arrow-right" aria-hidden="true"></i>Time limit: <?php 
		            if( !empty( @$subcategory[0]->examtimelimit ) )
		                echo round( @$subcategory[0]->examtimelimit / 2 );
		            else 
		                echo '0';
		            ?> minutes</li>
		        </ul>
		   </div>      
		    
		</div>
		
    </div>
</section>

<script>
    
$("body").on("click", "#resverem", function(){
    
    $.ajax({
    			url:"<?php echo base_url(); ?>register/veriemail/<?=$students[0]->stuid?>",
    			type: "POST",
    			data:{},
    			 success: function (res) {
    
    					console.log( res );
    										
    					alert('Email Sent. Please check your email.');
    					
    			 }
    });
    
});
    
</script>
