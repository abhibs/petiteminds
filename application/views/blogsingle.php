<style>
.error{
	color: red !important;
}

.nav-tabs li.active{
	height: auto;
}

.tab-content{
	margin-top: 25px;
}
</style>


	<div class="pg-header">
		
		<h1><?=@$blog[0]->title?></h1>
		
	</div>

	<section>
		<div class="container">
			
<div class="row single-blog">
            <div class="col-md-9">
                <div class="blog-box">
                    
                    <!--
                    <div class="blog-img img-responsive" style="background:url(<?=base_url()?>uploads/blogs/<?=@$blog[0]->img_file?>);">
                    </div>
                    -->
                    
                    <div class="blog-img1" style="width: initial;">
                            <h4 class="img-text"><?=@$blog[0]->img_txt?></h4>
                            <p class="constant">www.examroadmap.com</p>
                        </div>
                    
                    <p class="text-muted cat"><i class="fa fa-clock-o"></i> <?=date('d m Y h:i A', strtotime( @$blog[0]->created_at ) )?></p>
                    
                    <div class="blog-body">
						  <h4 class="blog-title"><?=@$blog[0]->title?></h4>
						  
                    <div class="blog-desc">
					
						<?=@$blog[0]->edesc?>

					</div>
					
					</div>
                </div>
            </div>
            <div class="col-md-3">
          <?php include 'include_front/sidebar.php' ?>
     </div>
            
        </div>
					
		</div>
	</section>
	