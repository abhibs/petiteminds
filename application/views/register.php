<style>
.error{
	color: red !important;
}
</style>

<script>
    function changecountry() {
        var value = $("#country").val();
        showerrors(value);
    }
    
    function showerrors(value) { 
    	var errArray = value.split("||");
    	$("#countrycode").val(errArray[0]);
    	
    }
</script>

<div class="pg-header">
    
    <h1>Register</h1>
    
</div>

<section id="login" style="padding-bottom: 500px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                 <div id="login-contn" class="form-box mt-20" >
                     <img src="<?=base_url()?>front/images/login.png" class="img-responsive">
                    <h2 class="title">Already have an account?</h2>
                    <p>Login now </p>
						
                        <div class="pt-20">
							<a href="<?=base_url()?>login" class="btn btn-transparent btn-rounded btn-large">Login</a>
                        </div>
                        
                        
            
                </div>
             </div>
            <div class="col-md-6">
                <div class="form-container">

  <div class="form-bg mt-20">
    <div class="container-btn">
       
    </div>
  </div>
<!-- FORM INFO -->
  <div class="form-info-container">

    <form action="" class="form-info" method="post" id="signinform">
      <div class="container-email">
        <label for="email">Name</label>
        <input id="stuname" name="stuname" type="text" placeholder="Name" value="<?php echo set_value( 'stuname' ); ?>"> 		
		<span class="text-danger"><?php echo form_error('stuname'); ?></span>
		
      </div> 
        <div class="container-email">
          <label for="email">Country</label>
          <?php /* $countries = get_Country(); ?>
          <?php echo form_dropdown('country',$countries, 0,'required'); */ ?>
          
            <select class="form-control" name="country" id="country" onchange="changecountry()">
                <option value="">Select Country</option>
                <?php foreach($records as $row) { ?>
                     <option value="<?php echo "+". $row->country_code."||".$row->countryname?>"><?=$row->countryname?></option>
                <?php } ?>
            </select>
            <span class="text-danger"><?php echo form_error('country'); ?></span>
        </div> 
      
	  <div class="container-email">
        <label for="email">Mobile</label>
           <span class="text-danger"><?php echo form_error('countrycode'); ?></span>
           
           <div class="input-group mobile-input-group">
                <span class="input-group-addon mobile-input-group">
                    <input name="countrycode" type="text" id="countrycode" class="" placeholder="Code" readonly/>
                </span>
                <input id="stumob" class="mobile-input" name="stumob" type="number" placeholder="Mobile" value="<?php echo set_value( 'stumob' ); ?>"> 
            </div>
		<span class="text-danger"><?php echo form_error('stumob'); ?></span>
      </div> 
      <div class="container-email">
        <label for="email">Email</label>
        <input id="stuemail" name="stuemail" type="text" placeholder="Email" value="<?php echo set_value( 'stuemail' ); ?>">
		<span class="text-danger"><?php echo form_error('stuemail'); ?></span>
      </div>   
      <div class="container-password">
        <label for="password">Password</label>
        <input id="stupass" name="stupass" type="password" placeholder="Password">
		<span class="text-danger"><?php echo form_error('stupass'); ?></span>
      </div>
      <div class="container-password">
        <label for="password">Confirm Password</label>
        <input id="cnfmpass" name="cnfmpass" type="password" placeholder="Confirm Password">
		<span class="text-danger"><?php echo form_error('cnfmpass'); ?></span>
      </div>
       
      <div class="container-button">
        <button id="submit" name="submit_btn" class="btn btn-transparent btn-rounded btn-large">Register</button>
      </div>
 
    </form>
    
    
  </div>

</div>
            </div>
             
        </div>
    </div>
</section>




 
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#signinform").validate({
                rules: {
                    stuname: {
                        required: true
                    },
                    stuemail: {
                        required: true,
                        email: true
                    },
                    stumob: {
                        required: true
                    },
                    stupass: {
                        required: true,
                        minlength: 4,
                        mypassword: true
                    },
                    cnfmpass: {
                        required: true,
                        minlength: 4,
						equalTo: "#stupass",
                        mypassword: true
						
                    }
                },
                messages: {
                    stuname: {
                        required: "Enter name"
                    },
                    stuemail: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    stumob: {
                        required: "Enter mobile number"
                    },
                    stupass: {
                        required: "Enter password"
                    },
                    cnfmpass: {
                        required: "Confirm password",
						equalTo: "Your password and confirm password do not match."
                    }
                }
            }); //validate

            $.validator.addMethod("mypassword", function(value, element) {
                return this.optional(element) || (value.match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%&*])[a-zA-Z0-9!@#$%&*]+$/));
            }, 'Password must contain at least one capital letter, numeric, alphabetic and special character.');
			
	// Restricts input for each element in the set of matched elements to the given inputFilter.
	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		  if (inputFilter(this.value)) {
			this.oldValue = this.value;
			this.oldSelectionStart = this.selectionStart;
			this.oldSelectionEnd = this.selectionEnd;
		  } else if (this.hasOwnProperty("oldValue")) {
			this.value = this.oldValue;
			this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		  }
		});
	  };
	}(jQuery));			
			
		/*	
		$("#city").inputFilter(function(value) {			  
			return /^-?[a-zA-Z\s]*$/.test(value); 			
		});
		*/
		
		$('#city').change( function() {
			
			var city = $(this).val();
			console.log( 'city:' + city );
			
			if( city == 'Other' )
			{
				$('#othercitydiv').show();
			}
			else
			{
				$('#othercitydiv').hide();
			}
			
		} );
		
		$("#mobile").inputFilter(function(value) {			  
			return /^-?\d*$/.test(value); 			
		});

    </script>