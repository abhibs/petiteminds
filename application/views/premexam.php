<style>
.error{
	color: red !important;
}

.nav-tabs li.active{
	height: auto;
}

.tab-content{
	margin-top: 25px;
}
</style>


	<div class="pg-header">
		
		<h1>My Personal Exams</h1>
		
	</div>

	<section>
		<div class="container">
			
<div class="row">
    <div class="col-md-9">
			
<?php

	if( !empty( $order_detail ) )
	{	
		echo '<div class="table-responsive"><table class="table table-bordered table-result" ><tr><th>Exam Name</th><th>Duration</th></th><th>Active / Expired</th></tr>';

		foreach( $order_detail as $key => $value )
		{
			$hours = floor($value->examtimelimit / 60);
			$minutes = ($value->examtimelimit % 60);
			
			$hours =  str_pad($hours, 2, 0, STR_PAD_LEFT );
			$minutes = str_pad($minutes, 2, 0, STR_PAD_LEFT );
			
			$minhours = floor( ( $value->examtimelimit / 2 ) / 60);
			$minminutes = ( ( $value->examtimelimit / 2 ) % 60);
			
			$minhours =  str_pad($minhours , 2, 0, STR_PAD_LEFT );
			$minminutes = str_pad($minminutes / 2, 2, 0, STR_PAD_LEFT );
			
			$dismin = $value->examtimelimit . ' minutes';
			
			if( !empty( $value->examtimelimit ) )
                $halfmin = round( $value->examtimelimit / 2 ) . ' minutes';
            else 
				$halfmin = '';


			$today_date = date("Y-m-d");
			$validity = $value->odet_validity;

			if($validity > $today_date){
				$get_valididty = '<span style="color:#1ada1a">Active</span>';
			} else{
				$get_valididty = '<span style="color:#ff4b15">Expired</span>';
			}


			
			/*
			echo '<tr><td><a href="'. base_url() .'exams/index/'. $value->examid .'/'. $value->odetsid .'">'. $value->examtitle .' - FULL</a></td><td>'. $hours.':'. $minutes. ':00' .'</td></tr>';
			
			echo '<tr><td><a href="'. base_url() .'exams/mini/'. $value->examid .'/'. $value->odetsid .'">'. $value->examtitle .' - MINI</a></td><td>'. $minhours.':'. $minminutes. ':00' .'</td></tr>';
			*/
			if($validity > $today_date){
			echo '<tr><td><a href="'. base_url() .'exams/index/'. $value->examid .'/'. $value->odetsid .'/'. $value->ordid .'">'. $value->examtitle .' - FULL</a></td><td>'. $dismin .'</td><td>'.$get_valididty.'</td></tr>';
			
			echo '<tr><td><a href="'. base_url() .'exams/mini/'. $value->examid .'/'. $value->odetsid .'/'. $value->ordid .'">'. $value->examtitle .' - MINI</a></td><td>'. $halfmin .'</td><td>'.$get_valididty.'</td></tr>';
			} else{
				echo '<tr style="color:#ff4b15"><td>'. $value->examtitle .' - FULL</td><td>'. $dismin .'</td><td>'.$get_valididty.'</td></tr></span>';
			
				echo '<tr style="color:#ff4b15"><td>'. $value->examtitle .' - MINI</td><td>'. $halfmin .'</td><td>'.$get_valididty.'</td></tr>';
			}
			
		}
		
		echo '</table></div>';
	}
	else
	{
		echo '<div class="table-responsive"><table class="table table-bordered table-result" ><tr><td>No premium exams found. Please click <a href="'. base_url() .'packages">here</a> to add exams.</td></tr>';	
		
		echo '</table></div>';

	}

?>

	</div>
   <div class="col-md-3">
       <?php include 'include_front/sidebar.php' ?>  
               
     </div>
</div>
					
		</div>
	</section> 