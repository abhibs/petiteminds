<style>
.error{
	color: red !important;
}
</style>


<div class="pg-header">
    
    <h1><?=@$msgh1?></h1>
    
</div>

<section id="msg-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
              
    
<?php

if( !empty( $msgsuc ) )
{

?>
	
    <div class="msg-box">
         <img src="<?=base_url()?>front/images/icons/tick.png" class="img-responsive"> 
    <h3><?=$msgsuc?></h3>
    </div>
	
<?php

}
if( !empty( $msgerr ) )
{

?>
    
    <div class="msg-box">
         <img src="<?=base_url()?>front/images/icons/close.png" class="img-responsive"> 
    <h3><?=$msgerr?></h3>
    </div>
    
<?php

}

?>
    
    
     </div>
             
        </div>
    </div>
</section>

 