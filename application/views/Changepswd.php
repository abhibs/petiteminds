<div id="page-wrapper">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">Change Password</h1>
</div>
<!-- /.col-lg-12 -->
</div>
   <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            
            <div class="panel-body">
               <?php echo $this->session->flashdata('msg'); ?>
<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>home/change_pswd" method="post">
                  <div class="row">
                    <div class="col-lg-4">
                            <div class="form-group">
                                <label>Password *</label>
                                <input type="password" class="form-control" name="passphrase" id="passphrase" >
                                <span class="text-danger"><?php echo form_error('passphrase'); ?></span>
                            </div>
                    </div>
                    <div class="col-lg-4">
                            <div class="form-group">
                                <label>Confirm Password *</label>
                                <input type="password" class="form-control" name="cnfmpassphrase" id="cnfmpassphrase" >
                                <span class="text-danger"><?php echo form_error('cnfmpassphrase'); ?></span>
                            </div>
                    </div>
                  </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
