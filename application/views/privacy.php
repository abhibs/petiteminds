<section>
    <div class="container">

        <h2 class="text-blue text-center pb-20">Privacy Policy</h2>

        <div class="term-row">
            <p><strong>Purpose</strong></p>
            <p>Petiteminds.com, we, or us is committed to protecting your privacy, we take data protection and privacy
                very seriously. This Privacy Policy describes how Petiteminds.com collects, uses, shares and secures the
                personal information you provide when you visit the Websites and Mobile Apps owned and operated by
                Petiteminds.com and when you use our Service(s). It also describes your choices regarding use, access
                and correction of your personal information. The use of information collected through our Service(s)
                shall be limited to the purpose of providing the service for which you have engaged Petiteminds.com. The
                capitalized terms used in this Policy but not defined herein shall have the same meaning as defined in
                our Terms of service. Please read this Privacy Policy carefully, as it governs how you use
                Petiteminds.com or its affiliate products. If you do not agree to this Privacy Policy, please do not use
                Petiteminds.com</p>
        </div>
        <div class="term-row">
            <p><strong>Information or data we are collecting</strong></p>
            <p>We collect information from you when you register on our site, place an order, subscribe to our
                newsletter, respond to a survey or fill out a form. When ordering or registering on our site, as
                appropriate, you may be asked to enter your: name, e-mail address, mailing address, phone number or
                credit card information. You may, however, visit our site anonymously. Our website utilizes a
                third-party vendor, Google. Google uses cookies to track user access and behavior. Google's use of the
                <strong>DART cookie</strong> enables it to serve ads to our users based on their visit to our sites and
                other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad
                and content network privacy policy.</p>
        </div>
        <div class="term-row">

            <p><strong>Use of your information</strong></p>
            <p>conditions or covenants hereof, which terms, conditions and covenants shall continue to be in full force
                and effect.No waiver by either party of any breach of any provision hereof shall be deemed a waiver of
                any subsequent or prior breach of the same or any other provision.</p>

            <p>Any of the information we collect from you may be used in one of the following ways:</p>
            <li>To personalize your experience (your information helps us to better respond to your individual needs)
            </li>
            <li>To improve our website (we continually strive to improve our website offerings based on the information
                and feedback we receive from you)</li>
            <li>To improve customer service (your information helps us to more effectively respond to your customer
                service requests and support needs)</li>
            <li>To process transactions</li>
            <li>To administer a contest, promotion, survey or other site feature</li>
            <li>To send periodic emails</li>
            <p>The email address you provide for order processing, will only be used to send you information and updates
                pertaining to your order. The email address you use to sign up for newsletters will only be used to send
                you information about the products, services, and other information we provide. Your information,
                whether public or private, will not be sold, exchanged, transferred, or given to any other company for
                any reason whatsoever, without your consent, other than for the express purpose of delivering the
                purchased product or service requested.</p>
            <p>Note: If at any time you would like to unsubscribe from receiving future emails, we include detailed
                unsubscribe instructions at the bottom of each email.</p>
        </div>
        <div class="term-row">
            <p><strong>How do we protect your information</strong></p>
            <p>Our Websites and Service(s) have industry standard security measures in place to protect against the
                loss, misuse, and alteration of the information under our control. When you provide us with sensitive
                information (such as credit card information or login credentials), we will encrypt that information via
                Secure Socket Layer (SSL).</p>
            <p>Although we will do our best to protect your personal data, we cannot guarantee the security of your data
                transmitted to our Websites or via the Service(s) and any transmission is at your own risk. Once we
                receive your personal information, we will use strict procedures and security features to try to prevent
                unauthorised access.</p>
            <p>We adopt appropriate data collection, storage and processing practices and security measures to protect
                against unauthorized access, alteration, disclosure or destruction of your personal information,
                username, password, transaction information and data stored on our Site.</p>
            <p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make
                your visit to our site as safe as possible.</p>
            <p>We use regular Malware Scanning.</p>
            <p>Your personal information is contained behind secured networks and is only accessible by a limited number
                of persons who have special access rights to such systems, and are required to keep the information
                confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket
                Layer (SSL) technology.</p>
            <p>All payment transactions are processed through secure gateway providers and are not stored or processed
                on our servers</p>
        </div>
        <div class="term-row">
            <p><strong>Amendments</strong></p>
            <p>Petiteminds.com has the discretion to update this privacy policy at any time. When we do, we will post a
                notification on the main page of our Site, revise the updated date at the bottom of this page and send
                you an email. We encourage Users to frequently check this page for any changes to stay informed about
                how we are helping to protect the personal information we collect. You acknowledge and agree that it is
                your responsibility to review this privacy policy periodically and become aware of modifications. Your
                continued use of this Websites or the Service(s) following the posting of any amendment, modification,
                or change to this Policy shall constitute your acceptance of the amendments to this Policy. You can
                choose to discontinue use of the Websites or Service(s), if you do not accept the terms of this Policy,
                or any modified version of this Policy.</p>
        </div>
        <div class="term-row">
            <p><strong>Do we use cookies</strong></p>
            <p>Yes, cookies are small files that a site or its service provider transfers to your computer&rsquo;s hard
                drive through your Web browser that enables the sites or service providers systems to recognize your
                browser and capture and remember certain information. We use cookies to help us remember and process the
                items in your shopping cart, understand and save your preferences for future visits, keep track of
                advertisements and compile aggregate data about site traffic and site interaction so that we can offer
                better site experiences and tools in the future. We may contract with third-party service providers to
                assist us in better understanding our site visitors. These service providers are not permitted to use
                the information collected on our behalf except to help us conduct and improve our business. If you
                prefer, you can choose to have your computer warn you each time a cookie is being sent, or you can
                choose to turn off all cookies via your browser settings. Like most websites, if you turn your cookies
                off, some of our services may not function properly. However, you can still place orders over the
                telephone or by contacting customer service.</p>
        </div>
        <div class="term-row">
            <p><strong>Do we disclose any information to outside parties</strong></p>
            <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information.
                This does not include trusted third parties who assist us in operating our website, conducting our
                business, or servicing you, so long as those parties agree to keep this information confidential. We may
                also release your information when we believe release is appropriate to comply with the law, enforce our
                site policies, or protect ours or others rights, property, or safety. However, non-personally
                identifiable visitor information may be provided to other parties for marketing, advertising, or other
                uses.</p>
        </div>
        <div class="term-row">
            <p><strong>Links to Third Party Sites</strong></p>
            <p>Our Websites contain links to other websites that are not owned or controlled by Petiteminds.com. Please
                be aware that we are not responsible for the privacy practices of such other websites or third parties.
                We encourage you to be aware when you leave our Websites and to read the privacy policies of each and
                every website that collects personal information.</p>
        </div>
        <div class="term-row">
            <p><strong>Advertisement</strong></p>
            <p>We partner with third parties to manage our advertising on other sites. Our third-party partners may use
                technologies such as cookies to gather information about your activities on our Website and other sites
                in order to provide you advertising based upon your browsing activities and interests. If you wish to
                not have this information used for the purpose of serving you interest-based ads, you may opt-out.</p>
        </div>
        <div class="term-row">
            <p><strong>Online Privacy Policy Only</strong></p>
            <p>This online privacy policy applies only to information collected through our website and not to
                information collected offline.</p>


        </div>
</section>