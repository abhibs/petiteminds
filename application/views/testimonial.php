<style>
.error{
	color: red !important;
}

.nav-tabs li.active{
	height: auto;
}

.tab-content{
	margin-top: 25px;
}

</style>


<div class="pg-header">
    
    <h1>Testimonials</h1>
    
</div>

<section>
    <div class="container">
        

<div class="row">
    <div class="col-md-9">
        <?php

	if( !empty( $testimonial ) )
	{	
		echo '<div class="tmondiv">';

		foreach( $testimonial as $key => $value )
		{
			if( strlen( $value->tmbody ) > 50 )
				$body = substr( $value->tmbody, 0, 50 ) . '...';
			else
				$body = $value->tmbody;
			
			
			echo '<div class="testimonial-box">
           
        

            <div class="row">

                <div class="col-md-12">

                    <p><a href="'. base_url() .'testimonial/view/'. $value->tmid .'" class="testimonial-title">'. $value->tmtitle .'</a></p>

                </div>

            </div>

      
		<div class="testimonial-content">

				<div class="row">

					<div class="col-md-12">

						
						<p class="testimonial-desc">'. $body .'</p>

					</div>

				</div>
				
				<div class="row">

					<div class="col-sm-4 col-xs-12">
						
						<p class="testimonial-name">'. $value->stuname .'</p>

					</div>
					
					<div class="col-sm-4 col-xs-12">
						
						<p class="testimonial-name"></p>

					</div>
					
					<div class="col-sm-4 col-xs-12">
						
						<p class="testimonial-date">Date: '. date('d-m-Y', strtotime( $value->tmdatec ) ) .'</p>

					</div>
					

				</div>

			</div>	

		</div>
		 ';
	   
		}
		
		echo '</div>';
	}

?>
	
    </div>
   <div class="col-md-3">
       <?php include 'include_front/sidebar.php' ?>  
               
     </div>
</div>

				
    </div>
</section>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#signinform").validate({
                rules: {
                    stuname: {
                        required: true
                    },
                    stuemail: {
                        required: true,
                        email: true
                    },
                    stumob: {
                        required: true
                    },
                    stupass: {
                        required: false,
                        minlength: 4,
                        mypassword: true
                    },
                    cnfmpass: {
                        required: false,
                        minlength: 4,
						equalTo: "#stupass",
                        mypassword: true
						
                    }
                },
                messages: {
                    stuname: {
                        required: "Enter name"
                    },
                    stuemail: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    stumob: {
                        required: "Enter mobile number"
                    },
                    stupass: {
                        required: "Enter password"
                    },
                    cnfmpass: {
                        required: "Confirm password",
						equalTo: "Your password and confirm password do not match."
                    }
                }
            }); //validate

            $.validator.addMethod("mypassword", function(value, element) {
                return this.optional(element) || (value.match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%&*])[a-zA-Z0-9!@#$%&*]+$/));
            }, 'Password must contain at least one capital letter, numeric, alphabetic and special character.');
			
	// Restricts input for each element in the set of matched elements to the given inputFilter.
	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		  if (inputFilter(this.value)) {
			this.oldValue = this.value;
			this.oldSelectionStart = this.selectionStart;
			this.oldSelectionEnd = this.selectionEnd;
		  } else if (this.hasOwnProperty("oldValue")) {
			this.value = this.oldValue;
			this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		  }
		});
	  };
	}(jQuery));			
			
		/*	
		$("#city").inputFilter(function(value) {			  
			return /^-?[a-zA-Z\s]*$/.test(value); 			
		});
		*/
		
		$('#city').change( function() {
			
			var city = $(this).val();
			console.log( 'city:' + city );
			
			if( city == 'Other' )
			{
				$('#othercitydiv').show();
			}
			else
			{
				$('#othercitydiv').hide();
			}
			
		} );
		
		$("#mobile").inputFilter(function(value) {			  
			return /^-?\d*$/.test(value); 			
		});

    </script>