<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Edit On Demand Exams</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  On Demand Exams Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>
							
							
							<!--
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Category</label>
								<div class="col-sm-10">
<?php

            /*
    		$opt = array( '' => 'Select' );
    		if( !empty( $catdemand ) )
    		{
    			
    			foreach( $catdemand as $key => $value )
    			{
    				$opt[ $value->catd_id ] = $value->catname;							
    			}
    			
    		}
    		echo form_dropdown('catd_id', $opt, @$subcatdemand[0]->catd_id, 'id="catd_id" class="form-control" required');
    		*/

?>
									
									<?php echo form_error('catd_id'); ?>
								</div>
							</div>
							-->
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Exam Code</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="scatdcode" required value="<?php echo @$subcatdemand[0]->scatdcode; ?>">
									
									<?php echo form_error('scatdcode'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Exam Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="scatdname" required value="<?php echo @$subcatdemand[0]->scatdname; ?>">
									
									<?php echo form_error('scatdname'); ?>
								</div>
							</div>
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Order</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="scatdatorder" required value="<?php echo @$subcatdemand[0]->scatdatorder; ?>">
									
									<?php echo form_error('scatdatorder'); ?>
								</div>
							</div>
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Price</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="scatdprice" required value="<?php echo @$subcatdemand[0]->scatdprice; ?>">
									
									<?php echo form_error('scatdprice'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Status</label>
								<div class="col-sm-10">
									<?php
									
									$opt = array( "" => "Select", "A" => "Active", "D" => "Deactivated" );
	
									echo form_dropdown( 'scatdstatus', $opt, @$subcatdemand[0]->scatdstatus, 'id="scatdstatus" class="form-control" required' );
									
									?>
									
									<?php echo form_error('scatdstatus'); ?>
								</div>
							</div>
							
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>