<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Add Demand Category</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  Demand Category Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="catname" required value="<?php echo set_value( 'catname' ); ?>">
									
									<?php echo form_error('catname'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Order</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="catorder" required value="<?php echo @$maxorder; ?>">
									
									<?php echo form_error('catorder'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Status</label>
								<div class="col-sm-10">
									<?php
									
									$opt = array( "" => "Select", "A" => "Active", "D" => "Deactivated" );
	
									echo form_dropdown( 'status', $opt, set_value('status'), 'id="status" class="form-control" required' );
									
									?>
									
									<?php echo form_error('status'); ?>
								</div>
							</div>
							
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>