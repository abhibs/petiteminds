<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">View Exam</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

                      <div class="panel-heading">

                          Exam Details

                      </div>

                      <div class="panel-body">

                         <?php echo $this->session->flashdata('msg'); ?>

			<form role="form" enctype="multipart/form-data" action="" method="post">

							<table class="table">
								<tbody>
									<tr>
										<th>Title</th>
										<td><?=@$exams[0]->examtitle?></td>
									</tr>
									<tr>
										<th>Description</th>
										<td><?=@$exams[0]->examdesc?></td>
									</tr>
									<tr>
										<th>Available</th>
										<td><?=$exams[0]->examavailable?></td>
									</tr>
									<tr>
										<th>Pass Rate</th>
										<td><?=@$exams[0]->exampassrate?></td>
									</tr>
									<tr>
										<th>Backward Navigation</th>
										<td><?php
										
										if( $exams[0]->exambacknav == 'Y' )
											echo 'Yes' ;
										else
											echo 'No' ; 
										
										?></td>
									</tr>
									<tr>
										<th>Time Limit (Minutes)</th>
										<td><?=@$exams[0]->examtimelimit?></td>
									</tr>
									<tr>
										<th>Total</th>
										<td><?=@$exams[0]->examtotal?></td>
									</tr>
									<tr>
										<th>Date Created</th>
										<td><?=date('d-m-Y', strtotime(@$exams[0]->examdatec))?></td>
									</tr>
								</tbody>
							</table>
			
                                      

            </form>

                              </div>



                          </div>

                      </div>

                  </div>

              </div>

          </div>