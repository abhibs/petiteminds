<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Import Exam Questions</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form role="form" enctype="multipart/form-data" action="" method="post">

                      <div class="panel-heading">

                          Exam Details

                      </div>

                      <div class="panel-body">

                         <?php echo $this->session->flashdata('msg'); ?>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Subcategory </label>

                                      <?php

					$opt = array( '' => 'Select' );

					if( !empty( $subcategory ) )
					{
						foreach( $subcategory as $key => $value )
						{
							$opt[ $value->id ] = $value->name;

						}

					}

					echo form_dropdown('sid', $opt, @$exams[0]->sid, 'id="sid" class="form-control" readonly disabled');

									  ?>

                                      <span class="text-danger"><?php echo form_error('sid'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                  </div>

                              </div>


                            </div>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Title </label>

                                      <input class="form-control" type="text" name="examtitle" readonly value="<?=@$exams[0]->examtitle?>" required>

                                      <span class="text-danger"><?php echo form_error('examtitle'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Description </label>

									  <textarea name="examdesc" id="examdesc" class="form-control" readonly><?=@$exams[0]->examdesc?></textarea>

                                      <span class="text-danger"><?php echo form_error('examdesc'); ?></span>

                                  </div>

                              </div>


                            </div>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Available </label>

									<?php

										$opt = array( "" => "Select", "Always" => "Always", "Never" => "Never" );

										echo form_dropdown( 'examavailable', $opt, @$exams[0]->examavailable, 'id="examavailable" class="form-control" readonly disabled' );

									?>


                                      <span class="text-danger"><?php echo form_error('examavailable'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Pass Rate </label>

									  <input class="form-control" type="number" name="exampassrate" readonly value="<?=@$exams[0]->exampassrate?>" required>

                                      <span class="text-danger"><?php echo form_error('exampassrate'); ?></span>

                                  </div>

                              </div>


                            </div>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Backward Navigation </label>

									<?php

										$opt = array( "" => "Select", "N" => "No", "Y" => "Yes" );

										echo form_dropdown( 'exambacknav', $opt, @$exams[0]->exambacknav, 'id="exambacknav" class="form-control" readonly disabled' );

									?>


                                      <span class="text-danger"><?php echo form_error('exambacknav'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Time Limit (Minutes) </label>

									  <input class="form-control" type="number" name="examtimelimit" readonly value="<?=@$exams[0]->examtimelimit?>">

                                      <span class="text-danger"><?php echo form_error('examtimelimit'); ?></span>

                                  </div>

                              </div>


                            </div>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Exam Type </label>

									<?php

										$opt = array( "" => "Select", "PAID" => "Paid", "FREE" => "Free" );

										echo form_dropdown( 'examtype', $opt, @$exams[0]->examtype, 'id="examtype" class="form-control" readonly disabled' );

									?>


                                      <span class="text-danger"><?php echo form_error('examtype'); ?></span>



                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">


                                      <label>Total </label>

									  <input class="form-control" type="number" name="examtotal" value="<?=count(@$questions)?>" readonly>

                                      <span class="text-danger"><?php echo form_error('examtotal'); ?></span>


                                  </div>

                              </div>


                            </div>



                              </div>

                        <div class="panel-heading" style="background-color: #334A58;color: white;">

                          Import Questions

                      </div>

					  <div class="panel-body">

						<div id="product-box">

							<input type="hidden" name="hdnimport" value="1">

							<div class="form-group">
								<!-- <input type="file" name="importfile" required accept=".xls, .xlsx,"> -->
								<input type="file" name="importfile" required accept=".csv">
								<span class="text-danger"><?php echo form_error('importfile'); ?></span>
							</div>

							<div class="form-group">
								<?php
								$que_imports = $this->session->flashdata('que_imports');
								if(!empty($que_imports)){ ?>
									<div class="panel-heading" style="background-color: #334A58;color: white;">

										File Imported Details

									</div>
									<table class="table" border=0>

										<thead>
										<tr>
											<th>Rows Imported</th>
											<th>Imported At</th>
										</tr>
									</thead>

									<tbody>
										<?php foreach ($que_imports as $key => $que_import) { ?>

										<tr>
											<td><?=@$que_import->import_row_count?></td>
											<td><?=@$que_import->import_datetime?></td>
										</tr>

									 <?php } ?>
									</tbody>

									</table>
								<?php } ?>
							</div>

            </div>

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

							<div id="delqdiv">

							</div>

            </div>


                            </form>

                          </div>

                      </div>

                  </div>

              </div>

          </div>

<?php include('Footer.php')?>

<script type="text/javascript">



</script>
