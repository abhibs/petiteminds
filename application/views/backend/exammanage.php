    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Exams</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Exams List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Title</th>
                                                    <th>Subcategory</th>
                                                    <th>Questions</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               
                                               $srno = 0;
                                               
                                               foreach($exams as $exam){ 
                                                    $srno++;
                                               
                                               ?>
											   
                <tr id="<?php echo $exam->examid;?>" class="odd gradeX">
							
						<td><?php echo $srno;?></td>						
					  <td><?php echo $exam->examtitle;?></td>
					  <td><?php echo $exam->subcname;?></td>

					  <td><?php 
					  		
					  $questions = $this->GeneralModel->GetSelectedRows($table = 'questions', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = array( 'questions.fk_examid' => $exam->examid ), $search = '');
					  
					  echo @$questions[0]->numrows;?></td>
														
					  <td>
					  
						<a href="<?php echo base_url(); ?>backend/exams/view/<?php echo $exam->examid; ?>" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i></a>
						<a href="<?php echo base_url(); ?>backend/exams/edit/<?php echo $exam->examid; ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
						<a href="<?php echo base_url(); ?>backend/exams/import/<?php echo $exam->examid; ?>" class="btn btn-primary btn-circle"><i class="fa fa-download"></i></a>
						<a href="<?php echo base_url().'backend/exams/delete/'.$exam->examid;?>" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash"></i></a>
					  </td>
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>