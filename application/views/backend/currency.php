<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Currency</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">
<?php echo form_open('backend/currency/update_completed'); ?>

<!--           <form role="form" enctype="multipart/form-data" action="" method="post"> -->
				  
                      <div class="panel-heading" style="">

                          Currency Details

                      </div>
					  
                      <div class="panel-body">

                         <?php echo $this->session->flashdata('msg'); ?>
						
		  
							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Dollar Value *</label>

                                      <input class="form-control" type="number" step="0.01" value="<?php echo $GetCurrencyData; ?>" name="dollar_price" required>

                                      <span class="text-danger"><?php echo form_error('dollar_price'); ?></span>

                                  </div>

                              </div>



                            </div>

													
						


                                      				<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>

                                  <?php echo form_close(); ?>


                              </div>
							  

							  
</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>