<div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Admin Testimonial</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Testimonial List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['show'] ) && $_GET['show'] == 'Y' )
							{
								echo '<h4 class="form-header" style="color: green;">Admin Testimonial is visible.</h4>';
								
							}
							
							if( !empty( $_GET['show'] ) && $_GET['show'] == 'N' )
							{
								echo '<h4 class="form-header" style="color: green;">Admin Testimonial is not visible.</h4>';
								
							}

                            if( !empty( $_GET['delete'] ) && $_GET['delete'] == 'delete' )
							{
								echo '<h4 class="form-header" style="color: green;">Admin Testimonial Deleted Successfully.</h4>';
								
							}
                            if( !empty( $_GET['added'] ) && $_GET['added'] == 'added' )
							{
								echo '<h4 class="form-header" style="color: green;">Admin Testimonial Added Successfully.</h4>';
								
							}

                            if( !empty( $_GET['edit'] ) && $_GET['edit'] == 'edit' )
							{
								echo '<h4 class="form-header" style="color: green;">Admin Testimonial Edited Successfully.</h4>';
								
							}
                            
							
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Name</th>
													<th>Body</th>
													<th>Show</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               $srno = 0;
                                               
                                               foreach( $testimonial as $tmon ){
                                               
                                               $srno++;
                                               
                                               ?>
											   
                <tr id="<?php echo $tmon->tmid;?>" class="odd gradeX">
												
						<td><?php echo $srno;?></td>						
					  <td><?php echo $tmon->name;?></td>
					  <td><?php echo $tmon->tmbody;?></td>
					  
					  <td><?php echo ( $tmon->tm_show == 'Y' ) ? 'Yes' : 'No' ;?></td>
						
					  <td>
					  
						<?php
						
						if( $tmon->tm_show == 'Y' )
						{
						?>
						
						<a href="<?php echo base_url().'backend/admintestimonials/changeshow/'.$tmon->tmid;?>/N" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to hide?')"><i class="fa fa-lock"></i></a>
						
						<?php
						}
						else
						{
							
						?>
						
						<a href="<?php echo base_url().'backend/admintestimonials/changeshow/'.$tmon->tmid;?>/Y" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to show?')"><i class="fa fa-unlock"></i></a>
						
						<?php
						}
						
						?>

                        <a href="<?php echo base_url().'backend/admintestimonials/edit/'.$tmon->tmid ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
						
						<a href="<?=base_url()?>backend/admintestimonials/delete/<?=$tmon->tmid?>/delete" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash" aria-hidden="true"></i></a>


					  </td>
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>