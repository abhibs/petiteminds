<style>
.count_box .name a, .table td a{
	color: #000 !important;
}
</style>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Dashboard</h1>
                        </div>
                    </div>
                    
<?php if($this->session->userdata('role')=='admin') { ?>
  
  <div class="row">
           
           <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                               <i class="fas fa-sitemap fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$category_cnt[0]->numrows?></div>
                                <div>Categories</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/category">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                               <i class="fas fa-sitemap fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$subcategory_cnt[0]->numrows?></div>
                                <div>Sub Categories</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/subcategory">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fas fa-question fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$paidexams_cnt[0]->numrows?></div>
                                <div>Paid Exams</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/exams/index/PAID">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fas fa-question fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$freeexams_cnt[0]->numrows?></div>
                                <div>Free Exams</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/exams/index/FREE">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
           <div class="col-lg-3 col-md-6">
            
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fas fa-file-alt fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$blog_cnt[0]->numrows?></div>
                                <div>Total Blogs</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/blog">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
        
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
                
            </div>
           <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fas fa-user-edit fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$testimonial_cnt[0]->numrows?></div>
                                <div>Total Testimonials</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/testimonial">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
        
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div> 
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                    <i class="fas fa-user-edit fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$livetmon_cnt[0]->numrows?></div>
                                <div>Live Testimonials</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/livetestimonials">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
        
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div> 
            
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary admin-dash-box">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                    <i class="fas fa-user-edit fa-3x" aria-hidden="true"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge"><?=@$demexams_cnt[0]->numrows?></div>
                                <div>On Demand Exams</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>backend/subcatdemand">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
        
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div> 
            
        </div>    
                    
<?php } ?>


<?php if($this->session->userdata('role')=='dataentry') { ?>
  
    <div class="row">
        
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary admin-dash-box">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fas fa-question fa-3x" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-10 text-right">
                            <div class="huge"><?=@$paidexams_cnt[0]->numrows?></div>
                            <div>Paid Exams</div>
                        </div>
                    </div>
                </div>
                <a href="<?=base_url()?>backend/exams/index/PAID">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary admin-dash-box">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fas fa-question fa-3x" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-10 text-right">
                            <div class="huge"><?=@$freeexams_cnt[0]->numrows?></div>
                            <div>Free Exams</div>
                        </div>
                    </div>
                </div>
                <a href="<?=base_url()?>backend/exams/index/FREE">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
      
    </div>    
                    
<?php } ?>

<?php if($this->session->userdata('role')=='testimonials') { ?>
  
  <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary admin-dash-box">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fas fa-user-edit fa-3x" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-10 text-right">
                            <div class="huge"><?=@$admintmon_cnt[0]->numrows?></div>
                            <div>Admin Testimonials</div>
                        </div>
                    </div>
                </div>
                <a href="<?=base_url()?>backend/Admintestimonials">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            
            <div class="panel panel-primary admin-dash-box">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fas fa-file-alt fa-3x" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-10 text-right">
                            <div class="huge"><?=@$blog_cnt[0]->numrows?></div>
                            <div>Total Blogs</div>
                        </div>
                    </div>
                </div>
                <a href="<?=base_url()?>backend/blog">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
    
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
            
        </div>
  </div>    
                  
<?php } ?>

                    
        <!--<div class="row">-->
        <!--        <div class="col-md-3">-->
        <!--            <div class="dashboard-box">-->
        <!--                <div class="icon-box">-->
        <!--                    <h3>Subcategory</h3>-->
        <!--                    <p><?=@$subcategory_cnt[0]->numrows?></p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
                
        <!--        <div class="col-md-3">-->
        <!--            <div class="dashboard-box">-->
        <!--                <div class="icon-box">-->
        <!--                    <h3>Exams</h3>-->
        <!--                    <p><?=@$exams_cnt[0]->numrows?></p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
                
        <!--    </div>-->
                </div>
             
      
            </div>
            
            
    