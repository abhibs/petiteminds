     <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Blog</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Blog List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Title</th>
                                                    <th>Image Text</th>
                                                    <th>Order</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
											   
											if( !empty( $blogs ) )
											{
											   $srno = 0;
											   foreach($blogs as $blog){ 
											   $srno++;
											   ?>
                                                <tr id="<?php echo $blog->id;?>" class="odd gradeX">
                                                    
                                                    <td><?php echo $srno;?></td>
                                                    <td><?php echo $blog->title;?></td>
                                                    
                                                    <td><?php echo $blog->img_txt;?></td>
                                                    <!--<td><img src="<?php echo base_url().'uploads/blogs/'.$blog->img_file;?>" width="100px"></td>-->
                                                    <td><?php echo $blog->orderno;?></td>
                                                    <td class="center"><?php echo $blog->status==1?'Active':'Inactive'; ?></td>
                  <td>
                    <a href="<?php echo base_url(); ?>backend/blog/edit/<?php echo $blog->id; ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo base_url().'backend/blog/delete_blog/'.$blog->id;?>" class="btn btn-primary btn-circle"><i class="fa fa-trash"></i></a>
                  </td>
                                                </tr>
                                               <?php } 
											}
											   ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>