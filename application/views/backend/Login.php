<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="icon" href="<?php echo base_url()?>assets/images/favicon.png">

    <title>Admin Panel</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/startmin.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">


</head>

<body>
    <div id="login_page" style="background-image: linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.9)), url('<?php echo base_url();?>assets/images/login_banner.jpg');">
        <div class="login_page_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-panel panel panel-default" id="admin_login">
                            <div class="panel-heading">
                                <!-- <img src="<?php echo base_url();?>assets/images/logo.jpg" alt="logo" > -->
                                <h3 class="panel-title">Login</h3>
                                <p class="subtitle">Enter your details below to continue</p>
                            </div>
                            <div class="panel-body">
                                <?php if(isset($error) && $error !='') { ?>
                                <div class="alert alert-danger">
                                    <?php echo $error; ?>
                                </div>
                                <?php  } ?>
                                <?php echo $this->session->flashdata('message'); ?>
                                <form role="form" method="post" action="<?php echo base_url(); ?>backend/home/do_login">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Email*" name="email" type="text" autofocus>
                                            <?php echo form_error('email'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password*" name="password" type="password" value="">
                                            <?php echo form_error('password'); ?>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="web_btn">Login</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                           <!-- <div class="panel-footer">
                                <a href="<?php echo base_url();?>home/forgot_password">Forgot Password?</a>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/startmin.js"></script>

</body>

</html>