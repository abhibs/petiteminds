    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Live Testimonials</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Live Testimonials List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['e'] ) && $_GET['e'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Edited Successfully</h4>';
								
							}
							
							if( !empty( $_GET['del'] ) && $_GET['del'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Deleted Successfully</h4>';
								
							}
							
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Url</th>
													<th>Order</th>
													<th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               
                                               $srno = 0;
                                               
                                               foreach( $livetmon as $ltmon ){
                                               
                                               $srno++;
                                               
                                               ?>
											   
                <tr id="<?php echo $ltmon->livetmonid;?>" class="odd gradeX">
							
					<td><?php echo $srno;?></td>		
												
					  <td>
					  
					  <?php 
					  						
						$livetmonurl = str_replace("watch?v=", "embed/", $ltmon->livetmonurl );
					  ?>
					  
					  <iframe width="250" height="150" src="<?=$livetmonurl?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					  
					  </td>
					  <td><?php echo $ltmon->livetmonord;?></td>
					  
					  <td><?php echo ( $ltmon->livetmonshow == 'Y' ) ? 'Yes' : 'No' ;?></td>
						
					  <td>
					  
						<a href="<?php echo base_url(); ?>backend/livetestimonials/edit/<?php echo $ltmon->livetmonid; ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
						<a href="<?php echo base_url().'backend/livetestimonials/delete/'.$ltmon->livetmonid;?>" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash"></i></a>
						
					  </td>
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>