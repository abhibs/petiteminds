<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">View Orders</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

                      <div class="panel-heading">

                          Orders Details

                      </div>

                      <div class="panel-body">

                         <?php echo $this->session->flashdata('msg'); ?>

			<form role="form" enctype="multipart/form-data" action="" method="post">

							<table class="table">
								<tbody>
									<tr>
										<th>ID</th>
										<td><?=@$orders[0]->ordid?></td>
									</tr>
									<tr>
										<th>Student</th>
										<td><?=@$orders[0]->stuname?></td>
									</tr>
									<tr>
										<th>Order Total</th>
										<td><?=$orders[0]->ordtotal; echo ' ('; echo $orders[0]->payment_currency; echo')';?></td>
									</tr>
									<tr>
										<th>GST</th>
										<td><?=@$orders[0]->ordgst?></td>
									</tr>
									<tr>
										<th>GST Rate</th>
										<td><?=@$orders[0]->ordgstrate?></td>
									</tr>									
									<tr>
										<th>Status</th>
										<td><?=@$orders[0]->ordstatus?></td>
									</tr>
									<tr>
										<th>Currency</th>
										<td><?=@$orders[0]->payment_currency?></td>
									</tr>
									<tr>
										<th>Taxation ID</th>
										<td><?=@$orders[0]->txn_id?></td>
									</tr>
									<tr>
										<th>Date</th>
										<td><?=date('d-m-Y', strtotime( @$orders[0]->orddatec ) )?></td>
									</tr>
									<tr>
										<th>Payment Gateway</th>
										<td><?=@$orders[0]->payment_gateway?></td>
									</tr>
									
								</tbody>
							</table>
							
							<table class="table" >
								<thead>
									<tr>
										<th>Subject</th>
										<th>Quantity</th>
										<th>Price</th>
										<th>Validity</th>

									</tr>
								</thead>
								<tbody>
								   <?php foreach( $order_detail as $odet ){ ?>
								   
									<tr id="<?php echo $odet->odetid;?>" class="odd gradeX">
																	
											<td><?php echo $odet->name;?></td>
											<td><?php echo $odet->odetqty;?></td>
											<td><?php echo $odet->odetprice;?></td>
											<td><?php echo date('d-m-Y', strtotime( $odet->odet_validity ) );?></td>

									</tr>
									
								   <?php } ?>
								</tbody>
							</table>
			
                                      

            </form>

                              </div>



                          </div>

                      </div>

                  </div>

              </div>

          </div>