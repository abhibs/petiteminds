<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">View Orders</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

                      <div class="panel-heading">

                          Orders Details

                      </div>

                      <div class="panel-body">

                         <?php echo $this->session->flashdata('msg'); ?>

			<form role="form" enctype="multipart/form-data" action="" method="post">

							<table class="table">
								<tbody>
									<tr>
										<th>ID</th>
										<td><?=@$order_demand_detail[0]->ordd_id?></td>
									</tr>
									<tr>
										<th>Student Name</th>
										<td><?=@$order_demand_detail[0]->ordd_stuname?></td>
									</tr>
									<tr>
										<th>Email</th>
										<td><?=@$order_demand_detail[0]->ordd_stuemail?></td>
									</tr>
									<tr>
										<th>Mobile</th>
										<td><?=@$order_demand_detail[0]->ordd_stumob?></td>
									</tr>
									<tr>
										<th>Exam Code</th>
										<td><?=@$order_demand_detail[0]->scatdcode?></td>
									</tr>
									<tr>
										<th>Exam Name</th>
										<td><?=@$order_demand_detail[0]->scatdname?></td>
									</tr>
									<tr>
										<th>Order Total</th>
										<td><?=$order_demand_detail[0]->ordtotal_inr; echo ' ('; echo $order_demand_detail[0]->payment_currency; echo')';?></td>
									</tr>
									<tr>
										<th>GST</th>
										<td><?=@$order_demand_detail[0]->ordgst?></td>
									</tr>
									<tr>
										<th>GST Rate</th>
										<td><?=@$order_demand_detail[0]->ordgstrate?></td>
									</tr>									
									<tr>
										<th>Status</th>
										<td><?=@$order_demand_detail[0]->ordstatus?></td>
									</tr>
									<tr>
										<th>Payment Gateway</th>
										<td><?=@$order_demand_detail[0]->payment_gateway?></td>
									</tr>
									<tr>
										<th>Currency</th>
										<td><?=@$order_demand_detail[0]->payment_currency?></td>
									</tr>
									<tr>
										<th>Taxation ID</th>
										<td><?=@$order_demand_detail[0]->txn_id?></td>
									</tr>
									<tr>
										<th>Date</th>
										<td><?=date('d-m-Y', strtotime( @$order_demand_detail[0]->orddatec ) )?></td>
									</tr>
									
									
								</tbody>
							</table>
							
							
                                      

            </form>

                              </div>



                          </div>

                      </div>

                  </div>

              </div>

          </div>