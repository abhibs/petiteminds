    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Orders</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Orders List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['e'] ) && $_GET['e'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Edited Successfully</h4>';
								
							}
							
							if( !empty( $_GET['del'] ) && $_GET['del'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Deleted Successfully</h4>';
								
							}
							
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
									
									
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>Sr No.</th>
                                                    <th>Order ID</th>
													<th>Purchased By</th>
													<th>Order Total</th>
													<th>Order Status</th>
                                                    <th>Payment Gateway</th>
													<th>Date</th>
													<th>Action</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               
                                               $srno = 0;
                                               
                                               foreach( $orders as $stu ){ 
                                               
                                               $srno++;
                                               
                                               ?>
											   
                <tr id="<?php echo $stu->ordid;?>" class="odd gradeX">
									
						<td><?php echo $srno;?></td>						
						<td><?php echo $stu->ordid;?></td>
						<td><a href="students/edit/<?php echo $stu->fk_stuid; ?>"><?php echo $stu->stuname;?></a></td>
						<td><?php echo $stu->ordtotal_inr; echo ' (';echo $stu->payment_currency; echo')';?></td>
						<td><?php echo $stu->ordstatus;?></td>
                        <td><?php echo $stu->payment_gateway;?></td>
						<td><?php echo date('d-m-Y', strtotime( $stu->orddatec ) );?></td>
						
						<td>
						    <a href="<?php echo base_url(); ?>backend/orders/details/<?php echo $stu->ordid; ?>" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i></a>
						    
						    <a href="<?php echo base_url(); ?>backend/orders/delete/<?php echo $stu->ordid; ?>" class="btn btn-primary btn-circle" onClick="return confirm('Do you want to delete this record?');"><i class="fa fa-trash"></i></a>
						    
						 </td>
					  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
										
										
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>