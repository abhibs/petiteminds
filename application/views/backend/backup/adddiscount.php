
<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Add Student Discount</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="<?=base_url().'backend/studiscount/discaddsave/add'?>" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  Student Discount Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>

                            <div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Student ID</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="stuid" name="stuid" required value="">
									
									<?php echo form_error('name'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Course</label>
								<div class="col-sm-10">
									
									<?php
									
									if( !empty( $exams ) )
									{
										$opt = array( '' => 'Select' );
										
										foreach( $exams as $key => $value )
										{
											$opt[ $value->sid ] = $value->examtitle;							
										}
										
										echo form_dropdown('sid', $opt, set_value('sid'), 'id="sid" class="form-control" required');
									}
									
									?>
									
									<?php echo form_error('sid'); ?>
								</div>
							</div>

                            <div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Amount</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="amount" name="amount" required value="">
									
									<?php echo form_error('name'); ?>
								</div>
							</div>
			
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>

<script type="text/javascript">

	$(document).ready(function() {
		  
        var x = 0; //Initial field counter is 1

        $('#product-box .add-btn').click(function(e) {

            x++; //Increment field counter

            var html = '<div class="product-box-row"><div class="row">								                                    <div class="col-md-10 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <label>Question</label>											<input class="form-control" type="text" name="question[]" required>                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            									<label>Select Type</label>		<select name="qtype[]" class="form-control" required>												<option value="MANS">Multiple Answer</option>												<option value="SANS">Single Answer</option>											</select>											                                            <span class="text-danger"><?php echo form_error('qtype'); ?></span>                                        </div>                                    </div>                                                                    </div>																<div class="row">									<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            											<input class="form-control" type="text" name="opt1[]" placeholder="Option 1" required>												                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>									                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt2[]" placeholder="Option 2" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt3[]" placeholder="Option 3" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt4[]" placeholder="Option 4" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt5[]" placeholder="Option 5" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">											<select name="answer'+x+'[]" class="form-control" multiple required>												<option value="">Select Correct Answers</option>												<option value="opt1_iscorr">Option 1</option>												<option value="opt2_iscorr">Option 2</option>												<option value="opt3_iscorr">Option 3</option>												<option value="opt4_iscorr">Option 4</option>												<option value="opt5_iscorr">Option 5</option>											</select>                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>                                                                    </div><div class="row"><div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div></div></div>';      
								
			$('#product-box').append(html);            
			
        });

        $('body').on('click', '#product-box .remove-btn', function(e) {
            e.preventDefault();
            $(this).closest('.product-box-row').remove();
            x--; //Decrement field counter
        });
		
	});
		
</script>

<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'scatdesc' );
</script>
