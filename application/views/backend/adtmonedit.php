
<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Add Admin Testimonial</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="<?=base_url().'backend/admintestimonials/editsave/'.$adtmon[0]->tmid?>/edit" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  Admin Testimonial Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>

                            <div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="nameadtmon" required value="<?php echo $adtmon[0]->name ?>">
								</div>
							</div>

							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="tmontitle" name="tmontitle" required value="<?php echo $adtmon[0]->tmtitle ?>">
								</div>
							</div>

							<div class="form-group row">
							<label for="input-5" class="col-sm-2 col-form-label">Module</label>
								<div class="col-sm-10">
								<?php

									$opt = array( $adtmon[0]->tmmodule );
									
									if( !empty( $category ) )
									{	
										foreach( $category as $key => $value )
										{
											$opt[ $value->catname ] = $value->catname;
										}
									}
									
									echo form_dropdown('tmmodule', $opt, set_value( 'tmmodule' ), 'id="tmmodule" name="tmmodule" class="form-control" required');

								?>
								</div>
							</div>

                            <div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Body</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="bodyadtmon" id="" cols="30" rows="10"><?php echo $adtmon[0]->tmbody ?> </textarea>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Show</label>
								<div class="col-sm-10">
									<?php
									$select = $adtmon[0]->tm_show;
                                    if($select == 'N'){
                                        $selval = 'No';
                                    } 
                                    else if($select == 'Y'){
                                        $selval = 'Yes';
                                    }

									$opt = array( $selval, "Y" => "Yes", "N" => "No" );
	
									echo form_dropdown( 'livetmonshow', $opt, set_value('livetmonshow'), 'id="livetmonshow" name="livetmonshow" class="form-control" required' );
									
									?>
									
								</div>
							</div>
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
				</div>
						</form>


                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>