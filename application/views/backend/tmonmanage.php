    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Testimonial</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Testimonial List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['show'] ) && $_GET['show'] == 'Y' )
							{
								echo '<h4 class="form-header" style="color: green;">Testimonial is visible.</h4>';
								
							}
							
							if( !empty( $_GET['show'] ) && $_GET['show'] == 'N' )
							{
								echo '<h4 class="form-header" style="color: green;">Testimonial is not visible.</h4>';
								
							}
							
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Title</th>
													<th>Module</th>
													<th>Body</th>
													<th>Keywords</th>
													<th>Show</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               $srno = 0;
                                               
                                               foreach( $testimonial as $tmon ){
                                               
                                               $srno++;
                                               
                                               ?>
											   
                <tr id="<?php echo $tmon->tmid;?>" class="odd gradeX">
												
						<td><?php echo $srno;?></td>						
					  <td><?php echo $tmon->tmtitle;?></td>
					  <td><?php echo $tmon->tmmodule;?></td>
					  <td><?php echo $tmon->tmbody;?></td>
					  <td><?php echo $tmon->tmkeyward;?></td>
					  
					  <td><?php echo ( $tmon->tm_show == 'Y' ) ? 'Yes' : 'No' ;?></td>
						
					  <td>
					  
						<?php
						
						if( $tmon->tm_show == 'Y' )
						{
						?>
						
						<a href="<?php echo base_url().'backend/testimonial/changeshow/'.$tmon->tmid;?>/N" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to hide?')"><i class="fa fa-lock"></i></a>
						
						<?php
						}
						else
						{
							
						?>
						
						<a href="<?php echo base_url().'backend/testimonial/changeshow/'.$tmon->tmid;?>/Y" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to show?')"><i class="fa fa-unlock"></i></a>
						
						<?php
						}
						
						?>
						
						<a href="<?=base_url()?>backend/testimonial/delete/<?=$tmon->tmid?>" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
						
					  </td>
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>