<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="icon" href="<?php echo base_url()?>assets/images/favicon.png">
    <meta name=”robots” content=”nofollow” />

    <title>Admin Panel</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/startmin.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/chosen.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">


 <script src="https://kit.fontawesome.com/a076d05399.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header navbar-brand">
                Welcome <?php echo $this->session->userdata('username');?></a>
            </div>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <ul class="nav navbar-right navbar-top-links">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $this->session->userdata('username');?> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo base_url(); ?>backend/home/change_password"><i class="fa fa-question-circle-o fa-fw"></i> Change Password</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>backend/home/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="<?php echo base_url().'backend/dashboard';?>"><i class="fas fa-tachometer-alt fa-fwblo"></i>Dashboard</a>
                        </li>

                        <?php if($this->session->userdata('role')=='admin') { ?>

						<li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Category<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/category/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/category';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
						<li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Subcategory<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/subcategory/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/subcategory';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>	
						
						<li>
                            <a href="#"><i class="fa fa-question fa-fw"></i>Exams<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/exams/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/exams/index/PAID';?>"><i class="fa fa-eye"></i>Paid Exams</a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url().'backend/exams/index/FREE';?>"><i class="fa fa-eye"></i>Free Exams</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
						<li>
                            <a href="#"><i class="fas fa-file-alt fa-fw"></i>Blog<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/blog/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/blog';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
						<li>
                            <a href="#"><i class="fas fa-user-edit fa-fw"></i>Testimonials<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                
                                <li>
                                    <a href="<?php echo base_url().'backend/testimonial';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
												
						<li>
                            <a href="#"><i class="fas fa-user-edit fa-fw"></i>Live Testimonials<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/livetestimonials/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/livetestimonials';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#"><i class="fas fa-user-edit fa-fw"></i>Admin Testimonials<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/Admintestimonials/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/Admintestimonials';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fas fa-users fa-fw"></i>Students<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/students/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/students';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        
						<li>
                            <a href="<?php echo base_url().'backend/exams/displayresultdashboard';?>"><i class="fa fa-user fa-fw"></i>Exam Results</a>
                        </li>
						<li>
                            <a href="<?php echo base_url().'backend/orders';?>"><i class="fa fa-user fa-fw"></i>Orders</a>
                        </li>
						<li>
                            <a href="<?php echo base_url().'backend/currency/add';?>"><i class="fa fa-money fa-fw"></i>Currency</a>
                        </li>                        
						
						<li>
                            <a href="#"><i class="fas fa-user-edit fa-fw"></i>On Demand<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                
                                <!--
                                <li>
                                    <a href="<?php echo base_url().'backend/catdemand';?>"><i class="fa fa-eye"></i>Category</a>
                                </li>
                                -->

                                <li>
                                    <a href="<?php echo base_url().'backend/subcatdemand';?>"><i class="fa fa-eye"></i>Exams</a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url().'backend/ordersdem';?>"><i class="fa fa-eye"></i>Orders</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        

                        <li>
                        <a href="#"><i class="fa fa-money fa-fw"></i>Student Discount<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <li>
                                        <a href="<?php echo base_url().'backend/studiscount/adddiscount';?>"><i class="fa fa-plus"></i>Add</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url().'backend/studiscount/discountmanage';?>"><i class="fa fa-eye"></i>Manage</a>
                                    </li>
                                    
                            </ul>
                        </li>


						
                        <?php } ?>

                        <?php if($this->session->userdata('role')=='dataentry') { ?>
                        
                        <li>
                            <a href="#"><i class="fa fa-question fa-fw"></i>Exams<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/exams/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/exams/index/PAID';?>"><i class="fa fa-eye"></i>Paid Exams</a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url().'backend/exams/index/FREE';?>"><i class="fa fa-eye"></i>Free Exams</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <?php } ?>

                        <?php if($this->session->userdata('role')=='testimonials') { ?>
                        <li>
                            <a href="#"><i class="fas fa-user-edit fa-fw"></i>Admin Testimonials<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/Admintestimonials/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/Admintestimonials';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fas fa-file-alt fa-fw"></i>Blog<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url().'backend/blog/add';?>"><i class="fa fa-plus"></i>Add</a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url().'backend/blog';?>"><i class="fa fa-eye"></i>Manage</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php } ?>


                    </ul>
                </div>
            </div>
        </nav>
        
<style>
    .sorting_desc, .sorting_asc, .sorting{
        color: black !important;
    }
</style>        