    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage On Demand Exams</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                   On Demand Exams List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['e'] ) && $_GET['e'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Edited Successfully</h4>';
								
							}
							
							if( !empty( $_GET['del'] ) && $_GET['del'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Deleted Successfully</h4>';
								
							}
							
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
									
									<a href="<?php echo base_url(); ?>backend/subcatdemand/add"><button type="button" class="btn btn-primary btn-xs" id="ordsub" style="float: right;">Add</button></a>
									<br>
									<br>
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Exam Code</th>
                                                    <th>Exam Name</th>
													<th>Order</th>
													<th>Price</th>
													<th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               
                                               $srno = 0;
                                               
                                               foreach( $subcatdemand as $scat ){ 
                                               
                                               $srno++;
                                               ?>
											   
                <tr id="<?php echo $scat->scatd_id;?>" class="odd gradeX">
												
						<td><?php echo $srno;?></td>						
					<td><?php echo $scat->scatdcode;?></td>
					  <td><?php echo $scat->scatdname;?></td>
					  <td><?php echo $scat->scatdatorder;?></td>
					  <td><?php echo $scat->scatdprice;?></td>

					  <td><?php echo ( $scat->scatdstatus == 'A' ) ? 'Active' : 'Deactive' ;?></td>
						
					  <td>
					  
						<a href="<?php echo base_url(); ?>backend/subcatdemand/edit/<?php echo $scat->scatd_id; ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
						<a href="<?php echo base_url().'backend/subcatdemand/delete/'.$scat->scatd_id;?>" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash"></i></a>
						
					  </td>
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>