
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script>

	function cpFunction(id) {
	  /* Get the text field */
	  var copyText = document.getElementById("url"+id);

	  /* Select the text field */
	  copyText.select();

	  /* Copy the text inside the text field */
	  document.execCommand("copy");

	  /* Alert the copied text */
	  //alert("Copied the text: " + copyText.value);
	   
	   
	}

</script>

<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Subjects</h4>
      </div>
      <div class="col-sm-3">
        <div class="btn-group float-sm-right">
		
		  <a href="<?=base_url()?>_ishubabu_/subcategory/mfeatures"><button type="button" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-cog mr-1"></i> Manage Features</button></a>	
		
          <a href="<?=base_url()?>_ishubabu_/subcategory/add"><button type="button" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-cog mr-1"></i> Add Subjects</button></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
			
			<?php
			
			$del = $this->input->get('del');
			
			if( !empty( $del ) )
			{
				echo '<h5 class="form-header" style="color: green;">Deleted Successfully</h5>';
				
			}
			?>
			
			
              <table id="default-datatable" class="table table-bordered">
                <thead>
                  <tr>
				  <!--
					<th><input type="checkbox" class="checkall" name="checkall" id="checkall" value="1" /></th>
					-->
                    <th>Action</th>
                    
                    <th>Course</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Status</th>
                    <!-- <th>Publish</th> -->
                    <th>Created At</th>
					<th>Link</th>
                  </tr>
                </thead>                                                                         
                 <tbody>
                  <?php 
                  
			  foreach( $RowsSelected as $key => $value )
			  {
					  
				  ?>
                  
                    <tr>
					<!--
						<td><input type="checkbox" class="checksel" name="checksel" id="checksel" value="<?=$value->id?>"/></td>
						-->
                        <td><a href="<?=base_url()?>_ishubabu_/subcategory/edit/<?=$value->id?>" class="btn btn-success">Edit</a>
                        <a class="btn btn-danger" href="<?=base_url()?>_ishubabu_/subcategory/delete/<?=$value->id?>" onclick="return confirm('Do you want to delete this record?')">Delete</a>
                        <a class="btn btn-info" href="#" onclick="cpFunction(<?=$value->id?>)">Copy Link</a>
                        
                      </td>
					  
                      <td><?php echo $value->catname?></td>
                      <td><?php echo $value->code . ' ' . $value->name?></td>
                      <td><?php echo ( $value->type == 'P' ) ? 'Paid' : 'Free' ; ?></td>
                      <td><?php 
					  if( $value->status == 'A' )
							echo 'Active';
						else					  
							echo 'Deactivated'; 
						?></td>
                      <td><?php echo date('d-m-Y', strtotime( $value->datec )) ; ?></td>
                      <td><?php echo '<input type="text" value="'.base_url() . 'subcat/index/' . $value->id .'" id="url'. $value->id .'" readonly>'; ?></td>
                    </tr>
                    
                <?php 
				} 
				?>                     
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    
<script src="<?=base_url()?>assets/js/jquery.min.js"></script>	
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

<script>	
$(function() { 
	$('#default-datatable').DataTable(); 
	
	$('.checkall').click( function() {								
		if( $(this).is(':checked') )						
			$('.checksel').prop('checked', true);					
		else							
			$('.checksel').prop('checked', false);						
		});
		
	$('#delcont').click( function() {								
	var checksel = $('.checksel:checked').map(function() {return this.value;}).get().join(',');	res = confirm('Do you want to delete this record?');								
		if( res )
		{											
			$.ajax({									
				url:"<?php echo base_url(); ?>helpers/ajax_helper.php",						
				type: "POST",									
				data:{fun: 'delrec', checksel: checksel, tab: 'custom', col: 'id'},		success: function (res) {												
				console.log( res );												
					location.reload();					 				
				}							
			});									
		}						
	});
	
});
</script>	