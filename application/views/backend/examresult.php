<div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Students Exam Details</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Exams List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                   <div class="web_table">
                                    <div class="table-responsive">
                                    
                                        
                                        <table id="dataTables-example">

                                      
                                            <thead>
                                                <tr>
                                                    
                                                    <th>Exam Title</th>
                                                    <th>Started</th>
													<th>Finished</th>
													<th>Exam Type</th>
													<th>Score</th>
													<th>Result</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                               foreach( $exams as $stuexam ){ 
                                             
                                               ?>
                <tr id="<?php echo $stuexam->fk_stuid;?>" class="odd gradeX">
												
					<!-- <td><?php echo $stuexam->examtitle;?></td>						 -->
					<td><a href="<?= base_url() . 'backend/exams/displayresultbyid/'.$stuexam->eresid ?>"><?php echo $stuexam->examtitle;?></a></td>						
					<td><?php echo $stuexam->eres_exstarted;?></td>
					<td><?php echo $stuexam->eres_exfinished;?></td>
					<td><?php echo $stuexam->exam_type;?></td>
					<td><?php echo $stuexam->eres_score;?></td>
					<td><?php echo $stuexam->eres_status;?></td>
                </tr>
                <?php } ?>                    

                                            </tbody>
                                        </table>
                                       
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>