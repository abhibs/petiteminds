<div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Students</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Students List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['e'] ) && $_GET['e'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Edited Successfully</h4>';
								
							}
							
							if( !empty( $_GET['del'] ) && $_GET['del'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Deleted Successfully</h4>';
								
							}
							
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>Sr No.</th>
                                                    <th>Name</th>
                                                    <th>Student ID</th>
													<th>Mobile</th>
													<th>Email</th>
													<th>Email Verified</th>
													<th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               $srno = 0;
                                            
                                               foreach( $students as $stu ){ 
                                               $srno++;
                                               ?>
											   
                <tr id="<?php echo $stu->stuid;?>" class="odd gradeX">
												
						<td><?php echo $srno;?></td>						
					  <td><a href="<?= base_url() . 'backend/exams/displayresult/'.$stu->stuid ?>"><?php echo $stu->stuname;?></a></td>

					  <!-- <td><a href="<?= base_url() . 'backend/students/edit?stuid=' . $stu->stuid?>"><?php echo $stu->stuname;?></a></td> -->

					  <td><?php echo $stu->stuid;?></td>
					  <td><?php echo $stu->stumob;?></td>
					   <td><?php echo $stu->stuemail;?></td>

					    <td><?php echo ( $stu->email_veri == 'Y' ) ? 'Yes' : 'No' ;?></td>
					    
					     <td><?php echo date('d-m-Y', strtotime( $stu->studatec ) );?></td>
					  
					   <td>
					  
    						<a href="<?php echo base_url(); ?>backend/students/edit/<?php echo $stu->stuid; ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
    						<a href="<?php echo base_url().'backend/students/delete/'.$stu->stuid;?>" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash"></i></a>
    						
    					</td>    
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>