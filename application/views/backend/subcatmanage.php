    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Subcategory</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Subcategory List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
							if( !empty( $_GET['e'] ) && $_GET['e'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Edited Successfully</h4>';
								
							}
							
							if( !empty( $_GET['del'] ) && $_GET['del'] == 's' )
							{
								echo '<h4 class="form-header" style="color: green;">Deleted Successfully</h4>';
								
							}
							?>
								
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Name</th>
													<th>Subscription Fee</th>
													<th>Questions</th>
													<!--<th>Order</th>-->
                                                    <th>Meta Title</th>
                                                    <th>Meta Description</th>
													<th>Meta Keywords</th>
													<th>Status</th>
													<th>Category</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               $srno = 0;
                                               foreach($subs as $sub){ 
                                               $srno++;
                                               ?>
											   
                <tr id="<?php echo $sub->id;?>" class="odd gradeX">
												
					    <td><?php echo $srno;?></td>							
					  <td><?php echo $sub->name;?></td>
					  <td><?php echo $sub->yearsubsc;?></td>
					  <td><?php echo $sub->qcnt_display;?></td>
                      <td><?php echo $sub->meta_title;?></td>
                      <td><?php echo $sub->meta_description;?></td>
                      <td><?php echo $sub->meta_keywords;?></td>
					  <!--<td><?php echo $sub->scatorder;?></td>-->
					  
					  <td><?php echo ( $sub->status == 'A' ) ? 'Active' : 'Deactive' ;?></td>
						
						<td><?php echo $sub->catname;?></td>
						
					  <td>
					  
						<!--
						<a href="<?php echo base_url(); ?>backend/subcategory/view/<?php echo $sub->id; ?>" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i></a>
						-->
						
						<a href="<?php echo base_url(); ?>backend/subcategory/edit/<?php echo $sub->id; ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>
						<a href="<?php echo base_url().'backend/subcategory/delete/'.$sub->id;?>" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash"></i></a>
						
					  </td>
				  
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>