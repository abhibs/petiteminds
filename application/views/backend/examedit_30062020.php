<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Edit Exam</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form role="form" enctype="multipart/form-data" action="" method="post">

                      <div class="panel-heading">

                          Exam Details

                      </div>

                      <div class="panel-body">

                         <?php echo $this->session->flashdata('msg'); ?>
						 
							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Subcategory *</label>

                                      <?php
									  
					$opt = array( '' => 'Select' );
					
					if( !empty( $subcategory ) )
					{						
						foreach( $subcategory as $key => $value )
						{
							$opt[ $value->id ] = $value->name;
							
						}
						
					}
					
					echo form_dropdown('sid', $opt, @$exams[0]->sid, 'id="sid" class="form-control" required');
									  
									  ?>

                                      <span class="text-danger"><?php echo form_error('sid'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                  </div>

                              </div>


                            </div>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Title *</label>

                                      <input class="form-control" type="text" name="examtitle" value="<?=@$exams[0]->examtitle?>" required>

                                      <span class="text-danger"><?php echo form_error('examtitle'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Description *</label>
									  
									  <textarea name="examdesc" id="examdesc" class="form-control" required><?=@$exams[0]->examdesc?></textarea>

                                      <span class="text-danger"><?php echo form_error('examdesc'); ?></span>

                                  </div>

                              </div>


                            </div>

							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Available *</label>

									<?php

										$opt = array( "" => "Select", "Always" => "Always", "Never" => "Never" );
										
										echo form_dropdown( 'examavailable', $opt, @$exams[0]->examavailable, 'id="examavailable" class="form-control" required' );
										
									?>
									  

                                      <span class="text-danger"><?php echo form_error('examavailable'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Pass Rate *</label>
									  
									  <input class="form-control" type="number" name="exampassrate" value="<?=@$exams[0]->exampassrate?>" required>

                                      <span class="text-danger"><?php echo form_error('exampassrate'); ?></span>

                                  </div>

                              </div>


                            </div>
							
							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Backward Navigation *</label>

									<?php

										$opt = array( "" => "Select", "N" => "No", "Y" => "Yes" );
										
										echo form_dropdown( 'exambacknav', $opt, @$exams[0]->exambacknav, 'id="exambacknav" class="form-control" required' );
										
									?>
									  

                                      <span class="text-danger"><?php echo form_error('exambacknav'); ?></span>

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                      <label>Time Limit (Minutes) *</label>
									  
									  <input class="form-control" type="number" name="examtimelimit" value="<?=@$exams[0]->examtimelimit?>" required>

                                      <span class="text-danger"><?php echo form_error('examtimelimit'); ?></span>

                                  </div>

                              </div>


                            </div>
							
							<div class="row">

                              <div class="col-lg-6">

                                  <div class="form-group">
                                      
                                      <label>Exam Type *</label>

									<?php

										$opt = array( "" => "Select", "PAID" => "Paid", "FREE" => "Free" );
										
										echo form_dropdown( 'examtype', $opt, @$exams[0]->examtype, 'id="examtype" class="form-control" required' );
										
									?>
									  

                                      <span class="text-danger"><?php echo form_error('examtype'); ?></span>

                                       

                                  </div>

                              </div>

                              <div class="col-lg-6">

                                  <div class="form-group">

                                        
                                      <label>Total *</label>
									  
									  <input class="form-control" type="number" name="examtotal" value="<?=count(@$questions)?>" readonly>

                                      <span class="text-danger"><?php echo form_error('examtotal'); ?></span>
                                      

                                  </div>

                              </div>


                            </div>



                              </div>
                              
                        <div class="panel-heading" style="background-color: #334A58;color: white;">

                          Questions

                      </div>
					  
					  <div class="panel-body">
					  
						<div id="product-box">
						
							
							
<?php

$x = -1;

if( !empty( $questions ) )
{	
    
    
    foreach( $questions as $quekey => $queval )
	{
	    $x++;
	    
	    echo '<div class="product-box-row"><div class="row">								                                    <div class="col-md-10 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <label>Question</label>											<input class="form-control" type="text" name="question[]" value="'. htmlentities( $queval->question ) .'" required>';
	    
	    /*<br><input type="hidden" name="queimgold[]" value="'. $queval->queimg .'" />*/
	    
	    if( !empty( $queval->queimg ) )
	        echo '<img src="'. base_url() .'uploads/'. $queval->queimg .'" style="width: 150px;" />';
	    
	    /*<br><input type="file" class="form-control" name="queimg[]" value="" />*/
	    
	    echo '                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            									<label>Select Type</label>';	
	    
        	$opt = array( "MANS" => "Multiple Answer", "SANS" => "Single Answer" );
        	
        	echo form_dropdown( 'qtype[]', $opt, $queval->quetype, 'id="status" class="form-control" required' );
	    
	    
	    echo '											                                            <span class="text-danger">'. form_error('qtype').'</span>                                        </div>                                    </div>                                                                    </div>																<div class="row">									<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            											<input class="form-control" type="text" name="opt1[]" placeholder="Option 1" value="'. htmlentities( $queval->opt1 ) .'" required>												                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>									                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt2[]" placeholder="Option 2" value="'. htmlentities( $queval->opt2 ) .'" required>	                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt3[]" placeholder="Option 3" value="'. htmlentities( $queval->opt3 ) .'" >	                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt4[]" placeholder="Option 4" value="'. htmlentities( $queval->opt4 ) .'" >	                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt5[]" placeholder="Option 5" value="'. htmlentities( $queval->opt5 ) .'" >	                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">';
	    
	    $opt1_iscorr = ( $queval->opt1_iscorr == 'Y' ) ? 'selected' : '';
	    $opt2_iscorr = ( $queval->opt2_iscorr == 'Y' ) ? 'selected' : '';
	    $opt3_iscorr = ( $queval->opt3_iscorr == 'Y' ) ? 'selected' : '';
	    $opt4_iscorr = ( $queval->opt4_iscorr == 'Y' ) ? 'selected' : '';
	    $opt5_iscorr = ( $queval->opt5_iscorr == 'Y' ) ? 'selected' : '';
	    
	    
	    echo '<select name="answer'. $x .'[]" class="form-control" multiple required>												<option value="">Select Correct Answers</option>												<option value="opt1_iscorr" '. $opt1_iscorr .'>Option 1</option>												<option value="opt2_iscorr" '. $opt2_iscorr .'>Option 2</option>												<option value="opt3_iscorr" '. $opt3_iscorr .'>Option 3</option>												<option value="opt4_iscorr" '. $opt4_iscorr .'>Option 4</option>												<option value="opt5_iscorr" '. $opt5_iscorr .'>Option 5</option>											</select>                                            <span class="text-danger">'. form_error('question').'</span>                                        </div>                                    </div>                                                                    </div><div class="row"><div class="col-md-1 col-sm-3 col-xs-3">                                        <label for="" class="placeholder">Add</label>                                        <div class="btn btn-default add-btn">                                            <a href="javascript:void(0)"><span>+</span></a>                                        </div>                                    </div><div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div></div></div>';   
	}
}

?>


<!--
<div class="product-box-row">
						
                                <div class="row">
								
                                    <div class="col-md-10 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Question</label>

											<input class="form-control" type="text" name="question[]" required>

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
										
											<label>Select Type</label>
                                            
											<select name="qtype[]" class="form-control" required>
												
												<option value="MANS">Multiple Answer</option>
												<option value="SANS">Single Answer</option>
											</select>
											
                                            <span class="text-danger"><?php echo form_error('qtype'); ?></span>
                                        </div>
                                    </div>

                                    
                                </div>
								
								<div class="row">
									<div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            
											<input class="form-control" type="text" name="opt1[]" placeholder="Option 1" required>	
											

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>
									
                                    <div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="opt2[]" placeholder="Option 2" required>	

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>
									
									<div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="opt3[]" placeholder="Option 3" >	

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>
									
									<div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="opt4[]" placeholder="Option 4" >	

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>
									
									<div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="opt5[]" placeholder="Option 5" >	

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>
									
									<div class="col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group">
			
<?php
$x++;
?>										
										
											<select name="answer<?=$x?>[]" class="form-control" multiple required>
												<option value="">Select Correct Answers</option>
												<option value="opt1_iscorr">Option 1</option>
												<option value="opt2_iscorr">Option 2</option>
												<option value="opt3_iscorr">Option 3</option>
												<option value="opt4_iscorr">Option 4</option>
												<option value="opt5_iscorr">Option 5</option>
											</select>

                                            <span class="text-danger"><?php echo form_error('question'); ?></span>
                                        </div>
                                    </div>

                                    
                                </div>
								
								<div class="row">
                                    
                                    <div class="col-md-1 col-sm-3 col-xs-3">
                                        <label for="" class="placeholder">Add</label>
                                        <div class="btn btn-default add-btn">
                                            <a href="javascript:void(0)"><span>+</span></a>
                                        </div>
                                    </div>
									
									<div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div>
									
                                </div>
																
							</div>
							
-->							
							
							
                        </div>
					  
							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
					  
                      </div>
                              

                            </form>

                          </div>

                      </div>

                  </div>

              </div>

          </div>
          
<?php include('Footer.php')?>

<script type="text/javascript">

	$(document).ready(function() {
		  
		/* give upper x to lower x */
		<?php
		$x = $x - 1; 
		?>		 
		  
        var x = <?=$x?>; //Initial field counter is 1

        /* $('#product-box .add-btn').click(function(e) { */
			
		$("body").on("click", ".add-btn", function(e){	

            x++; //Increment field counter

            /*<br><input type="hidden" name="queimgold[]" value="" /> */

            var html = '<div class="product-box-row"><div class="row">								                                    <div class="col-md-10 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <label>Question</label>											<input class="form-control" type="text" name="question[]" required>                                           <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            									<label>Select Type</label>		<select name="qtype[]" class="form-control" required>												<option value="MANS">Multiple Answer</option>												<option value="SANS">Single Answer</option>											</select>											                                            <span class="text-danger"><?php echo form_error('qtype'); ?></span>                                        </div>                                    </div>                                                                    </div>																<div class="row">									<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            											<input class="form-control" type="text" name="opt1[]" placeholder="Option 1" required>												                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>									                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt2[]" placeholder="Option 2" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt3[]" placeholder="Option 3" >	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt4[]" placeholder="Option 4" >	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt5[]" placeholder="Option 5" >	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">											<select name="answer'+x+'[]" class="form-control" multiple required>												<option value="">Select Correct Answers</option>												<option value="opt1_iscorr">Option 1</option>												<option value="opt2_iscorr">Option 2</option>												<option value="opt3_iscorr">Option 3</option>												<option value="opt4_iscorr">Option 4</option>												<option value="opt5_iscorr">Option 5</option>											</select>                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>                                                                    </div><div class="row"><div class="col-md-1 col-sm-3 col-xs-3">                                        <label for="" class="placeholder">Add</label>                                        <div class="btn btn-default add-btn">                                            <a href="javascript:void(0)"><span>+</span></a>                                        </div>                                    </div><div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div></div></div>';      											$('#product-box').append(html);            
			
        });

        $('body').on('click', '#product-box .remove-btn', function(e) {
            e.preventDefault();
            $(this).closest('.product-box-row').remove();
            x--; //Decrement field counter
        });
        
        var inputcnt = $('input').length;
        var selcnt = $('select').length;
        var txtcnt = $('textarea').length;
        
        var totfields = inputcnt + selcnt + txtcnt;
        console.log( 'totfields:' + totfields );
		
	});
		
</script>