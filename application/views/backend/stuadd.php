<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Add Student</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="" method="POST" enctype="multipart/form-data" id="stufrm" name="stufrm">
		  
				<div class="panel-heading" style="">

					  Student Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Name</label>
								<div class="col-sm-10">
									<input id="stuname" name="stuname" type="text" placeholder="Name" value="<?php echo set_value( 'stuname' ); ?>" class="form-control">
									
									<?php echo form_error('stuname'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Mobile</label>
								<div class="col-sm-10">
									<input id="stumob" name="stumob" type="text" placeholder="Mobile" value="<?php echo set_value( 'stumob' ); ?>" class="form-control">
									
									<?php echo form_error('stumob'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Email</label>
								<div class="col-sm-10">
									<input id="stuemail" name="stuemail" type="text" placeholder="Email" value="<?php echo set_value( 'stuemail' ); ?>" class="form-control">
									
									<?php echo form_error('stuemail'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Password</label>
								<div class="col-sm-10">
									<input id="stupass" name="stupass" type="password" placeholder="Password" class="form-control">
									
									<?php echo form_error('stupass'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Confirm Password</label>
								<div class="col-sm-10">
									<input id="cnfmpass" name="cnfmpass" type="password" placeholder="Confirm Password" class="form-control">
									
									<?php echo form_error('cnfmpass'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Email Verified</label>
								<div class="col-sm-10">
									<?php
									
									$opt = array( "" => "Select", "Y" => "Yes", "N" => "No" );
	
									echo form_dropdown( 'email_veri', $opt, '', 'id="email_veri" class="form-control" required' );
									
									?>
									
									<?php echo form_error('email_veri'); ?>
								</div>
							</div>
							
                            <div id="product-box">
                                <div class="row" id="product-box-row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Courses</label>

<?php

			$opt = array( '' => 'Select' );
			if( !empty( $subcategory ) )
			{				
				foreach( $subcategory as $key => $value )
				{
					$opt[ $value->id ] = $value->name;							
				}				
			}
			echo form_dropdown('odetsid[]', $opt, set_value( 'odetsid' ), ' id="odetsid" class="form-control" ');

?>

                                            <span class="text-danger"><?php echo form_error('odetsid'); ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-9 col-xs-9">
                                        <div class="form-group">
                                            <label>Validity</label>
                                            <input class="form-control" type="date" name="odet_validity[]">
                                            <span class="text-danger"><?php echo form_error('odet_validity'); ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-1 col-sm-3 col-xs-3">
                                        <label for="" class="placeholder">Add</label>
                                        <div class="btn btn-default add-btn">
                                            <a href="javascript:void(0)"><span>+</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <script>
            $("#stufrm").validate({
                rules: {
                    stuname: {
                        required: true
                    },
                    stuemail: {
                        required: true,
                        email: true
                    },
                    stumob: {
                        required: true
                    },
                    stupass: {
                        required: true,
                        minlength: 4,
                        mypassword: true
                    },
                    cnfmpass: {
                        required: true,
                        minlength: 4,
						equalTo: "#stupass",
                        mypassword: true
						
                    }
                },
                messages: {
                    stuname: {
                        required: "Enter name"
                    },
                    stuemail: {
                        required: "Enter email",
                        email: "Enter valid email"
                    },
                    stumob: {
                        required: "Enter mobile number"
                    },
                    stupass: {
                        required: "Enter password"
                    },
                    cnfmpass: {
                        required: "Confirm password",
						equalTo: "Your password and confirm password do not match."
                    }
                }
            }); //validate

            $.validator.addMethod("mypassword", function(value, element) {
                return this.optional(element) || (value.match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%&*])[a-zA-Z0-9!@#$%&*]+$/));
            }, 'Password must contain at least one capital letter, numeric, alphabetic and special character.');
			
	// Restricts input for each element in the set of matched elements to the given inputFilter.
	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		  if (inputFilter(this.value)) {
			this.oldValue = this.value;
			this.oldSelectionStart = this.selectionStart;
			this.oldSelectionEnd = this.selectionEnd;
		  } else if (this.hasOwnProperty("oldValue")) {
			this.value = this.oldValue;
			this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		  }
		});
	  };
	  
	}(jQuery));			
			
		/*	
		$("#city").inputFilter(function(value) {			  
			return /^-?[a-zA-Z\s]*$/.test(value); 			
		});
		*/
		
		$("#mobile").inputFilter(function(value) {			  
			return /^-?\d*$/.test(value); 			
		});
		
        $(document).ready( function() {
            
            var x = 0; //Initial field counter is 1
    
            $('#product-box .add-btn').click(function(e) {
    
                x++; //Increment field counter
    
                var html = '<div class="row product-remove-row" ><div class="col-md-4 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <label>Courses</label><?php			
                $opt = array( '' => 'Select' );			
                if( !empty( $subcategory ) )			
                {								
                    foreach( $subcategory as $key => $value )				
                    {					
                        $opt[ $value->id ] = $value->name;											
                        
                    }							
                    
                }			
                
                
    			$prodsel = form_dropdown('odetsid[]', $opt, set_value( 'odetsid' ), ' id="odetsid" class="form-control" ');
    			
    			$prodsel = str_replace( "\r\n", "", $prodsel );
    			$prodsel = str_replace( "\r", "", $prodsel );
    			$prodsel = str_replace( "\n", "", $prodsel );
    			
    			echo $prodsel;
                
                ?><span class="text-danger"><?php echo form_error('odetsid'); ?></span>                                        </div>                                    </div>                                    <div class="col-md-3 col-sm-9 col-xs-9">                                        <div class="form-group">                                            <label>Validity</label>                                            <input class="form-control" type="date" name="odet_validity[]">                                            <span class="text-danger"><?php echo form_error('odet_validity'); ?></span>                                        </div>                                    </div>                                    <div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div>                                </div>';
    			
                $('#product-box').append(html);
    
            });
    
            $('body').on('click', '#product-box .remove-btn', function(e) {
                e.preventDefault();
                $(this).closest('.product-remove-row').remove();
                x--; //Decrement field counter
            });
            
        } );

    </script>