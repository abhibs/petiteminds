        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Blog</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                       <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Blog Details
                                </div>
                                <div class="panel-body">
                                     <?php echo $this->session->flashdata('msg'); ?>
                    <form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>backend/blog/update_blog" method="post">
                                      <div class="row">
                                        <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Title *</label>
                                                   <input type="hidden" value="<?php echo $blog[0]['id'];?>" name="id">
                                                    <input class="form-control" type="text" name="title" value="<?php echo $blog[0]['title'];?>">
                                                    <span class="text-danger"><?php echo form_error('title'); ?></span>
                                                </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <!--
                                          <div class="form-group">
                                                    <label>Image <small>(Dimension: 750X420, Size:100kb)</small></label>
                                                   <?php if($blog[0]['img_file']!='') {  ?>
                                   <img src="<?php echo base_url().'uploads/blogs/'.$blog[0]['img_file']; ?>" style="width:100px;height: 100px;">
                                   <i class="fa fa-trash" onClick="deleteImg(<?php echo $blog[0]['id']; ?>);"></i>
                                                 <?php } else { ?> 
                                           <input class="form-control" name="file" type="file" id="file" accept="image/*"> 
                                           <span class="text-danger" id="image_upload"></span>
                                           <?php } ?>
                                                </div>
                                                -->
                                                
                                                <div class="form-group">
                                                    <label>Image Text</label>
                                                   <input class="form-control" name="img_txt" type="text" id="img_txt" value="<?php echo $blog[0]['img_txt'];?>" required> 
                                                   <span class="text-danger"><?php echo form_error('img_txt'); ?></span>
                                            </div>
                                                
                                                
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Small Description</label>
                                                    <textarea class="form-control" name="small_edesc" placeholder="Max 150 characters"><?php echo $blog[0]['small_edesc'];?></textarea>
                                                </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <textarea class="form-control" name="edesc" id="edesc" rows="12"><?php echo $blog[0]['edesc'];?></textarea>
                                                </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-lg-6">
                                         <div class="form-group">
                                                    <label>Order *</label>
                                                    <input type="text" name="orderno" class="form-control" value="<?php echo $blog[0]['orderno'];?>">
                                                    <span class="text-danger"><?php echo form_error('orderno'); ?></span>
                                          </div>
                                        </div>
                                        <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                   <select name="status" class="form-control">
                                        <option value="1" <?php echo $blog[0]['status']==1?'selected="selected"':''; ?>>Yes</option>
                                        <option value="0" <?php echo $blog[0]['status']==0?'selected="selected"':''; ?>>No</option>
                                                    </select>
                                                </div>
                                        </div>
                                      </div>

                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </form>
                                        </div>

                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'edesc' );
</script>

<script src="<?php echo base_url();?>assets/admin/js/jquery.min.js"></script>
<script>
           function deleteImg(id)
            {
            $.ajax({

                    type: "POST",
                    url: "<?php echo base_url()?>backend/blog/delete_image",
                    data: {"id": id},
                    success: function (count) {
                     window.location = "<?php echo base_url() . 'backend/blog/edit/'; ?>"+id;
                    }
                });
            }
</script>