
<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Edit Subcategory</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  Subcategory Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Parent</label>
								<div class="col-sm-10">
									
									<?php
									
									if( !empty( $category ) )
									{
										$opt = array( '' => 'Select' );
										
										foreach( $category as $key => $value )
										{
											$opt[ $value->id ] = $value->catname;							
										}
										
										echo form_dropdown('cid', $opt, @$subcategory[0]->cid, 'id="cid" class="form-control" required');
									}
									
									?>
									
									<?php echo form_error('cid'); ?>
								</div>
							</div>
			
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="name" required value="<?php echo $subcategory[0]->name; ?>">
									
									<?php echo form_error('name'); ?>
								</div>
							</div>
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Exam Code</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="scat_sku" name="scat_sku" required value="<?php echo $subcategory[0]->scat_sku; ?>">
									
									<?php echo form_error('scat_sku'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Description</label>
								<div class="col-sm-10">
									<textarea name="scatdesc" id="scatdesc" class="form-control" required><?php echo $subcategory[0]->scatdesc; ?></textarea>
									
									<?php echo form_error('scatdesc'); ?>
								</div>
							</div>
							
							
							<!--
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Icon</label>
								<div class="col-sm-10">
									
									<?php
									
									/*
									if( !empty( $subcategory[0]->icon ) )
									{
										echo '<img src="'.base_url().'uploads/'.$subcategory[0]->icon.'" style="width: 150px;" />';
									}
									*/
									
									?>
								
									<input type="file" class="form-control" name="icon" id="icon" value="<?php echo set_value( 'icon' ); ?>" <?php if( empty( $subcategory[0]->icon ) ) echo 'required'; ?> />
									
									<?php echo form_error('icon'); ?>
								</div>
							</div>
							-->
							
							
							
							<!--
							
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Type</label>
								<div class="col-sm-10">
									<?php
									
									$opt = array( "P" => "Paid", "F" => "Free" );
	
									echo form_dropdown( 'type', $opt, set_value('type'), 'id="type" class="form-control" required' );
									
									?>
									
									<?php echo form_error('twoyearsubsc'); ?>
								</div>
							</div>
							-->
							
							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Subscription Fee</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="yearsubsc" name="yearsubsc" value="<?php echo $subcategory[0]->yearsubsc; ?>">
									
									<?php echo form_error('yearsubsc'); ?>
								</div>
							</div>
							
							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Question Count</label>
								<div class="col-sm-10">
									<input type="number" class="form-control" id="qcnt_display" name="qcnt_display" value="<?php echo $subcategory[0]->qcnt_display; ?>">
									
									<?php echo form_error('qcnt_display'); ?>
								</div>
							</div>
							
							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Order</label>
								<div class="col-sm-10">
									<input type="number" class="form-control" id="scatorder" name="scatorder" value="<?php echo $subcategory[0]->scatorder; ?>">
									
									<?php echo form_error('scatorder'); ?>
								</div>
							</div>
								
							<!--	
							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Two year subscription</label>
								<div class="col-sm-10">
									<input type="number" class="form-control" id="twoyearsubsc" name="twoyearsubsc" value="<?php echo set_value( 'twoyearsubsc' ); ?>">
									
									<?php echo form_error('twoyearsubsc'); ?>
								</div>
							</div>
							-->
							
							
							<!--
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">GST</label>
								<div class="col-sm-10">
									<input type="number" class="form-control" id="gst" name="gst" required min="1" value="<?php echo set_value( 'gst' ); ?>">
									
									<?php echo form_error('gst'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Credit</label>
								<div class="col-sm-10">
									<input type="number" class="form-control" id="credit" name="credit" required min="1" value="<?php echo set_value( 'credit' ); ?>">
									
									<?php echo form_error('credit'); ?>
								</div>
							</div>
							
							-->

							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Meta Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="metatitle" name="metatitle" value="<?php echo $subcategory[0]->meta_title; ?>">
									
									<?php echo form_error('metatitle'); ?>
								</div>
							</div>

							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Meta Description</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="metadescription" name="metadescription" value="<?php echo $subcategory[0]->meta_description; ?>">
									
									<?php echo form_error('metadescription'); ?>
								</div>
							</div>

							<div class="form-group row paidopt">
								<label for="input-5" class="col-sm-2 col-form-label">Meta Keywords</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="metakeywords" name="metakeywords" value="<?php echo $subcategory[0]->meta_keywords; ?>">
									
									<?php echo form_error('metakeywords'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Status</label>
								<div class="col-sm-10">
									<?php
									
									$opt = array( "" => "Select", "A" => "Active", "D" => "Deactivated" );
	
									echo form_dropdown( 'status', $opt, $subcategory[0]->status, 'id="status" class="form-control" required' );
									
									?>
									
									<?php echo form_error('status'); ?>
								</div>
							</div>
							
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>

<script type="text/javascript">

	$(document).ready(function() {
		  
        var x = 0; //Initial field counter is 1

        $('#product-box .add-btn').click(function(e) {

            x++; //Increment field counter

            var html = '<div class="product-box-row"><div class="row">								                                    <div class="col-md-10 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <label>Question</label>											<input class="form-control" type="text" name="question[]" required>                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            									<label>Select Type</label>		<select name="qtype[]" class="form-control" required>												<option value="MANS">Multiple Answer</option>												<option value="SANS">Single Answer</option>											</select>											                                            <span class="text-danger"><?php echo form_error('qtype'); ?></span>                                        </div>                                    </div>                                                                    </div>																<div class="row">									<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            											<input class="form-control" type="text" name="opt1[]" placeholder="Option 1" required>												                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>									                                    <div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt2[]" placeholder="Option 2" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt3[]" placeholder="Option 3" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt4[]" placeholder="Option 4" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">                                            <input class="form-control" type="text" name="opt5[]" placeholder="Option 5" required>	                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>																		<div class="col-md-2 col-sm-12 col-xs-12">                                        <div class="form-group">											<select name="answer'+x+'[]" class="form-control" multiple required>												<option value="">Select Correct Answers</option>												<option value="opt1_iscorr">Option 1</option>												<option value="opt2_iscorr">Option 2</option>												<option value="opt3_iscorr">Option 3</option>												<option value="opt4_iscorr">Option 4</option>												<option value="opt5_iscorr">Option 5</option>											</select>                                            <span class="text-danger"><?php echo form_error('question'); ?></span>                                        </div>                                    </div>                                                                    </div><div class="row"><div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div></div></div>';      
								
			$('#product-box').append(html);            
			
        });

        $('body').on('click', '#product-box .remove-btn', function(e) {
            e.preventDefault();
            $(this).closest('.product-box-row').remove();
            x--; //Decrement field counter
        });
		
	});
		
</script>

<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'scatdesc' );
</script>