
<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Edit Live Testimonial</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  Live Testimonial Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>
							
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Url</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="livetmonurl" required value="<?php echo @$livetmon[0]->livetmonurl; ?>">
									
									<?php echo form_error('livetmonurl'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Order</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="livetmonord" required value="<?php echo @$livetmon[0]->livetmonord; ?>">
									
									<?php echo form_error('livetmonord'); ?>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Show</label>
								<div class="col-sm-10">
									<?php
									
									$opt = array( "" => "Select", "Y" => "Yes", "N" => "No" );
	
									echo form_dropdown( 'livetmonshow', $opt, @$livetmon[0]->livetmonshow, 'id="livetmonshow" class="form-control" required' );
									
									?>
									
									<?php echo form_error('livetmonshow'); ?>
								</div>
							</div>
							
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>