<div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage Exam Discount</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Discount List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
									<?php echo $this->session->flashdata('msg'); ?>
									
							<?php
                            if( !empty( $_GET['delete'] ) && $_GET['delete'] == 'delete' )
							{
								echo '<h4 class="form-header" style="color: green;">Student Discount Deleted Successfully.</h4>';
								
							}
                            if( !empty( $_GET['edit'] ) && $_GET['edit'] == 'edit' )
							{
								echo '<h4 class="form-header" style="color: green;">Student Discount Edited Successfully.</h4>';
								
							}

                            if( !empty( $_GET['add'] ) && $_GET['add'] == 'add' )
							{
								echo '<h4 class="form-header" style="color: green;">Student Discount Added Successfully.</h4>';
								
							}
							?>
                                   <div class="web_table">
                                    <div class="table-responsive">
                                        <table id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Name</th>
                                                    <th>Course</th>
													<th>Discount Type</th>
													<th>Discount Amount</th>
                                                    <th>Status</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php 
                                               $srno = 0;
                                               foreach( $discountinfo as $dicount ){
                                               $srno++;
                                               ?>
											   <?php $cid_count = count(explode(",",$dicount->cid)); 
                                               $more_cnt = $cid_count-1; ?>
                <tr id="<?php echo $dicount->id;?>" class="odd gradeX">
												
						<td><?php echo $srno;?></td>						
					  <td><?php echo $dicount->stuname;?></td>

                      <?php if($cid_count == 1){ ?>
					  <td><?php echo $dicount->examtitle;?></td>
                      <?php }else{?>
                        <td><?php echo $dicount->examtitle."<strong> & </strong>"."<strong>$more_cnt</strong>"."<strong> more </strong>" ?></td>
                        <?php }?>
					  
                      <td>
                      <?php if($dicount->amounttype == 'percent'){?>
                        <?php echo "Percentage";?>
                        <?php } else if($dicount->amounttype == 'amount'){
                            echo "Amount";
                        }?> 
                      </td>
					 
                      <td>
                      <?php if($dicount->amounttype == 'percent'){?>
                         <?php echo $dicount->discpercent; echo " %"?>
                         <?php } else if($dicount->amounttype == 'amount'){
                               echo "$ "; echo $dicount->disamount; }?> 
					  <td>
                      
                      
                        <?php 
                            if($dicount->status == 0){
                                echo "Available";
                            } else if($dicount->status == 1){
                                echo "Claimed";
                            }
                        ?>
                      </td>
                      <td><?php echo date('d-m-Y', strtotime( $dicount->disc_date ) ); ?></td>
                      <td>
                      <a href="<?php echo base_url().'backend/studiscount/discountedit/'.$dicount->id ?>" class="btn btn-primary btn-circle"><i class="fa fa-edit"></i></a>

                      <a href="<?=base_url()?>backend/studiscount/discountdelete/<?=$dicount->id?>/delete" class="btn btn-primary btn-circle" onclick="return confirm('Do you want to delete this record?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
						
                </tr>
                                               <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>