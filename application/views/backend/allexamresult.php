<div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Students Exam Results</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Exams List
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                   <div class="web_table">
                                    <div class="table-responsive">
                                    
                                        
                                        <table id="dataTables-example">

                                      
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Student Name</th>
                                                    <th>Exam Title</th>
                                                    <th>Started</th>
													<th>Finished</th>
                                                    <th>Exam Type</th>
													<th>Score</th>
													<th>Result</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                $srno = 0;
                                                // print_r($allexams) ; exit;
                                                //var_dump($allexams); exit;
                                               foreach( $allexams as $stuexam ){ 
                                                
                                                $started = $stuexam->eres_exstarted;
                                                $date = new DateTime($started);
                                                $startednewdate = $date->format('d-m-Y h:i:sa');

                                                $finished = $stuexam->eres_exfinished;
                                                $date = new DateTime($finished);
                                                $finishednewdate = $date->format('d-m-Y h:i:sa');
                                                $srno++;
                                             
                                               ?>
                <tr id="<?php echo $stuexam->stuid;?>" class="odd gradeX">
                    <td><?php echo $srno;?></td>
                    <td><?php echo $stuexam->stuname;?></td>
					<td><a href="<?= base_url() . 'backend/exams/displayresultbyidinfo/'.$stuexam->eresid ?>"><?php echo $stuexam->examtitle;?></a></td>						
					<td><?php echo $startednewdate;?></td>
					<td><?php echo $finishednewdate;?></td>
                    <td><?php echo $stuexam->exam_type;?></td>
					<td><?php echo $stuexam->eres_score;?></td>
					<td><?php echo $stuexam->eres_status;?></td>
                </tr>
                <?php } ?>                    

                                            </tbody>
                                        </table>
                                       
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>