
<style>
.product-box-row{
	margin-bottom: 25px;
    border: 1px solid lightgray;
    padding: 5px;
}

.panel-heading{
    background-color: #334A58;color: white;
}
</style>

<div id="page-wrapper">

  <div class="container-fluid">

      <div class="row">

          <div class="col-lg-12">

              <h1 class="page-header">Edit Student Discount</h1>

          </div>

          <!-- /.col-lg-12 -->

      </div>

             <div class="row">

              <div class="col-lg-12">

                  <div class="panel panel-default">

          <form action="<?=base_url().'backend/studiscount/discounteditsave/'.$discountedit[0]->id?>/edit" method="POST" enctype="multipart/form-data">
		  
				<div class="panel-heading" style="">

					  Student Discount Details

				</div>

				<div class="panel-body">				  
			
							<?php
							if( !empty( $msg ) )
							{
								echo '<h4 class="form-header" style="color: green;">'. $msg .'</h4>';
								
							}
							?>

                            <div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Student ID</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="stuid" name="stuid" required value="<?php echo $discountedit[0]->stuid ?>">
									
									<?php echo form_error('name'); ?>
								</div>
							</div>
							<?php /*foreach($discountcourse as $desc){
								echo $desc->examtitle;
							} */ //print_r($discountcourse); 
							// echo $discountcourse[0]->examtitle;  
							// for($i=0; $i<=count($discountcourse); $i++){
								//echo $discountcourse[$i]->examtitle; echo '<br>'; ?>
							
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Course</label>
								<div class="col-sm-8">
								<div class="product-remove-row">
									
									<?php 
									// echo $opt[$discountcourse[$i]->sid] = $discountcourse[$i]->examtitle;
									for($i=0; $i<=count($discountcourse); $i++){ ?>
									<div class="product-remove-row">
									<?php if( !empty( $exams ) )
									{
										$opt[$discountcourse[$i]->sid] = $discountcourse[$i]->examtitle;
										$val = $discountcourse[$i]->sid;
										$opt = array( '' => 'Select' );
										foreach( $exams as $key => $value )
										{
											$opt[ $value->sid ] = $value->examtitle;
											// echo $value->sid; echo '<br>';							
										}

										echo form_dropdown('sid[]', $opt, $val, 'id="sid" class="form-control"');

										// echo form_dropdown('sid[]', $opt, set_value('sid'), 'id="sid" class="form-control" required');
										echo '<div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div id="remove" class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div>';
										
									}?>
									
									<?php echo form_error('sid'); ?>
									</div>
									<?php }?>
								</div>
								</div>
								<div class="col-md-1 col-sm-3 col-xs-3">
                                    <div class="btn btn-default add-btn">
                                        <a href="javascript:void(0)"><span>+</span></a>
                                    </div>
                                </div>
							</div> 
							<div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label"></label>
								<div class="col-sm-8" id="add-here">

								</div>
							</div>
							

							

                            <div class="form-group row">
								<label for="input-5" class="col-sm-2 col-form-label">Discount Type</label>
								<div class="col-sm-10">
									<select name="amounttype" id="amounttype" class="form-control" onclick="changeselect()" value="<?php echo $discountedit[0]->amounttype;?>">
										<option value="">Select</option>
										<option value="amount">Amount</option>
										<option value="percent">Percentage</option>
									</select>
									
								</div>
							</div>

                            <div class="form-group row" id="amountshow">
								<label for="input-5" class="col-sm-2 col-form-label" >Amount</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="amount" name="amount"  value="<?php echo $discountedit[0]->disamount; ?>">

									<?php echo form_error('name'); ?>
								</div>
							</div>

							<div class="form-group row" id="percentshow">
								<label for="input-5" class="col-sm-2 col-form-label">Percentage</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="percent" name="percent"  value="<?php echo $discountedit[0]->discpercent; ?>">
									</div>
							</div>
			
							

							<button type="submit" id="submit" class="btn btn-primary">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>
							
							
				</div>
							
						</form>



                          </div>

                      </div>

                  </div>

              </div>

          </div>
		  
<?php include('Footer.php')?>

<script>
	
	function changeselect(){
		var amount_type = $("#amounttype").val();
		if(amount_type == 'amount') {
			$("#amountshow").show();
			$("#percentshow").hide();
		}if(amount_type == 'percent') {
			$("#amountshow").hide();
			$("#percentshow").show();
		}if(amount_type == '') {
			$("#amountshow").hide();
			$("#percentshow").hide();
		}
	}
</script>
<script type="text/javascript">
  		$(document).ready(function() {
		   $("#amountshow").hide();
		   $("#percentshow").hide();
		});
	</script>

<script>
	var x = 0; //Initial field counter is 1
	$('.add-btn').click(function(e) {
		// alert("clicked");
		x++; //Increment field counter
		var html = '<div class="product-remove-row"><?php
		 $opt = array( '' => 'Select' );			
		 if( !empty( $exams ) )
			{
				$opt = array( '' => 'Select' );			
					foreach( $exams as $key => $value ){
						$opt[ $value->sid ] = $value->examtitle;				
					}
			}	
		 
		 
		$prodsel = form_dropdown('sid[]', $opt, set_value( 'sid' ), ' id="sid" class="form-control" ');
		$prodsel = str_replace( "\r\n", "", $prodsel );
    	$prodsel = str_replace( "\r", "", $prodsel );
    	$prodsel = str_replace( "\n", "", $prodsel );
    			
    	echo $prodsel;		

		?><span class="text-danger"><?php echo form_error('odetsid'); ?></span><div class="col-md-1 col-sm-3 col-xs-3"> <label for="" class="placeholder">Remove</label> <div class="btn btn-default remove-btn"> <a href="javascript:void(0)"><span>-</span></a> </div> </div>';

	$('#add-here').append(html);

	$('body').on('click', '.remove-btn', function(e) {
		// alert("clicked");
                e.preventDefault();
                $(this).closest('.product-remove-row').remove();
                x--; //Decrement field counter
        });

	});

	$('body').on('click', '#remove', function(e) {
		alert("Are you sure want to remove");
                e.preventDefault();
                $(this).closest('.product-remove-row').remove();
                x--; //Decrement field counter
        });
</script>

<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'scatdesc' );
</script>
