<div id="u_sidemenu">
    <ul>
        <li><a href="<?=base_url()?>referral"><i class="fa fa-sticky-note-o" aria-hidden="true"></i> Referral Form</a></li>        
        <li><a href="<?=base_url()?>referral/listref"><i class="fa fa-book" aria-hidden="true"></i> All Referrals</a></li>		
		<li><a href="<?=base_url()?>referral/commission"><i class="fa fa-money"></i> Commission Structure</a></li>
		<li><a href="<?=base_url()?>referral/bankdet"><i class="fa fa-bank"></i> Bank Details</a></li>
		<li><a href="<?=base_url()?>referral/change_pass"><i class="fa fa-key"></i> Change Password</a></li>
		<li><a href="<?=base_url()?>referral/rvteam"><i class="fa fa-user"></i> RvTeam</a></li>
		<li><a href="<?=base_url()?>referral/escalation"><i class="fa fa-ticket"></i> Escalation</a></li>
		
    </ul>
    <a href="#" id="close_sidenav"><i class="fa fa-bars" aria-hidden="true"></i></a>
</div>