<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <!-- <title>SAP</title> -->

 
  <title><?php echo ( isset($title) ? $title : ( (isset($subcategory) && $subcategory[0]->meta_title != "") ? $subcategory[0]->meta_title : 'SAP' ) ) ?></title>
 

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta property="og:title" content="SAP Certification Questions and Online Practice Exam" />
  <meta name="description" content="<?php echo ( isset($discription) ? $discription : ( (isset($subcategory) && $subcategory[0]->meta_description != "") ? $subcategory[0]->meta_description : '' ) ) ?>" />
  <meta name="keywords" content="<?php echo ( isset($keywords) ? $keywords : ( (isset($subcategory) && $subcategory[0]->meta_keywords != "") ? $subcategory[0]->meta_keywords : '' ) ) ?>" />
  <meta property="og:description" content="SAP Certification Sample Questions and Online Practice Exam for ERP, CRM, NetWeaver, BusinessObjects, HANA, SRM, Business One and Many Other SAP Solutions." />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://www.examroadmap.com/" />
 
  <link rel="icon" href="<?= base_url() ?>front/images/favicon.png">

  <!-- all css -->
  <link rel="stylesheet" href="<?= base_url() ?>front/css/bootstrap.css">
  <link rel="stylesheet" href="<?= base_url() ?>front/css/font-awesome.css">
  <link rel="stylesheet" href="<?= base_url() ?>front/css/style.css">
  <link rel="stylesheet" href="<?= base_url() ?>front/css/animate.css">
  <link rel="stylesheet" href="<?= base_url() ?>front/css/responsive.css">

  <!-- all js -->
  <script src="<?= base_url() ?>front/js/jquery.js"></script>
  <script src="<?= base_url() ?>front/js/bootstrap.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="<?= base_url() ?>front/js/main.js"></script>

  <style>
    .error {
      color: red;
    }
  </style>

  <script src="<?= base_url() ?>front/js/wow.min.js"></script>
  <script>
    new WOW().init();
  </script>
<meta name="google-site-verification" content="zCiBtihdxHL4xsHeb0ZddF3kheOfeTFddBqFf-zD1mE" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-92RXCZ7E23"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-92RXCZ7E23');
</script>

</head>

<body>