<div class="top-bar">
<div class="container" style="padding:15px;">
    <div class="row" style="position:relative">

		<div class="translate-wrap">
			<div id="google_translate_element" class="google-trans"></div>
		</div>
		<script type="text/javascript">
			function googleTranslateElementInit() {
				new google.translate.TranslateElement({ pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE }, 'google_translate_element');
			}
		</script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		
        <div class="col-xs-5 col-sm-5 col-md-4 text-center">
             <a class="navbar-brand" href="<?=base_url()?>" >
				<img src="<?=base_url()?>front/images/logo.png" alt="" class="img-responsive">
			</a>
    		<div class="row" style="padding:0 0; letter-spacing:0;font-size:15px;margin:5px 0 0;"><strong><b>#1 SAP Certification Exam Roadmap</b></strong></div>
        </div>
		<!--
        <div class="col-md-3">
            <div class="top-contact-box">
                <i class="fas fa-phone-alt"></i>
                 <div class="top-contact-box-right">
                <h4>Phone</h4>
                <a href="tel:1234567890">1234567890</a>
                </div>
            </div>
        </div>
		-->
        <!--<div class="col-md-3">-->
        <!--    <div class="top-contact-box">-->
        <!--        <i class="fas fa-envelope-open-text"></i>-->
        <!--         <div class="top-contact-box-right">-->
        <!--        <h4>Email</h4>-->
        <!--        <a href="mailto:info@saprep.com">info@saprep.com</a>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div> -->
		
		<div class="col-xs-7 col-sm-7 col-md-8 text-right">
            <div class="top-contact-box">

                <div class="top-contact-box-right">
				
				<?php
				
				$loggedid = $this->session->userdata('loggedid');						
				$logstuname = $this->session->userdata('logstuname');
				
				if( !empty( $loggedid ) )			
				{
				?>				
					 <a href="<?=base_url()?>profile" class="top-bar-content">Welcome <?=$logstuname?></a>
				 
					<!--<a href="<?=base_url()?>profile/premexam">My Premium Exam</a>-->
				 
					<a href="<?=base_url()?>login/logout">Logout</a>
				
				<?php
				}
				else			
				{
				?>	
					<a href="<?=base_url()?>login"><i class="fas fa-sign-in-alt"></i> Login</a>
					 
					<a href="<?=base_url()?>register"><i class="fas fa-user-plus"></i> Register</a>
				

				<?php
				}
				?>		
					
                </div>
            </div>
        </div> 
        
        <!--<div class="col-md-3">-->
        <!--    <div class="cart-box-top">-->
        <!--        <i class="fas fa-shopping-cart"></i>-->
        <!--         <div class="top-contact-box-right"> -->
        <!--        <a href="<?=base_url()?>packages/cart"><h4>Cart(0)</h4></a>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
		

    </div>
</div>
</div>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand  hidden-xs hidden-sm hidden-md hidden-lg hidden-xl" href="<?=base_url()?>" >
        <img src="<?=base_url()?>front/images/logo.png" alt="">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="<?=base_url()?>">Home</a></li>
        
      
<?php

$category = $this->GeneralModel->GetSelectedRows($table = 'category', $limit = '', $start = '', $columns = '', $orderby ='catorder', $key = array( 'category.status' => 'A' ), $search = '');

if( !empty( $category ) )
{	
	foreach( $category as $catkey => $catval )
	{
		echo '<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">'. $catval->catname .' <span class="caret"></span></a><ul class="dropdown-menu">';
			  
		$subcategory = $this->GeneralModel->GetSelectedRows($table = 'subcategory', $limit = '', $start = '', $columns = '', $orderby ='subcategory.scatorder', $key = array( 'subcategory.cid' => $catval->id, 'subcategory.status' => 'A' ), $search = '');

		if( !empty( $subcategory ) )
		{	
			foreach( $subcategory as $scatkey => $scatval )
			{
				echo '<li><a href="'.base_url().'packages/view/'. $scatval->id .'">'. $scatval->name .'</a></li>';
			}
			
		}		
			  
		echo '</ul></li>';	  
		
	}
}

?>
		
		<li><a href="<?=base_url()?>ondemand">On Demand</a></li>
		
		<?php		
						
		$otp = $this->session->userdata('otp');	
/* 
echo "<hr><pre>uid: ";
var_dump( $uid );
echo "<hr><pre>otp: ";
var_dump( $otp );
exit; */

					
		if( !empty( $loggedid ) && empty($otp) )			
		{		
		?>

			<!--
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Profile <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="<?=base_url()?>profile">View Profile</a></li>
				
				<li><a href="<?=base_url()?>login/logout">Logout</a></li>
			  </ul>
			</li>
			
			<li><a href="javascript:void(0)"> Welcome <?=$logstuname?></a></li>
			-->
		
		<?php 
		}			
		else			
		{		
		?>
		
			
			<!--
			<li><a href="<?=base_url()?>login">Login</a></li>
			<li><a href="<?=base_url()?>register">Register</a></li>
			-->
		
		<?php 
		}		
		?>
		
      </ul>
    </div>
  </div>
</nav>