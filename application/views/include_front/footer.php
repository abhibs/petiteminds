<div id="copyright">
    <div class="container">
        <ul class="footer-menu-list">
           <li> <a href="<?=base_url()?>moneybackguarantee" target="_blank"> Money Back Guarantee </a> | </li>
           <li> <a href="<?=base_url()?>aboutus" target="_blank"> About Us </a> | </li>    
           <li> <a href="<?=base_url()?>contact" target="_blank"> Contact Us </a> | </li>   
           <li> <a href="<?=base_url()?>faq" target="_blank"> FAQ </a> | </li> 
           <li> <a href="<?=base_url()?>terms" target="_blank"> Terms & Conditions </a> | </li>   
           <li> <a href="<?=base_url()?>privacy" target="_blank"> Privacy Policy </a> | </li> 
           <li> <a href="<?=base_url()?>blogs" target="_blank"> Blog </a> | </li>
          <li> <a href="<?=base_url()?>testimonial" target="_blank"> Testimonial </a>  </li> 
        </ul>

        <div class="text-center">
            <ul class=" footer-social-list">
                <li><a href="https://g.page/petiteminds?gm" target="_blank"><i class="fab fa-google"></i></a></li>
                <li><a href="https://www.facebook.com/petiteminds.exam" target="_blank"><i class="fab fa-facebook"></i></a></li>
                <li><a href="https://in.linkedin.com/company/petiteminds" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                <li><a href="https://twitter.com/petiteminds" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <li><a href="https://www.instagram.com/petiteminds/" target="_blank"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>

	
	<div class="container">
        <p>© 2017 - 2021 PETITEMINDS. All Rights Reserved  
        </p>
        
    
        <p style="text-align: left;padding-left: 10px;font-size: 13px;padding-top: 0px !important;padding-bottom: 5px !important;">Disclaimer:<br><br>
        Petiteminds offers only Probable Questions and Answers.<br><br>
        Petiteminds offer learning materials and practice tests created by subject matter experts to assist and help learner prepare for those exams. <br><br>
        All Certification Brands used on the website are owned by the respective brand owners or respective exam board.<br><br>
        Petiteminds does not own or claim any ownership on any of the brands.<br><br>
        The site www.Petiteminds.com is in no way affiliated with nay board.<br><br>
        Petiteminds is a Registered Trade Mark. </p>
        
    </div>
        

	
</div>

<script> 
var BASE_URL;
var WEBSITE_TOKEN;
(function(d,t) {
  BASE_URL = 'https://chat-prod.quickwork.co';
  WEBSITE_TOKEN = 'eQFjkeui691rMAKCCGX6UqVS';
  var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
  g.src= BASE_URL + "/packs/js/sdk.js";
  s.parentNode.insertBefore(g,s);
  g.onload=function(){
    window.qwcSDK.run({
      websiteToken: WEBSITE_TOKEN,
      baseUrl: BASE_URL
    })
  }
})(document,"script");
</script> 

<script type="application/ld+json">
{
   "@context": "https://schema.org",
   "@type": "LocalBusiness",
   "name": "ExamRoadmap",
   "image": "https://www.examroadmap.com/front/images/logo.png",
   "@id": "",
   "url": "https://www.examroadmap.com/",
   "telephone": "900-454-2428",
   "address": {
     "@type": "PostalAddress",
     "streetAddress": "A-702 Millie Enclave, Marve Road, Orlem",
     "addressLocality": "Mumbai",
     "addressRegion": "Maharashtra",
     "postalCode": "400064",
     "addressCountry": "IN"
   },
   "openingHoursSpecification": {
     "@type": "OpeningHoursSpecification",
     "dayOfWeek": [
       "Monday",
       "Tuesday",
       "Wednesday",
       "Thursday",
       "Friday",
       "Saturday",
       "Sunday"
     ],
     "opens": "00:00",
     "closes": "23:59"
   },
   "sameAs": [
     "https://www.facebook.com/examroadmap.exam",
     "https://twitter.com/examroadmap",
     "https://www.instagram.com/examroadmap/",
     "https://in.linkedin.com/company/examroadmap",
     "https://www.examroadmap.com/"
   ]
}
</script>

</body>

</html>