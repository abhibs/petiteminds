<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial extends CI_Controller {

	public function __construct() {

        parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
		$this->load->library('cart');
	}
	
	public function index(  )
	{		
	
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		// $join_ar = array(
		// 					0 => array(
		// 						"table" => "students",
		// 						"condition" => "testimonial.tmby = students.stuid",
		// 						"type" => "inner"
		// 					)
		// 				);		
	
		// $data['testimonial'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'testimonial', $limit = '', $start = '', $columns = 'testimonial.*, students.stuname', $orderby = 'testimonial.tmdatec desc', $key = array( 'testimonial.tm_show' => 'Y' ), $search = '', $join_ar, $group_by = '' );


		$data['testimonial'] = $this->db->query("SELECT * FROM gettestomonials WHERE tm_show='Y'")->result();

			//print_r($data); exit;
		/* print "<hr><pre>".$this->db->last_query();exit; */

		$data['title'] = "Testimonials | ExamRoadmap.com";
        $data['discription'] = "Read our successful testimonial by clients.";
        $data['keywords'] = "Testimonials";
					
		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');	
		$this->load->view('testimonial', $data);
		$this->load->view('include_front/footer');	
	}	
	
	public function view( $tmid )
	{		
	
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		// $join_ar = array(
		// 					0 => array(
		// 						"table" => "students",
		// 						"condition" => "testimonial.tmby = students.stuid",
		// 						"type" => "inner"
		// 					)
		// 				);		
	
		// $data['testimonial'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'testimonial', $limit = '', $start = '', $columns = 'testimonial.*, students.stuname', $orderby = '', $key = array( 'tmid' => $tmid ), $search = '', $join_ar, $group_by = '' );
		$data['testimonial'] = $this->db->query("SELECT * FROM gettestomonials where tmid='$tmid'")->result();
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
		$data['title'] = "Testimonials | examroadmap.com";
					
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('testimonview', $data);
		$this->load->view('include_front/footer');	
	}
	
}

?>