<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set("memory_limit", "-1");
set_time_limit(0);
ini_set('max_input_vars', '10000');

class Exams extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper(array('form','url', 'security','date','function'));
        $this->load->library(array('session', 'form_validation', 'email','pagination'));
        $this->load->model('GeneralModel');

/*         if($this->session->userdata('adminid') == ''){
            redirect('home');
        } */
    
    }

    public function index( $examid = '', $scid = '', $orderids = '' ) {
		
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		if( empty( $loggedid ) )
		{
			$this->session->set_userdata('redtoexam', $examid);
			redirect('login');
		}
		else
		{
			$join_ar = array(
								0 => array(
									"table" => "orders",
									"condition" => "order_detail.fk_ordid = orders.ordid",
									"type" => "inner"
								),
								1 => array(
									"table" => "exams",
									"condition" => "order_detail.odetsid = exams.sid",
									"type" => "inner"
								)
							);

			$order_detail = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'exams.examid' => $examid, 'order_detail.odet_validity >=' => date('Y-m-d') ), $search = '', $join_ar, $group_by = '' );

			$ordervalid = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'exams.examid' => $examid, 'order_detail.odetsid' => $scid, 'orders.ordid' => $orderids, 'order_detail.odet_validity <=' => date('Y-m-d') ), $search = '', $join_ar, $group_by = '' );
			
			if( !empty( $order_detail ) )
			{						
				/*
				if purchased and in validity date
				*/
		
				$arr_page['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', array( 'examid' => $examid ));
				
				$subcategory = $this->GeneralModel->GetInfoRow($table = 'subcategory', array( 'subcategory.id' => @$arr_page['exams'][0]->sid ));
				
				if( $examid == ''  || empty( $arr_page['exams'] ) ){
					redirect('home');
				}
				
				$limit = '';
				$orderby = '';
				
				$orderby = 'RAND()';
				
				if( !empty( $subcategory ) )
    			{
    			    $limit = intval( @$subcategory[0]->qcnt_display );
    			}
    			
				/*RAND()*/

				
				$arr_page['questions']=$this->GeneralModel->GetSelectedRows($table = 'questions', $limit, $start = '', $columns = '', $orderby, $key = array( 'fk_examid' => $examid ), $search = '');
				
				/* print "<hr><pre>".$this->db->last_query();exit; */

				$arr_page['page'] = 'exams';
				$arr_page['exam_type'] = 'FULL';
				$arr_page['order_detail'] = $ordervalid;
				$data['title'] = "Personal Exam - Full | examroadmap.com";

				$this->load->view('include_front/head',$data);
				$this->load->view('include_front/nav');	
				$this->load->view('takeexam_1',$arr_page); 
				$this->load->view('include_front/footer');
		
			}
			else
			{

				redirect( base_url('packages/view/' . $scid) );
				// redirect( base_url('view/' . $scid) );
			}
		}
        
    }
    
    public function mini( $examid = '', $scid = '', $orderids = '' ) {

		//echo $examid."<br>".$scid; exit;
        
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		if( empty( $loggedid ) )
		{
			$this->session->set_userdata('redtominexam', $examid);
			redirect('login');
		}
		else
		{
			$join_ar = array(
								0 => array(
									"table" => "orders",
									"condition" => "order_detail.fk_ordid = orders.ordid",
									"type" => "inner"
								),
								1 => array(
									"table" => "exams",
									"condition" => "order_detail.odetsid = exams.sid",
									"type" => "inner"
								)
							);

			$order_detail = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'exams.examid' => $examid, 'order_detail.odetsid' => $scid, 'order_detail.odet_validity >=' => date('Y-m-d') ), $search = '', $join_ar, $group_by = '' );

			$ordervalid = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'exams.examid' => $examid, 'order_detail.odetsid' => $scid, 'orders.ordid' => $orderids, 'order_detail.odet_validity <' => date('Y-m-d') ), $search = '', $join_ar, $group_by = '' );
            
            //print_r($ordervalid); exit;

		
			if( !empty( $order_detail ) )
			{						
				/*
				if purchased and in validity date
				*/

				$arr_page['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', array( 'examid' => $examid ));
				
				$subcategory = $this->GeneralModel->GetInfoRow($table = 'subcategory', array( 'subcategory.id' => @$arr_page['exams'][0]->sid ));
				
				if( $examid == ''  || empty( $arr_page['exams'] ) ){
					redirect('home');
				}
				
				$limit = '';
				$orderby = '';
				
				$orderby = 'RAND()';
				
				if( !empty( $subcategory ) )
    			{
    			   $limit = floor( intval( @$subcategory[0]->qcnt_display ) / 2 );

    			}
    			
				/*RAND()
				
				,'queid' => 122450
				*/
				
				$arr_page['questions']=$this->GeneralModel->GetSelectedRows($table = 'questions', $limit, $start = '', $columns = '', $orderby, $key = array( 'fk_examid' => $examid ), $search = '');

/*				
				$arr_page['questions'] = $this->db->query( "SELECT *
FROM `questions`
WHERE `fk_examid` = '109'
AND ( `queid` = 122450 or `queid` = 122564 or `queid` = 122573 )
ORDER BY RAND()
 LIMIT 40" )->result();
 */
				/* print "<hr><pre>".$this->db->last_query();exit; */

				$arr_page['page'] = 'exams';
				$arr_page['exam_type'] = 'MINI';
				$arr_page['order_detail'] = $ordervalid;
				$data['title'] = "Personal Exam - Mini | examroadmap.com";

				//print_r($arr_page['order_detail']); exit;

				$this->load->view('include_front/head',$data);
				$this->load->view('include_front/nav');	
				$this->load->view('takeexam_1',$arr_page); 
				$this->load->view('include_front/footer');
		
			}
			else
			{
				/*
				redirect to package
				*/
				redirect( base_url('packages/view/' . $scid) );
			}
		}
        
    }
	
    public function free( $examid = '', $scid = '' ) {
		
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
						
		if( empty( $loggedid ) )
		{
			$this->session->set_userdata('redtoexamfree', $examid);
			redirect('login');
		}
		else
		{

			$arr_page['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', array( 'examid' => $examid, 'examtype' => 'FREE' ));
			
			if( $examid == ''  || empty( $arr_page['exams'] ) ){
				redirect('home');
			}
			
			/*RAND()*/
			
			$arr_page['questions'] = $this->GeneralModel->GetSelectedRows($table = 'questions', $limit = '', $start = '', $columns = '', $orderby = 'RAND()', $key = array( 'fk_examid' => $examid ), $search = '');
			
			/* print "<hr><pre>".$this->db->last_query();exit; */

			$arr_page['page'] = 'exams';
            $arr_page['exam_type'] = 'FREE';
			$data['title'] = "Personal Exam - Free | examroadmap.com";
            
			$this->load->view('include_front/head',$data);
			$this->load->view('include_front/nav');	
			$this->load->view('takeexam_1',$arr_page); 
			$this->load->view('include_front/footer');
		
		}
		
    }
	
    public function submit() {
		
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		/*
        echo "<hr><pre>post: ";
		var_dump( $_POST );
		exit;
		*/
		
		$queids = $this->input->post('queids');
		$examid = $this->input->post('examid');
		$exam_type = $this->input->post('exam_type');
		
		$arr_page['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', array( 'examid' => $examid ));	

		if( empty( $arr_page['exams'] ) )
		{
			redirect('home');
		}	
		
		/*
		$questions = $this->GeneralModel->GetSelectedRows($table = 'questions', $limit = '', $start = '', $columns = '', $orderby = 'queid', $key = array( 'fk_examid' => $examid ), $search = '');
		*/
		
		$in_queids = '';
		
		if( !empty( $queids ) )
		{
		    $in_queids = 'and queid in( ' .  implode(",", $queids ) . ' )';
		}
		else
		{
		    $in_queids = '';
		}
		
		$questions = $this->db->query( "SELECT * FROM `questions` WHERE`fk_examid`= '$examid' $in_queids ORDER BY `queid`" )->result();

		/*print "<hr><pre>".$this->db->last_query();exit;*/
		
		$resques = '';
		$points = 0;
		$totque = count( $questions );
		
		$resdet = array();
		
		if( !empty( $questions ) )
		{	
			/* get questions */
			$backnav = $arr_page['exams'][0]->exambacknav;
			$showback = false;
			$shownext = true;
			$hidepanel = '';
			$srno = 0;	
			
			foreach( $questions as $quekey => $queval )
			{		

				$srno++;

				if( $quekey )
				{
					$showback = true;
					$shownext = ( ( count($questions) - 1 ) == $quekey ) ? false : true;
					$hidepanel = 'style="display: none;"';
				}
			
/* 				echo "<hr><pre>queid: ";
				var_dump( $queval->queid );	 */
				
				$sel_opt1 = $this->input->post('opt1_' . $queval->queid);
				$sel_opt2 = $this->input->post('opt2_' . $queval->queid);
				$sel_opt3 = $this->input->post('opt3_' . $queval->queid);
				$sel_opt4 = $this->input->post('opt4_' . $queval->queid);
				$sel_opt5 = $this->input->post('opt5_' . $queval->queid);
				$sel_opt = $this->input->post('opt_' . $queval->queid);
				
				$sel_correct_cnt = 0;
				$opt1_chk = $opt2_chk = $opt3_chk = $opt4_chk = $opt5_chk = '';
				$opt1_rad = $opt2_rad = $opt3_rad = $opt4_rad = $opt5_rad = '';
				$is_opt1_corr = $is_opt2_corr = $is_opt3_corr = $is_opt4_corr = $is_opt5_corr = false;
				
				if( ( $queval->opt1_iscorr == 'Y' && !empty( $sel_opt1 ) ) || ( $queval->opt1_iscorr == 'Y' && $sel_opt == 'opt1_iscorr' ) )
				{
					$sel_correct_cnt++;
					$is_opt1_corr = true;
				}
				
				if( $queval->opt2_iscorr == 'Y' && !empty( $sel_opt2 ) || ( $queval->opt2_iscorr == 'Y' && $sel_opt == 'opt2_iscorr' ) )
				{
					$sel_correct_cnt++;
					$is_opt2_corr = true;
				}
				
				if( $queval->opt3_iscorr == 'Y' && !empty( $sel_opt3 ) || ( $queval->opt3_iscorr == 'Y' && $sel_opt == 'opt3_iscorr' ) )
				{
					$sel_correct_cnt++;
					$is_opt3_corr = true;
				}
				
				if( $queval->opt4_iscorr == 'Y' && !empty( $sel_opt4 ) || ( $queval->opt4_iscorr == 'Y' && $sel_opt == 'opt4_iscorr' ) )
				{
					$sel_correct_cnt++;
					$is_opt4_corr = true;
				}
				
				if( $queval->opt5_iscorr == 'Y' && !empty( $sel_opt5 ) || ( $queval->opt5_iscorr == 'Y' && $sel_opt == 'opt5_iscorr' ) )
				{
					$sel_correct_cnt++;
					$is_opt5_corr = true;
				}
				
				$resdet[ $queval->queid ] = '';
				
				if( !empty( $sel_opt1 ) || $sel_opt == 'opt1_iscorr' )
				{
					$opt1_chk = 'checked';
					$opt1_rad = 'checked';
					$opt2_rad = $opt3_rad = $opt4_rad = $opt5_rad = '';
					
					$resdet[ $queval->queid ] .= 'opt1_chk,';
					
				}
				if( !empty( $sel_opt2 ) || $sel_opt == 'opt2_iscorr' )
				{
					$opt2_chk = 'checked';
					$opt2_rad = 'checked';
					$opt1_rad = $opt3_rad = $opt4_rad = $opt5_rad = '';
					$resdet[ $queval->queid ] .= 'opt2_chk,';
				}
				if( !empty( $sel_opt3 ) || $sel_opt == 'opt3_iscorr' )
				{
					$opt3_chk = 'checked';
					$opt3_rad = 'checked';
					$opt1_rad = $opt2_rad = $opt4_rad = $opt5_rad = '';
					$resdet[ $queval->queid ] .= 'opt3_chk,';
				}
				if( !empty( $sel_opt4 ) || $sel_opt == 'opt4_iscorr' )
				{
					$opt4_chk = 'checked';
					$opt4_rad = 'checked';
					$opt1_rad = $opt2_rad = $opt3_rad = $opt5_rad = '';
					$resdet[ $queval->queid ] .= 'opt4_chk,';
				}
				if( !empty( $sel_opt5 ) || $sel_opt == 'opt5_iscorr' )
				{
					$opt5_chk = 'checked';
					$opt5_rad = 'checked';
					$opt1_rad = $opt2_rad = $opt3_rad = $opt4_rad = '';
					$resdet[ $queval->queid ] .= 'opt5_chk,';
				}
				
				$resdet[ $queval->queid ] = substr( trim( $resdet[ $queval->queid ] ), 0, -1 );
				
				
				/* if type multiple and all selected then answer is invalid */
				/* if( $queval->quetype == 'MANS' && ( ( !empty( $queval->opt1 ) && !empty( $sel_opt1 ) ) && ( !empty( $queval->opt2 ) && !empty( $sel_opt2 ) ) && ( !empty( $queval->opt3 ) && !empty( $sel_opt3 ) ) && ( !empty( $queval->opt4 ) && !empty( $sel_opt4 ) ) && ( !empty( $queval->opt5 ) && !empty( $sel_opt5 ) ) ) )
				{
					$sel_correct_cnt--;
				} */	

				if( $queval->quetype == 'MANS' && ( ( array_key_exists( 'opt1_'.$queval->queid, $_POST ) && !empty( $queval->opt1 ) ) && ( array_key_exists( 'opt2_'.$queval->queid, $_POST ) && !empty( $queval->opt2 ) ) && ( array_key_exists( 'opt3_'.$queval->queid, $_POST ) && !empty( $queval->opt3 ) ) && ( array_key_exists( 'opt4_'.$queval->queid, $_POST ) && !empty( $queval->opt4 ) ) && ( array_key_exists( 'opt5_'.$queval->queid, $_POST ) && !empty( $queval->opt5 ) ) ) )
				{
					$sel_correct_cnt++;
				}
				else if( $queval->quetype == 'MANS' && ( ( array_key_exists( 'opt1_'.$queval->queid, $_POST ) && !empty( $queval->opt1 ) ) && ( array_key_exists( 'opt2_'.$queval->queid, $_POST ) && !empty( $queval->opt2 ) ) && ( array_key_exists( 'opt3_'.$queval->queid, $_POST ) && !empty( $queval->opt3 ) ) && ( array_key_exists( 'opt4_'.$queval->queid, $_POST ) && !empty( $queval->opt4 ) ) && ( empty( $queval->opt5 ) ) ) )				
				{
					$sel_correct_cnt++;
				}	
				
				
				if( $opt1_chk == 'checked' )				
					$sel_opt_cnt++;
	    		if( $opt2_chk == 'checked' )				
					$sel_opt_cnt++;	
				if( $opt3_chk == 'checked' )				
					$sel_opt_cnt++;
				if( $opt4_chk == 'checked' )				
					$sel_opt_cnt++;	
				if( $opt5_chk == 'checked' )				
					$sel_opt_cnt++;
					
				/* if more than correct question selected then minus the count */	
				/*
				if( $sel_opt_cnt > $queval->correct_cnt )
				{
					$sel_correct_cnt++;
				}
				*/
				
				/* if selected question count = question correct count */
				$resicon = '<br><br>' . '
				<div class="ans-box"><img src="'. base_url() .'front/images/icons/close.png" class="img-responsive" ><p class="red">The given answer is wrong</p></div>';
				
				if( $sel_correct_cnt == $queval->correct_cnt )
				{
				    $resicon = '<br><br>' . '	<div class="ans-box"><img src="'. base_url() .'front/images/icons/tick.png" class="img-responsive"><p class="green">The given answer is right </p></div>';
					$points++;
				}

/* 				echo "<hr><pre>sel_correct_cnt: ";
				var_dump( $sel_correct_cnt );	
				echo "<hr><pre>correct_cnt: ";
				var_dump( $queval->correct_cnt );
				echo "<hr><pre>points: ";
				var_dump( $points ); */	
				
				/*
				show this question on result page
				*/
				
				$resques .= '<div class="panel panel-default quepanel" >
											<div class="panel-heading">							<div class="row">                <div class="col-md-10">
												<p class=""> '. htmlentities( $queval->question ).'</p>
												';  
												
				
				
				$resques .= '					
						</div>				
						<div class="col-md-2">    			
							<p style="">Question '.$srno.' of '. $totque .'</p><p></p>						
							  
									
						</div>		
						
						</div>                            </div>
											<div class="panel-body" style="">
																					
												
														<div class="form-group">
														
								';    
														  
				if( $queval->quetype == 'MANS' )
				{					
					if( !empty( $queval->opt1 ) )
					{
					    if( ( $is_opt1_corr && $opt1_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt1_corr && $opt1_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt1_iscorr == 'Y' && $opt1_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt1_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt1_'. $queval->queid .'" type="checkbox" value="Y" '. $opt1_chk .' disabled> ' . htmlentities( $queval->opt1 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt1 );
						
					}
					if( !empty( $queval->opt2 ) )
					{
					    if( ( $is_opt2_corr && $opt2_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt2_corr && $opt2_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt2_iscorr == 'Y' && $opt2_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt2_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt2_'. $queval->queid .'" type="checkbox" value="Y" '. $opt2_chk .' disabled> ' . htmlentities( $queval->opt2 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt2 );
					}
					if( !empty( $queval->opt3 ) )
					{
					    if( ( $is_opt3_corr && $opt3_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt3_corr && $opt3_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt3_iscorr == 'Y' && $opt3_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt3_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt3_'. $queval->queid .'" type="checkbox" value="Y" '. $opt3_chk .' disabled> ' . htmlentities( $queval->opt3 );
						*/
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt3 );
					}
					if( !empty( $queval->opt4 ) )
					{
					    
					    if( ( $is_opt4_corr && $opt4_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt4_corr && $opt4_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt4_iscorr == 'Y' && $opt4_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt4_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt4_'. $queval->queid .'" type="checkbox" value="Y" '. $opt4_chk .' disabled> ' . htmlentities( $queval->opt4 ); 
						*/
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt4 ); 
					}
					if( !empty( $queval->opt5 ) )
					{
					    if( ( $is_opt5_corr && $opt5_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt5_corr && $opt5_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt5_iscorr == 'Y' && $opt5_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt5_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt5_'. $queval->queid .'" type="checkbox" value="Y" '. $opt5_chk .' disabled> ' . htmlentities( $queval->opt5 );   
						. ' ' . $sel_opt_cnt . ' ' . $queval->correct_cnt 
						*/
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt5 );  
					}  
				}
				else
				{										
			
					if( !empty( $queval->opt1 ) )     
					{
					        
                        if( ( $is_opt1_corr && $opt1_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt1_corr && $opt1_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt1_iscorr == 'Y' && $opt1_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt1_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt1_iscorr" '. $opt1_rad .' disabled> ' . htmlentities( $queval->opt1 );       
						*/
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt1 );
					}
					if( !empty( $queval->opt2 ) )                        
					{
					        
                        if( ( $is_opt2_corr && $opt2_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt2_corr && $opt2_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt2_iscorr == 'Y' && $opt2_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt2_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt2_iscorr" '. $opt2_rad .' disabled> ' . htmlentities( $queval->opt2 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt2 );
					}
					if( !empty( $queval->opt3 ) )                        
					{       
                            
                        if( ( $is_opt3_corr && $opt3_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt3_corr && $opt3_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt3_iscorr == 'Y' && $opt3_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt3_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt3_iscorr" '. $opt3_rad .' disabled> ' . htmlentities( $queval->opt3 );       
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt3 );   
						
					}
					if( !empty( $queval->opt4 ) )                        
					{
                            
                        if( ( $is_opt4_corr && $opt4_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt4_corr && $opt4_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt4_iscorr == 'Y' && $opt4_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt4_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt4_iscorr" '. $opt4_rad .' disabled> ' . htmlentities( $queval->opt4 );       
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt4 );
					}
					if( !empty( $queval->opt5 ) )                        
					{
					    
                        if( ( $is_opt5_corr && $opt5_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt5_corr && $opt5_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt5_iscorr == 'Y' && $opt5_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt5_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt5_iscorr" '. $opt5_rad .' disabled> ' . htmlentities( $queval->opt5 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt5 );
					}
					
				}
													
				$resques .= $resicon . '</div>			
													
											</div>
										   
										</div>';
				

				
			}
		}
		
		$arr_page['resques'] = $resques;
		
		$score_perc = ( $points / $totque ) * 100;
		$score_perc = round( $score_perc );
		
		/* echo 'You got '. $points .' of '. $totque .' possible points. ';
		echo 'Your score: '. $score_perc .' %'; */
		
		if( $score_perc >= $arr_page['exams'][0]->exampassrate )
		{
			$arr_page['resclass'] = 'passcls' ;
			$eres_status = 'PASS';
			$arr_page['msg'] = 'Pass !!!<br>You got '. $points .' of '. $totque .' possible points. ' . '<br>Your score: '. $score_perc .' %';
		
		}
		else
		{
			$arr_page['resclass'] = 'failcls' ;
			$eres_status = 'FAIL';
			$arr_page['msg'] = 'Fail !!!<br>You got '. $points .' of '. $totque .' possible points. ' . '<br>Your score: '. $score_perc .' %';
		}

		/* save students exam result */
		
/* 		if( @$arr_page['exams'][0]->examtype == 'PAID' )
		{ */

            $msg1 = '<div class="panel panel-default quepanel">
			
				<div class="panel-heading '. $arr_page['resclass'] .'" style="">							
				
					<div class="row">                
					
						<div class="col-md-10">
						
							<p class="">'. $arr_page['msg'] .'</p>
																		
						</div>				
						
			
					</div>                            
				
				</div>
									
				</div>';

			$eres_exstarted = $this->input->post('eres_exstarted');
			$eres_exfinished = date( "Y-m-d H:i:s" );
			$fk_eresid = $this->GeneralModel->AddNewRow( "exam_results", array( 'fk_examid' => $examid, 'exam_type' => $exam_type, 'fk_stuid' => $loggedid, 'eres_exstarted' => $eres_exstarted, 'eres_exfinished' => $eres_exfinished, 'eres_score' => $score_perc, 'eres_status' => $eres_status, 'eres_html' => $msg1 . $arr_page['resques'] , 'eresdatec' => date('Y-m-d H:i:s') ) );
			
			if( !empty( $resdet ) )
			{	
				foreach( $resdet as $key => $value )
				{
					$this->GeneralModel->AddNewRow( "exam_results_det", array( 'fk_eresid' => $fk_eresid, 'fk_queid' => $key, 'option_sel' => $value ) );
				}
			}
		
		/* } */
		$data['title'] = "Exam - Submitted | examroadmap.com";
		
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('examresult_1',$arr_page);  
		$this->load->view('include_front/footer');
		
    }

}