<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct() {

        parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
		$this->load->library('cart');
	}
	
	public function single( $slug = '' )
	{		
		if( empty( $slug ) )
		{
			redirect( base_url() . "blogs"  );
		}
	
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
	
		$data['blog'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'blog', $limit = '', $start = '', $columns = '', $orderby = '', $key = array( 'blog.slug' => $slug, 'blog.status' => 1 ), $search = '', $join_ar = '', $group_by = '' );
		
		if( empty( $data['blog'] ) )
		{
			redirect( base_url() . "blogs"  );
		}
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
		$data['title'] = "Blogs | examroadmap.com";
					
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('blogsingle', $data);
		$this->load->view('include_front/footer');	
	}	
	

	
}

?>