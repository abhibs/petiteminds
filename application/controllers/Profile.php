<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {

        parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
		$this->load->library('cart');
		
         if (!$this->session->userdata('loggedid')) { 
            redirect('home');
        }	
	}
	
	public function index()
	{		
	
		$data['title'] = "MY PROFILE | examroadmap.com";
		//$data['discription'] = "View";

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
	
		$data['students'] = $this->GeneralModel->GetInfoRow( $table = 'students', $key = array( 'stuid' => $loggedid ) );
		
		/*
		$data['orders'] = $this->GeneralModel->GetInfoRow( $table = 'orders', $key = array( 'fk_stuid' => $loggedid, 'ordstatus' => '' ) );
		*/
		
        $data['orders'] = $this->db->query( 'select * from orders where orders.ordstatus != "" and fk_stuid = "'. $loggedid .'"' )->result();

		
		$join_ar = array(
							0 => array(
								"table" => "exams",
								"condition" => "exam_results.fk_examid = exams.examid",
								"type" => "inner"
							)
						);

		$data['exam_results'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results', $limit = '', $start = '', $columns = 'exam_results.*, exams.examtitle', $orderby ='', $key = array( 'fk_stuid' => $loggedid ), $search = '', $join_ar, $group_by = '' );
		
		$data['category'] = $this->GeneralModel->GetInfoRow( $table = 'category', $key = '' );
		
		$this->load->view('include_front/head', $data);
		// 
		$this->load->view('include_front/nav');	
		$this->load->view('profile', $data);
		$this->load->view('include_front/footer');	
	}	
	
    public function examres( $eresid )
	{
		$data['title'] = "Exam Results | examroadmap.com";
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
        
		$join_ar = array(
		    
							0 => array(
								"table" => "exams",
								"condition" => "exam_results.fk_examid = exams.examid",
								"type" => "inner"
							)
						);

		$exam_results = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'exam_results.fk_stuid' => $loggedid, 'exam_results.eresid' => $eresid ), $search = '', $join_ar, $group_by = '' );
		
            
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
		
        if( empty( $exam_results ) )
        {
            redirect( base_url() );
        }
        
        if( empty( $exam_results[0]->eres_html ) )
        {
            $this->examresold( $eresid );
            /*exit;*/
        }
        else
        {
            $arr_page['examtitle'] = @$exam_results[0]->examtitle;
            $arr_page['eres_html'] = @$exam_results[0]->eres_html;
            
    		$this->load->view('include_front/head',$data);
    		$this->load->view('include_front/nav');	
    		$this->load->view('examres1', $arr_page);
    		$this->load->view('include_front/footer');
        }
        
	}
	
	public function examresold( $eresid )
	{
		$data['title'] = "Exam Results  | examroadmap.com";
	    
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
        
		$join_ar = array(
		    
		                    0 => array(
								"table" => "exam_results",
								"condition" => "exam_results_det.fk_eresid = exam_results.eresid",
								"type" => "inner"
							),
							1 => array(
								"table" => "exams",
								"condition" => "exam_results.fk_examid = exams.examid",
								"type" => "inner"
							),
							2 => array(
								"table" => "questions",
								"condition" => "exam_results_det.fk_queid = questions.queid",
								"type" => "inner"
							)
						);

		$exam_results_det = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results_det', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'exam_results.fk_stuid' => $loggedid, 'exam_results.eresid' => $eresid ), $search = '', $join_ar, $group_by = '' );
		
            
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
		
        if( empty( $exam_results_det ) )
        {
            redirect( base_url() );
        }
        
        $arr_page['examtitle'] = @$exam_results_det[0]->examtitle;
		
		$resques = '';
		$points = 0;
		$totque = count( $exam_results_det );
		
		$resdet = array();
		
		if( !empty( $exam_results_det ) )
		{	
			/* get questions */
			$backnav = $exam_results_det[0]->exambacknav;
			$showback = false;
			$shownext = true;
			$hidepanel = '';
			$srno = 0;	
			
			foreach( $exam_results_det as $quekey => $queval )
			{		

				$srno++;

				if( $quekey )
				{
					$showback = true;
					$shownext = ( ( count($exam_results_det) - 1 ) == $quekey ) ? false : true;
					$hidepanel = 'style="display: none;"';
				}
			
/* 				echo "<hr><pre>queid: ";
				var_dump( $queval->queid );	 */
				
				$sel_correct_cnt = 0;
				$sel_opt_cnt = 0;
				$opt1_chk = $opt2_chk = $opt3_chk = $opt4_chk = $opt5_chk = '';
				$opt1_rad = $opt2_rad = $opt3_rad = $opt4_rad = $opt5_rad = '';
				$is_opt1_corr = $is_opt2_corr = $is_opt3_corr = $is_opt4_corr = $is_opt5_corr = false;
				
				
				if( $queval->opt1_iscorr == 'Y' && strpos( $queval->option_sel, 'opt1_chk' ) !== false )
				{
					$sel_correct_cnt++;
					$is_opt1_corr = true;
				}
				
				if( $queval->opt2_iscorr == 'Y' && strpos( $queval->option_sel, 'opt2_chk' ) !== false )
				{

					$sel_correct_cnt++;
					$is_opt2_corr = true;
				}
				
				if( $queval->opt3_iscorr == 'Y' && strpos( $queval->option_sel, 'opt3_chk' ) !== false )
				{
				    

					$sel_correct_cnt++;
					$is_opt3_corr = true;

				}
				
				if( $queval->opt4_iscorr == 'Y' && strpos( $queval->option_sel, 'opt4_chk' ) !== false )
				{

					$sel_correct_cnt++;
					$is_opt4_corr = true;
				}
				
				if( $queval->opt5_iscorr == 'Y' && strpos( $queval->option_sel, 'opt5_chk' ) !== false )
				{

					$sel_correct_cnt++;
					$is_opt5_corr = true;
				}
				
				if( strpos( $queval->option_sel, 'opt1_chk' ) !== false )
				{
					$opt1_chk = 'checked';
					$opt1_rad = 'checked';
					$opt2_rad = $opt3_rad = $opt4_rad = $opt5_rad = '';

				}
				
				if( strpos( $queval->option_sel, 'opt2_chk' ) !== false )
				{
				    
					$opt2_chk = 'checked';
					$opt2_rad = 'checked';
					$opt1_rad = $opt3_rad = $opt4_rad = $opt5_rad = '';

				}
				
				if( strpos( $queval->option_sel, 'opt3_chk' ) !== false )
				{
				    
					$opt3_chk = 'checked';
					$opt3_rad = 'checked';
					$opt1_rad = $opt2_rad = $opt4_rad = $opt5_rad = '';


				}
				
				if( strpos( $queval->option_sel, 'opt4_chk' ) !== false )
				{
					$opt4_chk = 'checked';
					$opt4_rad = 'checked';
					$opt1_rad = $opt2_rad = $opt3_rad = $opt5_rad = '';

				}
				
				if( strpos( $queval->option_sel, 'opt5_chk' ) !== false )
				{
					$opt5_chk = 'checked';
					$opt5_rad = 'checked';
					$opt1_rad = $opt2_rad = $opt3_rad = $opt4_rad = '';

				}
				
                if( strpos( $queval->option_sel, 'opt1_chk' ) !== false )				
					$sel_opt_cnt++;
	    		if( strpos( $queval->option_sel, 'opt2_chk' ) !== false )				
					$sel_opt_cnt++;	
				if( strpos( $queval->option_sel, 'opt3_chk' ) !== false )				
					$sel_opt_cnt++;
				if( strpos( $queval->option_sel, 'opt4_chk' ) !== false )				
					$sel_opt_cnt++;	
				if( strpos( $queval->option_sel, 'opt5_chk' ) !== false )				
					$sel_opt_cnt++;
				
				/* if more than correct question selected then minus the count */	
				
				/*
				if( $sel_opt_cnt > $queval->correct_cnt )
				{
					$sel_correct_cnt++;
				}
				*/
				
				
				/* if selected question count = question correct count */
				$resicon = '<br><br>' . '
				<div class="ans-box"><img src="'. base_url() .'front/images/icons/close.png" class="img-responsive" ><p class="red">  The given answer is wrong</p></div>';
				
				if( $sel_correct_cnt == $queval->correct_cnt )
				{
				    $resicon = '<br><br>' . '	<div class="ans-box"><img src="'. base_url() .'front/images/icons/tick.png" class="img-responsive"><p class="green">The given answer is right </p></div>';
					$points++;
				}

/* 				echo "<hr><pre>sel_correct_cnt: ";
				var_dump( $sel_correct_cnt );	
				echo "<hr><pre>correct_cnt: ";
				var_dump( $queval->correct_cnt );
				echo "<hr><pre>points: ";
				var_dump( $points ); */	
				
				/*
				show this question on result page
				*/
				
				$resques .= '<div class="panel panel-default quepanel" ><!-- test -->
											<div class="panel-heading">							<div class="row">                <div class="col-md-10">
												<p class=""> '. htmlentities( $queval->question ).'</p>
												';  
												
				
				
				$resques .= '					
						</div>				
						<div class="col-md-2">    			
							<p style="">Question '.$srno.' of '. $totque .'</p><p></p>						
							  
									
						</div>		
						
						</div>                            </div>
											<div class="panel-body" style="">
																					
												
														<div class="form-group">
														
								';    
														  
				if( $queval->quetype == 'MANS' )
				{					
					if( !empty( $queval->opt1 ) )
					{
					    if( ( $is_opt1_corr && $opt1_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt1_corr && $opt1_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt1_iscorr == 'Y' && $opt1_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt1_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt1_'. $queval->queid .'" type="checkbox" value="Y" '. $opt1_chk .' disabled> ' . htmlentities( $queval->opt1 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt1 );
						
					}
					if( !empty( $queval->opt2 ) )
					{
					    if( ( $is_opt2_corr && $opt2_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt2_corr && $opt2_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt2_iscorr == 'Y' && $opt2_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt2_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt2_'. $queval->queid .'" type="checkbox" value="Y" '. $opt2_chk .' disabled> ' . htmlentities( $queval->opt2 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt2 );
					}
					if( !empty( $queval->opt3 ) )
					{
					    if( ( $is_opt3_corr && $opt3_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt3_corr && $opt3_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt3_iscorr == 'Y' && $opt3_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt3_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt3_'. $queval->queid .'" type="checkbox" value="Y" '. $opt3_chk .' disabled> ' . htmlentities( $queval->opt3 );
						*/
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt3 );
					}
					if( !empty( $queval->opt4 ) )
					{
					    
					    if( ( $is_opt4_corr && $opt4_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt4_corr && $opt4_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt4_iscorr == 'Y' && $opt4_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt4_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt4_'. $queval->queid .'" type="checkbox" value="Y" '. $opt4_chk .' disabled> ' . htmlentities( $queval->opt4 ); 
						*/
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt4 ); 
					}
					if( !empty( $queval->opt5 ) )
					{
					    if( ( $is_opt5_corr && $opt5_chk == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt5_corr && $opt5_chk == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt5_iscorr == 'Y' && $opt5_chk == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';    
                        else if( $opt5_chk == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					    
					    /*
						$resques .= '<br><input name="opt5_'. $queval->queid .'" type="checkbox" value="Y" '. $opt5_chk .' disabled> ' . htmlentities( $queval->opt5 );   
						. ' ' . $sel_opt_cnt . ' ' . $queval->correct_cnt 
						*/
						$resques .= '<br>' . $show_rightwrong . htmlentities( $queval->opt5 );  
					}    
				}
				else
				{										
			
					if( !empty( $queval->opt1 ) )     
					{
					        
                        if( ( $is_opt1_corr && $opt1_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt1_corr && $opt1_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt1_iscorr == 'Y' && $opt1_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt1_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt1_iscorr" '. $opt1_rad .' disabled> ' . htmlentities( $queval->opt1 );       
						*/
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt1 );
					}
					if( !empty( $queval->opt2 ) )                        
					{
					        
                        if( ( $is_opt2_corr && $opt2_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt2_corr && $opt2_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt2_iscorr == 'Y' && $opt2_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt2_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt2_iscorr" '. $opt2_rad .' disabled> ' . htmlentities( $queval->opt2 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt2 );
					}
					if( !empty( $queval->opt3 ) )                        
					{       
                            
                        if( ( $is_opt3_corr && $opt3_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt3_corr && $opt3_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt3_iscorr == 'Y' && $opt3_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt3_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt3_iscorr" '. $opt3_rad .' disabled> ' . htmlentities( $queval->opt3 );       
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt3 );   
						
					}
					if( !empty( $queval->opt4 ) )                        
					{
                            
                        if( ( $is_opt4_corr && $opt4_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt4_corr && $opt4_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt4_iscorr == 'Y' && $opt4_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt4_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt4_iscorr" '. $opt4_rad .' disabled> ' . htmlentities( $queval->opt4 );       
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt4 );
					}
					if( !empty( $queval->opt5 ) )                        
					{
					    
                        if( ( $is_opt5_corr && $opt5_rad == 'checked' ) )
					        $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                        else if( !$is_opt5_corr && $opt5_rad == 'checked' )
                            $show_rightwrong = '<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                        else if( $queval->opt5_iscorr == 'Y' && $opt5_rad == '' )
                            $show_rightwrong = '<i class="fa fa-check" aria-hidden="true" style="color: gray;"></i>';
                        else if( $opt5_rad == '' )
                            $show_rightwrong = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';    
                            
					    
					    /*
						$resques .= '<br><input name="opt_'. $queval->queid .'" type="checkbox" value="opt5_iscorr" '. $opt5_rad .' disabled> ' . htmlentities( $queval->opt5 );
						*/
						
						$resques .= '<br>' . $show_rightwrong . ' '  . htmlentities( $queval->opt5 );
					}
					
				}
													
				$resques .= $resicon . '</div>			
													
											</div>
										   
										</div>';
				

				
			}
		}
		
		$arr_page['resques'] = $resques;
		
        $score_perc = ( $points / $totque ) * 100;
		$score_perc = round( $score_perc );
		
		/* echo 'You got '. $points .' of '. $totque .' possible points. ';
		echo 'Your score: '. $score_perc .' %'; */
		
		if( $score_perc >= $exam_results_det[0]->exampassrate )
		{
			$arr_page['resclass'] = 'passcls' ;
			$eres_status = 'PASS';
			$arr_page['msg'] = 'Pass !!!<br>You got '. $points .' of '. $totque .' possible points. ' . '<br>Your score: '. $score_perc .' %';
		
		}
		else
		{
			$arr_page['resclass'] = 'failcls' ;
			$eres_status = 'FAIL';
			$arr_page['msg'] = 'Fail !!!<br>You got '. $points .' of '. $totque .' possible points. ' . '<br>Your score: '. $score_perc .' %';
		}
		
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('examres', $arr_page);
		$this->load->view('include_front/footer');
	    
	}
	
	public function premexam()
	{
		$data['title'] = "My Personal Exams | examroadmap.com";

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
	
		$data['students'] = $this->GeneralModel->GetInfoRow( $table = 'students', $key = array( 'stuid' => $loggedid ) );
		
		$join_ar = array(
		
			0 => array(
				"table" => "orders",
				"condition" => "order_detail.fk_ordid = orders.ordid",
				"type" => "inner"
			),
			1 => array(
				"table" => "exams",
				"condition" => "order_detail.odetsid = exams.sid",
				"type" => "inner"
			)
		);

		$data['order_detail'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => $loggedid, 'orders.ordstatus' => 'Completed', 'exams.examtype' => 'PAID' ), $search = '', $join_ar, $group_by = '' );
					
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('premexam', $data);
		$this->load->view('include_front/footer');	
	}
	
	public function editsave()
	{
		$data['title'] = "Profile Edited Successfully | examroadmap.com";
		$failed['title'] = "Failed to Edit | examroadmap.com";
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');		
		$this->form_validation->set_rules('stuname', 'Name', 'trim|required');
		$this->form_validation->set_rules('stuemail', 'Email', 'trim|required|valid_email',
        array(
                'required'      => 'The Email field is required',
				'valid_email'      => 'The Email is not valid email'
        ));
		$this->form_validation->set_rules('stumob', 'Mobile', 'trim|required',
        array(
                'required'      => 'The mobile field is required'
        ));
		
		
		$arr_page['page'] = 'register';
		
		if ( $this->form_validation->run() == FALSE ) {
		
			$this->load->view('include_front/head',$failed);
			$this->load->view('include_front/nav');
			$this->load->view('profile',$arr_page);
			$this->load->view('include_front/footer');
				
		}
		else{		
		
			$idata['stuname']	 = $this->input->post('stuname');
			$idata['stumob']	 = $this->input->post('stumob');
			$idata['stuemail'] = $this->input->post('stuemail');
			$stupass	 = $this->input->post('stupass');
			
			if( !empty( $stupass ) )
			{
				$idata['stupass']	 = md5( $stupass );
			}
			
			$this->GeneralModel->UpdateRow( "students", $idata, array( 'stuid' => $loggedid ) );
							
			$data['msgh1'] = 'Saved Successfully';
			$data['msgsuc'] = 'You have saved your profile details successfully. ';
			
			$this->load->view('include_front/head',$data);
			$this->load->view('include_front/nav');
			$this->load->view('msg', $data);
			$this->load->view('include_front/footer');
				

		}
		
	}	
	
	public function addtestimon()
	{
		$data['title'] = "Testimonial Submitted | examroadmap.com";
		$failed['title'] = "Failed To Submitted | examroadmap.com";
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');		
		$this->form_validation->set_rules('tmtitle', 'Title', 'trim|required');
		$this->form_validation->set_rules('tmmodule', 'Module', 'trim|required');
		$this->form_validation->set_rules('tmbody', 'Body', 'trim|required');
		$this->form_validation->set_rules('tmkeyward', 'Keywords', 'trim|required');
		
		
		$arr_page['page'] = 'register';
		
		if ( $this->form_validation->run() == FALSE ) {
		
			$this->load->view('include_front/head',$failed);
			$this->load->view('include_front/nav');
			$this->load->view('profile',$arr_page);
			$this->load->view('include_front/footer');
				
		}
		else{		
		
		    $now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');
		
			$idata['tmtitle']	 = $this->input->post('tmtitle');
			$idata['tmmodule']	 = $this->input->post('tmmodule');
			$idata['tmbody'] = $this->input->post('tmbody');
			$idata['tmkeyward'] = $this->input->post('tmkeyward');
			$idata['tmdatec'] = $temp;
			$idata['tmby'] = $loggedid;
			
			$this->GeneralModel->AddNewRow( "testimonial", $idata );
							
			$data['msgh1'] = 'Submitted Successfully';
			$data['msgsuc'] = 'You have submitted your testimonial successfully.';
			
			$this->load->view('include_front/head',$data);
			$this->load->view('include_front/nav');
			$this->load->view('msg', $data);
			$this->load->view('include_front/footer');
				

		}
		
	}		
	
}

?>