<?php 
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
 use PHPMailer\PHPMailer\PHPMailer;
 use PHPMailer\PHPMailer\SMTP;
 use PHPMailer\PHPMailer\Exception;
 
 require_once 'PHPMailer/PHPMailer/src/PHPMailer.php';
 require_once 'PHPMailer/PHPMailer/src/SMTP.php';
 require_once 'PHPMailer/PHPMailer/src/Exception.php';


require 'vendor/autoload.php';
require_once(APPPATH."libraries/razorpay/razorpay-php/Razorpay.php");



if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {

        parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
		$this->load->library('cart');
		$this->load->library('paypal_lib');
	}
	
	public function index()
	{		
	
	}	
	
	public function notify()
	{
		/* echo 'notify';exit; */
		
		// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.

		// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).

		// Set this to 0 once you go live or don't require logging.

		define("DEBUG", 1);

		// Set to 0 once you're ready to go live

        /* for sandbox */
		/*define("USE_SANDBOX", 1);*/
		
		/* for live */
		define("USE_SANDBOX", $this->config->item('usesandbox'));

		define("LOG_FILE", "paypal-payment-gateway-integration-in-php/ipn.log");

		// Read POST data

		// reading posted data directly from $_POST causes serialization

		// issues with array data in POST. Reading raw POST data from input stream instead.

		$raw_post_data = file_get_contents('php://input');
		
		error_log(date('[Y-m-d H:i e] '). "Raw IPN: $raw_post_data ". PHP_EOL, 3, LOG_FILE);

		$raw_post_array = explode('&', $raw_post_data);

		$myPost = array();

		foreach ($raw_post_array as $keyval) {

			$keyval = explode ('=', $keyval);

			if (count($keyval) == 2)

				$myPost[$keyval[0]] = urldecode($keyval[1]);

		}

		// read the post from PayPal system and add 'cmd'

		$req = 'cmd=_notify-validate';

		if(function_exists('get_magic_quotes_gpc')) {

			$get_magic_quotes_exists = true;

		}

		foreach ($myPost as $key => $value) {

			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {

				$value = urlencode(stripslashes($value));

			} else {

				$value = urlencode($value);

			}

			$req .= "&$key=$value";

		}

		// Post IPN data back to PayPal to validate the IPN data is genuine

		// Without this step anyone can fake IPN data

		if(USE_SANDBOX == true) {

			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

		} else {

			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";

		}

		$ch = curl_init($paypal_url);

		if ($ch == FALSE) {

			return FALSE;

		}

		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

		if(DEBUG == true) {

			curl_setopt($ch, CURLOPT_HEADER, 1);

			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);

		}

		// CONFIG: Optional proxy configuration

		//curl_setopt($ch, CURLOPT_PROXY, $proxy);

		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

		// Set TCP timeout to 30 seconds

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path

		// of the certificate as shown below. Ensure the file is readable by the webserver.

		// This is mandatory for some environments.

		//$cert = __DIR__ . "./cacert.pem";

		//curl_setopt($ch, CURLOPT_CAINFO, $cert);

		$res = curl_exec($ch);

		if (curl_errno($ch) != 0) // cURL error

			{

			if(DEBUG == true) {	

				error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);

			}

			curl_close($ch);

			exit;

		} else {

				// Log the entire HTTP response if debug is switched on.

				if(DEBUG == true) {

					error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);

					error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);

				}

				curl_close($ch);

		}

		// Inspect IPN validation result and act accordingly

		// Split response headers and payload, a better way for strcmp

		$tokens = explode("\r\n\r\n", trim($res));

		$res = trim(end($tokens));

		if (strcmp ($res, "VERIFIED") == 0) {

			// assign posted variables to local variables

			$item_name = $_POST['item_name'];

			$item_number = $_POST['item_number'];

			$payment_status = $_POST['payment_status'];

			$payment_amount = $_POST['mc_gross'];

			$payment_currency = $_POST['mc_currency'];

			$txn_id = $_POST['txn_id'];

			$receiver_email = $_POST['receiver_email'];

			$payer_email = $_POST['payer_email'];

			

/* 			include("DBController.php");

			$db = new DBController(); */

			

			// check whether the payment_status is Completed

			$isPaymentCompleted = false;

			if($payment_status == "Completed") {

				$isPaymentCompleted = true;

			}

			// check that txn_id has not been previously processed

			$isUniqueTxnId = false; 

			$param_type="s";

			$param_value_array = array($txn_id);

/* 			$result = $db->runQuery("SELECT * FROM payment WHERE txn_id = ?",$param_type,$param_value_array); */

/* 			$result = $this->GeneralModel->GetInfoRow( "payment", array( 'txn_id' => $txn_id) );

			if(empty($result)) {

				$isUniqueTxnId = true;

			} */	

			// check that receiver_email is your PayPal email

			// check that payment_amount/payment_currency are correct

			if($isPaymentCompleted) {

				$param_type = "sssdss";

				$param_value_array = array($item_number, $item_name, $payment_status, $payment_amount, $payment_currency, $txn_id);

/* 				$payment_id = $db->insert("INSERT INTO payment(item_number, item_name, payment_status, payment_amount, payment_currency, txn_id) VALUES(?, ?, ?, ?, ?, ?)", $param_type, $param_value_array); */

/* 				$payment_id = $this->GeneralModel->AddNewRow( "payment", array( 'item_number' => $item_number, 'item_name' => $item_name, 'payment_status' => $payment_status, 'payment_amount' => $payment_amount, 'payment_currency' => $payment_currency, 'txn_id' => $txn_id ) ); */
				
				/* 
				save order of students 
				*/
				
				$loggedin = $this->session->userdata('loggedin');
				$loggedid = $this->session->userdata('loggedid');
				
				$time365 = date('Y-m-d H:i:s', strtotime(' + 60 days'));
				
				$this->GeneralModel->UpdateRow( "orders", array( 'ordstatus' => $payment_status, 'item_name' => $item_name, 'payment_currency' => $payment_currency, 'txn_id' => $txn_id, 'payment_gateway' => 'PayPal' ), array( 'ordid' => $item_number ) );
				$this->mailInvoice( $item_number );
				$this->session->unset_userdata('user_ordid');

				
				/* 
				mail invoice to student 
				*/
				

				
				/* test mail 
				
				$to = 'sibmum1@sibinfotech.in';
				
				$from = $this->config->item('msmtp_user');
				$fromnm = 'Contact';
				$sub = 'ExamRoadmap - subscription invoice';
				
				$this->load->library('email');		  
				$config['protocol'] = $this->config->item('mprotocol');		  
				$config['smtp_host'] = $this->config->item('msmtp_host');		  		  
				$config['smtp_port'] = $this->config->item('msmtp_port');		  		  
				$config['smtp_user'] = $this->config->item('msmtp_user');		  		  
				$config['smtp_pass'] = $this->config->item('msmtp_pass');		  		  
				$config['charset'] = $this->config->item('mcharset');		  		  
				$config['mailtype'] = $this->config->item('mmailtype');		  		  
				$config['wordwrap'] = $this->config->item('mwordwrap');	  
				$this->email->initialize($config);
				$this->email->from($from, $fromnm);		  
				$this->email->to( $to );	
				$this->email->subject( $sub );
				
				$body_message = "test"; 
							  
					  
				$this->email->message($body_message);	
				
				if($this->email->send())
				{
				}
				else
				{
				}
				*/
				
				
				/*
				MAIL INVOICE END
				*/

				error_log(date('[Y-m-d H:i e] '). "Vdddddddddddderified IPN: $req ". PHP_EOL, 3, LOG_FILE);

			} 

			// process payment and mark item as paid.

			if(DEBUG == true) {

				error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);

			}

			

		} else if (strcmp ($res, "INVALID") == 0) {

			// log for manual investigation

			// Add business logic here which deals with invalid IPN messages

			if(DEBUG == true) {

				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);

			}

		}
		
	}	
	
	public function mailInvoice( $ordid )
	{
		//$quesdocs = $this->db->query("SELECT `orders`.`ordid`, `order_detail`.*, `exams`.* FROM `orders` INNER JOIN `order_detail` ON `orders`.`ordid` = `order_detail`.`fk_ordid` INNER JOIN `exams` ON `order_detail`.`odetsid` = `exams`.`sid` WHERE orders.`ordstatus` != '' and orders.ordid = '". $ordid ."' ORDER BY `orders`.`ordid` ")->result();
		
		  //print_r($quesdocs); exit;
		  //$quesid = $quesdocs[0]->odetsid;
		  //print_r($quesid); exit;
		  $quesid = $this->db->query("SELECT * FROM order_detail WHERE `fk_ordid` = $ordid order by odetid desc limit 1")->result();
		  //print_r($quesid); exit;
		  $odetid = $quesid[0]->odetid;
		//   echo $odetid; exit;
		  
		  //echo $quesid[0]->odetsid; exit;
		  
		  $count = $this->db->query("SELECT COUNT(odetid) AS odetid FROM `order_detail` WHERE fk_ordid = $ordid")->result();
		  //print_r($count); exit;
		  $count_odetid = $count[0]->odetid;
		  
		
		//$dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/questiondocument/'.$filename;
		 //echo $dir; exit;
		 

		/*error_reporting(E_ALL);
		ini_set('display_errors', 1);*/
		
		/* echo 'mail invoice'; */
		
		$orders = $this->db->query( "SELECT `orders`.*, `students`.*, order_detail.odet_validity FROM `orders` INNER JOIN `students` ON `orders`.`fk_stuid` = `students`.`stuid` INNER JOIN `order_detail` ON `orders`.`ordid` = `order_detail`.`fk_ordid` WHERE orders.`ordstatus` != '' and orders.ordid = '". $ordid ."' ORDER BY `orders`.`ordid` desc" )->result();
		// var_dump($orders); exit;
		
		$this->load->library('Pdf');
		/* $this->load->view('view_file'); */

		$now = new DateTime();
		$now->setTimezone(new DateTimezone('Asia/Kolkata'));
		$time = $now->format('d-m-Y H:i:s');
		$time365 = date('Y-m-d H:i:s', strtotime(' + 730 days'));
		
		/*
		
                    */
		
       $invoicemsg = '<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
</head>

<body style="padding-top: 100px;">
    <table style="margin: 0 auto; width: 500px;" width="500" cellpadding="10">
        <tr>
            <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px; border-right: none;">
                <img src="/front/images/logo.png" alt="" style="height: 60px;" height="60">
            </td>
            <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px; text-align: right; border-left: none;" align="right">Date : '.$time.'</td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid #ddd; padding: 10px; font-size: 15px; text-align: center; background-color: #1A67C1; color: #fff; font-weight: bold;" align="center">Payment Receipt</td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">
                <table style="margin: 0 auto;" cellpadding="10">
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px !important; font-size: 15px;">Order ID</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.$ordid.'</td>
                    </tr>
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Name</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'. @$orders[0]->stuname .'</td>
                    </tr>
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Amount</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->ordtotal_inr.' '.@$orders[0]->payment_currency.'</td>
                    </tr>
					
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Taxation ID</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->txn_id.'</td>
                    </tr>

					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Payment Gateway</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->payment_gateway.'</td>
                    </tr>
                    
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Subjects</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->item_name.'</td>
                    </tr>
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Validity</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'. date('d-m-Y', strtotime( $orders[0]->odet_validity ) ).'</td> 
                    </tr>
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Buyer Details</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Name: '.@$orders[0]->stuname .'<br>Email: '.@$orders[0]->stuemail.'<br>Mobile: '.@$orders[0]->stumob.'</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="  padding: 10px; font-size: 15px; text-align: center; color: #1856a3; font-weight: bold;" align="center">Thank you for using ExamRoadmap</td>
        </tr>
        
    </table>
</body>

</html>'; //echo $invoicemsg; exit;
		
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		
/* 		
		$pdf->SetTitle('My Title');
		$pdf->SetHeaderMargin(30);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');

		$pdf->AddPage();

		$pdf->Write(5, 'Some sample text');
		$pdf->Output('My-File-Name.pdf', 'I'); */
		
        /* // set document information */
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('ExamRoadmap');
        $pdf->SetTitle('Invoice ' . $ordid);
        $pdf->SetSubject('Invoice ' .$ordid);
        $pdf->SetKeywords('ExamRoadmap, PDF, Invoice ' . $ordid);   
      
        /* // set header and footer fonts */
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
      
        /* // set default monospaced font */
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
      
        /* // set margins */
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
      
        /* // set auto page breaks */
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
      
        /* // set image scale factor */
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
      
        /* // set default font subsetting mode */
        $pdf->setFontSubsetting(true);   
      
        /* // Set font */
        $pdf->SetFont('helvetica', '', 14, '', true);   
      
        /* // Add a page */
        $pdf->AddPage(); 
      
        /* // set text shadow effect */
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
      
        /* // Print text using writeHTMLCell() */
        $pdf->writeHTMLCell(0, 0, '', '', $invoicemsg, 0, 1, 0, true, '', true);
        //echo $invoicemsg; exit;
      
		$invpdf = $_SERVER['DOCUMENT_ROOT'] . 'uploads/invoices/Invoice_' . $ordid . '.pdf' ;
		//echo $invpdf; exit;
		
        /* // saving pdf to uploads dir */
        $pdf->Output( $invpdf, 'F' ); 
		
		/* send email */
		/* $to = 'sibinfotech101@gmail.com'; */
		$to = @$orders[0]->stuemail;
		//$to = 'dheeraj@infouna.com';
		/*$to = 'sibmum1@sibinfotech.in';*/
		
		$from = $this->config->item('msmtp_user');
		$fromnm = 'Contact';
		$sub = 'ExamRoadmap - Invoice / System Acccess / PDF';
		
		
		/*
		$this->load->library('email');		  
		$config['protocol'] = $this->config->item('mprotocol');		  
		$config['smtp_host'] = $this->config->item('msmtp_host');		  		  
		$config['smtp_port'] = $this->config->item('msmtp_port');		  		  
		$config['smtp_user'] = $this->config->item('msmtp_user');		  		  
		$config['smtp_pass'] = $this->config->item('msmtp_pass');		  		  
		$config['charset'] = $this->config->item('mcharset');		  		  
		$config['mailtype'] = $this->config->item('mmailtype');		  		  
		$config['wordwrap'] = $this->config->item('mwordwrap');	  
		$this->email->initialize($config);
		$this->email->from($from, $fromnm);		  
		$this->email->to( $to );	
		$this->email->subject( $sub );
		
		$this->email->message($body_message);	
		$this->email->attach( $invpdf );		
		
		if($this->email->send())
        {
        }
        else
        {
        }
        
        $this->email->clear(true);
        */
        
        $validity = date('d-m-Y', strtotime( $orders[0]->odet_validity ));
        $studentid = $orders[0]->fk_stuid;
        //echo $studentid; exit;


for($i=0; $i<$count_odetid; $i++){

   
    $examid = $this->db->query("SELECT * FROM order_detail WHERE odetid = $odetid-$i")->result();
    $odetsid = $examid[0]->odetsid;
    
    $subject = $this->db->query("SELECT * FROM exams WHERE sid = $odetsid")->result();
    $subjectname = $subject[0]->examtitle;
  
    $var = $var."<tr style='margin: 0; padding: 0;'>
            <td class='common-padding' style='margin: 0; vertical-align: middle; padding: 10px;' valign='middle' colspan='2'>
            <p style='margin: 0 0 10px; padding: 0;'><strong>Subject: $subjectname</strong></p>
                <a href='". base_url() ."Order/questiondocument/".$ordid."/".$odetsid."' style='display:inline-block;padding:10px 15px;background:#1856a3;color:#fff;text-decoration:none;font-weight:bold;border-radius:5px;letter-spacing:0.025em;' download>Click to Download PDF</a>
        	</tr>";
    		
                
}


			    
$body_message = "<!DOCTYPE html>
<html lang='en' style='margin: 0; padding: 0;'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet'>
</head>

<body style='margin: 0; padding: 0; font-family: Roboto, sans-serif; font-size: 15px;'>

    <div class='welcome-verification-email' style='margin: 0; padding: 0;'>
        <table style='margin: 50px auto; border: 1px solid #e5e5e5; padding: 0; width: 600px;' width='600'>
            <tr class='center-content' style='margin: 0; padding: 0; text-align: center;' align='center'>
                <td class='common-padding' style='margin: 0; vertical-align: middle; padding: 10px;' valign='middle'>
                    <img src='/front/images/logo.png' class='company-logo' alt='' style='margin: 0; padding: 0; width: 240px;' width='240'>
                </td>
            </tr>
            <tr class='center-content' style='margin: 0; padding: 0; text-align: center;' align='center'>
                <td style='margin: 0; vertical-align: middle; padding: 0px;' valign='middle'>
                    <h2 class='welcome-text' style='margin: 0; color: #fff; background-color: #1856a3; font-size: 20px; padding: 10px; text-transform: uppercase; letter-spacing: 1px;'>Payment Successful</h2>
                </td>
            </tr>
            <tr style='margin: 0; padding: 0;'>
                <td class='common-padding top-padding' style='margin: 0; vertical-align: middle; padding: 10px; padding-top: 30px;' valign='middle'>
                    <h3 class='hello-text' style='margin: 0; padding: 0; font-size: 18px; font-weight: 600;'>
                        Hey ". @$orders[0]->stuname."!
                    </h3>
                </td>
            </tr>
            <tr style='margin: 0; padding: 0;'>
                <td class='common-padding' style='margin: 0; vertical-align: middle; padding: 10px;' valign='middle'>
                    <p style='margin: 0; padding: 0;'>Thank You! Your order has been placed! You have successfully paid for following subjects:-<br>". @$orders[0]->item_name."</p>
                </td>
            </tr>
            
            <tr style='margin: 0; padding: 0;'>
                <td class='common-padding' style='margin: 0; vertical-align: middle; padding: 10px;' valign='middle' colspan='2'>
                    <a href='". base_url() ."Order/downloadinvoices/".$ordid."/".$studentid."' style='display:inline-block;padding:10px 15px;background:#1856a3;color:#fff;text-decoration:none;font-weight:bold;border-radius:5px;letter-spacing:0.025em;' download>Click to Download Invoice</a>
                    
                </td>
                
            </tr>
            
             ".$var."

			 <tr style='margin: 0; padding: 0;'>
                <td class='common-padding bottom-padding' style='margin: 0; vertical-align: middle; padding: 10px; padding-bottom: 30px;' valign='middle'>
                    <p class='small-bold-txt regards-title' style='margin: 0; padding: 0; font-size: 16px; font-weight: 600; margin-bottom: 5px;'>System Access</p>
                    <p style='margin: 0; padding: 0;'>We have already provided the System Access automatically to you to practice the exams.
					(Goto Welcome tab/My Personal Exam/Left side you can see your exams)</p>
					<p style='margin: 0; padding: 0;'>Please let us know if you need any help at <a href='mailto:info@examroadmap.com'>info@examroadmap.com</a></p>
                </td>
            </tr>
			
		     <tr style='margin: 0; padding: 0;'>
                <td class='common-padding bottom-padding' style='margin: 0; vertical-align: middle; padding: 10px; padding-bottom: 30px;' valign='middle'>
                    <p class='small-bold-txt regards-title' style='margin: 0; padding: 0; font-size: 16px; font-weight: 600; margin-bottom: 5px;'>Regards,</p>
                    <p style='margin: 0; padding: 0;'>Team ExamRoadmap</p>
                </td>
            </tr>
			
        </table>
    </div>

</body>

</html>"; //echo $body_message; exit;


        // $mail = new PHPMailer();
		// $mail->XMailer = 'ExamRoadmap';
		// $mail->IsSMTP(); // telling the class to use SMTP
		// $mail->Host       = $this->config->item('msmtp_host'); // SMTP server
		// $mail->SMTPDebug  = 2;
		// // enables SMTP debug information (for testing)
		// 										   // 1 = errors and messages
		// 										   // 2 = messages only
		// $mail->SMTPAuth   = true;                  // enable SMTP authentication
		// $mail->SMTPSecure = "tls";                 
		// $mail->Host       = $this->config->item('msmtp_host');      // SMTP server
		// $mail->Port       = $this->config->item('msmtp_port');                   // SMTP port
		// $mail->Username   = $this->config->item('msmtp_user');  // username
		// $mail->Password   = $this->config->item('msmtp_pass');            // password

		// $mail->SetFrom($this->config->item('msmtp_user'), 'ExamRoadmap');
		// $mail->Subject    = $sub;
		// $mail->MsgHTML($body_message);		
		// /*$mail->AddAddress('sibmum1@sibinfotech.in');*/
		// $mail->AddAddress( $to );
		// $mail->addAttachment($invpdf);
		// if(!$mail->Send()) {
		//   /*echo "Mailer Error: " . $mail->ErrorInfo;exit;*/
		// } else {
		//   /*echo "Message sent!"; */
		// }
		
		/* echo 'pdf created successfully'; */

// 		$mail = new PHPMailer(true);
// 		$mail->SMTPDebug = $this->config->item('msmtp_debug');
// 		$mail->Host  = $this->config->item('msmtp_host');
// 		$mail->SMTPAuth = $this->config->item('msmtp_auth');
// 		$mail->Port = $this->config->item('msmtp_port');
// 		$mail->setFrom('info@examroadmap.com', 'ExamRoadmap');
// 		//$mail->addAddress( $to );
// 		$mail->addAddress( 'dheeraj@infouna.com' );
// 		$mail->addReplyTo('info@examroadmap.com', 'ExamRoadmap'); 

// 		$mail->IsHTML(true);
// 		$mail->Subject = $sub; 

// 		$mail->MsgHTML($body_message); //echo $body_message;exit;
// 		//$mail->addAttachment($invpdf); //echo $invoicemsg; exit;
// 		//$mail->addAttachment($dir);
// 		$mail->AltBody = 'Plain text message body for non-HTML email client. Gmail SMTP email body.';
// 		try{
// 		$mail->send();
// 		} catch(Exception $e) {
// 		     echo $e->mailInvoice();
// 		}


            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug   = false;
            $mail->Host        = $this->config->item('msmtp_host');
            $mail->Port        = $this->config->item('msmtp_port');
            $mail->SMTPAuth    = true;
            $mail->Username    = $this->config->item('msmtp_username');
            $mail->Password    = $this->config->item('msmtp_password');
            $mail->SMTPSecure  = 'ssl';
            $mail->setFrom('info@examroadmap.com', 'Exam Roadmap');
            $mail->addReplyTo('info@examroadmap.com', 'Exam Roadmap');
            $mail->addAddress($to);
            //$mail->addAddress('dheeraj@infouna.com');
            $mail->Subject = $sub;
            $mail->MsgHTML($body_message);
            $mail->send();
            
	}
	
	
	public function mailInvoiceAdmin( $ordid )
	{
		/*error_reporting(E_ALL);
		ini_set('display_errors', 1);*/
		
		/* echo 'mail invoice'; */
		
		$orders = $this->db->query( "SELECT `orders`.*, `students`.*, order_detail.odet_validity FROM `orders` INNER JOIN `students` ON `orders`.`fk_stuid` = `students`.`stuid` INNER JOIN `order_detail` ON `orders`.`ordid` = `order_detail`.`fk_ordid` WHERE orders.`ordstatus` != '' and orders.ordid = '". $ordid ."' ORDER BY `orders`.`ordid` desc" )->result();
		
		$this->load->library('Pdf');
		/* $this->load->view('view_file'); */

		$now = new DateTime();
		$now->setTimezone(new DateTimezone('Asia/Kolkata'));
		$time = $now->format('d-m-Y H:i:s');
		$time365 = date('Y-m-d H:i:s', strtotime(' + 730 days'));
		
		/*
		
                    */
		
       $invoicemsg = '<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
</head>

<body style="padding-top: 100px;">
    <table style="margin: 0 auto; width: 500px;" width="500" cellpadding="10">
        <tr>
            <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px; border-right: none;">
                <img src="/front/images/logo.png" alt="" style="height: 60px;" height="60">
            </td>
            <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px; text-align: right; border-left: none;" align="right">Date : '.$time.'</td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid #ddd; padding: 10px; font-size: 15px; text-align: center; background-color: #1A67C1; color: #fff; font-weight: bold;" align="center">Payment Receipt</td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">
                <table style="margin: 0 auto;" cellpadding="10">
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px !important; font-size: 15px;">Order ID</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.$ordid.'</td>
                    </tr>
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Name</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'. @$orders[0]->stuname .'</td>
                    </tr>
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Amount</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->ordtotal.' USD</td>
                    </tr>
					
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Taxation ID</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->txn_id.'</td>
                    </tr>
                    
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Subjects</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->item_name.'</td>
                    </tr>
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Validity</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'. date('d-m-Y', strtotime( $orders[0]->odet_validity ) ).'</td> 
                    </tr>
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Buyer Details</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Name: '.@$orders[0]->stuname .'<br>Email: '.@$orders[0]->stuemail.'<br>Mobile: '.@$orders[0]->stumob.'</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="  padding: 10px; font-size: 15px; text-align: center; color: #1856a3; font-weight: bold;" align="center">Thank you for using EXAM ROADMAP</td>
        </tr>
        
    </table>
</body>

</html>';
		
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		
/* 		
		$pdf->SetTitle('My Title');
		$pdf->SetHeaderMargin(30);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');

		$pdf->AddPage();

		$pdf->Write(5, 'Some sample text');
		$pdf->Output('My-File-Name.pdf', 'I'); */
		
        /* // set document information */
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Exam Roadmap');
        $pdf->SetTitle('Invoice ' . $ordid);
        $pdf->SetSubject('Invoice ' .$ordid);
        $pdf->SetKeywords('ExamRoadmap, PDF, Invoice ' . $ordid);   
      
        /* // set header and footer fonts */
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
      
        /* // set default monospaced font */
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
      
        /* // set margins */
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
      
        /* // set auto page breaks */
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
      
        /* // set image scale factor */
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
      
        /* // set default font subsetting mode */
        $pdf->setFontSubsetting(true);   
      
        /* // Set font */
        $pdf->SetFont('helvetica', '', 14, '', true);   
      
        /* // Add a page */
        $pdf->AddPage(); 
      
        /* // set text shadow effect */
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
      
        /* // Print text using writeHTMLCell() */
        $pdf->writeHTMLCell(0, 0, '', '', $invoicemsg, 0, 1, 0, true, '', true);   
      
		$invpdf = $_SERVER['DOCUMENT_ROOT'] . 'projects/sap/uploads/invoices/invoice_' . $ordid . '.pdf' ;
		
        /* // saving pdf to uploads dir */
        $pdf->Output( $invpdf, 'F' ); 
		
		/* send email */
		/*$to = @$orders[0]->stuemail;*/
		//$to = 'sibmum7@sibinfotech.in';
		
		$from = $this->config->item('msmtp_user');
		$fromnm = 'Contact';
		$sub = 'ExamRoadmap - New Order';
		
		
		/*
		$this->load->library('email');		  
		$config['protocol'] = $this->config->item('mprotocol');		  
		$config['smtp_host'] = $this->config->item('msmtp_host');		  		  
		$config['smtp_port'] = $this->config->item('msmtp_port');		  		  
		$config['smtp_user'] = $this->config->item('msmtp_user');		  		  
		$config['smtp_pass'] = $this->config->item('msmtp_pass');		  		  
		$config['charset'] = $this->config->item('mcharset');		  		  
		$config['mailtype'] = $this->config->item('mmailtype');		  		  
		$config['wordwrap'] = $this->config->item('mwordwrap');	  
		$this->email->initialize($config);
		$this->email->from($from, $fromnm);		  
		$this->email->to( $to );	
		$this->email->subject( $sub );
		
$body_message = "<!DOCTYPE html>
<html lang='en' style='margin: 0; padding: 0;'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet'>
</head>

<body style='margin: 0; padding: 0; font-family: Roboto, sans-serif; font-size: 15px;'>

    <div class='welcome-verification-email' style='margin: 0; padding: 0;'>
        <table style='margin: 50px auto; border: 1px solid #e5e5e5; padding: 0; width: 600px;' width='600'>
            <tr class='center-content' style='margin: 0; padding: 0; text-align: center;' align='center'>
                <td class='common-padding' style='margin: 0; vertical-align: middle; padding: 10px;' valign='middle'>
                    <img src='". base_url() ."front/images/logo.png' class='company-logo' alt='' style='margin: 0; padding: 0; width: 240px;' width='240'>
                </td>
            </tr>
            <tr class='center-content' style='margin: 0; padding: 0; text-align: center;' align='center'>
                <td style='margin: 0; vertical-align: middle; padding: 0px;' valign='middle'>
                    <h2 class='welcome-text' style='margin: 0; color: #fff; background-color: #1856a3; font-size: 20px; padding: 10px; text-transform: uppercase; letter-spacing: 1px;'>New Order</h2>
                </td>
            </tr>
            <tr style='margin: 0; padding: 0;'>
                <td class='common-padding top-padding' style='margin: 0; vertical-align: middle; padding: 10px; padding-top: 30px;' valign='middle'>
                    <h3 class='hello-text' style='margin: 0; padding: 0; font-size: 18px; font-weight: 600;'>
                        Hey Admin!
                    </h3>
                </td>
            </tr>
            <tr style='margin: 0; padding: 0;'>
                <td class='common-padding' style='margin: 0; vertical-align: middle; padding: 10px;' valign='middle'>
                    <p style='margin: 0; padding: 0;'>A new order has been placed by ". @$orders[0]->stuname.". Purchased subjects:-<br>". @$orders[0]->item_name.".</p>
                </td>
            </tr>
					
            <tr style='margin: 0; padding: 0;'>
                <td class='common-padding bottom-padding' style='margin: 0; vertical-align: middle; padding: 10px; padding-bottom: 30px;' valign='middle'>
                    <p class='small-bold-txt regards-title' style='margin: 0; padding: 0; font-size: 16px; font-weight: 600; margin-bottom: 5px;'>Regards,</p>
                    <p style='margin: 0; padding: 0;'>Team ExamRoadmap</p>
                </td>
            </tr>
			
        </table>
    </div>

</body>

</html>"; 

		$this->email->message($body_message);	
		$this->email->attach( $invpdf );		
		
		if($this->email->send())
        {
        }
        else
        {
        }
        
        $this->email->clear(true);
        */
        
        
        
        $mail = new PHPMailer();
		$mail->XMailer = 'ExamRoadmap';
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = $this->config->item('msmtp_host'); // SMTP server
		$mail->SMTPDebug  = 2;
		// enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 
		$mail->Host       = $this->config->item('msmtp_host');      // SMTP server
		$mail->Port       = $this->config->item('msmtp_port');                   // SMTP port
		$mail->Username   = $this->config->item('msmtp_user');  // username
		$mail->Password   = $this->config->item('msmtp_pass');            // password

		$mail->SetFrom($this->config->item('msmtp_user'), 'ExamRoadmap');
		$mail->Subject    = $sub;
		$mail->MsgHTML($body_message);		
		/*$mail->AddAddress('sibmum1@sibinfotech.in');*/
		$mail->AddAddress( $to );
		$mail->addAttachment($invpdf);
		if(!$mail->Send()) {
		  /*echo "Mailer Error: " . $mail->ErrorInfo;exit;*/
		} else {
		  /*echo "Message sent!"; */
		}
		
		
		
		
		/* echo 'pdf created successfully'; */
	}
	
	
	/**
	 * Function to convert a number to a the string literal for the number
	 */
	public function getStringOfAmount($num) {
	  $count = 0;
	  global $ones, $tens, $triplets;
	  $ones = array(
		'',
		' One',
		' Two',
		' Three',
		' Four',
		' Five',
		' Six',
		' Seven',
		' Eight',
		' Nine',
		' Ten',
		' Eleven',
		' Twelve',
		' Thirteen',
		' Fourteen',
		' Fifteen',
		' Sixteen',
		' Seventeen',
		' Eighteen',
		' Nineteen'
	  );
	  $tens = array(
		'',
		'',
		' Twenty',
		' Thirty',
		' Forty',
		' Fifty',
		' Sixty',
		' Seventy',
		' Eighty',
		' Ninety'
	  );

	  $triplets = array(
		'',
		' Thousand',
		' Million',
		' Billion',
		' Trillion',
		' Quadrillion',
		' Quintillion',
		' Sextillion',
		' Septillion',
		' Octillion',
		' Nonillion'
	  );
	  return $this->convertNum($num);
	}
	
	/**
	 * Function to dislay tens and ones
	 */
	public function commonloop($val, $str1 = '', $str2 = '') {
	  global $ones, $tens;
	  $string = '';
	  if ($val == 0)
		$string .= $ones[$val];
	  else if ($val < 20)
		$string .= $str1.$ones[$val] . $str2;  
	  else
		$string .= $str1 . $tens[(int) ($val / 10)] . $ones[$val % 10] . $str2;
	  return $string;
	}

	/**
	 * returns the number as an anglicized string
	 */
	public function convertNum($num) {
	  $num = (int) $num;    
	  
	  /* // make sure it's an integer */

	  if ($num < 0)
		return 'negative' . convertTri(-$num, 0);

	  if ($num == 0)
		return 'Zero';
	  return $this->convertTri($num, 0);
	}

	/**
	 * recursive fn, converts numbers to words
	 */
	public function convertTri($num, $tri) {
	  global $ones, $tens, $triplets, $count;
	  $test = $num;
	  $count++;
/* 	  // chunk the number, ...rxyy
	  // init the output string */
	  $str = '';
	  /* // to display hundred & digits */
	  if ($count == 1) {
		$r = (int) ($num / 1000);
		$x = ($num / 100) % 10;
		$y = $num % 100;
		/* // do hundreds */
		if ($x > 0) {
		  $str = $ones[$x] . ' Hundred';
		  /* // do ones and tens */
		  $str .= $this->commonloop($y, ' and ', '');
		}
		else if ($r > 0) {
		  /* // do ones and tens */
		  $str .= $this->commonloop($y, ' and ', '');
		}
		else {
		  /* // do ones and tens */
		  $str .= $this->commonloop($y);
		}
	  }
	  /* // To display lakh and thousands */
	  else if($count == 2) {
		$r = (int) ($num / 10000);
		$x = ($num / 100) % 100;
		$y = $num % 100;
		$str .= $this->commonloop($x, '', ' Lakh ');
		$str .= $this->commonloop($y);
		if ($str != '')
		  $str .= $triplets[$tri];
	  }
	  /* // to display till hundred crore */
	  else if($count == 3) {
		$r = (int) ($num / 1000);
		$x = ($num / 100) % 10;
		$y = $num % 100;
		/* // do hundreds */
		if ($x > 0) {
		  $str = $ones[$x] . ' Hundred';
		  /* // do ones and tens */
		  $str .= $this->commonloop($y,' and ',' Crore ');
		}
		else if ($r > 0) {
		  /* // do ones and tens */
		  $str .= $this->commonloop($y,' and ',' Crore ');
		}
		else {
		  /* // do ones and tens */
		  $str .= $this->commonloop($y);
		}
	  }
	  else {
		$r = (int) ($num / 1000);
	  }
/* 	  // add triplet modifier only if there
	  // is some output to be modified...
	  // continue recursing? */
	  if ($r > 0)
		return $this->convertTri($r, $tri+1) . $str;
	  else
		return $str;
	}	
	
	public function numwords()
	{
		echo $this->getStringOfAmount(1234);
	}	
	
	public function cancel()
	{
		$user_ordid = $this->session->userdata('user_ordid');
		
		if( empty( $user_ordid ) )
		{
			redirect(base_url());
		}
		else
		{
		    /* mark order as cancelled */
            $this->GeneralModel->UpdateRow( "orders", array( 'ordstatus' => 'Cancelled', 'payment_gateway' => 'PayPal' ), array( 'ordid' => $user_ordid ) );

		}
		
		$this->session->unset_userdata('user_ordid');
		$this->cart->destroy();
		
		/* echo $_SERVER['DOCUMENT_ROOT'] . 'projects/sap/uploads/invoices/' . $ordid . '.pdf';exit; */
		
		$data['msgh1'] = 'Payment Cancelled';
		$data['msgerr'] = 'Payment has been cancelled by user.';
		$data['title'] = "Payment Cancelled | examroadmap.com";
		
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('msg', $data);
		$this->load->view('include_front/footer');	
	}
	
	public function return()
	{
		$this->session->unset_userdata('user_ordid');
		$this->cart->destroy();
		
		
		$data['msgh1'] = 'Purchased Successfully';
		$data['msgsuc'] = 'Thank You! Your order has been placed!';
		$data['title'] = "Payment Successful | examroadmap.com";
		
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('msg', $data);
		$this->load->view('include_front/footer');		
	}

	public function paypalSubmit() {

		$ordtotal = $this->session->userdata('total_amount');
		$discount = $this->session->userdata('discount');
		$percent = $this->session->userdata('percent');
		$orderid = $_POST['item_number'];
		$gst = 0;
		$gtot = $ordtotal + $gst - $discount;
		
		$stuid = $this->db->query("SELECT `fk_stuid` FROM orders where ordid = $orderid")->result();
		$stuid_update = $stuid[0]->fk_stuid;

		//Set variables for paypal formbase_url().'paypal/success'; //pa
		$returnURL = base_url().'order/return';
		$cancelURL = base_url().'order/cancel'; //payment cancel url
		$notifyURL = base_url().'order/ipn'; //ipn url

		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name', $_POST['item_name']);
		$this->paypal_lib->add_field('custom', $userID);
		$this->paypal_lib->add_field('item_number',  $orderid);
		$this->paypal_lib->add_field('amount',  $gtot);        
		$this->paypal_lib->add_field('no_shipping', 0);     
		$this->paypal_lib->paypal_auto_form();
	}

	public function ipn(){
		//paypal return transaction details array
		
		$this->load->helper('file');
		$paypalInfo = $this->input->post();
		
		write_file('application/logs/paypal.txt', @file_get_contents('php://input'), 'a');

		// $data = array(
		// 	'order_id' => '123',
		// 	'email' => 'test@gmail.com',
		// 	'product' => 'My Product',
		// 	'amount' => '39',
		// 	'payment_id' => 'sdfvsjdfsd432432vhv44vv',
		// 	'payment_status' => 'success'
		// );
		// $this->db->insert('paypal_paymemnt',$data);

		$data['product'] = $paypalInfo['item_name'];
		$data['order_id']    = $paypalInfo["item_number"];
		$data['payment_id']    = $paypalInfo["txn_id"];
		$data['amount'] = $paypalInfo["mc_gross"];
		// $data['payment_currency'] = $paypalInfo["mc_currency"];
		$data['email'] = $paypalInfo["payer_email"];
		$data['payment_status']    = $paypalInfo["payment_status"];

		$paypalURL = $this->paypal_lib->paypal_url;        
		$result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
		//check whether the payment is verified
		if(preg_match("/VERIFIED/i",$result)){
		    
		    // Update paypal payment
		    $this->db->insert('paypal_paymemnt',$data);
		    
		    $user_ordid = $paypalInfo["item_number"];
		    $txn_id = $paypalInfo["txn_id"];
		    
	         $amount = $this->db->query("SELECT * FROM orders where ordid = $user_ordid")->result();
    	     $totamount = $amount[0]->ordtotal; 
    	     //$discount = $amount[0]->discamount;
    	     
    	     //$gtot = $totamount - $discount; 
    	     
    	    
    	    /* mark order as completed */
            
            $updateData=array('ordstatus' => 'Completed', 'ordtotal_inr' => $totamount,'payment_gateway' => 'PayPal', 'payment_currency' =>'USD', 'txn_id' => $txn_id );
            $this->db->where("ordid",$user_ordid);
            $this->db->update("orders",$updateData);
            
            
            $ciddata = $this->db->query("SELECT `odetsid` FROM order_detail where fk_ordid = $user_ordid")->result();
            		$cid = $ciddata[0]->odetsid;
            		
            // student discount 
            $stuid = $this->db->query("SELECT `fk_stuid` FROM orders where ordid = $user_ordid")->result();
    	    $stuid_update = $stuid[0]->fk_stuid;
    
    	    $this->GeneralModel->UpdateRow( "student_discount", array( 'status' => 1), array( 'cid' => $cid, 'stuid'=>$stuid_update ) );
    	    
    	    $this->mailInvoice( $user_ordid );
		    
		}
	}
	
	
	public function payusuccess(){

		$data['title'] = "Payment Successful | examroadmap.com";
		// PayU Code
		define("PAYU_LOG_FILE", "application/logs/payulogs.log"); 
		

		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		// $amount=intval($_POST["amount"]);	
		$amount=$_POST["amount"];	
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt=$this->config->item('salt');

		$phone=$_POST["phone"];
		$user_email=$_POST["udf2"];

		$currency_code=$_POST["udf1"];
		$phash=$_POST["udf3"];
		$orderid=$_POST["udf4"];
		
		if(isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			// $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
			$retHashSeq = $additionalCharges.'|'.$key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$user_email.'|'.$currency_code.'|'.$user_email.'||' . $orderid . '|||||||'.$salt;
		}else {
			// $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
			// $retHashSeq = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$user_email.'|'.$currency_code.'|'.$user_email.'|||||||||'.$salt;
			$retHashSeq = $salt.'|'.$status.'|||||||' . $orderid . '||'.$user_email.'|'.$currency_code.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
			// $retHashSeq = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$user_email.'|'.$currency_code.'|'.$user_email.'|||||||||'.$salt;
		}
	
		$hash = strtolower(hash("sha512", $retHashSeq));

		// log section
		error_log(date('[Y-m-d H:i e] '). "Payu Success Log For OrderId : ".$this->session->userdata('user_ordid') . PHP_EOL, 3, PAYU_LOG_FILE);
		error_log(date('[Y-m-d H:i e] '). "Payu Success POST: ".print_r($_POST, true) . PHP_EOL, 3, PAYU_LOG_FILE);
		error_log(date('[Y-m-d H:i e] '). "Payu Success hashsequence: $retHashSeq" . PHP_EOL, 3, PAYU_LOG_FILE);
		error_log(date('[Y-m-d H:i e] '). "Payu Success hash: $hash" . PHP_EOL, 3, PAYU_LOG_FILE);
		error_log(date('[Y-m-d H:i e] '). "Payu Success posted hash: $posted_hash" . PHP_EOL, 3, PAYU_LOG_FILE);
		error_log(date('[Y-m-d H:i e] '). "Payu Success OrderId: $orderid" . PHP_EOL, 3, PAYU_LOG_FILE);
		// log section ends
		
		if ($hash != $posted_hash) {

			$data['msgerr'] = 'Invalid Transaction. Please try again';

		}else {

			//$user_ordid = $this->session->userdata('user_ordid');
		    $user_ordid = $orderid;
		
			if( empty( $user_ordid ) ){

				redirect(base_url());

			}else{

				/* mark order as completed */
				$currencyinr = $this->db->query("SELECT `price_value` FROM currency where id=1")->result();
			    $currency = $currencyinr[0]->price_value;
				//echo $currency;exit;
				$result = $this->db->query("SELECT `ordtotal` FROM orders WHERE `ordid` = $user_ordid")->result();
			    $data = $result[0]->ordtotal;
			    $amount = $data*$currency;

				$this->GeneralModel->UpdateRow( "orders", array( 'ordstatus' => 'Completed', 'item_name' => $productinfo, 'payment_currency' => 'INR', 'txn_id' => $txnid, 'payment_gateway' => 'PayU', 'ordtotal_inr' => $amount ), array( 'ordid' => $user_ordid ) );

				$ciddata = $this->db->query("SELECT `odetsid` FROM order_detail where fk_ordid = $user_ordid")->result();
				$cid = $ciddata[0]->odetsid;
				//print_r($cid); exit;
				
				//$loggedid = $this->session->userdata('loggedid');
				$stuid = $this->db->query("SELECT `fk_stuid` FROM orders where ordid = $user_ordid")->result();
		        $stuid_update = $stuid[0]->fk_stuid;

				$this->GeneralModel->UpdateRow( "student_discount", array( 'status' => 1), array( 'cid' => $cid, 'stuid'=>$stuid_update ) );
				
				$this->mailInvoice( $user_ordid );

			}
			
			// $this->mailInvoice( $user_ordid );
			// $this->mailInvoiceAdmin( $user_ordid );
			
			$this->session->unset_userdata('user_ordid');
			$this->cart->destroy();
            
            $msg = "Thank You ! Your Order has been Placed.Your payment to EXAMROADMAP is in progress.Please check your email and find the link to download the Invoice and PDF.";
			
			$data['msgh1'] = 'Purchased Successfully';
			$data['msgsuc'] = 'Thank You! Your order has been placed!' . $msg;
			$data['ondemand'] = 0;

		}
		
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('payusuccess', $data);
		$this->load->view('include_front/footer');	

	}

	public function payufailure(){
		
		// $currency_code=$_POST["udf1"];
		// $amount=$_POST["amount"];	

		$user_ordid = $this->session->userdata('user_ordid');
		
		if( empty( $user_ordid ) )
		{
			redirect(base_url());
		}
		else
		{
			$id = $this->session->userdata('user_ordid');

			$currencyinr = $this->db->query("SELECT `price_value` FROM currency where id=1")->result();
		    $currency = $currencyinr[0]->price_value;
			
			$result = $this->db->query("SELECT `ordtotal` FROM orders WHERE `ordid` = $id")->result();
			$data = $result[0]->ordtotal;
			$cancelamount = $data*$currency;
			/* mark order as cancelled */
			// $this->GeneralModel->UpdateRow( "orders", array( 'ordstatus' => 'Cancelled','payment_currency' => $currency_code, 'ordtotal' => $amount ), array( 'ordid' => $user_ordid ) );
            $this->GeneralModel->UpdateRow( "orders", array( 'ordstatus' => 'Cancelled', 'payment_gateway' => 'PayU', 'payment_currency' => 'INR', 'ordtotal_inr' => $cancelamount ), array( 'ordid' => $user_ordid ) );
            
            

		}
		
		$this->session->unset_userdata('user_ordid');
		$this->cart->destroy();
		
		/* echo $_SERVER['DOCUMENT_ROOT'] . 'projects/sap/uploads/invoices/' . $ordid . '.pdf';exit; */
		
		$data['msgh1'] = 'Payment Cancelled';
		$data['msgerr'] = 'Payment has been cancelled by user.';
		$data['ondemand'] = 0;
		$data['title'] = "Payment Cancelled | examroadmap.com";
		
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('payufailure', $data);
		$this->load->view('include_front/footer');	

	}

	public function payucancel(){
	   
	}
	
	
	public function stripesuccess(){
	    
	    $this->load->helper('file');

	    \Stripe\Stripe::setApiKey("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");
	    
	    $body = @file_get_contents('php://input');

            write_file('application/logs/logs.txt', $body, 'a');
            
            $js = json_decode($body, true);
            
            $data['name'] = $js["data"]["object"]["charges"]["data"][0]["billing_details"]["name"];
            $data['email'] = $js["data"]["object"]["charges"]["data"][0]["billing_details"]["email"];
            $data['product'] = $js["data"]["object"]["charges"]["data"][0]["description"];
            $data['amount'] = $js["data"]["object"]["amount_received"];
            $data['payment_id'] = $js["data"]["object"]["id"];
            $data['receipt_url'] = $js["data"]["object"]["charges"]["data"][0]["receipt_url"];
            $data['payment_status'] = $js["data"]["object"]["status"];
            

            $address = $js["data"]["object"]["charges"]["data"][0]["billing_details"]["address"];
            $data['billing_address'] = $address["line1"].",".$address["line2"].",".$address["city"].",".$address["state"]."-".$address["postal_code"].",".$address["country"];
            
            $js["data"]["object"]["id"];
                        
           
           $meta_orderid = $js["data"]["object"]["charges"]["data"][0]["metadata"]["orderid"];
           $coursetype = $js["data"]["object"]["charges"]["data"][0]["metadata"]["coursetype"];
            $taxid = $js["data"]["object"]["id"];
            $gateway = "Stripe";
            $orderstatus = "Completed";
            $currency = strtoupper($js["data"]["object"]["charges"]["data"][0]["currency"]);
            
            $amount = $js["data"]["object"]["amount_received"];
            $stripeamount = $amount/100;
            
            if($coursetype == 'ondemand'){
                
                $updateData=array('ordstatus' => $orderstatus, 'payment_gateway' => $gateway, 'payment_currency' => $currency, 'txn_id' => $taxid, 'ordtotal_inr' => $stripeamount);
                    $this->db->where("ordd_id",$meta_orderid);
                    $this->db->update("orders_demand",$updateData);
                    
                    $this->mailInvoice( $meta_orderid );
            
            }else{
            
            
                   $updateData=array('ordstatus' => $orderstatus, 'payment_gateway' => $gateway, 'payment_currency' => $currency, 'txn_id' => $taxid, 'ordtotal_inr' => $stripeamount);
                    $this->db->where("ordid",$meta_orderid);
                    $this->db->update("orders",$updateData);
                    $data['order_id'] = $meta_orderid;
         
         
                    $this->db->insert('stripe_payment',$data);
        
            		$ciddata = $this->db->query("SELECT `odetsid` FROM order_detail where fk_ordid = $meta_orderid")->result();
            		$cid = $ciddata[0]->odetsid;
        		    //print_r($cid); exit;
        		
            		//$loggedid = $this->session->userdata('loggedid');
            		$stuid = $this->db->query("SELECT `fk_stuid` FROM orders where ordid = $meta_orderid")->result();
            		$stuid_update = $stuid[0]->fk_stuid;
            		//echo $stuid_update; exit;
            
            		$this->GeneralModel->UpdateRow( "student_discount", array( 'status' => 1), array( 'cid' => $cid, 'stuid'=>$stuid_update ) );
                        
                    $this->mailInvoice( $meta_orderid );
            }
            
	    
	}
	
	
	public function stripesuccessmsg(){
	    $this->session->unset_userdata('user_ordid');
		$this->cart->destroy();
		$this->load->view('striperesponse/success.html');
	
	}
        
	
	public function stripecancelmsg(){
        $id = $this->session->userdata('user_ordid');
        $stripeamount = $this->db->query("SELECT `ordtotal` FROM orders where ordid = $id")->result();
        $amount = $stripeamount[0]->ordtotal;
        //echo $amount; exit;
        
        $updateData=array('ordstatus' => 'Cancelled', 'payment_gateway' => 'Stripe', 'payment_currency' => 'USD', 'ordtotal_inr' => $amount);
            $this->db->where("ordid",$id);
            $this->db->update("orders",$updateData);
        
        $this->session->unset_userdata('user_ordid');
		$this->cart->destroy();
		
	    $this->load->view('striperesponse/cancel.html');

	}



	public function razorPaySuccess()
    { 
	    
        $this->load->helper('file'); 
		$body = @file_get_contents('php://input'); 
        write_file('application/logs/razorpay.log', $body, 'a');
        
        $js = json_decode($body, true);
        
        //echo $js; exit;
        
        $inramount = $js["payload"]["payment"]["entity"]["amount"]/100; 
         $data['amount'] = $inramount; 
        $data['payment_id'] = $js["payment_id"]; 
        $data['payment_status'] = $js["payload"]["payment"]["entity"]["status"];
        $data['email'] = $js["payload"]["payment"]["entity"]["email"];
        $data['product'] = $js["payload"]["payment"]["entity"]["description"];
        //$data['amount'] = $js["payload"]["payment"]["entity"]["amount"];
        $data['order_id'] = $js["payload"]["payment"]["entity"]["notes"]["order_id"];
        $data['	payment_id'] = $js["payload"]["payment"]["entity"]["id"];
        
        $payment_status = $js["payload"]["payment"]["entity"]["status"];

        $insert = $this->db->insert('razorpay_payment', $data);
        
        $meta_orderid = $js["payload"]["payment"]["entity"]["notes"]["order_id"];
        //echo $meta_orderid; exit;
        $taxid = $js["payload"]["payment"]["entity"]["id"];
        $gateway = "Razor Pay";
        $currency = $js["payload"]["payment"]["entity"]["currency"];
        
        if($data['payment_status'] == 'authorized'){
            $orderstatus = "Completed";
        }
        else{
            $orderstatus = "Cancelled";
        }
		
        $updateData=array('ordstatus' => $orderstatus, 'payment_gateway' => $gateway, 'payment_currency' => 'INR', 'txn_id' => $taxid, 'ordtotal_inr' => $inramount);
        $this->db->where("ordid",$meta_orderid);
        $this->db->update("orders",$updateData);
		
		//$meta_orderid = 1791;
		
// 		$ciddata = $this->db->query("SELECT `odetsid` FROM order_detail where fk_ordid = $meta_orderid")->result();
// 		$cid = $ciddata[0]->odetsid;
		//print_r($cid); exit;
		
		//$loggedid = $this->session->userdata('loggedid');
		
		$ciddata = $this->db->query("SELECT `odetsid` FROM order_detail where fk_ordid = $meta_orderid")->result();
		$cid = $ciddata[0]->odetsid;
		//print_r($cid); exit;
		
		//$loggedid = $this->session->userdata('loggedid');
		$stuid = $this->db->query("SELECT `fk_stuid` FROM orders where ordid = $meta_orderid")->result();
		$stuid_update = $stuid[0]->fk_stuid;

		$this->GeneralModel->UpdateRow( "student_discount", array( 'status' => 1), array( 'cid' => $cid, 'stuid'=>$stuid_update ) );

		
		
// 		$this->db->where('cid',$cid); 
// 		$this->db->where('stuid',$loggedid); 
//         $this->db->set('status','1');
//         $this->db->update('student_discount');

        
        $this->mailInvoice( $meta_orderid );
  
    }
   
     
    public function RazorThankYou()
    {   
        $this->session->unset_userdata('user_ordid');
 		$this->cart->destroy();
        $this->load->view('razorthankyou.html');
    }
    
    public function RazorFailed()
    {   
		$currencyinr = $this->db->query("SELECT `price_value` FROM currency where id=1")->result();
		$currency = $currencyinr[0]->price_value;
		 //print_r($currency); exit;
        $id = $this->session->userdata('user_ordid');
		$result = $this->db->query("SELECT `ordtotal` FROM orders WHERE `ordid` = $id")->result();
		$data = $result[0]->ordtotal;
		$cancelamount = $data*$currency;
        
        //print_r($id); exit;
        $updateData=array('ordstatus' => 'Cancelled', 'payment_gateway' => 'Razor Pay', 'ordtotal_inr' => $cancelamount, 'payment_currency' => 'INR');
            $this->db->where("ordid",$id);
            $this->db->update("orders",$updateData);
        $this->session->unset_userdata('user_ordid');
 		$this->cart->destroy();
        $this->load->view('razorfailed.html');
    }
	
	
	public function downloadinvoices($ordid, $studentid)
    {   
        //echo $studentid;
         $invvalue = $this->db->query("SELECT `orders`.`fk_stuid`, `students`.* FROM `orders` INNER JOIN `students` ON `orders`.`fk_stuid` = `students`.`stuid` WHERE orders.ordid = '". $ordid ."' ORDER BY `orders`.`fk_stuid` ")->result();
         
         $stud = $invvalue[0]->fk_stuid;
         //echo $stud; exit;
         
        if($stud == $studentid){
        
        //$file = basename($_GET['file']);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/invoices/Invoice_' . $ordid . '.pdf';
        $fileheader = "Invoice_$ordid.pdf";
        
        if(!file_exists($file)){
            die('file not found');
        } else {
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$fileheader");
            header("Content-Type: application/pdf");
            header("Content-Transfer-Encoding: binary");
        
            // read the file from disk
            readfile($file);
        }
        
    } else{ echo "File does not exist";
    } 
    
    }
    
  	public function questiondocument($ordid,$odetsid){
  	    
  	    $quesdocs = $this->db->query("SELECT `orders`.`ordid`, `order_detail`.*, `exams`.* FROM `orders` INNER JOIN `order_detail` ON `orders`.`ordid` = `order_detail`.`fk_ordid` INNER JOIN `exams` ON `order_detail`.`odetsid` = `exams`.`sid` WHERE orders.ordid = '". $ordid ."' AND order_detail.odetsid = $odetsid")->result();
		  //print_r($quesdocs); exit;
		  $filename = $quesdocs[0]->quedocs;
		  $validity = $quesdocs[0]->odet_validity;
		  //echo $validity; exit;
		  $today = date("Y-m-d");
		  //echo $today; exit;
		
		if($validity>=$today){
		//$dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/questiondocument/'.$filename;
		//$fileheader = "$filename.pdf";
		
		$file = $_SERVER['DOCUMENT_ROOT'] . '/uploads/questiondocument/'.$filename;
		if(!file_exists($file)){
            die('file not found');
        } else {
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/pdf");
            header("Content-Transfer-Encoding: binary");
        
            // read the file from disk
            readfile($file);
        }
  	    
  	} else{
  	    echo "Exam Validity Expired";
  	} 
	
    }
    
    public function testmail(){
        $this->mailInvoice( 1818 );
    }
}

?>