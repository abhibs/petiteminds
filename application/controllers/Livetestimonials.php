<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Livetestimonials extends CI_Controller {

	public function __construct() {

        parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
		$this->load->library('cart');
	}
	
	public function index()
	{		
	
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		
		$data['livetmon'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'livetmon', $limit = '', $start = '', $columns = '', $orderby = 'livetmon.livetmonord', $key = array( 'livetmon.livetmonshow' => 'Y' ), $search = '', $join_ar = '', $group_by = '' );
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
		$data['title'] = "Live Testimonials | examroadmap.com";
					
		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');	
		$this->load->view('livetestimon', $data);
		$this->load->view('include_front/footer');	
	}	
	
}

?>