<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packages extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session', 'form_validation'));
		$this->load->model('GeneralModel');
		$this->load->library('cart');
		$this->load->helper('meta');
	}

	public function index()
	{

		$data['title'] = "PACKAGES | examroadmap.com";
		$psearch = $this->input->post('psearch');

		$qry = "SELECT * FROM subcategory WHERE status='A'";

		if (!empty($psearch)) {
			/*$qry .= " WHERE lower( name ) like '%".$psearch."%' or lower( scatdesc ) like '%".$psearch."%'";*/
			$qry .= " and lower( name ) like '%" . $psearch . "%'";
		}

		/*
        echo "<hr><pre>qry: ";
        var_dump( $qry );
        exit;
        */

		$data['subcategory'] = $this->db->query($qry)->result();

		/*$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = '' );*/

		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');
		$this->load->view('packages', $data);
		$this->load->view('include_front/footer');
	}

	function add()
	{

		/* 		$post = $this->input->post();

		echo "<hr><pre>post: ";
		var_dump( $post );
		exit; */
		$id = $this->input->post('id');
		$cart_info = @$_POST['cart'];
		$addflg = true;

		$cart = $this->cart->contents();

		if (!empty($cart)) {
			foreach ($cart as $key => $cart) {

				/* 				echo "<hr><pre>id: ";
				var_dump( $cart['id'] ); */

				if ($id == $cart['id'])
					$addflg = false;
			}
		}

		/* exit; */

		/*		
echo "<hr><pre>addflg: ";
var_dump( $addflg );
exit;
*/

		/* // Set array for send data. */
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$price = $this->input->post('price');
		$icon = $this->input->post('icon');
		//echo $price; exit;

		// $loggedin = $this->session->userdata('loggedin');
		 $loggedid = $this->session->userdata('loggedid');
		//echo $loggedid; exit;

		// $data['discount'] = $this->db->get_where('student_discount',array('stuid'=>$loggedid, 'status'=>0, 'cid'=>$id))->result_array();
		// print_r($data); exit;


		$data['discount'] = $this->GeneralModel->GetSelectedRows( $table = 'student_discount', $limit = '', $start = '', $columns = 'disamount,discpercent,amounttype', $orderby ='', $key = array( 'stuid'=>$loggedid, 'status'=>0, 'cid'=>$id ), $search = '' );
		//print_r($data); exit;
		
		$disc_type = $data['discount'][0]->amounttype;

		if($disc_type == 'percent'){

			$percent = $data['discount'][0]->discpercent;
			
			$disrate = $percent/100; 
			$disc_amount = $price * $disrate;
		}else if($disc_type == 'amount'){
			
			$disc_amount = $data['discount'][0]->disamount;
		}

		//echo $disc_amount; exit;

		if ($addflg) {
			/*
			$insert_data = array(
			'id' => '1',
			'name' => 'SAP S4 HANA SOURCING AND PROCUREMENT',
			'price' => '5',
			'icon' => 'abc',
			'qty' => 1
			);
			*/

			$this->cart->product_name_rules = '[:print:]';

			$insert_data = array(
				'id' => $id,
				'name' => addslashes($name),
				'price' => $price,
				'icon' => $icon,
				'qty' => 1,
				'discount' => $disc_amount
			);

			/* // This function add items into cart. */
			$this->cart->insert($insert_data);
			
		
		}

		/* // This will show insert data in cart. */
		redirect('packages/cart');
	}

	public function cart()
	{
		// $loggedin = $this->session->userdata('loggedin');
		 $loggedid = $this->session->userdata('loggedid');
		//echo $loggedid; exit;

		//$data['discount'] = $this->db->get_where('student_discount',array('stuid'=>$loggedid, 'status'=>0))->result_array();
		//print_r($data); exit;



		$data['title'] = "YOUR CART | examroadmap.com";
		$loggedscarr = $this->session->userdata('loggedscarr');
		//print_r($loggedscarr); exit;

		$cart_info = @$_POST['cart'];


		if (!empty($cart_info)) {
			foreach ($cart_info as $id => $cart) {
				$rowid = $cart['rowid'];
				$price = $cart['price'];
				$amount = $price * $cart['qty'];
				$qty = $cart['qty'];
				$discount = $cart['discount'];

				$data = array(
					'rowid' => $rowid,
					'price' => $price,
					'amount' => $amount,
					'qty' => $qty,
					'discount' => $discount
				);
				$this->cart->update($data);
			}
		} 

	
		//$data['cart']['discount'] = $disc_amount;

		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');
		$this->load->view('cart');
		$this->load->view('include_front/footer');
	}

	public function checkout()
	{
		$data['title'] = "CHECKOUT | examroadmap.com";
		if (!$this->cart->total_items()) {
			redirect(base_url() . 'packages/cart');
		}

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
		$redchkt = $this->session->userdata('redchkt');

		if (empty($loggedid)) {
			/* 			if( ! isset( $redchkt ) )
			{
				$this->session->set_userdata('redchkt',true);
			}
			else
			{
				$this->session->unset_userdata('redchkt');
			} */

			$this->session->set_userdata('redtochk', true);

			redirect(base_url('login?chk=t'));
		}

		/* echo "<hr><pre>loggedid: ";
var_dump( $loggedid );
exit; */
		/* echo 'proceeding to checkout';exit; */

		/* 		$loggedscarr = $this->session->userdata('loggedscarr');	
		
		$data['discount'] = $this->GeneralModel->GetSelectedRows($table = 'discount', $limit = '', $start = '', $columns = '', $orderby = 'amount desc', $key = array( 'status' => 'A' ), $search = '');
		
		$loggedid = $this->session->userdata('loggedid');
		
		$data['users'] = $this->GeneralModel->GetInfoRow($table = 'users', array( 'id' => $loggedid ) );

		$this->load->view( 'checkout', $data); */

		/*
		$this->saveord();
		*/

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');

		$data['students'] = $this->GeneralModel->GetInfoRow($table = 'students', $key = array('stuid' => $loggedid));
		$data['currencydata'] = $this->GeneralModel->GetCurrencyData();

		$data['page'] = 'packages';

		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');
		$this->load->view('checkout', $data);
		$this->load->view('include_front/footer');
	}

	function remove($rowid)
	{

		/* // Check rowid value. */
		if ($rowid === "all") {
			/* // Destroy data which store in session. */
			$this->cart->destroy();
		} else {
			/* // Destroy selected rowid in session. */
			$data = array(
				'rowid' => $rowid,
				'qty' => 0
			);
			/* // Update cart data, after cancel. */
			$this->cart->update($data);
		}

		/* // This will show cancel data in cart. */
		redirect('packages/cart');
	}

	function view($sid)
	{

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');

		$data['students'] = $this->GeneralModel->GetInfoRow($table = 'students', $key = array('stuid' => $loggedid));

		$data['page'] = 'packages';

		$join_ar = array(
			0 => array(
				"table" => "category",
				"condition" => "subcategory.cid = category.id",
				"type" => "INNER"
			),
			1 => array(
				"table" => "exams",
				"condition" => "subcategory.id = exams.sid",
				"type" => "LEFT"
			)
		);

		$data['subcategory'] = $this->GeneralModel->GetSelectedRowsJoins($table = 'subcategory', $limit = '1', $start = '', $columns = 'subcategory.*, category.catname, exams.examid, exams.examtitle, exams.examtimelimit', $orderby = 'exams.examid desc', $key = array('subcategory.id' => $sid, 'exams.examtype' => 'PAID'), $search = '', $join_ar, $group_by = '');

		$join_ar = array(
			0 => array(
				"table" => "category",
				"condition" => "subcategory.cid = category.id",
				"type" => "INNER"
			),
			1 => array(
				"table" => "exams",
				"condition" => "subcategory.id = exams.sid",
				"type" => "LEFT"
			)
		);

		$data['subcat_free'] = $this->GeneralModel->GetSelectedRowsJoins($table = 'subcategory', $limit = '1', $start = '', $columns = 'subcategory.*, category.catname, exams.examid, exams.examtitle, exams.examtimelimit', $orderby = 'exams.examid desc', $key = array('subcategory.id' => $sid, 'exams.examtype' => 'FREE'), $search = '', $join_ar, $group_by = '');

		
		
		$join_ar = array(
			0 => array(
				"table" => "exams",
				"condition" => "exams.sid = student_discount.cid",
				"type" => "INNER"
			)
		);

		$data['discount'] = $this->GeneralModel->GetSelectedRowsJoins($table = 'student_discount', $limit = '1', $start = '', $columns = 'student_discount.*, exams.examid, exams.examtitle', $orderby = '', $key = array('exams.sid' => $sid, 'exams.examtype' => 'PAID','student_discount.stuid'=>$loggedid), $search = '', $join_ar, $group_by = ''); 
		//print_r($data); exit;

		/* print "<hr><pre>".$this->db->last_query();exit; */

		if (empty($data['subcategory'])) {
			redirect(base_url());
		}
		
		$data['inr_value'] = $data['subcategory'][0]->yearsubsc * $this->GeneralModel->GetCurrencyData();

		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('packsingle', $data);
		$this->load->view('include_front/footer');
	}

	function buynow($examid)
	{

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');

		$data['students'] = $this->GeneralModel->GetInfoRow($table = 'students', $key = array('stuid' => $loggedid));

		$data['page'] = 'packages';

		$join_ar = array(
			0 => array(
				"table" => "category",
				"condition" => "subcategory.cid = category.id",
				"type" => "INNER"
			),
			1 => array(
				"table" => "exams",
				"condition" => "subcategory.id = exams.sid",
				"type" => "LEFT"
			)
		);

		$data['subcategory'] = $this->GeneralModel->GetSelectedRowsJoins($table = 'subcategory', $limit = '', $start = '', $columns = 'subcategory.*, category.catname, exams.examid, exams.examtitle, exams.examdesc, exams.examtimelimit', $orderby = 'id', $key = array('exams.examid' => $examid), $search = '', $join_ar, $group_by = '');


		$join_ar = array(
			0 => array(
				"table" => "exams",
				"condition" => "exams.sid = student_discount.cid",
				"type" => "INNER"
			)
		);

		$data['discount'] = $this->GeneralModel->GetSelectedRowsJoins($table = 'student_discount', $limit = '1', $start = '', $columns = 'student_discount.*, exams.examid, exams.examtitle', $orderby = '', $key = array('exams.examid' => $examid, 'exams.examtype' => 'PAID','student_discount.stuid'=>$loggedid), $search = '', $join_ar, $group_by = ''); 
		//print_r($data); exit;


		if (empty($data['subcategory'])) {
			redirect(base_url());
		}
		
		$data['inr_value'] = $data['subcategory'][0]->yearsubsc * $this->GeneralModel->GetCurrencyData();
		$data['inr_convert'] = $this->db->query("select * from currency")->result();

		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');
		$this->load->view('buynow', $data);
		$this->load->view('include_front/footer');
	}

	function msg()
	{

		$data['msgh1'] = 'Purchased Successfully';
		$data['msgsuc'] = 'Thank You! Your order has been placed!';
		$data['title'] = "Purchased Successfully | examroadmap.com";

		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');
		$this->load->view('msg', $data);
		$this->load->view('include_front/footer');
	}

	function saveord()
	{

		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');

		$time365 = date('Y-m-d H:i:s', strtotime(' + 60 days'));

		if ($cart = $this->cart->contents()) {
			$ordtotal = 0;
			$fk_ordid = $this->GeneralModel->AddNewRow("orders", array('fk_stuid' => $loggedid, 'orddatec' => date('Y-m-d H:i:s')));

			foreach ($cart as $item) {
				/* store user subjects */
				$this->GeneralModel->AddNewRow("order_detail", array('fk_ordid' => $fk_ordid, 'odetsid' => $item['id'], 'odetqty' => $item['qty'], 'odetprice' => $item['price'], 'odet_validity' => $time365));

				$ordtotal = $ordtotal + $item['price'];
			}

			$this->GeneralModel->UpdateRow("orders", array('ordtotal' => $ordtotal, 'ordstatus' => 'Completed'), array('ordid' => $fk_ordid));
		}

		$this->cart->destroy();


		$data['msgh1'] = 'Purchased Successfully';
		$data['msgsuc'] = 'Thank You! Your order has been placed!';
		$data['title'] = "Purchased Successfully | examroadmap.com";

		$this->load->view('include_front/head',$data);
		$this->load->view('include_front/nav');
		$this->load->view('msg', $data);
		$this->load->view('include_front/footer');
	}
}