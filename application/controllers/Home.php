<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    
    public function __construct() {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->model('dashboard_model');
        $this->load->model('GeneralModel');
    }

	public function index()
	{		
	    
	    
		/* $arr_menu['supercatlist'] = $this->Cms_model->supercat_list();

		$arr['product_list'] = $this->Cms_model->product_home_list(); */
		
        
        $data['subcategory'] = $this->GeneralModel->GetSelectedRows($table = 'subcategory', $limit = '3', $start = '', $columns = '', $orderby ='', $key = '', $search = '');
        
        
        /*
		$join_ar = array(
							0 => array(
								"table" => "category",
								"condition" => "subcategory.cid = category.id",
								"type" => "INNER"
							),
							1 => array(
								"table" => "exams",
								"condition" => "subcategory.id = exams.sid",
								"type" => "LEFT"
							)
						);

		$data['subcategory'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'subcategory', $limit = '3', $start = '', $columns = 'subcategory.*, category.catname, exams.examid, exams.examtitle, exams.examtimelimit', $orderby = 'exams.examid desc', $key = array( 'exams.examtype' => 'PAID' ), $search = '', $join_ar, $group_by = 'subcategory.id' );
		*/
		
        /*print "<hr><pre>".$this->db->last_query();exit;*/
        
        $data['title'] = "SAP Certifications Preparation for All Levels | ExamRoadmap";
        $data['discription'] = "We are providing different forms of SAP certification exams. You can check the list of all the available certification exams that ExamRoadmap offers.";
        $data['keywords'] = "SAP certification, SAP global certification, SAP Sample questions, SAP Sample answer, SAP online exam, SAP online certification exam, practice for SAP exam, SAP preparation, preparation of SAP, SAP courses, SAP course details, SAP practice exams";
        
		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('index', $data);
		$this->load->view('include_front/footer');
	}
	
	public function aboutus()
	{		
		/* $arr_menu['supercatlist'] = $this->Cms_model->supercat_list();

        $arr['product_list'] = $this->Cms_model->product_home_list(); */
        
        $data['title'] = "About us | ExamRoadmap.com";
        $data['discription'] = "ExamRoadmap offers the ideal platform to meet the demands of the constantly evolving SAP market. We are one of the world’s leading certification exam questions and answer online training providers.";
        $data['keywords'] = "About us";

		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('aboutus');
		$this->load->view('include_front/footer');
	}
	
	public function faq()
	{		
		/* $arr_menu['supercatlist'] = $this->Cms_model->supercat_list();

        $arr['product_list'] = $this->Cms_model->product_home_list(); */
        
        $data['title'] = "Frequently Asked Questions | ExamRoadmap.com";
        $data['discription'] = "Here in our FAQs you can find a collection of frequently asked questions and provided solutions.";
        $data['keywords'] = "Frequently Asked Questions, FAQ";

		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('faq');
		$this->load->view('include_front/footer');
	}

	public function privacy()
	{		
		/* $arr_menu['supercatlist'] = $this->Cms_model->supercat_list();

        $arr['product_list'] = $this->Cms_model->product_home_list(); */
        
        $data['title'] = "Privacy Policy | ExamRoadmap.com";
        $data['discription'] = "We are committed to secure your privacy, we take data protection and privacy very seriously.";
        $data['keywords'] = "Privacy Policy";

		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('privacy');
		$this->load->view('include_front/footer');
	}
	
	public function terms()
	{		
		/* $arr_menu['supercatlist'] = $this->Cms_model->supercat_list();

        $arr['product_list'] = $this->Cms_model->product_home_list(); */
        
        $data['title'] = "Terms and Conditions | ExamRoadmap.com";
        $data['discription'] = "By clicking on the SIGNUP option, the participant (You or Your) agrees to the Terms and Conditions, obligations, representations, warranties, and agreements contained herein the Agreement.";
        $data['keywords'] = "Terms and Conditions";

		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('terms');
		$this->load->view('include_front/footer');
	}

    public function change_password() {

        $this->load->view('Header');
        $this->load->view('Changepswd');
        $this->load->view('Footer');
    }


    public function change_pswd() {

     $this->load->library('form_validation');

     $this->form_validation->set_rules('passphrase', 'Password', 'trim|required');
     $this->form_validation->set_rules('cnfmpassphrase', 'Confirm Password', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
           $arr['page'] = 'changepswd';
            $this->load->view('Header',$arr);
            $this->load->view('Changepswd',$arr);
            $this->load->view('Footer');
        }
        else {

        $passphrase = $_POST['passphrase'];
        $cnfmpassphrase = $_POST['cnfmpassphrase'];
        $id = $this->session->userdata('adminid');

        if($passphrase==$cnfmpassphrase){

         $sql2 = "update admin_login set password = '".md5($passphrase)."' where id = '".$id."' ";
         $val2 =  $this->db->query($sql2);
         
             $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Password Changed Successfully!</div>');
              redirect('home/logout');
        }     
        else {
             $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Password did not matched!</div>');
              redirect('home/change_password');
        }
       
      }
            
        }


    public function forgot_password() {
        
        if ($this->session->unset_userdata('adminid')) {
            redirect('dashboard');
        } else {
        $this->load->view('Forgotpswd');
        }
    }


    public function forgot_pswd()
    {
        $email = $this->input->post('email');   
        
        $sql = "SELECT * FROM admin_login WHERE email = ? "; 
                $val = $this->db->query($sql,array($email));

        
        if($val->num_rows()>0){
            foreach ($val->result_array() as $recs => $res) {
                      $name = $res['username'];
                      $email = $res['email'];
                      $id = $res['id'];
                    }
        
        $sql2 = "update admin_login set password = '".md5($name.'@123')."' where id = '".$id."' "; 
         $val2 =  $this->db->query($sql2);


           $this->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => 'mail.sibinfotech.co.uk',
          'smtp_user' => 'support@sibinfotech.co.uk',
          'smtp_pass' => 'Admin@123',
          'smtp_port' => 587,
          'crlf' => "\r\n",
          'newline' => "\r\n"
        ));

$body_message = ' <!DOCTYPE html>
<html lang="en" style="margin: 0;padding: 0;">

<head style="margin: 0;padding: 0;">
    <meta charset="UTF-8" style="margin: 0;padding: 0;">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" style="margin: 0;padding: 0;">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" style="margin: 0;padding: 0;">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet" style="margin: 0;padding: 0;">
</head>

<body style="margin: 0;padding: 0;font-family: Roboto, sans-serif;font-size: 15px;">

    <div class="welcome-verification-email" style="margin: 0;padding: 0;">
        <table style="margin: 50px auto;padding: 0;border: 1px solid #e5e5e5;width: 600px;">
            <tr class="center-content" style="margin: 0;padding: 0;text-align: center;">
                <td class="common-padding" style="margin: 0;padding: 10px;vertical-align: middle;">
                    <img src="" alt="" style="height: 60px;margin: 0;padding: 0;">
                </td>
            </tr>
            <tr style="margin: 0;padding: 0;">
                <td class="common-padding top-padding" style="margin: 0;padding: 10px;vertical-align: middle;padding-top: 30px;">
                    <h3 class="hello-text" style="margin: 0;padding: 0;font-size: 18px;font-weight: 600;">
                        Hey '.$name.', please login using this password('.$name.'@123).
                    </h3>
                </td>
            </tr>
        </table>
    </div>

</body>

</html> ';

        $this->email->from('support@sibinfotech.co.uk', 'Admin');
        $this->email->to($email);
        $this->email->set_mailtype("html");
        $this->email->subject('Forgot Password');
        $this->email->message($body_message);
        $this->email->send();

        $this->session->set_flashdata('message','<div class="alert alert-success">Email Sent, Please check your email.</div>');

        }
    else {
    $this->session->set_flashdata('message','<div class="alert alert-danger">Email Address is not registered.</div>');
    }
        redirect('home');
}


     public function makecompany() {
		 
		$now = new DateTime();
		$now->setTimezone(new DateTimezone('Asia/Kolkata'));
		$temp = $now->format('Y-m-d H:i:s');
		 
		$customers = $this->GeneralModel->GetSelectedRowsJoins( $table = 'customers', $limit = '', $start = '', $columns = '', $orderby ='', $key = '', $search = '', $join_ar = '', $group_by = 'custname' );
		
/* echo "<hr><pre>customers: ";
var_dump( $customers );
exit; */
		
		if( !empty( $customers ) )
		{	
			foreach( $customers as $key => $value )
			{
				$company = $this->GeneralModel->GetInfoRow($table = 'company', $key = array( 'compname' => $value->custname ));
				
/* echo "<hr><pre>company: ";
var_dump( $company );
exit; */
				
				if( empty( $company ) )
				{
					$this->GeneralModel->AddNewRow( "company", array( 'compname' => $value->custname, 'compdatec' => $temp ) );

				}
			}
		}

	
	 }	
		 
     public function do_login() {

	         
        if ($this->session->userdata('adminid')) {
            redirect('dashboard');
        } else {
						
            $user = $_POST['email'];
            $password = $_POST['password'];
			
			$enc_pass  = md5($password);

            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('Login');
            } else {
					 
				 $sql = "SELECT * FROM admin_login WHERE email = ? AND password = ?"; 
                  $val = $this->db->query($sql,array($user ,$enc_pass ));
                   
				 $sql = "SELECT * FROM employee WHERE email = ? AND password = ?"; 
                  $empval = $this->db->query($sql,array($user ,$enc_pass ));
				   
				  if ($val->num_rows()) {
                    foreach ($val->result_array() as $recs => $res) {
                        $this->session->set_userdata(array(
                            'adminid' => $res['id'],
                            'username' => $res['username'],
                            'email' => $res['email'],
                            'role' => $res['role']
                                )
                        );
                    }
					
					$this->makecompany();
					
                    redirect('dashboard');
                } 
				else if ($empval->num_rows()) {
                    foreach ($empval->result_array() as $recs => $res) {
                        $this->session->set_userdata(array(
                            'adminid' => $res['id'],
                            'username' => $res['name'],
                            'email' => $res['email'],
                            'role' => 'employee'
                                )
                        );
                    }
					
					$this->makecompany();
					
                    redirect('form/add');
                }
				else {
                    $err['error'] = '<strong>Access Denied</strong><br> Invalid Username/Password';
                    $this->load->view('Login', $err);
                }
                
                }
            }
        }
           

        
    public function logout() {
        $this->session->unset_userdata('adminid');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role');
          
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('home');
    }

	public function blogs()
	{		
	
		$loggedin = $this->session->userdata('loggedin');
		$loggedid = $this->session->userdata('loggedid');
	
        $data['blog'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'blog', $limit = '', $start = '', $columns = '', $orderby = 'created_at desc', $key = array( 'blog.status' => 1 ), $search = '', $join_ar = '', $group_by = '' );
        
        $data['title'] = "Our Blog | ExamRoadmap.com";
        $data['discription'] = "You can read and share the blogs on SAP exams and certifications.";
        $data['keywords'] = "Blogs on SAP certification exams";
					
		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');	
		$this->load->view('blogall', $data);
		$this->load->view('include_front/footer');	
	}	
	
	public function invtest( $ordid )
	{
		/*error_reporting(E_ALL);
		ini_set('display_errors', 1);*/
		
		/* echo 'mail invoice'; */
		
		$orders = $this->db->query( "SELECT `orders`.*, `students`.*, order_detail.odet_validity FROM `orders` INNER JOIN `students` ON `orders`.`fk_stuid` = `students`.`stuid` INNER JOIN `order_detail` ON `orders`.`ordid` = `order_detail`.`fk_ordid` WHERE orders.`ordstatus` != '' and orders.ordid = '". $ordid ."' ORDER BY `orders`.`ordid` desc" )->result();
		
		$this->load->library('Pdf');
		/* $this->load->view('view_file'); */

		$now = new DateTime();
		$now->setTimezone(new DateTimezone('Asia/Kolkata'));
		$time = $now->format('d-m-Y H:i:s');
		$time365 = date('Y-m-d H:i:s', strtotime(' + 730 days'));
		
		/*
		
                    */
		
       $invoicemsg = '<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
</head>

<body style="padding-top: 100px;">
    <table style="margin: 0 auto; width: 500px;" width="500" cellpadding="10">
        <tr>
            <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px; border-right: none;">
                <img src="'. base_url() .'front/images/logo.png" alt="" style="height: 60px;" height="60">
            </td>
            <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px; text-align: right; border-left: none;" align="right">Date : '.$time.'</td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid #ddd; padding: 10px; font-size: 15px; text-align: center; background-color: #1A67C1; color: #fff; font-weight: bold;" align="center">Payment Receipt</td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">
                <table style="margin: 0 auto;" cellpadding="10">
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px !important; font-size: 15px;">Order ID</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.$ordid.'</td>
                    </tr>
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Name</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'. @$orders[0]->stuname .'</td>
                    </tr>
                    <tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Amount</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->ordtotal.' USD</td>
                    </tr>
					
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Taxation ID</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->txn_id.'</td>
                    </tr>
                    
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Subjects</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'.@$orders[0]->item_name.'</td>
                    </tr>
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Validity</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">'. date('d-m-Y', strtotime( $orders[0]->odet_validity ) ).'</td> 
                    </tr>
					<tr>
                        <th style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Buyer Details</th>
                        <td style="border: 1px solid #ddd; padding: 10px; font-size: 15px;">Name: '.@$orders[0]->stuname .'<br>Email: '.@$orders[0]->stuemail.'<br>Mobile: '.@$orders[0]->stumob.'</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="  padding: 10px; font-size: 15px; text-align: center; color: #1856a3; font-weight: bold;" align="center">Thank you for using SAPREP</td>
        </tr>
        
    </table>
</body>

</html>';
		
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		
/* 		
		$pdf->SetTitle('My Title');
		$pdf->SetHeaderMargin(30);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');

		$pdf->AddPage();

		$pdf->Write(5, 'Some sample text');
		$pdf->Output('My-File-Name.pdf', 'I'); */
		
        /* // set document information */
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('exactprep');
        $pdf->SetTitle('Invoice ' . $ordid);
        $pdf->SetSubject('Invoice ' .$ordid);
        $pdf->SetKeywords('exactprep, PDF, Invoice ' . $ordid);   
      
        /* // set header and footer fonts */
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
      
        /* // set default monospaced font */
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
      
        /* // set margins */
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
      
        /* // set auto page breaks */
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
      
        /* // set image scale factor */
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
      
        /* // set default font subsetting mode */
        $pdf->setFontSubsetting(true);   
      
        /* // Set font */
        $pdf->SetFont('helvetica', '', 14, '', true);   
      
        /* // Add a page */
        $pdf->AddPage(); 
      
        /* // set text shadow effect */
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));    
      
        /* // Print text using writeHTMLCell() */
        $pdf->writeHTMLCell(0, 0, '', '', $invoicemsg, 0, 1, 0, true, '', true);   
      
		$invpdf = $_SERVER['DOCUMENT_ROOT'] . '/projects/sap/uploads/invoices/invoice_' . $ordid . '.pdf' ;
		
        /* // saving pdf to uploads dir */
        $pdf->Output( $invpdf, 'F' );
        
        /* $pdf->Output( 'sampleinv.pdf', 'D' ); */
		
    }
    
    public function moneybackguarantee()
	{		
		/* $arr_menu['supercatlist'] = $this->Cms_model->supercat_list();

        $arr['product_list'] = $this->Cms_model->product_home_list(); */
        
        $data['title'] = "100% Money Back Guarantee & Refund Policy |ExamRoadmap.com";
        $data['discription'] = "We promise you 100% Money Back Guarantee if you fail to clear the actual SAPCertification exam. We are confident that you will not get a chance to use this option";
        $data['keywords'] = "Money Back Guarantee, Refund Policy";

		$this->load->view('include_front/head', $data);
		$this->load->view('include_front/nav');
		$this->load->view('moneybackguarantee');
		$this->load->view('include_front/footer');
	}
	

}

