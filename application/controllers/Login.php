<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {

        parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
	}
	
	public function logout()
	{
		/*
		mark user as logged out
		*/				
		
		$this->session->sess_destroy();
		
		redirect('login');
	}	

	public function password_check($str)
	{
	   $uemail = $this->session->userdata('uemail');
	   $users = $this->GeneralModel->GetInfoRow( 'users', $key = array( 'email' => $uemail, 'pass' => md5( $str ) ) );
		
	   if ( strlen( $str) >= 8 && preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {	
	   
			if( !empty( $users ) )
			{
				return TRUE;	
			}
			else
			{
				$this->form_validation->set_message('password_check', 'Old password is incorrect');			
				return FALSE;
			}
		
	   }
	   else
	   {
			$this->form_validation->set_message('password_check', 'The Password Must Be At Least 8 characters long with One Capital Letter, Numeric, Alphabetic And Special Character.');
			return FALSE;
	   }
	}
	
	public function changepass()
	{
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|callback_password_check');
		$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required');
		
		$data['error'] = ''; 
		
		if ($this->form_validation->run() == FALSE) {

			$data['email'] = $this->session->userdata('uemail');
			$data['title'] = "Required Fields | examroadmap.com";

			$this->load->view('include_front/head',$data);
			$this->load->view('include_front/nav');
			$this->load->view('changepass', $data);	
			$this->load->view('include_front/footer');


		}
		else{
			$email = $this->input->post('email');
			$newpassword = md5( $this->input->post('newpassword') );
			
			$users = $this->GeneralModel->UpdateRow($table = 'users', array('pass' => $newpassword ), array( 'email' => $email ));
			
			$data['msg'] = 'Password changed successfully. Please login again.';
			$data['title'] = "Password changed successfully. Please login again | examroadmap.com";
			
			$this->load->view('include_front/head',$data);
			$this->load->view('include_front/nav');

			$this->load->view( 'msg', $data );
			$this->load->view('include_front/footer');

		}
	}
	
	public function generateNumericOTP($n){
		
		$generator = "1357902468"; 	  		
		$result = ""; 	  		
		for ($i = 1; $i <= $n; $i++) 
		{ 			
			$result .= substr($generator, (rand()%(strlen($generator))), 1); 		
		} 	  
		return $result; 	
		
	}
	
	public function index()
	{		
		

		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		$data['error'] = ''; 
		
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "Login | examroadmap.com";
			
			$this->load->view('include_front/head',$data);
			$this->load->view('include_front/nav');	
			$this->load->view('login');
			$this->load->view('include_front/footer');
			
		}
		else{
			
/* 			error_reporting(E_ALL);
			ini_set('display_errors', 1); */
			
			$key['stuemail'] = $this->input->post('email');
			$key['stupass'] = md5( $this->input->post('password') );
			
			$users = $this->GeneralModel->GetInfoRow($table = 'students', $key);
			
			/* print "<hr><pre>".$this->db->last_query();exit; */
			
			if( !empty( $users ) )
			{
				$scarr = array();
								
/* 				$res = $this->db->query( 'SELECT * FROM `userscat` where userid = ' . @$users[0]->id . ' and validtill > now()' )->result();

				if( !empty( $res ) )
				{	
					foreach( $res as $key => $value )
					{
						$scarr[] = $value->scid;
					}
				} */				
				
				$this->session->set_userdata('loggedin',true);
				$this->session->set_userdata('loggedid',@$users[0]->stuid);
				$this->session->set_userdata('logstuname', @$users[0]->stuname);
				$this->session->set_userdata('logstuemail', @$users[0]->stuemail);
				$this->session->set_userdata('loggedscarr',@$scarr);
				
				/* redirect to exam */
				$redtoexam = $this->session->userdata('redtoexam');			
				$redtominexam = $this->session->userdata('redtominexam');	
				$redtoexamfree = $this->session->userdata('redtoexamfree');				
				if( !empty( $redtoexam ) && $redtoexam )
				{
					$join_ar = array(
										0 => array(
											"table" => "orders",
											"condition" => "order_detail.fk_ordid = orders.ordid",
											"type" => "inner"
										),
										1 => array(
											"table" => "exams",
											"condition" => "order_detail.odetsid = exams.sid",
											"type" => "inner"
										)
									);

					$order_detail = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders.fk_stuid' => @$users[0]->stuid, 'exams.examid' => $redtoexam ), $search = '', $join_ar, $group_by = '' );
					
					if( !empty( $order_detail ) )
					{						
						$this->session->unset_userdata('redtoexam');
						redirect( base_url('exams/index/' . $redtoexam) );	
					}
				}
				else if( !empty( $redtominexam ) && $redtominexam )
				{					
					$this->session->unset_userdata('redtominexam');
					redirect( base_url('exams/min/' . $redtominexam) );						
				}
				else if( !empty( $redtoexamfree ) && $redtoexamfree )
				{					
					$this->session->unset_userdata('redtoexamfree');
					redirect( base_url('exams/free/' . $redtoexamfree) );						
				}
				
				/* redirect to checkout or packages */
				if( !empty( $_GET['chk'] ) && $_GET['chk'] == 't' )
					redirect( base_url('packages/checkout') );	
				else 
					redirect( base_url('profile') );
				
			}
			else
			{
				$data['msg'] = '<span class="text-danger">Incorrect Login</span><br><br>';
				$data['title'] = "Incorrect Login | examroadmap.com";
				$this->load->view('include_front/head',$data);
				$this->load->view('include_front/nav');
				$this->load->view('login', $data);
				$this->load->view('include_front/footer');
			}
			
			
	}
	
	}
	
}

?>