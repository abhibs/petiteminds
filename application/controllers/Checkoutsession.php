<?php

// echo "hello"; exit;
// echo $_SERVER['DOCUMENT_ROOT']; exit;
    require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
    
class Checkoutsession extends CI_Controller {
    public function __construct() {

        parent::__construct();
		
    }
    
    function createsession($amount,$oredr_id,$stuname,$stuid){
        
        

        
        // $name = "Demo products";
        // include("application/views/checkout.php");
        // ini_set('display_errors',1);
        // error_reporting(E_ALL);
        //  $YOUR_AMOUNT = $_GET['amount'];
        //'success_url' => $YOUR_DOMAIN.'/stripe-response/success.php'
        
        \Stripe\Stripe::setApiKey('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');

        header('Content-Type: application/json');
        
	     $query = $this->db->select('item_name')->from('orders')->where('ordid',$oredr_id)->get();
	     
         $order_name = $query->result(); 
         $orders = json_encode($order_name);
         $orders_name = json_decode($orders);
         $orders = $orders_name[0]->item_name;


        $YOUR_DOMAIN = 'https://www.examroadmap.com';
       
        $checkout_session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
            'price_data' => [
                'unit_amount' => $amount,
                'currency' => 'USD',
                'product_data' => [
                'name' =>  $orders,
                // 'name' => 'Order ID' ,
                // 'images' => ["https://i.imgur.com/EHyR2nP.png"],
                ],
            ],
            'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $YOUR_DOMAIN.'/Order/stripesuccessmsg',
            'cancel_url' => $YOUR_DOMAIN.'/Order/stripecancelmsg',
            'payment_intent_data' => [
            'description' => $orders,
            'metadata' => [
                'id' => $stuid,
                'orderid' => $oredr_id,
                ],
            'shipping' => [
                'name' => 'Sapprep',
                'address' => [
                'line1' => 'A 702 Millie Enclave',
                'postal_code' => '400064',
                'city' => 'Mumbai',
                'state' => 'Maharashtra',
                'country' => 'US',
                ],
            ]
            ]
        ]);
        

        echo json_encode(['id' => $checkout_session->id]);
    }
    
    function createsessionondemand($amount,$oredr_id,$stuname,$stuid,$exam,$course){
        
        $newexam = urldecode($exam);
        
        \Stripe\Stripe::setApiKey('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');

        header('Content-Type: application/json');
        
        //$query = $this->db->select('scatdname')->from('subcatdemand')->where('scatdcode',$newexam)->get();
        
	   //  $query = $this->db->select('item_name')->from('orders')->where('ordid',$oredr_id)->get();
	     
    //      $order_name = $query->result(); 
    //      $orders = json_encode($order_name);
    //      $orders_name = json_decode($orders);
    //      $orders = $orders_name[0]->item_name;


        $YOUR_DOMAIN = 'https://www.examroadmap.com';
       
        $checkout_session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
            'price_data' => [
                'unit_amount' => $amount,
                'currency' => 'USD',
                'product_data' => [
                'name' =>  $newexam,
                // 'name' => 'Order ID' ,
                // 'images' => ["https://i.imgur.com/EHyR2nP.png"],
                ],
            ],
            'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $YOUR_DOMAIN.'/Ondemand/stripesuccessmsg',
            'cancel_url' => $YOUR_DOMAIN.'/Ondemand/stripecancelmsg',
            'payment_intent_data' => [
            'description' => $newexam,
            'metadata' => [
                'id' => $stuid,
                'orderid' => $oredr_id,
                'coursetype'=>$course,
                ],
            'shipping' => [
                'name' => 'Sapprep',
                'address' => [
                'line1' => 'A 702 Millie Enclave',
                'postal_code' => '400064',
                'city' => 'Mumbai',
                'state' => 'Maharashtra',
                'country' => 'US',
                ],
            ]
            ]
        ]);
        

        echo json_encode(['id' => $checkout_session->id]);
    }

}

?>