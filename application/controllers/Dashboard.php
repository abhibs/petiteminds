<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
		
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('dashboard_model');
        $this->load->model('GeneralModel');
         $this->load->library('email');
          $this->load->helper('url');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }
		
    }

    public function index() {
		
        $arr['page']='dashboard';
		
		$adminid = $this->session->userdata('adminid');
		$role = $this->session->userdata('role');

		/*$key = array( 'company.compleadsts' => 'COLD' );*/

		$arr['subcategory_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'subcategory', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');
		
		/*print "<hr><pre>".$this->db->last_query();exit;*/

		
		$arr['exams_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'exams', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');
		
	

        $this->load->view('Header',$arr);
        $this->load->view('index',$arr);
		$this->load->view('Footer');
    }


}