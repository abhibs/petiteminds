<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livetestimonials extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{	
		$data['livetmon'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'livetmon', $limit = '', $start = '', $columns = '', $orderby = 'livetmonid desc', $key = '', $search = '', $join_ar = '', $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/livetmonmanage', $data);
		$this->load->view('backend/Footer');
	}

	
	
	public function add()
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('livetmonurl', 'Url', 'trim|required');
		$this->form_validation->set_rules('livetmonord', 'Order', 'trim|required');
		$this->form_validation->set_rules('livetmonshow', 'Show', 'trim|required');

		$data['msg'] = ''; 
		
		$data['maxorder'] = $this->GeneralModel->GetSelectedRows($table = 'livetmon', $limit = '', $start = '', $columns = 'max(livetmon.livetmonord) as maxorder', $orderby ='', $key = '', $search = '');
		
		$data['maxorder'] = ( !empty( $data['maxorder'] ) ) ? $data['maxorder'][0]->maxorder + 1 : 0 ;
		/* print "<hr><pre>".$this->db->last_query();exit; */
		
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('backend/Header');
			$this->load->view('backend/livetmonadd', $data);
			
		}
		else{
			
			$idata['livetmonurl']	= $this->input->post('livetmonurl');
			$idata['livetmonord']	= $this->input->post('livetmonord');			
			$idata['livetmonshow']	= $this->input->post('livetmonshow');			
			$idata['livetmondatec']	= date('Y-m-d H:i:s');
			
			$cid = $this->GeneralModel->AddNewRow( "livetmon", $idata );
		
			$data['msg'] = 'Live Testimonial Added Successfully'; 
			
			$this->load->view('backend/Header');
			$this->load->view('backend/livetmonadd', $data);
			$this->load->view('backend/Header');

		}
	}

	public function edit( $id )
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('livetmonurl', 'Url', 'trim|required');
		$this->form_validation->set_rules('livetmonord', 'Order', 'trim|required');
		$this->form_validation->set_rules('livetmonshow', 'Show', 'trim|required');

		$data['msg'] = ''; 
		
		$data['maxorder'] = $this->GeneralModel->GetSelectedRows($table = 'livetmon', $limit = '', $start = '', $columns = 'max(livetmon.livetmonord) + 1 as maxorder', $orderby ='', $key = '', $search = '');
		
		$data['maxorder'] = ( !empty( $data['maxorder'] ) ) ? $data['maxorder'][0]->maxorder + 1 : 0 ;
		
		$data['msg'] = ''; 
		
		if ($this->form_validation->run() == FALSE) {
			
			$data['livetmon'] = $this->GeneralModel->GetInfoRow( $table = 'livetmon', $key = array('livetmonid'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/livetmonedit', $data);
			/* $this->load->view('backend/Header'); */
			
		}
		else{
			
			$idata['livetmonurl']	= $this->input->post('livetmonurl');
			$idata['livetmonord']	= $this->input->post('livetmonord');		
			$idata['livetmonshow']	= $this->input->post('livetmonshow');	
			
			$scid = $this->GeneralModel->UpdateRow( "livetmon", $idata, array('livetmonid'=>$id) );
		
/* 			$data['msg'] = 'Subcategory Edited Successfully'; 
			
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			$this->load->view('backend/Header'); */
			
			redirect( base_url('backend/livetestimonials?e=s') );

		}
	}

	
	
	public function delete($id)
	{					
		
		$this->GeneralModel->DeleteRow($table = 'livetmon', array('livetmonid'=>$id));
		
		redirect( base_url() . 'backend/livetestimonials?del=s' );
	}
	
}
?>