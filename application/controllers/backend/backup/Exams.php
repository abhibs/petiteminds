<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set("memory_limit", "-1");
set_time_limit(0);
ini_set('max_input_vars', '10000');

class Exams extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper(array('form','url', 'security','date','function'));
        $this->load->library(array('session', 'form_validation', 'email','pagination'));
        $this->load->model('GeneralModel');

         if (!$this->session->userdata('adminid')) {
            redirect('home');
        }

    }


    public function index( $type = '' ) {

        /*phpinfo();exit;*/

        $join_ar = array(
        					0 => array(
        						"table" => "subcategory",
        						"condition" => "exams.sid = subcategory.id",
        						"type" => "INNER"
        					)
        				);

        $key = array();

        if( !empty( $type ) )
        {
            $key['exams.examtype'] = $type;
        }

        $arr_page['exams']=$this->GeneralModel->GetSelectedRowsJoins( $table = 'exams', $limit = '', $start = '', $columns = 'exams.*, subcategory.name as subcname', $orderby = '', $key, $search = '', $join_ar, $group_by = '' );

        $arr['page'] = 'exams';

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/exammanage',$arr_page);
        $this->load->view('backend/Footer');

    }


    public function add() {

        $arr['page'] = 'exams';
        $this->createexams();
    }


    function createexams()
    {

		$this->load->library('form_validation');

		$this->form_validation->set_rules('examtitle', 'title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('examtype', 'Exam Type', 'trim|required|xss_clean');

		$data['page'] = 'exams';

        if ($this->form_validation->run() == FALSE)
        {
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = '' );

            $this->load->view('backend/Header');
            $this->load->view('backend/examadd', $data);
            /* $this->load->view('backend/Footer'); */
        }
        else
        {

/*
echo "<hr><pre>post: ";
var_dump( $_POST );
echo "<hr><pre>FILES: ";
var_dump( $_FILES );
exit;
*/

          $allwedtypes = array('image/jpeg','image/png','image/jpg');

			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');

            $data = array(
                'sid' => $this->input->post('sid'),
                'examtitle' => $this->input->post('examtitle'),
                'examdesc' => $this->input->post('examdesc'),
                'examavailable' => $this->input->post('examavailable'),
                'exampassrate' => $this->input->post('exampassrate'),
                'examtimelimit' => $this->input->post('examtimelimit'),
                'exambacknav' => $this->input->post('exambacknav'),
                'examtype' => $this->input->post('examtype'),
                'examtotal' => count( @$question ),
                'examdatec' => $temp
            );

			$examid = $this->GeneralModel->AddNewRow( "exams", $data );

            if( $examid )
            {
				$question = $this->input->post('question');
				$queimg = $_FILES['queimg'];
				$queimges = $_FILES['queimges'];

				$qtype = $this->input->post('qtype');
				$opt1 = $this->input->post('opt1');
				$opt2 = $this->input->post('opt2');
				$opt3 = $this->input->post('opt3');
				$opt4 = $this->input->post('opt4');
				$opt5 = $this->input->post('opt5');


				/* insert questions */
				for( $i = 0 ; $i < count( $question ) ; $i++ )
				{
				    if( !empty( $question[ $i ] ) )
					{

					    $data = array(
    						'fk_examid' => $examid,
    						'question' => $question[ $i ],
    						'quetype' => $qtype[ $i ],
    						'opt1' => $opt1[ $i ],
    						'opt2' => $opt2[ $i ],
    						'opt3' => $opt3[ $i ],
    						'opt4' => $opt4[ $i ],
    						'opt5' => $opt5[ $i ],
    						'quedatec' => $temp
    					);

    				    /*
    				    upload question image
    				    */

    				  
    				    if( !empty( $queimg['name'][ $i ] ) && in_array( $queimg['type'][ $i ], $allwedtypes ) )
    					{
        				    $target_dir = "./uploads/";
                			$target_file = $target_dir . 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
                			move_uploaded_file( $queimg['tmp_name'][ $i ], $target_file );
                			$data['queimg']	= 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
    					}
    					
    					if( !empty( $queimges['name'][ $i ] ) && in_array( $queimges['type'][ $i ], $allwedtypes ) )
                        {
                            $target_dir = "./uploads/";
                            $target_file = $target_dir . 'qimges_' . date('YmdHis') . '_' . $queimges['name'][ $i ];
                            move_uploaded_file( $queimges['tmp_name'][ $i ], $target_file );
                            $data['queimges'] = 'qimges_' . date('YmdHis') . '_' . $queimges['name'][ $i ];
                        }
    					

    					$answer = $this->input->post('answer'.$i);

    					if( !empty( $answer ) )
    					{
    						foreach( $answer as $key => $value )
    						{
    							$data[ $value ] = 'Y';
    						}
    					}

    					$data[ 'correct_cnt' ] = count( $answer );

    					$this->GeneralModel->AddNewRow( "questions", $data );

				    }

				}

			}


            if( $examid )
            {
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Created!</div>');
				redirect('backend/exams');
            }
            else
            {
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('backend/exams');
            }
        }

    }

    public function view( $id ) {

		$data['page'] = 'exams';

		$data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));

		/* print "<hr><pre>".$this->db->last_query();exit; */

		$this->load->view('backend/Header');
		$this->load->view('backend/examview', $data);
		$this->load->view('backend/Footer');
    }

    public function edit( $id ) {

        /*phpinfo();exit;*/

		$this->load->library('form_validation');

		$this->form_validation->set_rules('examtitle', 'title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('examtype', 'Exam Type', 'trim|required|xss_clean');

	    $data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));
        $examtype =  @$data['exams'][0]->examtype;

        if ($this->form_validation->run() == FALSE)
        {


			$data['questions']=$this->GeneralModel->GetSelectedRows($table = 'questions', $limit = '', $start = '', $columns = '', $orderby = 'queid', $key = array( 'fk_examid' => $id ), $search = '');

			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = '' );


			/* print "<hr><pre>".$this->db->last_query();exit; */

            $this->load->view('backend/Header');
            $this->load->view('backend/examedit', $data);
            /*$this->load->view('backend/Footer');*/
        }
        else
        {


/*echo preg_replace('/<(.*?)>/', '$1', "<title>");exit;*/

/*
echo "<hr><pre>post: ";
var_dump( $_POST );
exit;
*/

          $allwedtypes = array('image/jpeg','image/png','image/jpg');


			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');

            $data = array(
				'sid' => $this->input->post('sid'),
                'examtitle' => $this->input->post('examtitle'),
                'examdesc' => $this->input->post('examdesc'),
                'examavailable' => $this->input->post('examavailable'),
                'exampassrate' => $this->input->post('exampassrate'),
                'examtimelimit' => $this->input->post('examtimelimit'),
                'exambacknav' => $this->input->post('exambacknav'),
                'examtype' => $this->input->post('examtype'),
                'examtotal' => count( @$question ),
                'examdatec' => $temp
            );

			$this->GeneralModel->UpdateRow( "exams", $data, array( 'examid' => $id ) );

			/* delete only minus clicked queids */

			$del_queid = $this->input->post('del_queid');

			for( $i = 0 ; $i < count( $del_queid ) ; $i++ )
			{
			    if( !empty( $del_queid[ $i ] ) )
				{
				    $this->GeneralModel->DeleteRow($table = 'questions', array( 'queid' => $del_queid[ $i ] ));
				}

			}


            if( $id )
            {
                $queid = $this->input->post('queid');
				$question = $this->input->post('question');
				$queimgold = $this->input->post('queimgold');
				$queimg = $_FILES['queimg'];
				$queimges = $_FILES['queimges'];
				$qtype = $this->input->post('qtype');
				$opt1 = $this->input->post('opt1');
				$opt2 = $this->input->post('opt2');
				$opt3 = $this->input->post('opt3');
				$opt4 = $this->input->post('opt4');
				$opt5 = $this->input->post('opt5');

				/* insert questions */
				for( $i = 0 ; $i < count( $question ) ; $i++ )
				{
				    if( !empty( $question[ $i ] ) )
					{

						$data = array(
							'fk_examid' => $id,
							'question' => $question[ $i ],
							'quetype' => $qtype[ $i ],
							'opt1' => $opt1[ $i ],
							'opt2' => $opt2[ $i ],
							'opt3' => $opt3[ $i ],
							'opt4' => $opt4[ $i ],
							'opt5' => $opt5[ $i ],
							'quedatec' => $temp
						);

						/*
    				    upload question image
    				    */
    				    
    				    if( !empty( $queimg['name'][ $i ] ) && in_array( $queimg['type'][ $i ], $allwedtypes ) )
    					{
        				    $target_dir = "./uploads/";
                			$target_file = $target_dir . 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
                			move_uploaded_file( $queimg['tmp_name'][ $i ], $target_file );
                			$data['queimg']	= 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
    					}
    					else
    					{
    					    $data['queimg']	= @$queimgold[ $i ];
    					}
    					
    					if( !empty( $queimges['name'][ $i ] ) && in_array( $queimges['type'][ $i ], $allwedtypes ) )
                        {
                            $target_dir = "./uploads/";
                            $target_file = $target_dir . 'qimges_' . date('YmdHis') . '_' . $queimges['name'][ $i ];
                            move_uploaded_file( $queimges['tmp_name'][ $i ], $target_file );
                            $data['queimges'] = 'qimges_' . date('YmdHis') . '_' . $queimges['name'][ $i ];
                        }
                        else
                        {
                            $data['queimges'] = @$queimgoldes[ $i ];
                        }
						
              
						$answer = $this->input->post('answer'.$i);

						if( !empty( $answer ) )
						{
							$data[ 'opt1_iscorr' ] = 'N';
							$data[ 'opt2_iscorr' ] = 'N';
							$data[ 'opt3_iscorr' ] = 'N';
							$data[ 'opt4_iscorr' ] = 'N';
							$data[ 'opt5_iscorr' ] = 'N';

							foreach( $answer as $key => $value )
							{
								$data[ $value ] = 'Y';
							}
						}

            $data[ 'correct_cnt' ] = count( $answer );
            
            

                        if( !empty( $queid[ $i ] ) )
						    $this->GeneralModel->UpdateRow( "questions", $data, array( 'queid' => $queid[ $i ] ) );
						else
						    $this->GeneralModel->AddNewRow( "questions", $data );


				    }

				}

			}


            if( $id )
            {
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Edited!</div>');
				redirect('backend/exams/index/' . $examtype);
            }
            else
            {
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('backend/exams/index/' . $examtype);
            }
        }
    }

    public function import( $id ) {

        /*phpinfo();exit;*/

        $this->load->library('form_validation');
        // $this->load->library('PHPExcel/PHPExcel_IOFactory');

        // $this->form_validation->set_rules('importfile', 'Import File', 'required');

        $data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));
            $examtype =  @$data['exams'][0]->examtype;

        if ($this->input->post('hdnimport') == 0){
        // if ($this->form_validation->run() == FALSE){


          $data['questions']=$this->GeneralModel->GetSelectedRows($table = 'questions', $limit = '', $start = '', $columns = '', $orderby = 'queid', $key = array( 'fk_examid' => $id ), $search = '');

          $data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = '' );


        /* print "<hr><pre>".$this->db->last_query();exit; */

          $this->load->view('backend/Header');
          $this->load->view('backend/examimport', $data);
          /*$this->load->view('backend/Footer');*/
        }
        else{


            /*echo preg_replace('/<(.*?)>/', '$1', "<title>");exit;*/

            /*
            echo "<hr><pre>post: ";
            var_dump( $_POST );
            exit;
            */

            /*$allwedtypes = array('image/jpeg','image/png','image/jpg');*/


            $now = new DateTime();
            $now->setTimezone(new DateTimezone('Asia/Kolkata'));
            $temp = $now->format('Y-m-d H:i:s');

            // $data = array(
            //     'sid' => $this->input->post('sid'),
            //     'examtitle' => $this->input->post('examtitle'),
            //     'examdesc' => $this->input->post('examdesc'),
            //     'examavailable' => $this->input->post('examavailable'),
            //     'exampassrate' => $this->input->post('exampassrate'),
            //     'examtimelimit' => $this->input->post('examtimelimit'),
            //     'exambacknav' => $this->input->post('exambacknav'),
            //     'examtype' => $this->input->post('examtype'),
            //     'examtotal' => count( @$question ),
            //     'examdatec' => $temp
            // );

            // $this->GeneralModel->UpdateRow( "exams", $data, array( 'examid' => $id ) );

            /* delete only minus clicked queids */

            // $del_queid = $this->input->post('del_queid');
            //
            // for( $i = 0 ; $i < count( $del_queid ) ; $i++ ){
            //     if( !empty( $del_queid[ $i ] ) ){
            //       $this->GeneralModel->DeleteRow($table = 'questions', array( 'queid' => $del_queid[ $i ] ));
            //   }
            // }

            $import_cnt = 0;
            $Error_Message = "";
            $que_import_id = 0;
            $que_imports = [];

            if( $id ){

                $path = $_FILES["importfile"]["tmp_name"];
                $ext = pathinfo($_FILES["importfile"]["name"], PATHINFO_EXTENSION);

                if(isset($path)){
                  if ($ext != 'csv'){
                    $Error_Message = $Error_Message."File type is not supported.";
                  }
                }else{
                  $Error_Message = $Error_Message."Please Select the file";
                }

                if($Error_Message == "")
                {


                  // $object = PHPExcel_IOFactory::load($path);
                  // foreach($object->getWorksheetIterator() as $worksheet)
                  // {
                  //   $highestRow = $worksheet->getHighestRow();
                  //   $highestColumn = $worksheet->getHighestColumn();
                  //   for($row=2; $row<=$highestRow; $row++)
                  //   {
                  //     $correct_cnt = 0;
                  //     $question = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                  //     $qtype = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                  //     $opt1 = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                  //     $opt2 = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                  //     $opt3 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                  //     $opt4 = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                  //     $opt5 = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                  //     $opt1_iscorrect = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                  //     $opt2_iscorrect = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                  //     $opt3_iscorrect = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                  //     $opt4_iscorrect = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                  //     $opt5_iscorrect = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                  //
                  //     if($opt1_iscorrect == 'Y')
                  //       $correct_cnt++;
                  //     if($opt2_iscorrect == 'Y')
                  //         $correct_cnt++;
                  //     if($opt3_iscorrect == 'Y')
                  //         $correct_cnt++;
                  //     if($opt4_iscorrect == 'Y')
                  //         $correct_cnt++;
                  //     if($opt5_iscorrect == 'Y')
                  //         $correct_cnt++;
                  //
                  //     $data = array(
                  //       'fk_examid' => $id,
                  //       'question' => $question,
                  //       'quetype' => $qtype,
                  //       'quemark' => 0,
                  //       'opt1' => $opt1,
                  //       'opt2' => $opt2,
                  //       'opt3' => $opt3,
                  //       'opt4' => $opt4,
                  //       'opt5' => $opt5,
                  //       'opt1_iscorr' => $opt1_iscorrect,
                  //       'opt2_iscorr' => $opt2_iscorrect,
                  //       'opt3_iscorr' => $opt3_iscorrect,
                  //       'opt4_iscorr' => $opt4_iscorrect,
                  //       'opt5_iscorr' => $opt5_iscorrect,
                  //       'correct_cnt' => $correct_cnt,
                  //       'quedatec' => $temp
                  //     );
                  //
                  //     $insert_id = $this->GeneralModel->AddNewRow( "questions", $data );
                  //
                  //     if($insert_id != 0){
                  //         $import_cnt++;
                  //     }
                  //
                  //   }
                  //
                  //   $importdata = array(
                  //     'fk_examid' => $id,
                  //     'import_row_count' => $import_cnt,
                  //     'import_datetime' => $temp,
                  //   );
                  //
                  //   $this->GeneralModel->AddNewRow( "questions_import", $importdata );
                  //
                  // }

                  // Comparing Questions
                  // $quehandle = fopen($path,"r");

                  // $que_array = [];
                  // $que_cnt = 0;
                  // while($quedata = fgetcsv($quehandle)){
                  //   $que_cnt++;
                  //   if($que_cnt != 1)
                  //     $que_array[] = $quedata;
                  // }
                  // fclose($quehandle);

                  $handle = fopen($path,"r");
                  $count = 0;
                  $qtype = "";

                  //var_dump($que_array);exit;

                	while($data = fgetcsv($handle)){

                    $count = $count + 1;
                    	if($count != 1){

                        // Check if Question cell is empty or not
                        if($data[0] == "")
                          break;

                        // $is_duplicate_que = 0;
                        // $questions_count = 1;

                        // // Check for Duplicate questions
                        // foreach ($que_array as $key => $que) {
                        //   $questions_count++;

                        //   if($count == $questions_count)
                        //     continue;
                        //   else{
                        //     if(strcasecmp($data[0], $que[0]) == 0 && $is_duplicate_que != 1){
                        //       $Error_Message = $Error_Message."Please remove Duplicate questions in import file!!!";
                        //       $is_duplicate_que = 1;
                        //     }
                        //   }
                        // }

                        // if($is_duplicate_que == 1)
                        //   break;

                        $correct_cnt = 0;
                        $question = $data[0];
                        // $qtype = $data[1];
                        $opt1 = $data[1];
                        $opt2 = $data[2];
                        $opt3 = $data[3];
                        $opt4 = $data[4];
                        $opt5 = $data[5];
                        $opt1_iscorrect = $data[6];
                        $opt2_iscorrect = $data[7];
                        $opt3_iscorrect = $data[8];
                        $opt4_iscorrect = $data[9];
                        $opt5_iscorrect = $data[10];

                        if($opt1_iscorrect == '')
                          $opt1_iscorrect = 'N';
                        if($opt2_iscorrect == '')
                          $opt2_iscorrect = 'N';
                        if($opt3_iscorrect == '')
                          $opt3_iscorrect = 'N';
                        if($opt4_iscorrect == '')
                          $opt4_iscorrect = 'N';
                        if($opt5_iscorrect == '')
                          $opt5_iscorrect = 'N';

                        $opt1_iscorrect = strtoupper($opt1_iscorrect);
                        $opt2_iscorrect = strtoupper($opt2_iscorrect);
                        $opt3_iscorrect = strtoupper($opt3_iscorrect);
                        $opt4_iscorrect = strtoupper($opt4_iscorrect);
                        $opt5_iscorrect = strtoupper($opt5_iscorrect);


                        if($opt1_iscorrect == 'Y')
                          $correct_cnt++;
                        if($opt2_iscorrect == 'Y')
                            $correct_cnt++;
                        if($opt3_iscorrect == 'Y')
                            $correct_cnt++;
                        if($opt4_iscorrect == 'Y')
                            $correct_cnt++;
                        if($opt5_iscorrect == 'Y')
                            $correct_cnt++;

                        if($correct_cnt == 1)
                            $qtype = 'SANS';
                        else if($correct_cnt > 1)
                            $qtype = 'MANS';

                        $data = array(
                          'fk_examid' => $id,
                          'question' => $question,
                          'quetype' => $qtype,
                          'quemark' => 0,
                          'opt1' => $opt1,
                          'opt2' => $opt2,
                          'opt3' => $opt3,
                          'opt4' => $opt4,
                          'opt5' => $opt5,
                          'opt1_iscorr' => $opt1_iscorrect,
                          'opt2_iscorr' => $opt2_iscorrect,
                          'opt3_iscorr' => $opt3_iscorrect,
                          'opt4_iscorr' => $opt4_iscorrect,
                          'opt5_iscorr' => $opt5_iscorrect,
                          'correct_cnt' => $correct_cnt,
                          'quedatec' => $temp
                        );

                        $insert_id = $this->GeneralModel->AddNewRow( "questions", $data );

                        if($insert_id != 0){
                            $import_cnt++;
                        }

                      }

                  }

                  $importdata = array(
                    'fk_examid' => $id,
                    'import_row_count' => $import_cnt,
                    'import_datetime' => $temp,
                  );

                  $que_import_id = $this->GeneralModel->AddNewRow( "questions_import", $importdata );

                }

              }

              if($Error_Message != ""){

                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">'.$Error_Message.'</div>');
                redirect('backend/exams/import/' . $id);

              }else{

                if($import_cnt != 0){

                  $que_imports = $this->GeneralModel->GetInfoRow( $table = 'questions_import', $key = array( 'id' => $que_import_id ) );

                  $this->session->set_flashdata('que_imports', $que_imports);

                  $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Imported!</div>');
                  redirect('backend/exams/import/' . $id);
                  // $this->load->view('examimport' ,$data);
                }else{

                  $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                  redirect('backend/exams/import/' . $id);

                }

              }

        }
    }


    public function delete($id='') {

       $arr['page'] = 'exams';
	   $this->GeneralModel->DeleteRow($table = 'questions', array( 'fk_examid' => $id ));
	   $this->GeneralModel->DeleteRow($table = 'exams', array( 'examid' => $id ));
       redirect('backend/exams');
   }



   public function displayresult( $stu ) {

    //$data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'fk_stuid' => $stu )); 

    $join_ar = array(
		    
      0 => array(
        "table" => "exams",
        "condition" => "exam_results.fk_examid = exams.examid",
        "type" => "inner"
      )
    );

    $data['exams'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'exam_results.fk_stuid' => $stu ), $search = '', $join_ar, $group_by = '' );


		$this->load->view('backend/Header');
		$this->load->view('backend/examresult', $data);
		$this->load->view('backend/Footer');
    }



    public function displayresultbyid( $eresid ) {
      $join_ar = array(
		    
        0 => array(
          "table" => "exams",
          "condition" => "exam_results.fk_examid = exams.examid",
          "type" => "inner"
        )
      );

      $exam_results = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'exam_results.eresid' => $eresid ), $search = '', $join_ar, $group_by = '' );

      $arr_page['examtitle'] = @$exam_results[0]->examtitle;
      $arr_page['eres_html'] = @$exam_results[0]->eres_html;

      $this->load->view('examres1', $arr_page);
      
    }




    public function displayresultdashboard() {
      $join_ar = array(
		    
        0 => array(
          "table" => "students",
          "condition" => "exam_results.fk_stuid = students.stuid",
          "type" => "inner"
        ),
        1 => array(
          "table" => "exams",
          "condition" => "exam_results.fk_examid = exams.examid",
          "type" => "inner"
        )
      );

      $data1['allexams'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results', $limit = '', $start = '', $columns = '', $orderby ='exam_results.eres_exstarted DESC', $key = array(), $search = '', $join_ar, $group_by = '' );
      // print_r($data1); exit;

      $this->load->view('backend/Header');
		  $this->load->view('backend/allexamresult', $data1);
		  $this->load->view('backend/Footer');

    }


   
    public function  displayresultbyidinfo( $eresid ) {
      $join_ar = array(
		    
        0 => array(
          "table" => "exams",
          "condition" => "exam_results.fk_examid = exams.examid",
          "type" => "inner"
        ),
        1 => array(
          "table" => "students",
          "condition" => "students.stuid = exam_results.fk_stuid",
          "type" => "inner"
        )

      );

      $exam_results = $this->GeneralModel->GetSelectedRowsJoins( $table = 'exam_results', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'exam_results.eresid' => $eresid ), $search = '', $join_ar, $group_by = '' );

      $arr_page['examtitle'] = @$exam_results[0]->examtitle;
      $arr_page['eres_html'] = @$exam_results[0]->eres_html;
      $arr_page['stuname'] = @$exam_results[0]->stuname;
      
      $this->load->view('backend/Header');
      $this->load->view('backend/examresadmin', $arr_page);
      $this->load->view('backend/Footer');
      
    }


}
