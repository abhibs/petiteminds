<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studiscount extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}

    public function adddiscount(){

		$data['msg'] = '';
		
		$data['exams'] = $this->GeneralModel->GetInfoRow( $table = 'exams', $key = array('examtype' => 'PAID') );

		// $idata['stuid']	= $this->input->post('stuid');
		// $idata['cid'] = $this->input->post('sid');
		// $idata['disamount'] = $this->input->post('amount');
		// $idata['status'] = 0;
		// $idata['disc_date'] = date('Y-m-d h:i:s');

		// $result = $this->GeneralModel->AddNewRow( "student_discount", $idata );
			
		$this->load->view('backend/Header');
		$this->load->view('backend/adddiscount',$data);
		$this->load->view('backend/Footer');

	}

	public function discaddsave($add){
		 
		$idata['stuid']	= $this->input->post('stuid');
		$idata['cid'] = $this->input->post('sid');
		$idata['disamount'] = $this->input->post('amount');
		$idata['status'] = 0;
		$idata['disc_date'] = date('Y-m-d h:i:s');

		$result = $this->GeneralModel->AddNewRow( "student_discount", $idata );
		redirect('backend/studiscount/discountmanage?add='.$add);
	}

	public function discountmanage(){

		$msg = "add";
		$join_ar = array(
			0 => array(
				"table" => "students",
				"condition" => "students.stuid = student_discount.stuid",
				"type" => "INNER"
			),
			1 => array(
				"table" => "exams",
				"condition" => "student_discount.cid = exams.sid && exams.examtype='PAID'",
				"type" => "INNER"
			)
		);

		$data['discountinfo']=$this->GeneralModel->GetSelectedRowsJoins( $table = 'student_discount', $limit = '', $start = '', $columns = 'student_discount.*, students.*, exams.examtitle', $orderby ='student_discount.id DESC', $key = '', $search = '', $join_ar, $group_by = '');
		//print_r($data); exit;

		$this->load->view('backend/Header');
		$this->load->view('backend/discountmanage',$data);
		$this->load->view('backend/Footer');
	}

	
	public function discountedit($id){
		// $join_ar = array(
		// 	0 => array(
		// 		"table" => "exams",
		// 		"condition" => "exams.sid = student_discount.cid",
		// 		"type" => "INNER"
		// 	)
		// );

		// $data['discountinfo']=$this->GeneralModel->GetSelectedRowsJoins( $table = 'student_discount', $limit = '', $start = '', $columns = 'student_discount.*, exams.examtitle', $orderby ='', $key = array('student_discount.cid'=>$id), $search = '', $join_ar, $group_by = '');
		$data['discountedit'] = $this->db->query("SELECT * FROM student_discount WHERE id = $id")->result();
		$sid = $data['discountedit'][0]->cid;
		$data['discountcourse'] = $this->db->query("SELECT * FROM exams WHERE sid = $sid")->result();

		$data['exams'] = $this->GeneralModel->GetInfoRow( $table = 'exams', $key = array('examtype' => 'PAID') );

		//print_r($data['discountedit']); exit;

		$this->load->view('backend/Header');
		$this->load->view('backend/discountedit',$data);
		$this->load->view('backend/Footer');
	}

	public function discounteditsave($id, $edit){

		$cid = $this->input->post('sid');
		$amount = $this->input->post('amount');
		$stuid = $this->input->post('stuid');
		//echo $amount; exit;

		$data = array(
            'cid' => $cid,
			'disamount' => $amount,
			'stuid' => $stuid
         );

		 //print_r($data); exit;

    $this->db->where('id', $id);
    $this->db->update('student_discount', $data); 

    redirect('backend/studiscount/discountmanage?edit='.$edit);
	}


	public function discountdelete($id,$data){
		$delete = $data;
		$this->GeneralModel->DeleteRow($table = 'student_discount', $key = array( 'id' => $id ) );
		redirect('backend/studiscount/discountmanage?delete='.$delete);
	}

}
?>