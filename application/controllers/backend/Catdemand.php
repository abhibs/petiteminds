<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catdemand extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{	
		$data['catdemand'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'catdemand', $limit = '', $start = '', $columns = 'catdemand.* ', $orderby = 'catorder', $key = '', $search = '', $join_ar = '', $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/catdemmanage', $data);
		$this->load->view('backend/Footer');
	}

	public function add()
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('catname', 'name', 'trim|required');
		$this->form_validation->set_rules('catorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		$data['msg'] = ''; 
		
		$data['maxorder'] = $this->GeneralModel->GetSelectedRows($table = 'catdemand', $limit = '', $start = '', $columns = 'max(catdemand.catorder) as maxorder', $orderby ='', $key = '', $search = '');
		
		$data['maxorder'] = ( !empty( $data['maxorder'] ) ) ? $data['maxorder'][0]->maxorder + 1 : 0 ;
		
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('backend/Header');
			$this->load->view('backend/catdemadd', $data);
			
		}
		else{
			
			$idata['catname']	= $this->input->post('catname');
			$idata['catorder']	= $this->input->post('catorder');			
			$idata['status']	= $this->input->post('status');			
			$idata['datec']	= date('Y-m-d H:i:s');
			
			$catd_id = $this->GeneralModel->AddNewRow( "catdemand", $idata );
		
			$data['msg'] = 'catdemand Added Successfully'; 
			
			$this->load->view('backend/Header');
			$this->load->view('backend/catdemadd', $data);
			$this->load->view('backend/Header');

		}
	}
	
	public function code_check( $code )
	{
		$id = $this->input->post('id');
		$res = $this->GeneralModel->GetInfoRow( $table = 'subcatdemand', $key = array( 'id !=' => $id, 'code' => $code ) );
		
		if( !empty( $res ) )
		{
			$this->form_validation->set_message('code_check', 'The {field} already exists');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function edit( $catd_id )
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('catname', 'name', 'trim|required');
		$this->form_validation->set_rules('catorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		
		$data['msg'] = ''; 
		
		if ($this->form_validation->run() == FALSE) {
			
			$data['catdemand'] = $this->GeneralModel->GetInfoRow( $table = 'catdemand', $key = array('catd_id'=>$catd_id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/catdemedit', $data);
			/* $this->load->view('backend/Header'); */
			
		}
		else{
			
			$idata['catname']	= $this->input->post('catname');
			$idata['catorder']	= $this->input->post('catorder');
			$idata['status']	= $this->input->post('status');		
			
			$this->GeneralModel->UpdateRow( "catdemand", $idata, array('catd_id'=>$catd_id) );
		
/* 			$data['msg'] = 'Subcatdemand Edited Successfully'; 
			
			$data['subcatdemand'] = $this->GeneralModel->GetInfoRow( $table = 'subcatdemand', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			$this->load->view('backend/Header'); */
			
			redirect( base_url('backend/catdemand?e=s') );

		}
	}

	
	
	public function delete($catd_id)
	{					
		
		$this->GeneralModel->DeleteRow($table = 'catdemand', array('catd_id'=>$catd_id));
		
		redirect( base_url() . 'backend/catdemand?del=s' );
	}
	
}
?>