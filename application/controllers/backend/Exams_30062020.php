<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set("memory_limit", "-1");
set_time_limit(0);
ini_set('max_input_vars', '10000');

class Exams extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper(array('form','url', 'security','date','function'));
        $this->load->library(array('session', 'form_validation', 'email','pagination'));
        $this->load->model('GeneralModel');

         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }
    
    }


    public function index( $type = '' ) {
        
        /*phpinfo();exit;*/

        $join_ar = array(
        					0 => array(
        						"table" => "subcategory",
        						"condition" => "exams.sid = subcategory.id",
        						"type" => "INNER"
        					)
        				);
        			
        $key = array();
        
        if( !empty( $type ) )
        {
            $key['exams.examtype'] = $type;
        }
        
        $arr_page['exams']=$this->GeneralModel->GetSelectedRowsJoins( $table = 'exams', $limit = '', $start = '', $columns = 'exams.*, subcategory.name as subcname', $orderby = '', $key, $search = '', $join_ar, $group_by = '' );

        $arr['page'] = 'exams';

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/exammanage',$arr_page);  
        $this->load->view('backend/Footer');		

    }


    public function add() {

        $arr['page'] = 'exams';
        $this->createexams();
    }


    function createexams()
    {

		$this->load->library('form_validation');

		$this->form_validation->set_rules('examtitle', 'title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('examtype', 'Exam Type', 'trim|required|xss_clean');

		$data['page'] = 'exams'; 

        if ($this->form_validation->run() == FALSE)
        { 	
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = '' );
	
            $this->load->view('backend/Header');
            $this->load->view('backend/examadd', $data);
            /* $this->load->view('backend/Footer'); */
        }
        else
        {
		
/*			
echo "<hr><pre>post: ";
var_dump( $_POST );
echo "<hr><pre>FILES: ";
var_dump( $_FILES );
exit; 
*/

            /*$allwedtypes = array('image/jpeg','image/png','image/jpg');*/

			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');

            $data = array(
                'sid' => $this->input->post('sid'),
                'examtitle' => $this->input->post('examtitle'),
                'examdesc' => $this->input->post('examdesc'),
                'examavailable' => $this->input->post('examavailable'),
                'exampassrate' => $this->input->post('exampassrate'),
                'examtimelimit' => $this->input->post('examtimelimit'),
                'exambacknav' => $this->input->post('exambacknav'),
                'examtype' => $this->input->post('examtype'),
                'examtotal' => count( @$question ),
                'examdatec' => $temp
            );

			$examid = $this->GeneralModel->AddNewRow( "exams", $data );
			
            if( $examid )
            {
				$question = $this->input->post('question');
				/*$queimg = $_FILES['queimg'];*/

				$qtype = $this->input->post('qtype');
				$opt1 = $this->input->post('opt1');
				$opt2 = $this->input->post('opt2');
				$opt3 = $this->input->post('opt3');
				$opt4 = $this->input->post('opt4');
				$opt5 = $this->input->post('opt5');
				
				
				/* insert questions */
				for( $i = 0 ; $i < count( $question ) ; $i++ )
				{
				    if( !empty( $question[ $i ] ) )
					{
					    
					    $data = array(
    						'fk_examid' => $examid,
    						'question' => $question[ $i ],
    						'quetype' => $qtype[ $i ],
    						'opt1' => $opt1[ $i ],
    						'opt2' => $opt2[ $i ],
    						'opt3' => $opt3[ $i ],
    						'opt4' => $opt4[ $i ],
    						'opt5' => $opt5[ $i ],
    						'quedatec' => $temp
    					);
				    
    				    /*
    				    upload question image
    				    */
    				    
    				    /*
    				    if( !empty( $queimg['name'][ $i ] ) && in_array( $queimg['type'][ $i ], $allwedtypes ) )
    					{
        				    $target_dir = "./uploads/";
                			$target_file = $target_dir . 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
                			move_uploaded_file( $queimg['tmp_name'][ $i ], $target_file );
                			$data['queimg']	= 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
    					}
    					*/
    				    
    					$answer = $this->input->post('answer'.$i);
    					
    					if( !empty( $answer ) )
    					{	
    						foreach( $answer as $key => $value )
    						{
    							$data[ $value ] = 'Y';
    						}
    					}
    					
    					$data[ 'correct_cnt' ] = count( $answer );
    
    					$this->GeneralModel->AddNewRow( "questions", $data );
					
				    }
					
				}
				
			}
			
			
            if( $examid )
            {
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Created!</div>');
				redirect('backend/exams');
            }
            else
            {
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('backend/exams');
            }
        }

    }

    public function view( $id ) {   
	
		$data['page'] = 'exams'; 

		$data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));
		
		/* print "<hr><pre>".$this->db->last_query();exit; */

		$this->load->view('backend/Header');
		$this->load->view('backend/examview', $data);
		$this->load->view('backend/Footer');
    }

    public function edit( $id ) {
        
        /*phpinfo();exit;*/

		$this->load->library('form_validation');

		$this->form_validation->set_rules('examtitle', 'title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('examtype', 'Exam Type', 'trim|required|xss_clean');
		
	    $data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));
        $examtype =  @$data['exams'][0]->examtype;   

        if ($this->form_validation->run() == FALSE)
        { 
	
			
			$data['questions']=$this->GeneralModel->GetSelectedRows($table = 'questions', $limit = '', $start = '', $columns = '', $orderby = 'queid', $key = array( 'fk_examid' => $id ), $search = '');
			
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = '' );

			
			/* print "<hr><pre>".$this->db->last_query();exit; */

            $this->load->view('backend/Header');
            $this->load->view('backend/examedit', $data);
            /*$this->load->view('backend/Footer');*/
        }
        else
        {


/*echo preg_replace('/<(.*?)>/', '$1', "<title>");exit;*/

/*
echo "<hr><pre>post: ";
var_dump( $_POST );
exit;
*/

            /*$allwedtypes = array('image/jpeg','image/png','image/jpg');*/


			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');

            $data = array(
				'sid' => $this->input->post('sid'),
                'examtitle' => $this->input->post('examtitle'),
                'examdesc' => $this->input->post('examdesc'),
                'examavailable' => $this->input->post('examavailable'),
                'exampassrate' => $this->input->post('exampassrate'),
                'examtimelimit' => $this->input->post('examtimelimit'),
                'exambacknav' => $this->input->post('exambacknav'),
                'examtype' => $this->input->post('examtype'),
                'examtotal' => count( @$question ),
                'examdatec' => $temp
            );

			$this->GeneralModel->UpdateRow( "exams", $data, array( 'examid' => $id ) );
			
            if( $id )
            {
                $this->GeneralModel->DeleteRow($table = 'questions', array( 'fk_examid' => $id ));
                
				$question = $this->input->post('question');
				/*$queimgold = $this->input->post('queimgold');
				$queimg = $_FILES['queimg'];*/
				$qtype = $this->input->post('qtype');
				$opt1 = $this->input->post('opt1');
				$opt2 = $this->input->post('opt2');
				$opt3 = $this->input->post('opt3');
				$opt4 = $this->input->post('opt4');
				$opt5 = $this->input->post('opt5');
				
				/* insert questions */
				for( $i = 0 ; $i < count( $question ) ; $i++ )
				{
				    if( !empty( $question[ $i ] ) )
					{
				    
						$data = array(
							'fk_examid' => $id,
							'question' => $question[ $i ],
							'quetype' => $qtype[ $i ],
							'opt1' => $opt1[ $i ],
							'opt2' => $opt2[ $i ],
							'opt3' => $opt3[ $i ],
							'opt4' => $opt4[ $i ],
							'opt5' => $opt5[ $i ],
							'quedatec' => $temp
						);
						
						/*
    				    upload question image
    				    */
    				    /*
    				    if( !empty( $queimg['name'][ $i ] ) && in_array( $queimg['type'][ $i ], $allwedtypes ) )
    					{
        				    $target_dir = "./uploads/";
                			$target_file = $target_dir . 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
                			move_uploaded_file( $queimg['tmp_name'][ $i ], $target_file );
                			$data['queimg']	= 'qimg_' . date('YmdHis') . '_' . $queimg['name'][ $i ];
    					}
    					else
    					{
    					    $data['queimg']	= @$queimgold[ $i ];
    					}
						*/
						
						$answer = $this->input->post('answer'.$i);
						
						if( !empty( $answer ) )
						{	
							foreach( $answer as $key => $value )
							{
								$data[ $value ] = 'Y';
							}
						}
						
						$data[ 'correct_cnt' ] = count( $answer );

						$this->GeneralModel->AddNewRow( "questions", $data );
					
				    }
					
				}
				
			}
			
			
            if( $id )
            {
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Edited!</div>');
				redirect('backend/exams/index/' . $examtype);
            }
            else
            {
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('backend/exams/index/' . $examtype);
            }
        } 
    }


    public function delete($id='') { 

       $arr['page'] = 'exams'; 
	   $this->GeneralModel->DeleteRow($table = 'questions', array( 'fk_examid' => $id ));
	   $this->GeneralModel->DeleteRow($table = 'exams', array( 'examid' => $id ));
       redirect('backend/exams');
   }



}