<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{	
		$data['testimonial'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'testimonial', $limit = '', $start = '', $columns = '', $orderby = 'tmid desc', $key = 'tmby != 0', $search = '', $join_ar = '', $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/tmonmanage', $data);
		$this->load->view('backend/Footer');
	}

	
	
	public function changeshow($id, $tm_show)
	{					
		$data['tm_show'] = $tm_show;
		
		$this->GeneralModel->UpdateRow($table = 'testimonial', $data, array('tmid'=>$id));
		
		redirect( base_url() . 'backend/testimonial?show=' . $tm_show );
	}
	
    public function delete($id='') { 

       $arr['page'] = 'exams'; 
	   $this->GeneralModel->DeleteRow($table = 'testimonial', array( 'tmid' => $id ));
	   
       redirect('backend/testimonial');
   }
	
}
?>