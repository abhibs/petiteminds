<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set('max_input_vars', '5000');

class Currency extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper(array('form','url', 'security','date','function'));
        $this->load->library(array('session', 'form_validation', 'email','pagination'));
        $this->load->model('GeneralModel');

         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }
    
    }


    public function index( $type = '' ) {

        $join_ar = array(
        					0 => array(
        						"table" => "subcategory",
        						"condition" => "exams.sid = subcategory.id",
        						"type" => "INNER"
        					)
        				);
        			
        $key = array();
        
        if( !empty( $type ) )
        {
            $key['exams.examtype'] = $type;
        }
        
        $arr_page['exams']=$this->GeneralModel->GetSelectedRowsJoins( $table = 'exams', $limit = '', $start = '', $columns = 'exams.*, subcategory.name as subcname', $orderby = '', $key, $search = '', $join_ar, $group_by = '' );

        $arr['page'] = 'exams';

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/exammanage',$arr_page);  
        $this->load->view('backend/Footer');		

    }


    public function add() {

        $arr['page'] = 'currency';
        $this->createcurrency();
    }


    function createcurrency()
    {
		$this->load->library('form_validation');

		$this->form_validation->set_rules('dollar_price', 'title', 'trim|required|xss_clean');

		$data['page'] = 'exams'; 

        if ($this->form_validation->run() == FALSE)
        { 	
			$data['GetCurrencyData'] = $this->GeneralModel->GetCurrencyData();

            $this->load->view('backend/Header');
            $this->load->view('backend/currency', $data);
            /* $this->load->view('backend/Footer'); */
        }
       
    }

	function  update_completed(){
		$data['price'] =$this->GeneralModel->currency_update($this->input->post('dollar_price'));
		$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Price Updated Successfully!</div>');

		redirect('backend/currency/add/');
	}
}