<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/

use PHPMailer\PHPMailer\PHPMailer;
/* The main PHPMailer class. */
require_once 'PHPMailer/PHPMailer/src/PHPMailer.php';
/* SMTP class, needed if you want to use SMTP. */
require_once 'PHPMailer/PHPMailer/src/SMTP.php';

require_once 'PHPMailer/PHPMailer/src/Exception.php';


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    
    public function __construct() {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->model('dashboard_model');
        $this->load->model('GeneralModel');
    }

    public function index() {
		
        
         if ($this->session->userdata('adminid')) {
            redirect('backend/dashboard');
        } else {
        $this->load->view('backend/Login');
        }
    }


    public function change_password() {

        $this->load->view('backend/Header');
        $this->load->view('backend/Changepswd');
        $this->load->view('backend/Footer');
    }


    public function change_pswd() {

     $this->load->library('form_validation');

     $this->form_validation->set_rules('passphrase', 'Password', 'trim|required');
     $this->form_validation->set_rules('cnfmpassphrase', 'Confirm Password', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
           $arr['page'] = 'changepswd';
            $this->load->view('backend/Header',$arr);
            $this->load->view('backend/Changepswd',$arr);
            $this->load->view('backend/Footer');
        }
        else {

        $passphrase = $_POST['passphrase'];
        $cnfmpassphrase = $_POST['cnfmpassphrase'];
        $id = $this->session->userdata('adminid');

        if($passphrase==$cnfmpassphrase){

         $sql2 = "update admin_login set password = '".md5($passphrase)."' where id = '".$id."' ";
         $val2 =  $this->db->query($sql2);
         
             $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Password Changed Successfully!</div>');
              redirect('backend/home/logout');
        }     
        else {
             $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Password did not matched!</div>');
              redirect('backend/home/change_password');
        }
       
      }
            
        }


    public function forgot_password() {
        
        if ($this->session->unset_userdata('adminid')) {
            redirect('backend/dashboard');
        } else {
        $this->load->view('backend/Forgotpswd');
        }
    }


    public function forgot_pswd()
    {
        $email = $this->input->post('email');   
        
        $sql = "SELECT * FROM admin_login WHERE email = ? "; 
                $val = $this->db->query($sql,array($email));

        
        if($val->num_rows()>0){
            foreach ($val->result_array() as $recs => $res) {
                      $name = $res['username'];
                      $email = $res['email'];
                      $id = $res['id'];
                    }
        
        $sql2 = "update admin_login set password = '".md5($name.'@123')."' where id = '".$id."' "; 
         $val2 =  $this->db->query($sql2);

        /*    
           $this->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => 'mail.sibinfotech.co.uk',
          'smtp_user' => 'support@sibinfotech.co.uk',
          'smtp_pass' => 'Admin@123',
          'smtp_port' => 587,
          'crlf' => "\r\n",
          'newline' => "\r\n"
        ));



        $this->email->from('support@sibinfotech.co.uk', 'Admin');
        $this->email->to($email);
        $this->email->set_mailtype("html");
        $this->email->subject('Forgot Password');
        $this->email->message($body_message);
        $this->email->send();
        */
        
$body_message = ' <!DOCTYPE html>
<html lang="en" style="margin: 0;padding: 0;">

<head style="margin: 0;padding: 0;">
    <meta charset="UTF-8" style="margin: 0;padding: 0;">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" style="margin: 0;padding: 0;">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" style="margin: 0;padding: 0;">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet" style="margin: 0;padding: 0;">
</head>

<body style="margin: 0;padding: 0;font-family: Roboto, sans-serif;font-size: 15px;">

    <div class="welcome-verification-email" style="margin: 0;padding: 0;">
        <table style="margin: 50px auto;padding: 0;border: 1px solid #e5e5e5;width: 600px;">
            <tr class="center-content" style="margin: 0;padding: 0;text-align: center;">
                <td class="common-padding" style="margin: 0;padding: 10px;vertical-align: middle;">
                    <img src="" alt="" style="height: 60px;margin: 0;padding: 0;">
                </td>
            </tr>
            <tr style="margin: 0;padding: 0;">
                <td class="common-padding top-padding" style="margin: 0;padding: 10px;vertical-align: middle;padding-top: 30px;">
                    <h3 class="hello-text" style="margin: 0;padding: 0;font-size: 18px;font-weight: 600;">
                        Hey '.$name.', please login using this password('.$name.'@123).
                    </h3>
                </td>
            </tr>
        </table>
    </div>

</body>

</html> ';

        $mail = new PHPMailer();
		$mail->XMailer = 'Sapprep';
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = $this->config->item('msmtp_host'); // SMTP server
		$mail->SMTPDebug  = 2;
		// enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 
		$mail->Host       = $this->config->item('msmtp_host');      // SMTP server
		$mail->Port       = $this->config->item('msmtp_port');                   // SMTP port
		$mail->Username   = $this->config->item('msmtp_user');  // username
		$mail->Password   = $this->config->item('msmtp_pass');            // password

		$mail->SetFrom($this->config->item('msmtp_user'), 'Sapprep');
		$mail->Subject    = 'Forgot Password';
		$mail->MsgHTML($body_message);		
		/*$mail->AddAddress('sibmum1@sibinfotech.in');*/
		$mail->AddAddress($email);
		
		if(!$mail->Send()) {
		  /*echo "Mailer Error: " . $mail->ErrorInfo;exit;*/
		} else {
		  /*echo "Message sent!"; */
		}
        

        $this->session->set_flashdata('message','<div class="alert alert-success">Email Sent, Please check your email.</div>');

        }
    else {
    $this->session->set_flashdata('message','<div class="alert alert-danger">Email Address is not registered.</div>');
    }
        redirect('backend/home');
}


     
		 
     public function do_login() {

	         
        if ($this->session->userdata('adminid')) {
            redirect('backend/dashboard');
        } else {
						
            $user = $_POST['email'];
            $password = $_POST['password'];
			
			$enc_pass  = md5($password); 

            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('backend/Login');
            } else {
					 
				 $sql = "SELECT * FROM admin_login WHERE email = ? AND password = ?"; 
                  $val = $this->db->query($sql,array($user ,$enc_pass ));
                   
				  if ($val->num_rows()) {
                    foreach ($val->result_array() as $recs => $res) {
                        $this->session->set_userdata(array(
                            'adminid' => $res['id'],
                            'username' => $res['username'],
                            'email' => $res['email'],
                            'role' => $res['role']
                                )
                        );
                    }
					
					
                    redirect('backend/dashboard');
                } 
				else {
                    $err['error'] = '<strong>Access Denied</strong><br> Invalid Username/Password';
                    $this->load->view('backend/Login', $err);
                }
                
                }
            }
        }
           

        
    public function logout() {
        $this->session->unset_userdata('adminid');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role');
          
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('backend/home');
    }

    

}

