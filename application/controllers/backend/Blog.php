<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('date');
        $this->load->helper('function');
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->library('pagination');
        $this->load->model('blog_model');

        $config = array(
        'field' => 'slug',
        'title' => 'title',
        'table' => 'blog',
        'id' => 'id',
        );
        $this->load->library('slug', $config);

    }


    public function index() {
if($this->session->userdata('adminid') == ''){
            redirect('backend/home');
        }

        $arr_page["blogs"] = $this->blog_model->getblogs();

        $arr['page'] = 'blog';

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/blog',$arr_page);  
        $this->load->view('backend/Footer');		

    }


    public function add() {
if($this->session->userdata('adminid') == ''){
            redirect('backend/home');
        }

        $arr['page'] = 'blog';
        $this->createblog();
    }


    function createblog()
    {

     $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('img_txt', 'Image Text', 'trim|required');
        $this->form_validation->set_rules('orderno', 'Order No', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        { 
            $arr['page'] = 'blog';

            $this->load->view('backend/Header',$arr);
            $this->load->view('backend/blogadd');
            $this->load->view('backend/Footer');
        }
        else
        {
            if (!empty($_FILES["file"]["name"])) {
         $target_path = "uploads/blogs"; //Declaring Path for uploaded images

         $validextensions = array("jpeg", "jpg", "png");  //Extensions which are allowed

                $ext = explode('.', basename($_FILES['file']['name'])); //explode file name from dot(.) 

                $file_extension = end($ext); //store extensions in the variable

                  if (($_FILES["file"]["size"] < 10000000) //Approx. 100kb files can be uploaded.

                        && in_array($file_extension, $validextensions)) {
                        $file = pathinfo($_FILES['file']['tmp_name']);
                        $fileNameNew = $_FILES['file']['name'];
                        $ext = pathinfo($fileNameNew, PATHINFO_EXTENSION); // get ext
                        $file = basename($fileNameNew, ".".$ext); // get filename without ext
                        $fileNameNew1 = $file.".".$ext;
                        $path[3] = $target_path. "/" .$fileNameNew1;

                    move_uploaded_file($_FILES['file']['tmp_name'] , $path[3]); 
                }
            }
            else
            {
               $fileNameNew1 = ''; 
            }

        $now = new DateTime();
        $now->setTimezone(new DateTimezone('Asia/Kolkata'));
        $temp = $now->format('Y-m-d H:i:s');

             $data1 = array(
               'title' => $this->input->post('title'),
               );

               $slug = $this->slug->create_uri($data1);

            $data = array(
                'title' => $this->input->post('title'),
                'slug' => $slug,
                'img_txt' => $this->input->post('img_txt'),
                'img_file' => $fileNameNew1,
                'small_edesc' => $this->input->post('small_edesc'),
                'edesc' => $this->input->post('edesc'),
                'orderno' => $this->input->post('orderno'),
                'status' => $this->input->post('status'),
                'created_at' => $temp

            );

            if ($this->blog_model->insertblog($data))
            {
            $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Created!</div>');
            redirect('backend/blog/add');
            }
            else
            {
            $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            redirect('backend/blog/add');
            }
        }

    }


     public function edit($id='') {
if($this->session->userdata('adminid') == ''){
            redirect('backend/home');
        }

        $arr['page'] = 'blog';

        if($id!=''){
            $arr['blog'] = $this->blog_model->editblogs($id);

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/blogedit',$arr);
		$this->load->view('backend/Footer');

        }else{
            redirect('backend/blog');

        }
    }


    public function update_blog() {   
if($this->session->userdata('adminid') == ''){
            redirect('backend/home');
        }

        //set validation rules
		$id = $this->input->post('id');

     $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('img_txt', 'Image Text', 'trim|required');

        $this->form_validation->set_rules('orderno', 'Order No', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        { 
            $arr['blog'] = $this->blog_model->editblogs($id);

        $arr['page'] = 'blog';
        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/blogedit',$arr);
        $this->load->view('backend/Footer');
        }
        else
        {

             $fileNameNew1 = $this->blog_model->getfeaturefile($id);
 
           if($fileNameNew1==''){
         $target_path = "uploads/blogs"; //Declaring Path for uploaded images

         $validextensions = array("jpeg", "jpg", "png");  //Extensions which are allowed

                $ext = explode('.', basename($_FILES['file']['name'])); //explode file name from dot(.) 

                $file_extension = end($ext); //store extensions in the variable

                  if (($_FILES["file"]["size"] < 10000000) //Approx. 100kb files can be uploaded.

                        && in_array($file_extension, $validextensions)) {
                        $file = pathinfo($_FILES['file']['tmp_name']);
                        $fileNameNew = $_FILES['file']['name'];
                        $ext = pathinfo($fileNameNew, PATHINFO_EXTENSION); // get ext
                        $file = basename($fileNameNew, ".".$ext); // get filename without ext
                        $fileNameNew1 = $file.".".$ext;
                        $path[3] = $target_path. "/" .$fileNameNew1;

                    move_uploaded_file($_FILES['file']['tmp_name'] , $path[3]); 
                }
            }


            $data = array(

                'title' => $this->input->post('title'),
                'img_file' => $fileNameNew1,
                'img_txt' => $this->input->post('img_txt'),
                'small_edesc' => $this->input->post('small_edesc'),
                'edesc' => $this->input->post('edesc'),
                'orderno' => $this->input->post('orderno'),
                'status' => $this->input->post('status')

            );
    
            if ($this->blog_model->updateblog($data,$this->input->post('id')))
            {
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Updated!</div>');
                redirect('backend/blog/edit/'.$this->input->post('id'));
            }
            else
            {
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('backend/blog/edit/'.$this->input->post('id'));
            }
        }  
    }


    public function delete_blog($id='') { 
if($this->session->userdata('adminid') == ''){
            redirect('backend/home');
        }

       $arr['page'] = 'blog'; 
       $this->blog_model->deleteblog($id);
       redirect('backend/blog');
   }

     public function delete_image($id='') {
  if($this->session->userdata('adminid') == ''){
            redirect('backend/home');
        }

        $get_liked = '';
        $id=$_POST['id'];
        $get_liked = $this->blog_model->deleteimage($id);
        if ($get_liked) {
            echo '';
        } else {
            echo '';
        }
    }




}







