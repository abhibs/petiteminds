<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintestimonials extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{	
		$data['testimonial'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'testimonial', $limit = '', $start = '', $columns = '', $orderby = 'tmid desc', $key = 'tmby = 0', $search = '', $join_ar = '', $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/admintmonmanage', $data);
		$this->load->view('backend/Footer');
	}

	
	
	public function changeshow($id, $tm_show)
	{					
		$data['tm_show'] = $tm_show;
		
		$this->GeneralModel->UpdateRow($table = 'testimonial', $data, array('tmid'=>$id));
		
		redirect( base_url() . 'backend/admintestimonials?show=' . $tm_show );
	}
	

    public function delete($id='',$data) { 
		//echo $data; exit;
		$delete = $data;
       $arr['page'] = 'exams'; 
	   $this->GeneralModel->DeleteRow($table = 'testimonial', array( 'tmid' => $id ));
	   
       redirect('backend/admintestimonials?delete='.$delete);
   }


   public function add()
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('livetmonurl', 'Url', 'trim|required');
		$this->form_validation->set_rules('livetmonord', 'Order', 'trim|required');
		$this->form_validation->set_rules('livetmonshow', 'Show', 'trim|required');

		$data['category'] = $this->GeneralModel->GetInfoRow( $table = 'category', $key = '' );

		//$data['msg'] = ''; 
		
		//$data['maxorder'] = $this->GeneralModel->GetSelectedRows($table = 'livetmon', $limit = '', $start = '', $columns = 'max(livetmon.livetmonord) as maxorder', $orderby ='', $key = '', $search = '');
		
		//$data['maxorder'] = ( !empty( $data['maxorder'] ) ) ? $data['maxorder'][0]->maxorder + 1 : 0 ;
		/* print "<hr><pre>".$this->db->last_query();exit; */
		
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('backend/Header');
			$this->load->view('backend/admintmonadd', $data);
			
		}
	}

    public function addsave($add){
			 
			//echo $add; exit;
            $idata['name']	= $this->input->post('nameadtmon');
			$idata['tmbody']	= $this->input->post('bodyadtmon');			
			$idata['tm_show']	= $this->input->post('livetmonshow');			
			$idata['tmmodule']	= $this->input->post('tmmodule');			
			$idata['tmtitle']	= $this->input->post('tmontitle');			
			$idata['tmdatec']	= date('Y-m-d H:i:s');
			
			$cid = $this->GeneralModel->AddNewRow( "testimonial", $idata );

			$data['msg'] = 'Admin Testimonial Added Successfully'; 
			
			$this->load->view('backend/Header');
			$this->load->view('backend/admintmonadd', $data);
			$this->load->view('backend/Header');

            redirect('backend/admintestimonials?added='.$add);
        
    }

    public function edit($adtmid=''){
        $data['adtmon'] = $this->db->query("SELECT * FROM testimonial WHERE tmid = $adtmid")->result();
        //print_r($data); exit;

		$data['category'] = $this->GeneralModel->GetInfoRow( $table = 'category', $key = '' );

        $this->load->view('backend/Header');
		$this->load->view('backend/adtmonedit', $data);
		$this->load->view('backend/Header');

    }

    public function editsave($adtmid='',$edit){
		//echo $edit; exit;

        $name = $this->input->post('nameadtmon');
        $body = $this->input->post('bodyadtmon');
        $show = $this->input->post('livetmonshow');
        $category = $this->input->post('tmmodule');
        $title = $this->input->post('tmontitle');
		
        //echo $show; exit;

        $data = array(
            'name' => $name,
            'tmbody' => $body,
            'tm_show' => $show,
			'tmmodule' => $category,
			'tmtitle' => $title
         );

    $this->db->where('tmid', $adtmid);
    $this->db->update('testimonial', $data); 

    redirect('backend/admintestimonials?edit='.$edit);
    }
	
}
?>