<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordersdem extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');		
		
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	

	}
	
	public function index()
	{		
		// $data['RowsSelected'] = $this->GeneralModel->GetSelectedRows( $table = 'users', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'type' => 'N' ), $search = '' );
				
		$data['orders_demand'] = $this->db->query( "SELECT `orders_demand`.* FROM `orders_demand` WHERE orders_demand.`ordstatus` != '' and orders_demand.`admin_deleted` = 'N' ORDER BY `orders_demand`.`ordd_id` desc" )->result();
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
	
		$this->load->view('backend/Header');
		$this->load->view('backend/orddemmanage', $data);
		$this->load->view('backend/Footer');
	}
	
	public function details( $ordd_id = '' )
	{		
	
         if ( empty( $ordd_id ) ) { 
            redirect('backend');
        }	
	

		
		$join_ar = array(
							0 => array(
								"table" => "orders_demand",
								"condition" => "order_demand_detail.fk_ordd_id = orders_demand.ordd_id",
								"type" => "INNER"
							),
							1 => array(
								"table" => "subcatdemand",
								"condition" => "order_demand_detail.odet_scatd_id = subcatdemand.scatd_id",
								"type" => "INNER"
							)
						);

		$data['order_demand_detail'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_demand_detail', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'orders_demand.ordd_id' => $ordd_id ), $search = '', $join_ar, $group_by = '' );
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
	
		$this->load->view('backend/Header');
		$this->load->view('backend/orddemdetails', $data);
		$this->load->view('backend/Footer');
	}
	
    public function delete( $ordd_id = '' )
	{
         if ( empty( $ordd_id ) ) { 
            redirect('backend/ordersdem');
        }
        
        $this->GeneralModel->UpdateRow( "orders_demand", array( 'admin_deleted' => 'Y' ), array( 'ordd_id' => $ordd_id ) );
        
        redirect('backend/ordersdem?del=s');    

	}
	
}
?>