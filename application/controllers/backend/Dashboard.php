<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
		
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('dashboard_model');
        $this->load->model('GeneralModel');
         $this->load->library('email');
          $this->load->helper('url');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }
		
    }

    public function index() {
		
        $arr['page']='dashboard';
		
		$adminid = $this->session->userdata('adminid');
		$role = $this->session->userdata('role');

		/*$key = array( 'company.compleadsts' => 'COLD' );*/

		$arr['category_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'category', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');

		$arr['subcategory_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'subcategory', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');
		
		/*print "<hr><pre>".$this->db->last_query();exit;*/

		
		$arr['paidexams_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'exams', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = array('exams.examtype' => 'PAID'), $search = '');
		
		$arr['freeexams_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'exams', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = array('exams.examtype' => 'FREE'), $search = '');
		
		$arr['blog_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'blog', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');
		
		$arr['testimonial_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'testimonial', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');
		
		$arr['livetmon_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'livetmon', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = '', $search = '');

        $arr['admintmon_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'testimonial', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby ='', $key = array( 'tmby' => 0 ), $search = '');
		
		$arr['demexams_cnt'] = $this->GeneralModel->GetSelectedRows($table = 'subcatdemand', $limit = '', $start = '', $columns = 'count(*) as numrows', $orderby = '', $key = '', $search = '');

		

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/index',$arr);
		$this->load->view('backend/Footer');
    }


}