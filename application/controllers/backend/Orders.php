<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');		
		
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	

	}
	
	public function index()
	{		
	    
        $stuid = $this->input->get('stuid');
	    
		// $data['RowsSelected'] = $this->GeneralModel->GetSelectedRows( $table = 'users', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'type' => 'N' ), $search = '' );
		
		$stuqry = '';
		
        if( !empty( $stuid ) )
        {
			// $stuqry = " AND `orders`.`fk_stuid` = '$stuid'";
			$stuqry = " AND `orders`.`fk_stuid` = '$stuid' OR `orders`.`payment_gateway` LIKE %'$stuid'%";
        }
				
		$data['orders'] = $this->db->query( "SELECT `orders`.*, `students`.`stuname` FROM `orders` INNER JOIN `students` ON `orders`.`fk_stuid` = `students`.`stuid` WHERE orders.`ordstatus` != '' and orders.`admin_deleted` = 'N' and admin_added = 'N' $stuqry ORDER BY `orders`.`ordid` desc" )->result();
		
/*		print "<hr><pre>".$this->db->last_query();exit;*/
	
		$this->load->view('backend/Header');
		$this->load->view('backend/ordmanage', $data);
		$this->load->view('backend/Footer');
	}
	
	public function details( $ordid = '' )
	{		
	
         if ( empty( $ordid ) ) { 
            redirect('backend/orders');
        }	
	
		// $data['RowsSelected'] = $this->GeneralModel->GetSelectedRows( $table = 'users', $limit = '', $start = '', $columns = '', $orderby ='', $key = array( 'type' => 'N' ), $search = '' );
				
		$data['orders'] = $this->db->query( "SELECT `orders`.*, `students`.* FROM `orders` INNER JOIN `students` ON `orders`.`fk_stuid` = `students`.`stuid` WHERE orders.`ordstatus` != '' and orders.ordid = '". $ordid ."' ORDER BY `orders`.`ordid` desc" )->result();
		
		$data['order_detail'] = $this->GeneralModel->GetInfoRow($table = 'order_detail', $key = array( 'fk_ordid' => $ordid ));
		
		$join_ar = array(
							0 => array(
								"table" => "orders",
								"condition" => "order_detail.fk_ordid = orders.ordid",
								"type" => "INNER"
							),
							1 => array(
								"table" => "subcategory",
								"condition" => "order_detail.odetsid = subcategory.id",
								"type" => "INNER"
							)
						);

		$data['order_detail'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'order_detail', $limit = '', $start = '', $columns = 'order_detail.*, subcategory.name', $orderby ='', $key = array( 'fk_ordid' => $ordid ), $search = '', $join_ar, $group_by = '' );
		
		/* print "<hr><pre>".$this->db->last_query();exit; */
	
		$this->load->view('backend/Header');
		$this->load->view('backend/orddetails', $data);
		$this->load->view('backend/Footer');
	}
	
    public function delete( $ordid = '' )
	{
         if ( empty( $ordid ) ) { 
            redirect('backend/orders');
        }
        
        $this->GeneralModel->UpdateRow( "orders", array( 'admin_deleted' => 'Y' ), array( 'ordid' => $ordid ) );
        
        redirect('backend/orders?del=s');    

	}
	
}
?>