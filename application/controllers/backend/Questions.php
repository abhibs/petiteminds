<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper(array('form','url', 'security','date','function'));
        $this->load->library(array('session', 'form_validation', 'email','pagination'));
        $this->load->model('GeneralModel');

         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }
    
    }


    public function index() {

		$arr_page['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', '');

        $arr['page'] = 'exams';

        $this->load->view('backend/Header',$arr);
        $this->load->view('backend/exammanage',$arr_page);  
        $this->load->view('backend/Footer');		

    }


    public function add() {

        $arr['page'] = 'exams';
        $this->createquestion();
    }


    function createquestion()
    {

		$this->load->library('form_validation');

		$this->form_validation->set_rules('examtitle', 'title', 'trim|required|xss_clean');

		$data['page'] = 'exams'; 

        if ($this->form_validation->run() == FALSE)
        { 

            $this->load->view('backend/Header');
            $this->load->view('backend/examadd', $data);
            $this->load->view('backend/Footer');
        }
        else
        {

			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');

            $data = array(
                'examtitle' => $this->input->post('examtitle'),
                'examdesc' => $this->input->post('examdesc'),
                'examavailable' => $this->input->post('examavailable'),
                'exampassrate' => $this->input->post('exampassrate'),
                'examtimelimit' => $this->input->post('examtimelimit'),
                'exambacknav' => $this->input->post('exambacknav'),
                'examtotal' => $this->input->post('examtotal'),
                'examdatec' => $temp
            );

			$examid = $this->GeneralModel->AddNewRow( "exams", $data );
			
            if( $examid )
            {
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Successfully Created!</div>');
				redirect('backend/exams');
            }
            else
            {
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('backend/exams');
            }
        }

    }

    public function view( $id ) {   
	
		$data['page'] = 'exams'; 

		$data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));
		
		/* print "<hr><pre>".$this->db->last_query();exit; */

		$this->load->view('backend/Header');
		$this->load->view('backend/examview', $data);
		$this->load->view('backend/Footer');
    }

    public function edit( $id ) {   

		$this->load->library('form_validation');

		$this->form_validation->set_rules('examtitle', 'title', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        { 
	
			$data['exams'] = $this->GeneralModel->GetInfoRow($table = 'exams', $key = array( 'examid' => $id ));
			
			/* print "<hr><pre>".$this->db->last_query();exit; */

            $this->load->view('backend/Header');
            $this->load->view('backend/examedit', $data);
            $this->load->view('backend/Footer');
        }
        else
        {

			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Kolkata'));
			$temp = $now->format('Y-m-d H:i:s');
			
            $data = array(
                'examtitle' => $this->input->post('examtitle'),
                'examdesc' => $this->input->post('examdesc'),
                'examavailable' => $this->input->post('examavailable'),
                'exampassrate' => $this->input->post('exampassrate'),
                'examtimelimit' => $this->input->post('examtimelimit'),
                'exambacknav' => $this->input->post('exambacknav'),
                'examtotal' => $this->input->post('examtotal'),
                'examdatec' => $temp
            );

			$this->GeneralModel->UpdateRow( "exams", $data, array( 'examid' => $id ) );
			
            if ($id)
            {
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Edited Successfully!</div>');
				redirect('backend/exams');
            }
            else
            {
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('backend/exams');
            }
        } 
    }


    public function delete($id='') { 

       $arr['page'] = 'exams'; 
	   $this->GeneralModel->DeleteRow($table = 'exams', array( 'examid' => $id ));
       redirect('backend/exams');
   }



}