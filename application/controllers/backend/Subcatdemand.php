<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcatdemand extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{	
		$data['subcatdemand'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'subcatdemand', $limit = '', $start = '', $columns = 'subcatdemand.* ', $orderby = 'scatdatorder', $key = '', $search = '', $join_ar = '', $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/subcatdemmanage', $data);
		$this->load->view('backend/Footer');
	}

	public function add()
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		
		/*
		$this->form_validation->set_rules('catd_id', 'Category', 'trim|required');
		*/
		
		$this->form_validation->set_rules('scatdcode', 'Category', 'trim|required');

		$this->form_validation->set_rules('scatdname', 'name', 'trim|required');
		$this->form_validation->set_rules('scatdatorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('scatdprice', 'Price', 'trim|required');

		$this->form_validation->set_rules('scatdstatus', 'Status', 'trim|required');

		$data['msg'] = ''; 
		
		$data['maxorder'] = $this->GeneralModel->GetSelectedRows($table = 'subcatdemand', $limit = '', $start = '', $columns = 'max(subcatdemand.scatdatorder) as maxorder', $orderby ='', $key = '', $search = '');
		
		$data['maxorder'] = ( !empty( $data['maxorder'] ) ) ? $data['maxorder'][0]->maxorder + 1 : 0 ;
		
		if ($this->form_validation->run() == FALSE) {
		    
            $data['catdemand'] = $this->GeneralModel->GetSelectedRows($table = 'catdemand', $limit = '', $start = '', $columns = '', $orderby ='catdemand.catorder', $key = array( 'catdemand.status' => 'A' ), $search = '');

			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatdemadd', $data);
			
		}
		else{
			/*$idata['catd_id']	= $this->input->post('catd_id');*/
			
			$idata['scatdname']	= $this->input->post('scatdname');
			$idata['scatdcode']	= $this->input->post('scatdcode');
			$idata['scatdatorder']	= $this->input->post('scatdatorder');
			$idata['scatdprice']	= $this->input->post('scatdprice');
			$idata['scatdstatus']	= $this->input->post('scatdstatus');			
			$idata['scatddatec']	= date('Y-m-d H:i:s');
			
			$scatd_id = $this->GeneralModel->AddNewRow( "subcatdemand", $idata );
		
			$data['msg'] = 'On Demand Exam Added Successfully'; 
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatdemadd', $data);
			$this->load->view('backend/Footer');

		}
	}
	
	public function code_check( $code )
	{
		$id = $this->input->post('id');
		$res = $this->GeneralModel->GetInfoRow( $table = 'subsubcatdemand', $key = array( 'id !=' => $id, 'code' => $code ) );
		
		if( !empty( $res ) )
		{
			$this->form_validation->set_message('code_check', 'The {field} already exists');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function edit( $scatd_id )
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		
		
		$this->form_validation->set_rules('scatdcode', 'Code', 'trim|required');
		$this->form_validation->set_rules('scatdname', 'name', 'trim|required');
		$this->form_validation->set_rules('scatdatorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('scatdprice', 'Price', 'trim|required');

		$this->form_validation->set_rules('scatdstatus', 'Status', 'trim|required');
		
		$data['msg'] = ''; 
		
		if ($this->form_validation->run() == FALSE) {
		    

            $data['catdemand'] = $this->GeneralModel->GetSelectedRows($table = 'catdemand', $limit = '', $start = '', $columns = '', $orderby ='catdemand.catorder', $key = array( 'catdemand.status' => 'A' ), $search = '');

			
			$data['subcatdemand'] = $this->GeneralModel->GetInfoRow( $table = 'subcatdemand', $key = array('scatd_id'=>$scatd_id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatdemedit', $data);
			/* $this->load->view('backend/Header'); */
			
		}
		else{
		    
		    
			$idata['scatdcode']	= $this->input->post('scatdcode');
			$idata['scatdname']	= $this->input->post('scatdname');
			$idata['scatdatorder']	= $this->input->post('scatdatorder');
			$idata['scatdprice']	= $this->input->post('scatdprice');
			$idata['scatdstatus']	= $this->input->post('scatdstatus');		
			
			$this->GeneralModel->UpdateRow( "subcatdemand", $idata, array('scatd_id'=>$scatd_id) );
		
/* 			$data['msg'] = 'Subsubcatdemand Edited Successfully'; 
			
			$data['subsubcatdemand'] = $this->GeneralModel->GetInfoRow( $table = 'subsubcatdemand', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			$this->load->view('backend/Header'); */
			
			redirect( base_url('backend/subcatdemand?e=s') );

		}
	}

	
	
	public function delete($scatd_id)
	{					
		
		$this->GeneralModel->DeleteRow($table = 'subcatdemand', array('scatd_id'=>$scatd_id));
		
		redirect( base_url() . 'backend/subcatdemand?del=s' );
	}
	
}
?>