<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{			
		$join_ar = array(
							0 => array(
								"table" => "category",
								"condition" => "subcategory.cid = category.id",
								"type" => "INNER"
							)
						);

		$data['subs'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'subcategory', $limit = '', $start = '', $columns = 'category.catname, subcategory.* ', $orderby = 'id desc', $key = '', $search = '', $join_ar, $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/subcatmanage', $data);
		$this->load->view('backend/Footer');
	}

	public function mfeatures()
	{
		$data['features']=$this->GeneralModel->GetSelectedRows($table = 'features', $limit = '', $start = '', $columns = '', $orderby ='featord', $key = '', $search = '');

		$this->load->view('backend/Header');
		$this->load->view('backend/mfeatadd', $data);
		$this->load->view('backend/Header');
	}
	
	public function featsave()
	{
		/*
		$post = $this->input->post();

		echo "<hr><pre>post: ";
		var_dump( $post );
		exit;
		*/
		
		/* $this->db->query( 'delete from features where featid > 1;' ); */
		
		$featname = $this->input->post('featname');
		$featstatus = $this->input->post('featstatus');
		$featid = $this->input->post('featid');
		$featord = $this->input->post('featord');
		$featdel = $this->input->post('featdel');
		
		for( $i = 0; $i < count( $featname ); $i++ )
		{
			if( !empty( $featdel[$i] ) && !empty( $featid[$i] ) )
			{
				$this->db->query( 'delete from subcatfeat where featid = '. $featid[$i] . ';' );
				$this->db->query( 'delete from features where featid = '. $featid[$i] . ';' );
			}
			
			if( !empty( $featid[$i] ) )
			{
				$idata = array();
				$idata['featname']	= $featname[$i];
				$idata['featstatus']	= $featstatus[$i];
				$idata['featord']	= $featord[$i];
				
				$this->GeneralModel->UpdateRow( "features", $idata, array( 'featid' => $featid[$i] ) );	
			}
			else if( empty( $featdel[$i] ) )
			{			
				$idata = array();
				$idata['featname']	= $featname[$i];
				$idata['featstatus']	= $featstatus[$i];
				$idata['featord']	= $featord[$i];
				$idata['datec']	= date('Y-m-d H:i:s');
				
				$this->GeneralModel->AddNewRow( "features", $idata );			
			}
		}
		
		redirect('_ishubabu_/subcategory/mfeatures');
		
	}
	
	public function add()
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('scat_sku', 'SKU', 'trim|required|is_unique[subcategory.scat_sku]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
        $this->form_validation->set_rules('scatorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|max_length[250]');
		$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|max_length[250]');
		$this->form_validation->set_rules('metakeywords', 'Meta Keywords', 'trim|max_length[250]');
		
		$data['msg'] = ''; 
		$data['category'] = $this->GeneralModel->GetInfoRow( $table = 'category', $key = '' );
		/* $data['features']=$this->GeneralModel->GetSelectedRows($table = 'features', $limit = '', $start = '', $columns = '', $orderby ='featord', $key = '', $search = ''); */
		
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			/* $this->load->view('backend/Header'); */
			
		}
		else{
			
			$idata['cid']	= $this->input->post('cid');
			/* $idata['code']	= $this->input->post('code'); */
			$idata['name']	= $this->input->post('name');
			$idata['scatdesc']	= $this->input->post('scatdesc');
			$idata['scat_sku']	= $this->input->post('scat_sku');
/* 			$idata['type']	= $this->input->post('type');
			$idata['psd_vurl']	= $this->input->post('psd_vurl'); */
/* 			$idata['gst']	= $this->input->post('gst');
			$idata['credit']	= $this->input->post('credit'); */
			$idata['status']	= $this->input->post('status');
			$idata['yearsubsc']	= $this->input->post('yearsubsc');
			$idata['qcnt_display']	= $this->input->post('qcnt_display');
			$idata['scatorder']	= $this->input->post('scatorder');

			$idata['meta_title']	= $this->input->post('metatitle');
			$idata['meta_description']	= $this->input->post('metadescription');
			$idata['meta_keywords']	= $this->input->post('metakeywords');

			$idata['datec']	= date('Y-m-d H:i:s');
			
			/*
			$target_dir = "./uploads/";
			$target_file = $target_dir . basename($_FILES["icon"]["name"]);
			move_uploaded_file($_FILES["icon"]["tmp_name"], $target_file);
			$idata['icon']	= basename($_FILES["icon"]["name"]); 
			*/
			
			/* $idata['twoyearsubsc']	= $this->input->post('twoyearsubsc'); */
			
/* 			if( $idata['type'] == 'F' )
			{
				$idata['yearsubsc']	= $idata['twoyearsubsc'] = 0;
			} */
			
			$scid = $this->GeneralModel->AddNewRow( "subcategory", $idata );
		
			$data['msg'] = 'Subcategory Added Successfully'; 
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			$this->load->view('backend/Header');

		}
	}
	
	public function code_check( $code )
	{
		$id = $this->input->post('id');
		$res = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = array( 'id !=' => $id, 'code' => $code ) );
		
		if( !empty( $res ) )
		{
			$this->form_validation->set_message('code_check', 'The {field} already exists');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function edit( $id )
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('scat_sku', 'SKU', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('scatorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|max_length[250]');
		$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|max_length[250]');
		$this->form_validation->set_rules('metakeywords', 'Meta Keywords', 'trim|max_length[250]');
		
		$data['msg'] = ''; 
		$data['category'] = $this->GeneralModel->GetInfoRow( $table = 'category', $key = '' );

		
		/* $data['features']=$this->GeneralModel->GetSelectedRows($table = 'features', $limit = '', $start = '', $columns = '', $orderby ='featord', $key = '', $search = ''); */
		
		if ($this->form_validation->run() == FALSE) {
			
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatedit', $data);
			/* $this->load->view('backend/Header'); */
			
		}
		else{
			
			$idata['cid']	= $this->input->post('cid');
			/* $idata['code']	= $this->input->post('code'); */
			$idata['name']	= $this->input->post('name');
			$idata['scatdesc']	= $this->input->post('scatdesc');
			$idata['scat_sku']	= $this->input->post('scat_sku');
/* 			$idata['type']	= $this->input->post('type');
			$idata['psd_vurl']	= $this->input->post('psd_vurl'); */
/* 			$idata['gst']	= $this->input->post('gst');
			$idata['credit']	= $this->input->post('credit'); */
			$idata['status']	= $this->input->post('status');
			$idata['yearsubsc']	= $this->input->post('yearsubsc');
			$idata['qcnt_display']	= $this->input->post('qcnt_display');
			$idata['scatorder']	= $this->input->post('scatorder');
			$idata['datec']	= date('Y-m-d H:i:s');

			$idata['meta_title']	= $this->input->post('metatitle');
			$idata['meta_description']	= $this->input->post('metadescription');
			$idata['meta_keywords']	= $this->input->post('metakeywords');
			
			/*
			if( !empty( $_FILES["icon"]["name"] ) )
			{
				$target_dir = "./uploads/";
				$target_file = $target_dir . basename($_FILES["icon"]["name"]);
				move_uploaded_file($_FILES["icon"]["tmp_name"], $target_file);
				$idata['icon']	= basename($_FILES["icon"]["name"]);
			}
			*/
			
			/* $idata['twoyearsubsc']	= $this->input->post('twoyearsubsc'); */
			
/* 			if( $idata['type'] == 'F' )
			{
				$idata['yearsubsc']	= $idata['twoyearsubsc'] = 0;
			} */
			
			$scid = $this->GeneralModel->UpdateRow( "subcategory", $idata, array('id'=>$id) );
		
/* 			$data['msg'] = 'Subcategory Edited Successfully'; 
			
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			$this->load->view('backend/Header'); */
			
			redirect( base_url('backend/subcategory?e=s') );

		}
	}

	
	
	public function delete($id)
	{					
		
		$this->GeneralModel->DeleteRow($table = 'subcategory', array('id'=>$id));
		
		redirect( base_url() . 'backend/subcategory?del=s' );
	}
	
}
?>