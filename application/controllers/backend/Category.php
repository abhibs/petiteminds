<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->helper('download');
		$this->load->helper('url');
		$this->load->library(array('session','form_validation'));
		$this->load->model('GeneralModel');
         if (!$this->session->userdata('adminid')) { 
            redirect('home');
        }	
		
	}
	
	public function index()
	{	
		$data['category'] = $this->GeneralModel->GetSelectedRowsJoins( $table = 'category', $limit = '', $start = '', $columns = 'category.* ', $orderby = 'catorder', $key = '', $search = '', $join_ar = '', $group_by = '' );
	
		$this->load->view('backend/Header');
		$this->load->view('backend/catmanage', $data);
		$this->load->view('backend/Footer');
	}

	public function add()
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('catname', 'name', 'trim|required');
		$this->form_validation->set_rules('catorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		$data['msg'] = ''; 
		
		$data['maxorder'] = $this->GeneralModel->GetSelectedRows($table = 'category', $limit = '', $start = '', $columns = 'max(category.catorder) as maxorder', $orderby ='', $key = '', $search = '');
		
		$data['maxorder'] = ( !empty( $data['maxorder'] ) ) ? $data['maxorder'][0]->maxorder + 1 : 0 ;
		
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('backend/Header');
			$this->load->view('backend/catadd', $data);
			
		}
		else{
			
			$idata['catname']	= $this->input->post('catname');
			$idata['catorder']	= $this->input->post('catorder');			
			$idata['status']	= $this->input->post('status');			
			$idata['datec']	= date('Y-m-d H:i:s');
			
			$cid = $this->GeneralModel->AddNewRow( "category", $idata );
		
			$data['msg'] = 'Category Added Successfully'; 
			
			$this->load->view('backend/Header');
			$this->load->view('backend/catadd', $data);
			$this->load->view('backend/Header');

		}
	}
	
	public function code_check( $code )
	{
		$id = $this->input->post('id');
		$res = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = array( 'id !=' => $id, 'code' => $code ) );
		
		if( !empty( $res ) )
		{
			$this->form_validation->set_message('code_check', 'The {field} already exists');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function edit( $id )
	{		
		$this->form_validation->set_error_delimiters('<label class="control-label error mb-10">', '</label>');
		$this->form_validation->set_rules('catname', 'name', 'trim|required');
		$this->form_validation->set_rules('catorder', 'Order', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		
		$data['msg'] = ''; 
		
		if ($this->form_validation->run() == FALSE) {
			
			$data['category'] = $this->GeneralModel->GetInfoRow( $table = 'category', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/catedit', $data);
			/* $this->load->view('backend/Header'); */
			
		}
		else{
			
			$idata['catname']	= $this->input->post('catname');
			$idata['catorder']	= $this->input->post('catorder');
			$idata['status']	= $this->input->post('status');		
			
			$scid = $this->GeneralModel->UpdateRow( "category", $idata, array('id'=>$id) );
		
/* 			$data['msg'] = 'Subcategory Edited Successfully'; 
			
			$data['subcategory'] = $this->GeneralModel->GetInfoRow( $table = 'subcategory', $key = array('id'=>$id) );
			
			$this->load->view('backend/Header');
			$this->load->view('backend/subcatadd', $data);
			$this->load->view('backend/Header'); */
			
			redirect( base_url('backend/category?e=s') );

		}
	}

	
	
	public function delete($id)
	{					
		
		$this->GeneralModel->DeleteRow($table = 'category', array('id'=>$id));
		
		redirect( base_url() . 'backend/category?del=s' );
	}
	
}
?>