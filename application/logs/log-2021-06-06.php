<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2021-06-06 00:05:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 00:05:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 00:05:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 00:05:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 00:06:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 00:06:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 00:26:21 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('354', 'FREE', NULL, '2021-05-08 21:23:42', '2021-06-06 00:26:21', 50, 'FAIL', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading failcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Fail !!!<br>You got 5 of 10 possible points. <br>Your score: 50 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What are the use cases for adding additional entity types in SAP-delivered standard data models? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usage type 2 can reuse a standard delivered reuse area.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usage type 4 can reuse usage type 3 flex area.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Usage type 3 can reuse existing database tables in SAP ERP.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Usage type 1 can reuse a standard delivered reuse area.<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What are the advantages of having SAP Master Data Governance deployed as a hub system? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No system integration required<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Higher flexibility with mapping<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Increased flexibility for master data lifecycle management<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No latency for data replication and no cross-system monitoring<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Master data integration for the SAP intelligent suite is based on aligned domain models and geared towards which key processes? Note there are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Manufacture to Build<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Source to Pay<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Recruit to Hire<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Order to Procure<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lead to Cash<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which of the following attributes are common across business partner, customer and vendor in SAP Master Data Governance for Business Partner? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dunning data<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tax indicators<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Company code data<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> Tax numbers<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You want to reduce the impact on system performance after a mass data load by avoiding the non-intentional replication of large amounts of data. Which replication modes are applicable? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>SOA-based replication<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Limit changes using time interval<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>DRFLOG transaction<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filters and change pointers<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You have completed the final approval in the governance workflow and are setting up the replication. The business requires you to restrict the data to downstream systems based on certain criteria. Which of the following steps are required to achieve this? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Define Web service and filter by target system.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Define outbound implementation and assign filter objects.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Define replication model and replication channel by target system.<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Define replication model and filter by target system.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\">  Which actions are necessary to successfully import a file with changes in mass processing? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Ensure there is one worksheet for each table to be updated.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Upload a workbook to update tables and fields.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monitor the process for each worksheet and the volume of data.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Find duplicates based on business rules.<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What can you determine with the change request status and the workflow step used in the process logic of the rule-based workflow? Note: There are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 8 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The next change request status<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>User responsible for the next workflow step.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The next workflow step<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Possible actions in the user interface<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Predecessor steps in the workflow<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which options are available when performing data load using SAP Master Data Governance consolidation? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 9 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>CSV or XLSX file format to upload source records<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Find duplicates based on matching rules<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>External load using business partner SOAP service<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Validate against central governance checks<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What are the three main areas that can be configured for the Business Context Viewer? Note: There are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 10 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Search integration<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Data providers<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Query management<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New change request type<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>User interface<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-06-06 00:26:21')
ERROR - 2021-06-06 00:26:21 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 00:39:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 01:32:44 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 02:01:30 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 02:28:36 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 02:46:09 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 03:11:03 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 03:25:40 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 04:40:21 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 05:20:49 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 07:00:00 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 07:04:02 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 07:13:15 --> 404 Page Not Found: Apple-app-site-association/index
ERROR - 2021-06-06 07:15:17 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 07:20:45 --> 404 Page Not Found: Well-known/apple-app-site-association
ERROR - 2021-06-06 08:06:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:06:56 --> 404 Page Not Found: Assets/images
ERROR - 2021-06-06 08:10:51 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 08:12:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:12:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:13:44 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 08:13:44 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 08:13:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:17:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:17:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:19:07 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 08:19:07 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 08:19:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:19:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:20:06 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 08:20:06 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 08:20:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:20:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:21:25 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 08:27:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 08:53:47 --> 404 Page Not Found: Images/Nxrs4tAtO
ERROR - 2021-06-06 09:21:28 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 09:26:43 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 09:37:39 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 09:38:40 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 09:54:14 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 10:01:22 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 10:03:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 10:08:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 10:10:08 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 10:10:08 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 10:10:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 10:30:22 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 10:31:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 10:32:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 10:32:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 10:41:35 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 11:02:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 11:05:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 11:06:06 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 11:06:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 11:06:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 11:06:41 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 11:06:41 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 11:06:41 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 435
ERROR - 2021-06-06 11:06:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 11:22:32 --> 404 Page Not Found: Well-known/security.txt
ERROR - 2021-06-06 11:52:29 --> 404 Page Not Found: GponForm/diag_Form
ERROR - 2021-06-06 12:12:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 12:15:53 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 12:31:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 12:47:40 --> 404 Page Not Found: Wwwtrainingsapcom/index
ERROR - 2021-06-06 12:48:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:03:03 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 13:19:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:19:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:19:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:35:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:35:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:43:23 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-06 13:43:23 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-06 13:46:35 --> Severity: Warning --> Illegal string offset 'msgh1' /home/examindialms/public_html/application/controllers/Order.php 1383
ERROR - 2021-06-06 13:46:35 --> Severity: Warning --> Illegal string offset 'msgsuc' /home/examindialms/public_html/application/controllers/Order.php 1384
ERROR - 2021-06-06 13:46:35 --> Severity: Warning --> Illegal string offset 'ondemand' /home/examindialms/public_html/application/controllers/Order.php 1385
ERROR - 2021-06-06 13:52:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:52:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:54:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:54:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:54:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:56:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 13:57:18 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 14:38:45 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 14:46:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:46:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:46:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:46:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:46:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:47:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:47:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:47:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:47:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:53:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:55:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 14:58:24 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 14:59:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:14:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:21:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:24:13 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 15:28:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:09 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 15:29:09 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 371
ERROR - 2021-06-06 15:29:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:29:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:30:04 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 15:30:04 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 15:30:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:30:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:21 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 15:31:21 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 371
ERROR - 2021-06-06 15:31:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:31:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:35:00 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 15:35:00 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 15:35:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:35:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:35:29 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-06 15:35:29 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-06 15:35:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 15:39:18 --> 404 Page Not Found: Owa/index
ERROR - 2021-06-06 15:54:14 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 16:03:53 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-06-06 16:03:57 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-06 16:03:58 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-06 16:04:41 --> Severity: Warning --> Illegal string offset 'msgh1' /home/examindialms/public_html/application/controllers/Order.php 1430
ERROR - 2021-06-06 16:04:41 --> Severity: Warning --> Illegal string offset 'msgerr' /home/examindialms/public_html/application/controllers/Order.php 1431
ERROR - 2021-06-06 16:04:41 --> Severity: Warning --> Illegal string offset 'ondemand' /home/examindialms/public_html/application/controllers/Order.php 1432
ERROR - 2021-06-06 16:04:41 --> Severity: Warning --> Illegal string offset 'title' /home/examindialms/public_html/application/controllers/Order.php 1433
ERROR - 2021-06-06 16:10:38 --> 404 Page Not Found: Groovyconsole/index
ERROR - 2021-06-06 16:21:17 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 16:29:35 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 16:31:32 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 17:17:50 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 17:23:29 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 17:31:47 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 18:05:13 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 18:43:52 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-06-06 18:43:52 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-06-06 18:43:52 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-06-06 18:43:52 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-06-06 18:43:52 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: 2018/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: 2019/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: Shop/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-06-06 18:43:53 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-06-06 18:43:54 --> 404 Page Not Found: Media/wp-includes
ERROR - 2021-06-06 18:43:54 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-06-06 18:43:54 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-06-06 18:43:54 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-06-06 18:43:54 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-06-06 18:46:08 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 18:47:39 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 19:02:44 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 19:02:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:02:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:03:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:12:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:12:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:15:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:21:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:23:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:23:54 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:23:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:24:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:24:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 19:35:00 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 19:39:36 --> 404 Page Not Found: TelerikWebUIWebResourceaxd/index
ERROR - 2021-06-06 19:54:04 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 20:08:36 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 20:20:08 --> 404 Page Not Found: Assets/images
ERROR - 2021-06-06 20:20:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 20:22:33 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 20:57:52 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 21:38:47 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 21:45:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 21:46:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 21:47:09 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 21:47:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 21:55:42 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 21:57:51 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-06 21:57:59 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 22:02:13 --> 404 Page Not Found: GponForm/diag_Form
ERROR - 2021-06-06 22:28:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 22:52:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 22:52:47 --> 404 Page Not Found: Assets/images
ERROR - 2021-06-06 22:52:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 22:56:08 --> 404 Page Not Found: Env/index
ERROR - 2021-06-06 22:56:14 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-06 22:56:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 22:56:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 22:58:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 22:58:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:00:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:00:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:03:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:04:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:05:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:05:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:08:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:08:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:09:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:10:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:11:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:12:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:14:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:14:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:16:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:16:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:18:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:18:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:20:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:21:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:21:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:21:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:22:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:23:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:28:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:28:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:30:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:30:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-06 23:32:49 --> 404 Page Not Found: Assets/css
