<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2021-01-11 00:12:26 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 00:22:41 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 00:22:56 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 00:32:26 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 00:33:10 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 01:08:15 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 02:09:45 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('303', 'FREE', NULL, '2021-01-10 15:13:50', '2021-01-11 02:09:45', 10, 'FAIL', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading failcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Fail !!!<br>You got 1 of 10 possible points. <br>Your score: 10 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which of the following parameters define the tax code on sales transactions? Choose Two correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tax event<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Tax exemption reason code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tax deductibility<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Tax types<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which account assignments are possible when using the general ledger? Choose Three correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Business partners<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>G/L Account<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Functional Area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Profit Center \'<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Fixed assets<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which accrual methods are supported for project sales (fixed price / time and material)? Choose Three correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recognize at point of delivery<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recognize at completed contract (revenue only)<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Recognize using cost-to-cost POC<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Recognize using work-to-work POC<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Recognize at point of invoice<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which of the following steps in the production process can be automated with the help of &quot;Automated Runs&quot;? Choose Two correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Closing of production lots<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Finishing of production orders<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Confirmation of production tasks<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Release of production orders<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You change an active project baseline and save it. Then, the system creates a new baseline. What are the statuses of the previous baseline and new baseline? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Previous baseline: &quot;active with pending changes&quot; . New baseline: &quot;in planning&quot;<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i> Previous baseline: &quot;obsolete&quot;. New baseline: &quot;in planning&quot;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Previous baseline: &quot;obsolete&quot;. New baseline: &quot;active&quot;<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Previous baseline: &quot;active with pending changes&quot; .New baseline: &quot;in approval&quot;<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which structure type for financial reporting structures is not based on general ledger accounts? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Income statement by nature of expense<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cash Flow Statement (Indirect Method)<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i> Income statement by function of expense<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cash Flow Statement (Direct Method)<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> When creating a customer invoice in SAP Business ByDesign, after which step is the XML/PDF output generated? Choose Two correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Change the price<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resubmit the output<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Save and close the customer invoice<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Release the customer invoice<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> In which of the following settings do you maintain the payment terms to be automatically determined in sales orders? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 8 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the company settings<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the account settings<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> In the product settings<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the ship-from settings<br><br>	<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> In which one of these areas do you store user credentials for accounts that are involved with communication between your company and a communications partner? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 9 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the communication scenario<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> In the communication arrangement<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i> In output channel definition<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the communication system<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which types of actual costs can you allocate to customer projects with sales integration? Choose Three correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 10 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Overhead costs from distribution runs<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Service costs from time recordings<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Directs costs from expense reports<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Overhead costs from absorption runs<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Material costs from inventory consumption<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-01-11 02:09:45')
ERROR - 2021-01-11 02:09:45 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 02:10:21 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 02:19:19 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 02:22:33 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 02:30:14 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('46', 'FREE', NULL, '2021-01-10 01:36:41', '2021-01-11 02:30:14', 0, 'FAIL', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading failcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Fail !!!<br>You got 0 of 10 possible points. <br>Your score: 0 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You need to enable real-time streaming using SAP HANA smart data integration (SDI) to load data into a DataStore object (advanced). What are prerequisites for this requirement? Note: There are 2 correct answers to this question. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>The data target is a partitioned DataStore Object (advanced) <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The adapter is real-time enabled <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>The process chain is running in streaming mode <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The source System is pushing data into SAP BW/4HANA <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> When modeling a characteristic, which SAP BW/4HANA features are intended to reduce the physical data volume? Note: There are 2 correct answers to this question. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Transitive Attribute <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enhanced master data update <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Reference characteristic <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Compounding <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which processing types can you define for a formula variable? Note: There are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replacement path from another query <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Replacement path from an InfoObjeet <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorization <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Manual input <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Replacement path from another variable <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> When can you NOT use a characteristic with the High Cardinality property? Note: There are 3 correct answers to this question. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>When it will be part Of a hierarchy <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;When it will be used in a report-to-report interface <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>When it will be a navigation attribute <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>When it will be a compound characteristic <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;When it will be a time-dependent display attribute <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What must you do if you want to use an object from Business Content in SAP BW/4HANA? Choose the correct answer. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Activate the object in the Data Warehousing Workbench. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Copy the Business Content object into the customer name space. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Convert the object from A version to D version. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Identify the Business Content objects in SAP HANA studio. <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What must you do before you can add an SAP HANA system to an SAP BW project? Choose the correct answer. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Assign an SAP BW user to the SAP HANA system <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Add an InfoArea in the SAP BW projects favorites section <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Activate all inactive SAP HANA calculation views of the SAP HANA system <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Connect to the SAP HANA database as a system in the SAP HANA Systems view <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You loaded a 5 MB table from a non-SAP source system to SAP BW/4HANA. How do you explain that the size of the table is reduced to 1 MB? Choose the correct answer. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The target is defined as row store and a delta merge was executed <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The target is defined as row store without delta merge execution. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The target is defined as column store without delta merge execution. <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> The target is defined as column store and a delta merge was executed. <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> For which activity do you use the simple key figure matrix? Choose the correct answer. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 8 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Structure the information from the requirement analysis <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; List properties of the data sources <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Allocate project resources <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Provide information on the relations between characteristics <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which condition must be met to replicate data based on a database view with SAP Landscape Transformation Replication Server (SLT) into an SAP HANA table? Choose the correct answer. </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 9 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> An SAP HANA source system must be defined to consume the view.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Each table of the view requires a defined trigger to enable data replication.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; View-based data result sets must be kept persistent before data replication can take place.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; One table (as part of the view) must be defined as primary table and will be used to trigger data replication.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which source types can you select when you create an Open ODS view? Note: There are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 10 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Transformation <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Virtual table using SAP HANA Smart Data Access <br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>DataSource (BW) <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Characteristic <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BW Query <br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-01-11 02:30:14')
ERROR - 2021-01-11 02:30:14 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 02:57:10 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 02:59:00 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 02:59:15 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 03:02:17 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 03:04:30 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 03:17:49 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 03:26:47 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 03:54:03 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 03:54:29 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 04:05:29 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 04:14:12 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 04:29:38 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 04:36:25 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 04:43:35 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 04:46:34 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 05:07:48 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 05:25:20 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 05:49:35 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 06:06:47 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 06:14:23 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-01-11 06:14:23 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-01-11 06:14:24 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-01-11 06:14:24 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-01-11 06:14:24 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-01-11 06:14:25 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-01-11 06:14:25 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-01-11 06:14:25 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-01-11 06:14:25 --> 404 Page Not Found: 2018/wp-includes
ERROR - 2021-01-11 06:14:26 --> 404 Page Not Found: 2019/wp-includes
ERROR - 2021-01-11 06:14:26 --> 404 Page Not Found: Shop/wp-includes
ERROR - 2021-01-11 06:14:26 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-01-11 06:14:26 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-01-11 06:14:27 --> 404 Page Not Found: Media/wp-includes
ERROR - 2021-01-11 06:14:27 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-01-11 06:14:27 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-01-11 06:14:27 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-01-11 06:14:28 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-01-11 06:16:37 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 06:50:55 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 07:18:09 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 07:29:08 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 07:55:07 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 08:18:25 --> 404 Page Not Found: Assets/images
ERROR - 2021-01-11 08:18:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:18:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:18:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:42:07 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 08:46:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:48:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:48:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:48:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:53:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:54:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:54:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:54:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 08:54:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 09:47:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 09:47:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 10:33:01 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 10:42:18 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 10:54:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 10:55:05 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 10:55:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 10:58:02 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 11:01:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 11:02:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 11:13:15 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-01-11 11:13:15 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-01-11 11:13:16 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-01-11 11:13:16 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-01-11 11:13:16 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-01-11 11:13:17 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-01-11 11:13:17 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-01-11 11:13:17 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-01-11 11:13:18 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-01-11 11:13:18 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-01-11 11:13:18 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-01-11 11:13:18 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-01-11 11:13:19 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-01-11 11:13:19 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-01-11 11:26:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 11:38:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 12:03:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 12:03:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 12:05:55 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-01-11 12:05:55 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-01-11 12:05:56 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-01-11 12:05:56 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-01-11 12:05:56 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-01-11 12:05:56 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-01-11 12:05:57 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-01-11 12:05:57 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-01-11 12:05:57 --> 404 Page Not Found: 2018/wp-includes
ERROR - 2021-01-11 12:05:57 --> 404 Page Not Found: 2019/wp-includes
ERROR - 2021-01-11 12:05:58 --> 404 Page Not Found: Shop/wp-includes
ERROR - 2021-01-11 12:05:58 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-01-11 12:05:58 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-01-11 12:05:58 --> 404 Page Not Found: Media/wp-includes
ERROR - 2021-01-11 12:05:59 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-01-11 12:05:59 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-01-11 12:05:59 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-01-11 12:06:00 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-01-11 12:19:37 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 12:31:38 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 12:48:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 13:22:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 13:27:30 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 13:50:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 13:54:50 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 13:58:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 14:06:17 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 14:08:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 14:24:13 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-01-11 14:24:13 --> 404 Page Not Found: Wp-content/plugins
ERROR - 2021-01-11 14:24:17 --> 404 Page Not Found: Api/jsonws
ERROR - 2021-01-11 14:24:24 --> 404 Page Not Found: Console/index
ERROR - 2021-01-11 14:24:24 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 14:24:27 --> 404 Page Not Found: Autodiscover/Autodiscover.xml
ERROR - 2021-01-11 14:24:29 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-01-11 14:27:12 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 14:31:54 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 14:37:11 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 14:38:03 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 14:41:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 14:43:47 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 14:53:29 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 14:55:48 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 14:59:04 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 15:15:10 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 15:16:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:16:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:18:14 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 15:18:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:23:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:23:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:25:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:26:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:33:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:37:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 15:47:48 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 15:49:22 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 15:52:39 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 16:05:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 16:05:41 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 16:07:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 16:17:36 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 16:19:38 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 16:24:55 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 16:25:28 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 16:29:14 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 16:36:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 16:37:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 16:37:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 16:42:03 --> Severity: error --> Exception: Too few arguments to function Packages::view(), 0 passed in /home/sapprepuser/public_html/system/core/CodeIgniter.php on line 532 and exactly 1 expected /home/sapprepuser/public_html/application/controllers/Packages.php 225
ERROR - 2021-01-11 16:43:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 16:52:30 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 16:53:15 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 17:00:22 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 17:04:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 17:04:25 --> 404 Page Not Found: Solr/index
ERROR - 2021-01-11 17:08:09 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 17:10:41 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 17:18:55 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:18:56 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:18:56 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:18:59 --> Severity: Warning --> require(/home/sapprepuser/public_html/vendor/autoload.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:18:59 --> Severity: Compile Error --> require(): Failed opening required '/home/sapprepuser/public_html/vendor/autoload.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:19:06 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:19:06 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:19:21 --> Severity: Warning --> require(/home/sapprepuser/public_html/vendor/autoload.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:19:21 --> Severity: Compile Error --> require(): Failed opening required '/home/sapprepuser/public_html/vendor/autoload.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:19:50 --> Severity: Warning --> require(/home/sapprepuser/public_html/vendor/autoload.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:19:50 --> Severity: Compile Error --> require(): Failed opening required '/home/sapprepuser/public_html/vendor/autoload.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:22:36 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 17:27:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:27:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:28:19 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 17:29:04 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 17:29:04 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 278
ERROR - 2021-01-11 17:29:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:15 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:29:15 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:29:15 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:29:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:21 --> Severity: Warning --> require(/home/sapprepuser/public_html/vendor/autoload.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:29:21 --> Severity: Compile Error --> require(): Failed opening required '/home/sapprepuser/public_html/vendor/autoload.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:29:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:29:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 17:30:18 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:30:18 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:30:18 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:30:20 --> Severity: Warning --> require(/home/sapprepuser/public_html/vendor/autoload.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:30:20 --> Severity: Compile Error --> require(): Failed opening required '/home/sapprepuser/public_html/vendor/autoload.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:32:45 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:32:45 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:32:46 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:32:47 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:32:48 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:32:48 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:32:48 --> 404 Page Not Found: Packages/Checkoutsession
ERROR - 2021-01-11 17:32:53 --> 404 Page Not Found: Packages/Checkoutsession
ERROR - 2021-01-11 17:32:56 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:32:56 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:33:14 --> 404 Page Not Found: Packages/Checkoutsession
ERROR - 2021-01-11 17:33:14 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-01-11 17:33:55 --> 404 Page Not Found: Packages/Checkoutsession
ERROR - 2021-01-11 17:33:59 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:33:59 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:33:59 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:34:01 --> Severity: Warning --> require(/home/sapprepuser/public_html/vendor/autoload.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:34:01 --> Severity: Compile Error --> require(): Failed opening required '/home/sapprepuser/public_html/vendor/autoload.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 5
ERROR - 2021-01-11 17:37:41 --> 404 Page Not Found: Packages/Checkoutsession
ERROR - 2021-01-11 17:38:43 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:38:43 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:39:14 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:39:14 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:40:13 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 17:43:12 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:43:12 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:43:16 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:43:16 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:43:27 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 17:44:34 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 17:51:25 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:51:28 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:51:28 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:51:28 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:51:28 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:51:28 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:51:56 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:51:59 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:51:59 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:51:59 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:51:59 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:51:59 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:51:59 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:52:06 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:52:06 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:55:06 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:55:06 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:55:08 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-01-11 17:55:08 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:55:59 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:56:21 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:56:22 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:58:21 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:58:36 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 17:58:36 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-01-11 18:01:54 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:01:55 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:05:13 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:05:18 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:05:38 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:05 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:11 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:27 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:32 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:36 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:39 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:42 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:06:42 --> 404 Page Not Found: Front/css
ERROR - 2021-01-11 18:07:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:07:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:08:37 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 18:08:37 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 278
ERROR - 2021-01-11 18:08:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:08:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:12:54 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 18:24:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 18:25:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:25:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:27:20 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 18:28:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:34:05 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 18:35:09 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 18:36:17 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 18:48:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 18:58:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:05:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:06:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:06:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:08:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:10:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:10:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:10:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:10:49 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 19:13:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:16:17 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 19:20:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:31:15 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 19:36:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:39:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:51:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:51:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:07 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 19:52:07 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 303
ERROR - 2021-01-11 19:52:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:52:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:53:28 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 19:53:28 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 278
ERROR - 2021-01-11 19:53:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:53:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:53:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:53:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:54:20 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 19:54:20 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 303
ERROR - 2021-01-11 19:54:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:54:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:54:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:55:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:55:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:55:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:56:50 --> 404 Page Not Found: Exams/min
ERROR - 2021-01-11 19:57:16 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 19:57:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:57:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:15 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 19:58:15 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 303
ERROR - 2021-01-11 19:58:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:58:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 19:59:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:05:04 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 20:05:04 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 278
ERROR - 2021-01-11 20:05:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:05:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:14:42 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 20:18:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:18:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:18:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:22:40 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 20:23:19 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 20:34:27 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 20:38:04 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 20:39:51 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 20:40:23 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 20:47:47 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 20:50:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:51:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:51:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:56:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:58:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 20:58:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 21:05:43 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 21:10:48 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 21:18:41 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 21:20:35 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 21:24:17 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 21:26:13 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 21:26:36 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 21:27:05 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 22:06:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:06:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:06:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:17:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:18:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:19:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:19:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:19:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:21:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:21:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:21:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:24:01 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 22:27:18 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 22:30:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:30:34 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 22:31:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:31:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:52:43 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 22:53:02 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 22:58:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:59:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 22:59:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 23:06:55 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 268
ERROR - 2021-01-11 23:06:55 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/sapprepuser/public_html/application/controllers/backend/Exams.php 278
ERROR - 2021-01-11 23:06:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 23:07:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 23:20:29 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 23:21:52 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 23:27:03 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 23:30:21 --> 404 Page Not Found: Env/index
ERROR - 2021-01-11 23:34:01 --> 404 Page Not Found: Exams/min
ERROR - 2021-01-11 23:34:14 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 23:47:31 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 23:48:54 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-01-11 23:52:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-01-11 23:53:04 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 23:53:17 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 397
ERROR - 2021-01-11 23:58:13 --> 404 Page Not Found: Assets/css
