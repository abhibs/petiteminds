<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2021-06-15 00:21:21 --> 404 Page Not Found: Env/index
ERROR - 2021-06-15 00:21:23 --> 404 Page Not Found: Laravel/.env
ERROR - 2021-06-15 00:26:56 --> 404 Page Not Found: Sdk/index
ERROR - 2021-06-15 00:26:58 --> 404 Page Not Found: Nmaplowercheck1623697015/index
ERROR - 2021-06-15 00:26:59 --> 404 Page Not Found: Evox/about
ERROR - 2021-06-15 00:26:59 --> 404 Page Not Found: HNAP1/index
ERROR - 2021-06-15 01:46:04 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('317', 'FREE', NULL, '2021-06-14 23:38:11', '2021-06-15 01:46:04', 86, 'PASS', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading passcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Pass !!!<br>You got 6 of 7 possible points. <br>Your score: 86 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What happens when you set the impex legacy mode property to true? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The ServiceLayer interceptors are triggered during the import/export process<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The ServiceLeayer interceptors and validators are NOT triggered during the import/export process<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The operations INSERT UPDATE and REMOVE are NOT performed by the mode/service during the import<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The operations INSERT UPDATE and REMOVE are performed by the Mode/Service during the import<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which cleanup actions can you perform to reduce performance issues related to the props table? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Decrease the size of the props table the database. This result the overflowing values being rejected<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Remove all the entries from the props table since the content is irrelevant<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Remove the CronJobs that are no longer issue. This results in the corresponding log entries being deleted from the props table<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Decrease the value of the hmc string modified values size property in the local properties file. This result in the overflowing values <br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> How are bug fixes provided to partners and customers? </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAP Commerce provides a patch release which contains only bug fixes and security patches<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> SAP commerce provides a patch release for the extension that is affected<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAP Commerce delivers small jar archives called Hot Fix for the extension that fixes a specific bug<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAP Commerce provides SAP Notes with code corrections describing the best way to fix a specific bug<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which of the following cases should you treat as a support incident? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A partner contacts SAP Product for assistance with a custom solution<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A partner requests information on how to customize the backoffice<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>A partner needs specialized advice regarding the architectural aspects of a project<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>A partner contacts SAP product support regarding a platform with slow performance<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Your customer reports an incident where the SAP Commerce system crashes in production. What would you ask for? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The database dump from the production system<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The thread dumps taken right before the crash<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The specific scenario to reproduce the issue<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The thread dumps taken after system reboot<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which benefits does SAP Cloud Platform Extension Factory provide? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>A customization layer for SAP commerce Cloud which depends on the API registry module<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Microservices which customize the standard functions of SAP commerce Cloud without compromising or upgradability <br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>An alternative and independent application to the SAP Commerce cloud based on the Backoffice<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;An analysis layer to capture customer interactions contexts and behaviors for future processing<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You see the following entry in the thread dumps while analyzing a systems performance: ajp-8019-43&rdquo; nid=183 state=WAITING -waiting on &lt;0x515a41f6&gt; ( a org apache tomcat until.net.JioEndpooints$Workder) -locked &lt;0x515a41f6&gt; ( a org.apache.tomcat.util.net.JioEndpoint$workder) At java lang Object wait(Native Method) At Java.lang Object wait(Object.java.485) At org.apache.tomat.util.net  JioEndpoint$Worker.await(JioEndpoint.java.4710) At.org.apache tomcat util. net JioEndpont$Workder.run(JioEndpoint.java:497) At java.lang Thread.run (Thread.java.662) Locked synchronizers count=0 How do you interpret this thread dump? </p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 7</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> The Tomact AJP parameter maxThreads is too small to handle simultaneous requests<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The thread is waiting for an incoming connection<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The system is about to crash as the thread is blocked and cannot perform its job<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The JVM option maxAJPThreads is too small to handle simultaneous requests<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-06-15 01:46:04')
ERROR - 2021-06-15 01:46:04 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 01:59:38 --> 404 Page Not Found: Cgi-bin/jarrewrite.sh
ERROR - 2021-06-15 03:38:30 --> 404 Page Not Found: Ecp/Current
ERROR - 2021-06-15 03:47:20 --> 404 Page Not Found: Env/index
ERROR - 2021-06-15 04:10:35 --> 404 Page Not Found: Images/Nxrs4tAtO
ERROR - 2021-06-15 04:13:57 --> 404 Page Not Found: Env/index
ERROR - 2021-06-15 04:22:33 --> 404 Page Not Found: Env/index
ERROR - 2021-06-15 05:15:29 --> 404 Page Not Found: Owa/index
ERROR - 2021-06-15 06:11:20 --> 404 Page Not Found: Env/index
ERROR - 2021-06-15 06:52:20 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-06-15 07:18:38 --> 404 Page Not Found: Assets/images
ERROR - 2021-06-15 07:18:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 07:18:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 07:18:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 07:29:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 07:30:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 07:33:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 08:39:25 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-06-15 08:39:27 --> 404 Page Not Found: Api/jsonws
ERROR - 2021-06-15 08:39:28 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-06-15 08:39:35 --> 404 Page Not Found: Wp-content/plugins
ERROR - 2021-06-15 08:39:37 --> 404 Page Not Found: Autodiscover/Autodiscover.xml
ERROR - 2021-06-15 08:39:38 --> 404 Page Not Found: Console/index
ERROR - 2021-06-15 08:39:41 --> 404 Page Not Found: _ignition/execute-solution
ERROR - 2021-06-15 08:55:12 --> 404 Page Not Found: Images/Nxrs4tAtO
ERROR - 2021-06-15 09:24:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:24:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:24:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:26:59 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 09:32:28 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 09:32:29 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 09:33:45 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 09:33:46 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 09:37:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:38:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:39:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:39:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:39:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:39:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:41:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:41:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:42:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:43:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:43:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:43:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:43:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:44:54 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:45:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:45:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:45:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:46:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:46:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:46:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 09:49:20 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 10:00:39 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 10:00:45 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 10:10:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:10:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:10:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:21:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:24:18 --> 404 Page Not Found: GponForm/diag_Form
ERROR - 2021-06-15 10:39:46 --> 404 Page Not Found: Env/index
ERROR - 2021-06-15 10:44:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:49:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:49:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:50:44 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 105
ERROR - 2021-06-15 10:50:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:50:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:51:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:51:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:51:49 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 10:51:49 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 371
ERROR - 2021-06-15 10:51:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 10:58:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:00:48 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 11:01:11 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 11:02:28 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 11:07:00 --> 404 Page Not Found: Remote/fgt_lang
ERROR - 2021-06-15 11:07:54 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 11:08:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:08:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:08:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:09:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:10:15 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 105
ERROR - 2021-06-15 11:10:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:10:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:10:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:10:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:10:44 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 11:10:44 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 371
ERROR - 2021-06-15 11:10:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:10:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:12:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:15:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:16:26 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 11:19:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:19:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:27:11 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 11:27:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:27:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:29:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:29:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:30:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:30:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:31:18 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 11:32:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:32:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:41:37 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 11:42:39 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 11:46:10 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 11:56:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:57:20 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 11:58:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:58:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:58:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 11:59:42 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 11:59:42 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 12:00:20 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 12:00:20 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 12:00:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:00:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:01:13 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 12:08:15 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-06-15 12:09:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:09:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:11:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:17:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:18:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:18:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:18:51 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 12:18:51 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 12:18:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:18:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:19:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:19:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:20:09 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 12:20:09 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 12:20:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:32:09 --> 404 Page Not Found: Owa/index
ERROR - 2021-06-15 12:32:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 12:38:11 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 12:42:12 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 13:06:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:11:00 --> 404 Page Not Found: TelerikWebUIWebResourceaxd/index
ERROR - 2021-06-15 13:13:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:15:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:15:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:17:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:21:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:23:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:24:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:24:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:26:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:26:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:26:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:27:57 --> 404 Page Not Found: Owa/auth
ERROR - 2021-06-15 13:29:54 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:30:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:30:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:33:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:34:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:36:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:36:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:36:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:36:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:49:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:49:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:54:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:54:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:55:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:56:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:56:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:56:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:57:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:57:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:58:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:58:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:58:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 13:59:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:01:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:02:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:05:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:06:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:06:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:06:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:07:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:08:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:08:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:08:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:09:54 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:10:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:10:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:10:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:16:24 --> 404 Page Not Found: Apple-touch-icon-120x120-precomposedpng/index
ERROR - 2021-06-15 14:16:24 --> 404 Page Not Found: Apple-touch-icon-120x120png/index
ERROR - 2021-06-15 14:16:24 --> 404 Page Not Found: Apple-touch-icon-precomposedpng/index
ERROR - 2021-06-15 14:16:24 --> 404 Page Not Found: Apple-touch-iconpng/index
ERROR - 2021-06-15 14:18:35 --> 404 Page Not Found: Ecp/a6238d175fee4206b4fb5173a655b82c.js
ERROR - 2021-06-15 14:23:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:24:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:25:57 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('267', 'FREE', NULL, '2021-06-15 11:01:11', '2021-06-15 14:25:57', 0, 'FAIL', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading failcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Fail !!!<br>You got 0 of 15 possible points. <br>Your score: 0 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Your team had developed an online voting application for a photo competition in AWS using CloudFormation. The application accepts high-quality images of each contestant and stores them in S3 then records the information about the image as well as the contestant\'s profile in RDS. After the competition, the CloudFormation stack is not used anymore and to save resources, the stack can be terminated. Your manager instructed you to back up the RDS database and the S3 bucket so the data can still be used even after the CloudFormation template is deleted.Which of the following options is the MOST suitable solution to fulfill this requirement? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set the DeletionPolicy to<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Set the DeletionPolicy on the RDS resource to<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set the DeletionPolicy on the S3 bucket to snapshot.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set the DeletionPolicy for the RDS instance to snapshot and then enable S3 bucket replication on the source bucket to a destination bucket to maintain a copy of all the S3 objects.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> An advertising firm is required to analyze the clickstream data of all of their websites in real time. The data will be used for clickstream analysis which contains the pages that the user visits and the sequential stream of clicks they create as they move across the website. Which of the following options will fulfill this requirement in AWS? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Collect the clickstreams from the websites and store to an S3 bucket. Analyze the data with AWS Lambda.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Send the clickstream data directly to a DynamoDB table and then create a custom application using Elastic Beanstalk that fetches the data from the table and analyzes the clickstream.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Send the clickstream data by user session to an Amazon SQS queue which buffers the data to a Multi-AZ RDS database. Create and deploy a custom application to a large EC2 instance that analyzes the data in RDS.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Push the clickstream data by session to an Amazon Kinesis stream and then analyze the data by using Amazon Kinesis workers.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> A company has recently adopted a hybrid cloud architecture which requires them to migrate their databases from their on-premises data center to AWS. One of their applications requires a heterogeneous database migration in which they need to transform their on-premises Oracle database to PostgreSQL. A schema and code transformation should be done first in order to successfully migrate the data.Which of the following options is the most suitable approach to migrate the database in AWS? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use the AWS Serverless Application Model (SAM) service to transform your database to PostgreSQL using AWS Lambda functions. Migrate the database to RDS using the AWS Database Migration Service (DMS).<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Migrate the database from your on-premises data center using the AWS Server Migration Service (SMS). Afterwards, use the AWS Database Migration Service to convert and migrate your data to Amazon RDS for PostgreSQL database.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use a combination of AWS Data Pipeline service and CodeCommit to convert the source schema and code to match that of the target PostgreSQL database in RDS. Use AWS Batch with Spot EC2 instances to cost-effectively migrate the data from the source database<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Use the AWS Schema Conversion Tool (SCT) to convert the source schema to match that of the target database. Migrate the data using the AWS Database Migration Service (DMS) from the source database to an Amazon RDS for PostgreSQL database.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are working as a Solutions Architect in a company which has a central VPC (VPC A) with one subnet. You have a VPC peering connection between VPC A and VPC B (pcx-aaaabbbb) and also between VPC A and VPC C (pcx-aaaacccc). Both VPC B and VPC C have matching CIDR blocks of 10.0.0.0/16 as shown in the diagram below:  You want to use the VPC peering connection pcx-aaaabbbb to route traffic between VPC A and a specific instance in VPC B. The route table is configured to route all other traffic destined for the 10.0.0.0/16 IP address range through pcx-aaaacccc between VPC A and VPC C as shown below:  They noticed that if an EC2 instance in VPC B, which has an IP address other than 10.0.0.77/32, sends traffic to VPC A, the response traffic is always routed to VPC C instead of VPC B. However, there is no 10.0.0.77 IP address in VPC C.Which of the following is the most suitable solution that you should do to fix this issue? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Add a static route on VPC A\'s route table with a destination of<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Since the route table uses longest prefix match to prioritize the routes, you can create an individual route to each of the IP addresses of your EC2 instances in VPC B to ensure that traffic is properly routed back to the correct server.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Add a static route on VPC C\'s route table with a destination of<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The AWS VPC peering connection does not support overlapping CIDR blocks which is why this issue is happening. You have to remove the VPC C peering connection to rectify the issue.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are a Solutions Architect for a software development company and you have been instructed to manage your AWS cloud infrastructure as code to automate the build and deploy process. The company would like to have the ability to easily deploy exact copies of different versions of your cloud infrastructure, stage changes into different environments, revert back to previous versions, and identify the specific versions running in the VPC. Plus, all new public-facing applications should also have global content delivery network (CDN) service.Which of the following can meet this requirement? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use CloudWatch as the CDN and Elastic Beanstalk to deploy and manage the cloud architecture.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use CloudFront as the CDN and Elastic Beanstalk to deploy and manage the cloud architecture.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Use AWS CloudFormation to manage the cloud architecture and CloudFront as the CDN.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use CloudWatch as the CDN and CloudFormation to manage the cloud architecture.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are working as a Solutions Architect for a cryptocurrency analytics website which uses a CloudFront distribution with a custom domain name (tutorialsdojo.com) to speed up the loading time of the site. Since the data being distributed are quite confidential, your manager instructed you to require HTTPS communication between the viewers (web visitors) and the CloudFront distribution. You are also instructed to improve the performance by increasing the proportion of your viewer requests that are served from CloudFront edge caches instead of going to your origin servers.What should you do to accomplish the above requirement? Choose Two correct answers.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Use an SSL/TLS certificate provided by AWS Certificate Manager (ACM).<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Import an SSL/TLS certificate from a third-party certificate authority into a private S3 bucket with versioning and MFA enabled.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Configure your origin to add a<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Associate your CloudFront web distribution with Lambda@Edge which provides automatic scalability from a few requests per day to thousands of requests per second.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Integrate your CloudFront web distribution with Amazon Elasticsearch (ES) and Kibana to improve the performance of your origin servers and to visualize the cache data in real time.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are looking to reduce the amount of time you spend managing database instances in your on-premises data center by migrating to a managed relational database service in AWS such as Amazon Relational Database Service (RDS). In addition, you are also planning to move your application hosted in your data center to a fully managed platform such as AWS Elastic Beanstalk.As the Solutions Architect of the company, which of the following is the most cost-effective migration strategy that you should implement to meet the above requirement? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rehost<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Refactor / Re-architect<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Repurchase<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Replatform<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are working for a shipping firm that has web applications running on their data center. Their servers have a dependency on non-x86 hardware and they plan to use AWS to scale their on-premises data storage. However, your backup application is only able to write to POSIX-compatible block-based storage. There are a total of 1,000 TB of data files that need to be mounted to a single folder on your file server. Existing users must also be able to access portions of this data while the backups are taking place.In this scenario, what backup solution would be most appropriate? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 8 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use Amazon S3 as the target for your data backups.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use Amazon Glacier as the target for your data backups.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Provision Gateway Cached Volumes from AWS Storage Gateway.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Provision Gateway Stored Volumes from AWS Storage Gateway.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are instructed to perform a Total Cost of Ownership (TCO) analysis and prepare a cost optimized migration plan for the systems hosted in your on-premises network to AWS. It is required that you collect configuration, usage, and behavior data from your on-premises servers to help you better understand your workloads before doing the migration.Which of the following is the most suitable solution that you should implement to meet this requirement? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 9 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Use the AWS Application Discovery Service to gather data about your on-premises data center and perform the TCO analysis.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use the AWS Migration Hub service to collect data from each server in your on-premises data center and perform the TCO analysis.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use the AWS Server Migration Service (SMS) to migrate VM servers to AWS and collect data required to complete your TCO analysis.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use the AWS SAM service to move your data to AWS which will also help you perform the TCO analysis.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are working as a Solutions Architect for a rental payment startup which enables users to pay for their rent using either their debit or credit cards. Since the system is not fully compliant with the Payment Card Industry Data Security Standard (PCI DSS), they are using a third-party payment service to handle and process credit card payments on their platform. Their prototype payments portal uses auto-scaled EC2 instances</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 10 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Route credit card payment requests from the EC2 instances through NAT Gateway with an associated Elastic IP address.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Route payment requests from the application servers through the ELB directly, which will then be routed to a Customer Gateway.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Whitelist the Internet Gateway Public IP in the Security Group and route payment requests through the Internet Gateway.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Route credit card payment requests from the Amazon EC2 instances through NAT Instance with an associated Elastic IP address.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You are a Solutions Architect for a global financial company which has a lot of data centers around the globe. Due to the ever-growing data that your company is storing, you were instructed to set up a durable, cost-effective solution to archive your data from your existing tape-based backup infrastructure to AWS Cloud. How could you implement this solution in AWS? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 11 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set up a File Gateway which will back up your data in Amazon S3 and archive in Amazon Glacier using your existing tape-based processes.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set up a Stored Volume Gateway which will back up your data in Amazon S3 with point-in-time backups as EBS snapshots.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set up a Tape Gateway which will back up your data in Amazon S3 with point-in-time backups as tapes which will be stored in the Virtual Tape Shelf.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Set up a Tape Gateway which will back up your data in Amazon S3 and archive in Amazon Glacier using your existing tape-based processes.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> A small telecommunications company has recently adopted a hybrid cloud architecture with AWS. They are storing static files of their on-premises web application on a 5 TB gateway-stored volume in AWS Storage Gateway, which is attached to the application server via an iSCSI interface. As part of their disaster recovery plan, they should be able to run the web application on AWS in case that their on-premises network encountered any technical issues.Which of the following options is the MOST suitable solution that you should implement? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 12 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Generate an EBS snapshot of the static content from the AWS Storage Gateway service. Afterwards, restore it to an EBS volume that you can then attach to the EC2 instance where the application server is hosted.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; For the static content, create an EFS file system from the AWS Storage Gateway service and mount it to the EC2 instance where the application server is hosted.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Restore the static content by<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Restore the static content<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> A leading e-commerce company plans to launch a donation website for all the victims of the recent super typhoon in South East Asia for its Corporate and Social Responsibility program. The company will advertise their program on TV and in social media, which is why they anticipate incoming traffic on their donation website. Donors can send their donations in cash, which can be transferred electronically, or they can simply post their home address where a team of volunteers can pick-up their used clothes, canned goods and other donations. Donors can optionally write a positive and encouraging message to the victims along with their donations. These features of the donation website will eventually result in a high number of write operations on their database tier considering that there are millions of generous donors around the globe who want to help. Which of the following options is the best solution for this scenario?Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 13 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Amazon DynamoDB with a provisioned write throughput. Use an SQS queue to buffer the large incoming traffic to your Auto Scaled EC2 instances, which processes and writes the data to DynamoDB.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use an Amazon RDS instance with Provisioned IOPS.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use an Oracle database hosted on an extra large Dedicated EC2 instance as your database tier and an SQS queue for buffering the write operations.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Use DynamoDB as a database storage and a CloudFront web distribution for hosting static resources.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> The national election will be held in 6 months time and your startup won the bid to build an e-voting system. There would be millions of voters, which is why the system must be able to handle large incoming requests and also have a web page to show the real-time poll. What would be the best and most cost-effective method of architecting this system?</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 14 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set up a Lamba service which pulls the most recent DynamoDB results. Publish the real-time poll results to a custom HTML page hosted in an S3 bucket. Use a CloudFront distribution and Route 53 for routing and DNS.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Build a javascript application using Angular or React for the UI of the voting system and host it in S3 static website hosting. Use CloudFront as the CDN and Route 53 for routing. Build an API using Lambda and API Gateway which communicates directly with <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Set up an Auto Scaling group of Spot EC2 instances which pulls the most recent DynamoDB results. Publish the real-time poll results to a custom HTML page hosted in an S3 bucket. Use Route 53 for routing and DNS.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Launch an Auto Scaling group of Spot EC2 instances with an Application Load Balancer. The EC2 instances shall host the services that fetch the data from DynamoDB table. Launch a web server on an on-demand EC2 instance which is dedicated to publish the pol<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> A technology company has a large enterprise resource planning (ERP) system with 70 TB of data hosted in their data center, which they want to migrate to AWS. The migration activities should not exceed 7 days to avoid any downtime as the on-premises data storage being used by the ERP system is almost full. Since the data to be migrated is 79 TB, the company is choosing between Snowball and Snowball Edge as their preferred service to transfer their data. The service should also provide a durable local storage to ensure data durability.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 15 of 15</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Import data into Amazon S3.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Use with AWS Greengrass (IoT).<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Local compute with AWS Lambda.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Local compute instances<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Transfer files through NFS with a GUI.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-06-15 14:25:57')
ERROR - 2021-06-15 14:25:57 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 14:26:13 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 14:26:14 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 14:27:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:27:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:29:48 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 14:32:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:32:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:38:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:45:42 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 14:50:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:50:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:50:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 14:50:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:01:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:01:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:02:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:02:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:04:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:04:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:05:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:07:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:07:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:09:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:09:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:09:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:09:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:12:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:12:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:12:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:12:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:14:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:15:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:15:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:15:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:16:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:17:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:17:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:17:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:25:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:26:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:36:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:36:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:39:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:39:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:41:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:42:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:42:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:42:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:44:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:44:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:45:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:45:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:47:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:47:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:47:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:47:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:49:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:49:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:49:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:49:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:50:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:51:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:52:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:53:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 15:53:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:07:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:07:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:08:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:08:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:08:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:36:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:36:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 16:36:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:36:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:38:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:39:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:39:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:40:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:40:43 --> 404 Page Not Found: Wwwtrainingsapcom/index
ERROR - 2021-06-15 17:44:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:45:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:45:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:46:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:51:41 --> 404 Page Not Found: Actuator/health
ERROR - 2021-06-15 17:54:20 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 17:54:24 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-06-15 17:57:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:57:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:57:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:57:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 17:58:32 --> 404 Page Not Found: Remote/login
ERROR - 2021-06-15 18:03:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:04:42 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 105
ERROR - 2021-06-15 18:04:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:04:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:05:42 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 105
ERROR - 2021-06-15 18:05:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:05:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:05:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:06:05 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 18:06:05 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 371
ERROR - 2021-06-15 18:06:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:06:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:06:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:06:27 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 18:06:27 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 371
ERROR - 2021-06-15 18:06:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:06:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:06:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:11:34 --> 404 Page Not Found: Front/css
ERROR - 2021-06-15 18:41:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:42:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:42:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:42:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:42:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:43:50 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 18:43:50 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 18:43:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:43:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:44:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:44:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:46:21 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 18:46:21 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 18:46:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:46:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:46:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:46:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:47:20 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 18:47:20 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 18:47:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:48:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:49:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:49:54 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:50:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:50:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 18:50:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:06:05 --> 404 Page Not Found: Owa/index
ERROR - 2021-06-15 19:08:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:09:39 --> 404 Page Not Found: Cgi-bin/jarrewrite.sh
ERROR - 2021-06-15 19:13:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:18:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:18:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:18:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:35:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:35:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:36:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:36:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:40:10 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 19:40:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:40:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:40:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:40:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:07 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 19:41:07 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 19:41:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:34 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-06-15 19:41:34 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-06-15 19:41:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:41:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 19:42:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 21:15:53 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 22:20:25 --> 404 Page Not Found: Assets/images
ERROR - 2021-06-15 22:21:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:35:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:35:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:35:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:38:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:44:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:50:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 22:55:09 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 22:59:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:20:18 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-06-15 23:25:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:25:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:25:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:28:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:28:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:29:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:29:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:29:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:30:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:30:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:31:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:32:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:33:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:33:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:35:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:35:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:37:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:37:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:38:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:38:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:40:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:40:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:41:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:42:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:43:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:43:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:46:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:46:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:48:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:48:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:49:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:49:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:51:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:51:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:54:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-06-15 23:58:06 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-06-15 23:59:13 --> 404 Page Not Found: Env/index
