<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2021-09-13 04:52:20 --> 404 Page Not Found: Ecp/Current
ERROR - 2021-09-13 06:35:41 --> 404 Page Not Found: Assets/images
ERROR - 2021-09-13 06:35:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 06:35:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 07:47:09 --> 404 Page Not Found: Actuator/health
ERROR - 2021-09-13 07:52:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 08:29:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 08:46:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 09:04:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 09:06:26 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('354', 'FREE', NULL, '2021-05-08 21:23:42', '2021-09-13 09:06:26', 50, 'FAIL', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading failcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Fail !!!<br>You got 5 of 10 possible points. <br>Your score: 50 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What are the use cases for adding additional entity types in SAP-delivered standard data models? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usage type 2 can reuse a standard delivered reuse area.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usage type 4 can reuse usage type 3 flex area.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Usage type 3 can reuse existing database tables in SAP ERP.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Usage type 1 can reuse a standard delivered reuse area.<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What are the advantages of having SAP Master Data Governance deployed as a hub system? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No system integration required<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Higher flexibility with mapping<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Increased flexibility for master data lifecycle management<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No latency for data replication and no cross-system monitoring<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Master data integration for the SAP intelligent suite is based on aligned domain models and geared towards which key processes? Note there are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Manufacture to Build<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Source to Pay<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Recruit to Hire<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Order to Procure<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lead to Cash<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which of the following attributes are common across business partner, customer and vendor in SAP Master Data Governance for Business Partner? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dunning data<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tax indicators<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Company code data<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> Tax numbers<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You want to reduce the impact on system performance after a mass data load by avoiding the non-intentional replication of large amounts of data. Which replication modes are applicable? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>SOA-based replication<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Limit changes using time interval<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>DRFLOG transaction<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filters and change pointers<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You have completed the final approval in the governance workflow and are setting up the replication. The business requires you to restrict the data to downstream systems based on certain criteria. Which of the following steps are required to achieve this? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Define Web service and filter by target system.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Define outbound implementation and assign filter objects.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Define replication model and replication channel by target system.<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Define replication model and filter by target system.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\">  Which actions are necessary to successfully import a file with changes in mass processing? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Ensure there is one worksheet for each table to be updated.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Upload a workbook to update tables and fields.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monitor the process for each worksheet and the volume of data.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Find duplicates based on business rules.<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What can you determine with the change request status and the workflow step used in the process logic of the rule-based workflow? Note: There are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 8 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The next change request status<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>User responsible for the next workflow step.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>The next workflow step<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Possible actions in the user interface<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Predecessor steps in the workflow<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which options are available when performing data load using SAP Master Data Governance consolidation? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 9 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>CSV or XLSX file format to upload source records<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Find duplicates based on matching rules<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>External load using business partner SOAP service<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Validate against central governance checks<br><br>	<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/tick.png\" class=\"img-responsive\"><p class=\"green\">The given answer is right </p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What are the three main areas that can be configured for the Business Context Viewer? Note: There are 3 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 10 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Search integration<br><i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color: red;\"></i>Data providers<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i>Query management<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New change request type<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>User interface<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.examroadmap.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-09-13 09:06:26')
ERROR - 2021-09-13 09:06:26 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-09-13 09:36:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 09:36:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:06:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:07:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:13:59 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 10:27:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:31:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:31:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:32:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 10:40:21 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 10:43:26 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-09-13 10:53:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 11:22:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:02:24 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 12:15:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:15:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:15:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:36:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:36:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:36:29 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 328
ERROR - 2021-09-13 12:36:29 --> Severity: Warning --> count(): Parameter must be an array or an object that implements Countable /home/examindialms/public_html/application/controllers/backend/Exams.php 343
ERROR - 2021-09-13 12:36:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:36:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 12:47:32 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 12:58:26 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 13:01:46 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 13:27:01 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 13:47:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 14:04:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 15:22:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 15:23:37 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 15:23:41 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 15:26:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 15:27:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 15:41:18 --> 404 Page Not Found: Owa/auth
ERROR - 2021-09-13 16:01:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 16:16:08 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 16:16:08 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 16:17:46 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 16:17:46 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 16:18:37 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-09-13 16:20:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 16:25:54 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 16:25:54 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 16:34:23 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 16:42:08 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 16:43:00 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 16:48:43 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 16:50:01 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 17:12:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:12:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:25:46 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 17:35:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:38:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:38:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:38:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:38:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:41:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:41:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:41:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:41:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 17:46:18 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-09-13 17:46:19 --> 404 Page Not Found: _ignition/execute-solution
ERROR - 2021-09-13 17:46:24 --> 404 Page Not Found: Wp-content/plugins
ERROR - 2021-09-13 17:46:27 --> 404 Page Not Found: Autodiscover/Autodiscover.xml
ERROR - 2021-09-13 17:46:28 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-09-13 17:46:29 --> 404 Page Not Found: Console/index
ERROR - 2021-09-13 17:46:31 --> 404 Page Not Found: Api/jsonws
ERROR - 2021-09-13 17:59:33 --> 404 Page Not Found: Remote/fgt_lang
ERROR - 2021-09-13 18:05:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 18:06:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 18:15:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 18:25:05 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 18:25:05 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-09-13 18:39:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 18:55:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 19:26:07 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 19:46:25 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 19:50:26 --> 404 Page Not Found: Owa/auth
ERROR - 2021-09-13 19:56:26 --> 404 Page Not Found: Owa/auth
ERROR - 2021-09-13 19:57:23 --> 404 Page Not Found: Ecp/Current
ERROR - 2021-09-13 20:00:50 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 21:14:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:14:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:14:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:14:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:14:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:17:12 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-09-13 21:17:13 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-09-13 21:17:13 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-09-13 21:17:13 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-09-13 21:17:13 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-09-13 21:17:14 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-09-13 21:17:14 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-09-13 21:17:14 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-09-13 21:17:14 --> 404 Page Not Found: 2018/wp-includes
ERROR - 2021-09-13 21:17:15 --> 404 Page Not Found: 2019/wp-includes
ERROR - 2021-09-13 21:17:15 --> 404 Page Not Found: Shop/wp-includes
ERROR - 2021-09-13 21:17:15 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-09-13 21:17:15 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-09-13 21:17:15 --> 404 Page Not Found: Media/wp-includes
ERROR - 2021-09-13 21:17:16 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-09-13 21:17:16 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-09-13 21:17:16 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-09-13 21:17:16 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-09-13 21:34:29 --> 404 Page Not Found: Assets/images
ERROR - 2021-09-13 21:34:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:35:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:35:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:35:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:39:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:40:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:41:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:41:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:42:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:42:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:43:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:43:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:45:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:45:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:46:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:46:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:47:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:48:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:49:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:49:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:51:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:51:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:52:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:52:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:54:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:54:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:54:41 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 21:57:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:57:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:58:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 21:59:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 22:00:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 22:00:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 22:01:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 22:05:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 22:32:48 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 22:42:31 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/examresult_1.php 147
ERROR - 2021-09-13 22:50:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-09-13 23:28:13 --> 404 Page Not Found: Env/index
ERROR - 2021-09-13 23:52:08 --> Severity: Warning --> A non-numeric value encountered /home/examindialms/public_html/application/views/takeexam_1.php 412
ERROR - 2021-09-13 23:54:38 --> 404 Page Not Found: Env/index
