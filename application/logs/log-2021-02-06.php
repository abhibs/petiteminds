<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2021-02-06 00:19:53 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 00:41:07 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 00:48:46 --> Query error: Column 'fk_stuid' cannot be null - Invalid query: INSERT INTO `exam_results` (`fk_examid`, `exam_type`, `fk_stuid`, `eres_exstarted`, `eres_exfinished`, `eres_score`, `eres_status`, `eres_html`, `eresdatec`) VALUES ('333', 'FREE', NULL, '2021-02-05 16:55:15', '2021-02-06 00:48:46', 0, 'FAIL', '<div class=\"panel panel-default quepanel\">\n			\n				<div class=\"panel-heading failcls\" style=\"\">							\n				\n					<div class=\"row\">                \n					\n						<div class=\"col-md-10\">\n						\n							<p class=\"\">Fail !!!<br>You got 0 of 10 possible points. <br>Your score: 0 %</p>\n																		\n						</div>				\n						\n			\n					</div>                            \n				\n				</div>\n									\n				</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> You need to enable real-time streaming using SAP HANA smart data integration (SDI) to load data into a DataStore object (advanced). What are prerequisites for this requirement? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 1 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The source system is pulling data into SAP BW/4HANA.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>The process chain is running in streaming mode.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>The data target is a DataStore object (advanced) of the InfoCube model template.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The adapter is real-time enabled.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Your customer is using a pure SAP HANA native data warehouse approach. Which tools should the customer install? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 2 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAP solutions for enterprise information management<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>BW Modeling Tools<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAP Information Steward<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Process Chain Monitoring app<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> For which business requirements do you suggest an SAP BW/4HANA modeling focus rather than an SAP HANA modeling focus? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 3 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report on a harmonized set of master data.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Load snapshots or deltas on a periodic basis.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Use existing HANA content models.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Leverage SQL in-house knowledge.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which condition must you ensure if you want to replicate data based on a view with SAP Landscape Transformation Replication Server? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 4 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; View-based data result sets must be kept persistent first until data replication can take place.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> An SAP HANA source system must be defined to consume the view.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Each table of the view requires a defined trigger to enable data replication.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; One table as part of the view has to be defined as primary table and is used to trigger data replication.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What must you do if you want to use an object from BI Content? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 5 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Copy the BI Content object into the customer name space.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Convert the object from A version to D version.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Activate the object in the Data Warehousing Workbench.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Identify the BI Content objects in SAP HANA studio.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> What happens when you execute the Trigger Delta Merge operation for a given data target? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 6 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The data from the delta queue is extracted and merged into the data target.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The delta between the new data and the already loaded data is removed.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> Data is transferred from the delta store to the main store.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The before and after images of the delta queues are merged.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which actions can you perform with an SAP HANA calculation view that has the DIMENSION data category assigned? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 7 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Join serveral tables.<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Define text joins.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Persist master data.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aggregate measures.<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which of the following methods supports the SAP HANA data virtualization approach with OBDC technology? Choose the correct answer.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 8 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i> SAP HANA smart data access<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAP HANA smart data integration<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAP Landscape Transformation Replication Server<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAP Data Services<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which objects can you associate with a source field that is defined as a characteristic within an Open ODS view? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 9 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAP BW InfoObject<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Open ODS view type Master Data for Attributes<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Open ODS view type Facts<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Open ODS view type Master Data for Hierarchies<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div><div class=\"panel panel-default quepanel\" >\n											<div class=\"panel-heading\">							<div class=\"row\">                <div class=\"col-md-10\">\n												<p class=\"\"> Which variables types allow filter values to be determined based on the user\'s authorizations? Note: There are 2 correct answers to this question.</p>\n																	\n						</div>				\n						<div class=\"col-md-2\">    			\n							<p style=\"\">Question 10 of 10</p><p></p>						\n							  \n									\n						</div>		\n						\n						</div>                            </div>\n											<div class=\"panel-body\" style=\"\">\n																					\n												\n														<div class=\"form-group\">\n														\n								<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Characteristic value variable<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Formula variable<br><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: gray;\"></i>Hierarchy node variable<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hierarchy variable<br><br>\n				<div class=\"ans-box\"><img src=\"https://www.sapprep.com/front/images/icons/close.png\" class=\"img-responsive\" ><p class=\"red\">The given answer is wrong</p></div></div>			\n													\n											</div>\n										   \n										</div>', '2021-02-06 00:48:46')
ERROR - 2021-02-06 00:48:46 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 00:56:24 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 00:57:31 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 01:12:10 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 03:50:03 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 03:53:57 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 03:56:34 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 04:12:27 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 04:23:12 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 04:56:32 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 04:57:05 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 05:20:51 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 05:36:09 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 05:39:14 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 06:55:55 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 06:58:13 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 06:58:24 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 07:12:16 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 07:31:44 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 07:41:32 --> 404 Page Not Found: Assets/images
ERROR - 2021-02-06 07:41:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 07:41:36 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 07:41:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 07:42:46 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 07:43:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 07:48:29 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 08:01:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 08:20:20 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 08:27:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 08:38:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 08:40:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 08:40:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 08:42:03 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 08:49:18 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 08:58:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 09:00:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 09:00:51 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 09:01:04 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 09:15:59 --> 404 Page Not Found: Remote/fgt_lang
ERROR - 2021-02-06 09:16:46 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 09:27:57 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 09:28:51 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 09:35:35 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 09:55:48 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 10:10:01 --> 404 Page Not Found: Aaa9/index
ERROR - 2021-02-06 10:10:03 --> 404 Page Not Found: Aab9/index
ERROR - 2021-02-06 10:10:29 --> 404 Page Not Found: Aaa9/index
ERROR - 2021-02-06 10:10:31 --> 404 Page Not Found: Aab9/index
ERROR - 2021-02-06 10:19:47 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 10:40:38 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 10:46:24 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 10:52:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 10:55:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 10:57:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 10:58:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:03:02 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:03:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:03:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:03:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:04:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:04:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:04:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:04:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:04:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:05:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:06:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:06:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:06:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:06:45 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:07:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:12:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:12:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:13:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:13:10 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 11:13:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:13:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:13:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:14:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:14:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:14:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:14:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:15:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:15:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:16:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:16:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:16:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:16:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:16:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:16:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:37:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:37:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:38:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:38:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:38:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:38:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:38:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:35 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:39:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 11:57:19 --> 404 Page Not Found: Front/css
ERROR - 2021-02-06 11:57:23 --> 404 Page Not Found: Front/css
ERROR - 2021-02-06 11:57:50 --> 404 Page Not Found: Front/css
ERROR - 2021-02-06 11:58:14 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-02-06 11:58:14 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-02-06 11:58:14 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-02-06 11:58:15 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-02-06 11:58:15 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-02-06 11:58:15 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-02-06 11:58:15 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-02-06 11:58:15 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-02-06 11:58:16 --> 404 Page Not Found: 2018/wp-includes
ERROR - 2021-02-06 11:58:16 --> 404 Page Not Found: 2019/wp-includes
ERROR - 2021-02-06 11:58:16 --> 404 Page Not Found: Shop/wp-includes
ERROR - 2021-02-06 11:58:16 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-02-06 11:58:17 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-02-06 11:58:17 --> 404 Page Not Found: Media/wp-includes
ERROR - 2021-02-06 11:58:17 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-02-06 11:58:17 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-02-06 11:58:17 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-02-06 11:58:18 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-02-06 12:05:09 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 12:06:53 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 12:07:01 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 12:09:18 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 12:09:29 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-02-06 12:10:28 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 12:10:46 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 12:36:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 12:46:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:00:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:01:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:01:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:03:11 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 13:04:26 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 13:05:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:05:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:05:52 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 13:10:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:17:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 13:46:12 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 13:48:54 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 13:49:34 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 13:52:04 --> 404 Page Not Found: Rss/index
ERROR - 2021-02-06 13:52:04 --> 404 Page Not Found: Rss/index
ERROR - 2021-02-06 13:52:04 --> 404 Page Not Found: Atom/index
ERROR - 2021-02-06 13:52:04 --> 404 Page Not Found: Feed/index
ERROR - 2021-02-06 13:52:56 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 14:02:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:05:07 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 14:05:14 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:14 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:23 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:23 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:34 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:34 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:37 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 14:05:40 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:40 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:05:46 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-02-06 14:06:11 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 14:06:14 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:14 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:15 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:15 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:21 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:21 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:52 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:52 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:06:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:07:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:07:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:07:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:07:56 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 14:07:58 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:07:58 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:01 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:01 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:02 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:02 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:03 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:03 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:03 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:03 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:27 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:27 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:28 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:28 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:28 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:08:28 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:09:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:09:37 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:09:37 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:09:37 --> Severity: Warning --> require(razorpay-php/Razorpay.php): failed to open stream: No such file or directory /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:09:37 --> Severity: Compile Error --> require(): Failed opening required 'razorpay-php/Razorpay.php' (include_path='.:/opt/cpanel/ea-php72/root/usr/share/pear') /home/sapprepuser/public_html/application/controllers/Checkoutsession.php 6
ERROR - 2021-02-06 14:19:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:19:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:21:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:21:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:30:49 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 14:32:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:32:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:33:04 --> 404 Page Not Found: Wp-content/plugins
ERROR - 2021-02-06 14:33:06 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-02-06 14:33:09 --> 404 Page Not Found: Autodiscover/Autodiscover.xml
ERROR - 2021-02-06 14:33:11 --> 404 Page Not Found: Console/index
ERROR - 2021-02-06 14:33:12 --> 404 Page Not Found: Api/jsonws
ERROR - 2021-02-06 14:33:14 --> 404 Page Not Found: _ignition/execute-solution
ERROR - 2021-02-06 14:33:15 --> 404 Page Not Found: Vendor/phpunit
ERROR - 2021-02-06 14:38:53 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 14:49:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:54:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:54:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:55:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:55:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:55:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:56:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:56:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:57:28 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 14:59:49 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 15:03:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:03:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:06:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:06:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:06:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:06:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:07:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:07:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:07:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:07:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:08:46 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:18:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:18:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:19:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:19:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 15:22:25 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 15:34:10 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 15:58:31 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 16:02:15 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 16:34:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:35:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:35:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:35:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:39:11 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 16:42:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:50:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 16:51:24 --> 404 Page Not Found: Apple-touch-icon-precomposedpng/index
ERROR - 2021-02-06 16:51:24 --> 404 Page Not Found: Apple-touch-iconpng/index
ERROR - 2021-02-06 16:51:24 --> 404 Page Not Found: Faviconico/index
ERROR - 2021-02-06 16:54:07 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 16:54:07 --> 404 Page Not Found: Packages/style.css
ERROR - 2021-02-06 16:54:09 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 16:54:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:54:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:55:13 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 16:58:09 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 16:59:16 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 17:04:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:05:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:05:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:05:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:06:55 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:07:00 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:08:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:09:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:09:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:09:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:09:57 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 17:14:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:14:42 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 17:26:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:27:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:39:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 17:39:44 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 17:51:56 --> 404 Page Not Found: Wp-includes/wlwmanifest.xml
ERROR - 2021-02-06 17:51:57 --> 404 Page Not Found: Xmlrpcphp/index
ERROR - 2021-02-06 17:51:57 --> 404 Page Not Found: Blog/wp-includes
ERROR - 2021-02-06 17:51:57 --> 404 Page Not Found: Web/wp-includes
ERROR - 2021-02-06 17:51:57 --> 404 Page Not Found: Wordpress/wp-includes
ERROR - 2021-02-06 17:51:58 --> 404 Page Not Found: Website/wp-includes
ERROR - 2021-02-06 17:51:58 --> 404 Page Not Found: Wp/wp-includes
ERROR - 2021-02-06 17:51:58 --> 404 Page Not Found: News/wp-includes
ERROR - 2021-02-06 17:51:58 --> 404 Page Not Found: 2018/wp-includes
ERROR - 2021-02-06 17:51:59 --> 404 Page Not Found: 2019/wp-includes
ERROR - 2021-02-06 17:51:59 --> 404 Page Not Found: Shop/wp-includes
ERROR - 2021-02-06 17:51:59 --> 404 Page Not Found: Wp1/wp-includes
ERROR - 2021-02-06 17:51:59 --> 404 Page Not Found: Test/wp-includes
ERROR - 2021-02-06 17:51:59 --> 404 Page Not Found: Media/wp-includes
ERROR - 2021-02-06 17:52:00 --> 404 Page Not Found: Wp2/wp-includes
ERROR - 2021-02-06 17:52:00 --> 404 Page Not Found: Site/wp-includes
ERROR - 2021-02-06 17:52:00 --> 404 Page Not Found: Cms/wp-includes
ERROR - 2021-02-06 17:52:00 --> 404 Page Not Found: Sito/wp-includes
ERROR - 2021-02-06 18:05:19 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 18:11:57 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 19:13:34 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 19:14:06 --> 404 Page Not Found: Owa/auth
ERROR - 2021-02-06 20:27:32 --> 404 Page Not Found: Env/index
ERROR - 2021-02-06 20:39:06 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 20:56:41 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 21:26:35 --> 404 Page Not Found: Invoker/readonly
ERROR - 2021-02-06 21:29:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 21:29:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 21:29:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 21:31:18 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 21:32:43 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 21:33:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 21:34:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 21:34:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 22:00:23 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:04:25 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:14:53 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:18:14 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:28:07 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:33:16 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 22:36:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 22:37:38 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:40:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 22:44:32 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:45:09 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:45:35 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:46:33 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:46:43 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:47:12 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:47:15 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 22:47:21 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 22:59:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:00:56 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 23:03:24 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:03:33 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:03:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:03:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:03:57 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:08 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:36 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:42 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:04:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:06 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:09 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:37 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:05:52 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:06:48 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:06:50 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 23:06:58 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:07:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:07:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:07:56 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/examresult_1.php 147
ERROR - 2021-02-06 23:10:45 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 23:11:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:11:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:11:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:11:49 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:14:20 --> Severity: Warning --> A non-numeric value encountered /home/sapprepuser/public_html/application/views/takeexam_1.php 412
ERROR - 2021-02-06 23:17:32 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:17:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:17:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:17:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:19:39 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:19:50 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:19:53 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:19:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:20:11 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:20:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:20:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:20:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:21:07 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:21:14 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:21:17 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:21:21 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:22:20 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:22:27 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:22:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:22:34 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:23:31 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:23:38 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:23:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:23:44 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:24:10 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:24:19 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:24:22 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:24:25 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:25:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:25:12 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:25:15 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:25:18 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:25:51 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:01 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:03 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:05 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:47 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:54 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:56 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:26:59 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:27:26 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:27:29 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:49:41 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:50:30 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:53:40 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:55:23 --> 404 Page Not Found: Assets/css
ERROR - 2021-02-06 23:55:47 --> 404 Page Not Found: Assets/css
