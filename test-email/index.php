<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__ . '/vendor/phpmailer/src/Exception.php';
require_once __DIR__ . '/vendor/phpmailer/src/PHPMailer.php';
require_once __DIR__ . '/vendor/phpmailer/src/SMTP.php';

// passing true in constructor enables exceptions in PHPMailer
$mail = new PHPMailer(true);

//try {
    // Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
    //$mail->isSMTP();
    $mail->Host = 'smtpout.secureserver.net';
    $mail->SMTPAuth = false;
    //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    //$mail->SMTPSecure = "ssl";
    $mail->Port = 25;
        //$mail->SMTPDebug  = 1;

    //$mail->Username = 'info@examroadmap.com'; // YOUR gmail email
    //$mail->Password = 'Kota@123'; // YOUR gmail password

    // Sender and recipient settings
    $mail->setFrom('info@examroadmap.com', 'Exactprep');
    $mail->addAddress('dheeraj@infouna.com', 'Receiver');
    $mail->addReplyTo('info@examroadmap.com', 'Exactprep'); // to set the reply to

    // Setting the email content
    $mail->IsHTML(true);
    $mail->Subject = "Send email using Gmail SMTP and PHPMailer";
    $mail->Body = 'HTML message body. <b>Gmail</b> SMTP email body.';
    $mail->AltBody = 'Plain text message body for non-HTML email client. Gmail SMTP email body.';

    $mail->send();
    //echo "Email message sent.";
//} catch (Exception $e) { //echo $e; exit;
    //echo "Error in sending email. Mailer Error: {$mail->ErrorInfo}";
//}
?>
